/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.lang.System.out;
import java.util.Properties;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author fnell
 */
public class JenkinsSuite 
{
 static TestMarshall instance;
    
    @Test
    public void executeDoddleTestPack()
    { 
        
        System.out.println("***************************************");
            System.out.println("JenkinsSuite");
            System.out.println("***************************************");
        Properties props = System.getProperties();
        
        if(props.getProperty("TestPack").contains("Mobile") || props.getProperty("TestPack").contains("mobile"))
        {
            JenkinsSuiteAndroid jenkinsSuiteAndroid = new JenkinsSuiteAndroid();
            jenkinsSuiteAndroid.executeDoddleTestPack();
        }
        else
        {
            try{
            
                String browser = props.getProperty("Browser");

                String testpack = props.getProperty("TestPack");

                String myTest = "Test";

                String environment = props.getProperty("Environment");

                out.println(" Executing tests: Browser - " + browser + ", Environment - " + environment + ", Test Pack - " + testpack + ", MyTest = " + myTest);

                ApplicationConfig appConfig = new ApplicationConfig();

                ApplicationConfig.browserType = appConfig.resolveBrowserType(browser);

                TestMarshall.currentEnvironment = Enums.resolveTestEnvironment(environment);

                instance = new TestMarshall("TestPacks\\" + testpack, appConfig.resolveBrowserType(browser));

                out.println(" Executing tests: Browser - " + browser + ", Environment - " + environment + ", Test Pack - " + testpack);

                instance.runKeywordDrivenTests();

            }
            catch(Exception e)
            {
                System.out.println("***************************************************************");
                System.out.println("********************"+e.getMessage()+"*************************");
                System.out.println("***************************************************************");
            }
             //Assert.assertTrue(instance.reportEmailer.failCount < 1);
        }   
    }
}

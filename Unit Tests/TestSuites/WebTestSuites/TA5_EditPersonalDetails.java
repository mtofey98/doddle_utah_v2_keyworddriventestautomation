/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.WebTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass._isRemote;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA5_EditPersonalDetails
{

    static TestMarshall instance;

    public TA5_EditPersonalDetails()
    {

        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;

    }

    @Test
    public void RunDoddleEditPersonalDetials() throws FileNotFoundException, InterruptedException, IOException
    {
        _isRemote = false;
        Narrator.logDebug("Doddle - Edit Personal details  - Test Pack");
        instance = new TestMarshall("TestPacks\\Web\\TA5_EditPersonalDetails.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }

}

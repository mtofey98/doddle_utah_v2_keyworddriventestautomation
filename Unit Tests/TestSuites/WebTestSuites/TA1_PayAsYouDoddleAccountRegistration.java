/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.WebTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass._isRemote;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA1_PayAsYouDoddleAccountRegistration
{
    static TestMarshall instance;

    public TA1_PayAsYouDoddleAccountRegistration()
    {
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunDoddlePayAsYouDoddleAccountRegistration() throws FileNotFoundException, InterruptedException, IOException
    {
        _isRemote = false;
        Narrator.logDebug("Doddle - Pay As You Doddle Account Registration - Test Pack");
        instance = new TestMarshall("TestPacks\\Web\\TA1_PayAsYouDoddleAccountRegistration.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}

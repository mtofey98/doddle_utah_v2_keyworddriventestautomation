/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.WebTestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Sbeck
 */
public class SaveBarcode extends BaseClass {
    
    
    static TestMarshall instance;
      
    @Test 
    public void RunDoddleGenerateBarcode() throws FileNotFoundException, InterruptedException, IOException
      {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Testing  - Test Pack");  
        instance = new TestMarshall("TestPacks\\SaveBarcodeFromURL.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
      }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.WebTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass._isRemote;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Sbeck
 */
public class ExampleSuite {
      static TestMarshall instance;
    
    public ExampleSuite(){
        
        
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        
        
    }                                
                                     
    
    
    @Test 
    public void RunDoddleGenerateBarcode() throws FileNotFoundException, InterruptedException, IOException
      {
        _isRemote = false;
        Narrator.logDebug("Doddle - Testing  - Test Pack");  
        instance = new TestMarshall("C:\\doddlle_poc_utah_v2\\TestPacks\\RunDoddleGenerateBarcode.xlsx",Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
      }
    
    
    
}

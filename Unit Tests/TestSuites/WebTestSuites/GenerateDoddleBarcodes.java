/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.WebTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass._isRemote;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class GenerateDoddleBarcodes {
      static TestMarshall instance;
    
    public GenerateDoddleBarcodes(){
        
        
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
         instance.currentEnvironment = Enums.Environment.DoddleStaging1;
        
        
    }                                
                                     
    
    
    @Test 
       public void RunDoddleGenerateBarcode() throws FileNotFoundException, InterruptedException, IOException
      {
        Narrator.logDebug("Doddle - Testing  - Test Pack");  
        instance = new TestMarshall("TestPacks\\Web\\BarcodeGenerator.xlsx",Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
      }
    
}

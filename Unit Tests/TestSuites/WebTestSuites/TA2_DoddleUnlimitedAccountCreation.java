
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.WebTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass._isRemote;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA2_DoddleUnlimitedAccountCreation
{

    static TestMarshall instance;

    public TA2_DoddleUnlimitedAccountCreation()
    {
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunDoddlePayAsYouDoddleAccountRegistration() throws FileNotFoundException, InterruptedException, IOException
    {
        _isRemote = false;
        Narrator.logDebug("Doddle - Doddle Unlimited Account Creation  - Test Pack");
        instance = new TestMarshall("TestPacks\\Web\\TA2_DoddleUnlimitedAccountCreation.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.WebTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass._isRemote;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA3_ForgottenPasswordFlow {
      static TestMarshall instance;
    
    public TA3_ForgottenPasswordFlow(){
        
        
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
         instance.currentEnvironment = Enums.Environment.DoddleStaging1;
        
        
    }                                
                                     
    
    
    @Test 
    public void RunDoddleForgottenPasswordFlow() throws FileNotFoundException, InterruptedException, IOException
      {
        _isRemote = false;
        Narrator.logDebug("Doddle - Forgotten Password Flow - Test Pack");  
        instance = new TestMarshall("TestPacks\\Web\\TA3_ForgottenPasswordFlow.xlsx",Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
      }
    
}

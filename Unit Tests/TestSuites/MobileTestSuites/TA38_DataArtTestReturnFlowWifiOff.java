/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA38_DataArtTestReturnFlowWifiOff
{

    static TestMarshall instance;

    public TA38_DataArtTestReturnFlowWifiOff()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunCustomerReturnFlow()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Customer Return Flow - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA38_DataArtTestReturnFlowWifiOff.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }
    


}

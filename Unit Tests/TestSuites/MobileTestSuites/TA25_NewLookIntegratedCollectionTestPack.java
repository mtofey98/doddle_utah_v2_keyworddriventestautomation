/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA25_NewLookIntegratedCollectionTestPack
{

    static TestMarshall instance;

    public TA25_NewLookIntegratedCollectionTestPack()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunNewLookIntegratedCollection() 
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - New look Integrated Collection  - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA25_NewLookIntegratedCollection.xlsx");
        instance.runKeywordDrivenTests();

    }

}

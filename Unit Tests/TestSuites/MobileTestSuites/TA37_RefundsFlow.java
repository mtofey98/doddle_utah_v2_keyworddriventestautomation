/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TA37_RefundsFlow
{
    static TestMarshall instance;

    public TA37_RefundsFlow()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunCustomerRefundsFlow()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Customer Refunds Flow - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA40_CustomerRefundFlow.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }
}

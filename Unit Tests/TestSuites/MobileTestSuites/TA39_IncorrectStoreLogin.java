/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TA39_IncorrectStoreLogin
{
    static TestMarshall instance;

    public TA39_IncorrectStoreLogin()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TA39Login()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - New Login  - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA39_IncorrectStoreLogin.xlsx");
        instance.runKeywordDrivenTests();
    }
}

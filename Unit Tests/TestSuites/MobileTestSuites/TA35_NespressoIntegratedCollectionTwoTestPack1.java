/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA35_NespressoIntegratedCollectionTwoTestPack1
{

    static TestMarshall instance;

    public TA35_NespressoIntegratedCollectionTwoTestPack1()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunNespresso2IntegratedCollection()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Nespresso2 Integrated Collection  - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA35_NespressoIntegratedCollection.xlsx");
        instance.runKeywordDrivenTests();

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA10_DoddleScanningVolumeTestTestPack
{

    static TestMarshall instance;

    public TA10_DoddleScanningVolumeTestTestPack()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunDoddleVolumeScann()
    {
        requiresBrowser = true;
        Narrator.logDebug("Doddle - Volume test  - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA10_ScanningVolumeTest.xlsx");
        instance.runKeywordDrivenTests();

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA9_DoddleCheckInTestPack
{

    static TestMarshall instance;

    public TA9_DoddleCheckInTestPack()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunDoddleCheckin()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - App Check In  - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA9_DoddleCheckin.xlsx");
        instance.runKeywordDrivenTests();

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA30_AirmoneyIntegratedCollectionTestPack
{

    static TestMarshall instance;

    public TA30_AirmoneyIntegratedCollectionTestPack()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunAirmoneyIntegratedCollection()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Air money integrated Collection  - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA30_AirmoneyIntegratedCollection.xlsx");
        instance.runKeywordDrivenTests();

    }

}

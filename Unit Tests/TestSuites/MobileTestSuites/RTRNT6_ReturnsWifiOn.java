/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RTRNT6WifiOn;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class RTRNT6_ReturnsWifiOn
{
     static TestMarshall instance;
    
    public RTRNT6_ReturnsWifiOn()
    {
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }
    
    @Test
    public void RunCustomerReturnsFlow()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Customer Returns with wifi on - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\RTRNT6_ReturnsWifiOn.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites.MSTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TA40_VanillaFlowTestPack
{
     static TestMarshall instance;
    
    public TA40_VanillaFlowTestPack()
    {
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }
    
    @Test
    public void RunVanillaFlow()
    {
        requiresBrowser = false;
        Narrator.logDebug("M&S - VanillaFlow - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\MSTestSuites\\TA40_VanillaFlowTestPack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }
}

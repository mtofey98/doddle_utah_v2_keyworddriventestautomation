/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.sql.Driver;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TA47_DataArtReturnsItemTooLarge
{

    static TestMarshall instance;

    public TA47_DataArtReturnsItemTooLarge()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunCustomerReturnFlow()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Customer Return Flow - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA47_DataArtReturnsItemTooLarge.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }

    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class RFNDN3_NonExistingUPI
{
    static TestMarshall instance;
    
    public RFNDN3_NonExistingUPI()
    {
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }
    
    @Test
    public void RunCustomerReturnsFlow()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Customer Refunds return - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\RFND_N3_NonExistingUPI.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }
}

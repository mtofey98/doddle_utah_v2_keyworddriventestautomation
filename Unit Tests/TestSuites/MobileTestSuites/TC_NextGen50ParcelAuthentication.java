/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen50ParcelAuthentication
{
    static TestMarshall instance;

    public TC_NextGen50ParcelAuthentication()
    {
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }
    
    @Test
    public void TC_NextGen50ParcelAuthentication()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - RetakeCollection - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TCNextGen50ParcelAuthentication.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }
}

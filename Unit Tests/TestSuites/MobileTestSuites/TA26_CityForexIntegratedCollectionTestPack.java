/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author gdean
 */
public class TA26_CityForexIntegratedCollectionTestPack
{

    static TestMarshall instance;

    public TA26_CityForexIntegratedCollectionTestPack()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunCityForexIntegratedCollection()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - City Forex Integrated collection  - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\TA26_CityForexIntegratedCollection.xlsx");
        instance.runKeywordDrivenTests();

    }

}

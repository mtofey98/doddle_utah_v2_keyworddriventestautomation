/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.line;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.putStoreSystemConfigNoCollectionChargeTrue;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.List;
import nl.flotsam.xeger.Xeger;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 */
public class Sandbox extends BaseClass
{
    static StringEntity input;
    String newBearer;
    String apiResponse;
    String itemID;
    public static TestEntity currentData;
    Shortcuts shortcut = new Shortcuts();
    
    @Test
    public void test()
    {
//        String barcode  = MobileDoddlePageObjects.barcode();
//        
//         SaveBarcodesFromURL.saveBarcode(barcode, MobileDoddlePageObjects.rand());
//        Shortcuts.scanAndRescanBarcode(barcode);
       
        tester();
    }
    
    public void tester()
    {
        try
        {
            Shortcuts.scanBarcode("6f1f231c29634");
        }
        catch(Exception e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    public static void cleanCollections()
    {
        try
        {
            Shortcuts.newBearer();
            
            SeleniumDriverInstance = new SeleniumDriverUtility(Enums.BrowserType.Chrome);
            
            SeleniumDriverInstance.startDriver();
            
            SeleniumDriverInstance.navigateTo("http://tmpstage-couchbaseadmin-445592012.eu-west-1.elb.amazonaws.com:4985/_admin/db/doddle_stores_sync/channels/store_456DVT");
            
            List <WebElement> elements = SeleniumDriverInstance.Driver.findElements(By.xpath("//*[contains(@href,'ITEM_COLLECTION')]"));
            
            for (int i = 0; i < elements.size(); i++) 
            {
                String itemsID = elements.get(i).getText();
                System.out.println("PROCESSING ITEM::"+itemsID);
                itemsID = itemsID.replace("ITEM_COLLECTION::", "");
                doCleanUpAPICall(itemsID.trim(), SettersAndGetters.getNewBearer());
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public static void doCleanUpAPICall(String itemID, String bearerToken)
    {
        String header = "Authorization";
        String headerValue = "Bearer "+bearerToken;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPut httpPut = new HttpPut("https://stage-apigw.doddle.it/v1/items/collections/" + itemID + "?api_key=40AGV34DJUO153Z5DDJAJ23PE");
            httpPut.addHeader(header, headerValue);

            StringEntity input = new StringEntity(MobileDoddlePageObjects.collectionsCleanUpBody());
            input.setContentType("application/json");
            httpPut.setEntity(input);
            
            HttpResponse response = client.execute(httpPut);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            System.out.println("ITEM:: "+itemID+" SUCCESSFULLY CLEANED");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.err.println("FAILED TO CLEAN ITEM:: "+itemID);
            System.out.println(e.getMessage());
        }
    }
    
    public static String amazonBarcode()
    {
        String regex = "[QH]{1}[0-9]{11}";
        Xeger generator = new Xeger(regex);
        String amazonBarcode = generator.generate();

        return amazonBarcode;
    }
    
 
    public boolean apiCall()
    {
        if (!Shortcuts.newBearer())
        {
            return false;
        }
        
        newBearer = Shortcuts.newBearer;
        Shortcuts.putStoreSystemUIConfigMultipleContainers();
//        Shortcuts.doLabelValueAPICall("7e7d15f03d06", Shortcuts.newBearer);
        
        System.out.println(Shortcuts.line);
        
        return true;
    }
    
    
    

    
//    public void testing123()
//    {
//        String response = "{\"resources\":[{\"alerts\":{\"isHeavy\":true,\"isLarge\":false,\"requiresAdditionalIdCheck\":false,\"requiresPhotoId\":false},\"collectionDeadlineDateTime\":\"2017-12-27T00:00:00.000Z\",\"deliveryCarrierId\":\"DHL\",\"eventHistory\":[{\"clientVersion\":\"9.12.2\",\"dateTime\":\"2017-09-27T09:49:21.843Z\",\"deviceIdentifier\":\"16072522500917\",\"eventType\":\"DELIVERED_TO_STORE\",\"location\":{\"storeId\":\"456DVT\"},\"newStatus\":\"AT_GOODS_IN\",\"staffId\":\"dvt_automation\"},{\"clientVersion\":\"9.12.2\",\"dateTime\":\"2017-09-27T09:49:58.029Z\",\"deviceIdentifier\":\"16072522500917\",\"eventType\":\"PUT_ON_SHELF\",\"location\":{\"shelfId\":\"DPS371968\",\"storeId\":\"456DVT\"},\"newStatus\":\"ON_SHELF\",\"oldStatus\":\"AT_GOODS_IN\",\"staffId\":\"dvt_automation\"},{\"dateTime\":\"2017-09-27T09:49:25.744Z\",\"eventData\":{\"referenceId\":\"DC9035014\"},\"eventType\":\"ENRICHED_REFERENCE_ID\"},{\"dateTime\":\"2017-09-27T09:49:33.772Z\",\"eventType\":\"ENRICHED_ID_REQUIREMENTS\"},{\"dateTime\":\"2017-09-27T09:49:34.212Z\",\"eventData\":{\"photoIds\":[\"1506505795053\"]},\"eventType\":\"PHOTO_BACKED_UP\"},{\"dateTime\":\"2017-09-27T09:49:36.899Z\",\"eventData\":{\"ticketType\":\"CUSTOMER_ENRICHMENT\",\"zendeskTicketNumber\":6711},\"eventType\":\"ZENDESK_TICKET_RAISED\"},{\"dateTime\":\"2017-09-27T09:49:41.001Z\",\"eventData\":{\"collectionDeadlineDateTime\":\"2017-12-27T00:00:00.000Z\"},\"eventType\":\"ENRICHED_COLLECTION_DEADLINE\"}],\"idChecks\":{\"requiredType\":\"NONE\"},\"isDemo\":false,\"itemId\":\"56a9e9af-3b33-4cdc-bad1-08dc224df645\",\"labelValue\":\"4deff70534d4\",\"location\":{\"shelfId\":\"DPS371968\",\"storeId\":\"456DVT\"},\"organisationId\":\"DODDLE\",\"photos\":[{\"contentType\":\"image/png\",\"id\":\"1506505795053\",\"takenAtDateTime\":\"2017-09-27T09:49:55.052Z\",\"url\":\"https://stage-doddle.s3-eu-west-1.amazonaws.com/item-photos/56a9e9af-3b33-4cdc-bad1-08dc224df645_1506505795053.png\"}],\"referenceId\":\"DC9035014\",\"resourceType\":\"ITEM_COLLECTION\",\"signatureRequired\":false,\"status\":\"ON_SHELF\"}]}";
//        String regex = ",\"itemId\":\"";
//        String []lines = response.split(regex);
//        String []liness = lines[1].split("\",\"labelValue\":");
//        apiResponse = liness[0];
//        itemID = apiResponse;
//        
//        System.out.println(itemID);
//    }

}

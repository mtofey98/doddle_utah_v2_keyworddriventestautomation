/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.MobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class Doddle_UnstableMobileTestPack
{
  static TestMarshall instance;

    public Doddle_UnstableMobileTestPack()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void RunMobileRegression()
    {
        requiresBrowser = true;
        Narrator.logDebug("Doddle - Broken testcases  - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\UnstableMobileTestPack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;
import static KeywordDrivenTestFramework.Core.BaseClass._isRemote;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author lmaarman
 */
public class CouchBaseTestSuite {
   static TestMarshall instance;

//    public CouchBaseTestSuite()
//    {
//        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
//    }

    @Test
    public void CouchBaseTestSuite() throws FileNotFoundException, InterruptedException, IOException
    {
        _isRemote = false;
        Narrator.logDebug("CouchBaseValidatorTestPack");
        instance = new TestMarshall("TestPacks\\CouchBaseValidatorTestPack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
  
}

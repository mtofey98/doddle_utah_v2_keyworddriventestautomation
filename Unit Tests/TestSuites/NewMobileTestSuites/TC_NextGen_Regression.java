/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_Regression
{
    static TestMarshall instance;

//    public TC_NextGen_Regression()
//    {
//
//        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
//        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
//        instance.currentPlatform = Enums.MobilePlatform.Android;
//        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
//    }

    @Test
    public void TC_NextGen_Regression()
    {
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
        
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TC_NextGen_Regression - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_Regression.xlsx");
        instance.runKeywordDrivenTests();

    }
//    
//    @Test
//    public void TC_NextGen_Regression1()
//    {
//        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
//        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
//        instance.currentPlatform = Enums.MobilePlatform.Android;
//        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
//        
//        requiresBrowser = false;
//        Narrator.logDebug("Doddle - TC_NextGen_Regression - Test Pack");
//        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_Regression.xlsx");
//        instance.runKeywordDrivenTests();
//
//    }
}

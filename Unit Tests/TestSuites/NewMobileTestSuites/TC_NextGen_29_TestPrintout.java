/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_29_TestPrintout
{
    static TestMarshall instance;

    public TC_NextGen_29_TestPrintout()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC29_NextGenTestPrintout()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TC29_NextGenTestPrintout - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_29_TestPrintout.xlsx");
        instance.runKeywordDrivenTests();

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author mlopes
 */
public class TC_NextGen_07_Storage_StoreCheckProcess
{

    static TestMarshall instance;

    public TC_NextGen_07_Storage_StoreCheckProcess()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC_NextGen_05_Storage_AmazonParcels()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TC_NextGen_07_Storage_StoreCheckProcess - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_07_Storage_StoreCheckProcess.xlsx");
        instance.runKeywordDrivenTests();

    }

}

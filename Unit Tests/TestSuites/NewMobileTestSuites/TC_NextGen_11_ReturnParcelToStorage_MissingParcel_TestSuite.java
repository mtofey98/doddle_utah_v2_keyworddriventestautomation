/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author mlopes
 */
public class TC_NextGen_11_ReturnParcelToStorage_MissingParcel_TestSuite
{

    static TestMarshall instance;

    public TC_NextGen_11_ReturnParcelToStorage_MissingParcel_TestSuite()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC_NextGen_11_ReturnParcelToStorage_MissingParcel()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Multiple Collection - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TC_NextGen_11_ReturnParcelToStorage_MissingParcel.xlsx");
        instance.runKeywordDrivenTests();

    }
}

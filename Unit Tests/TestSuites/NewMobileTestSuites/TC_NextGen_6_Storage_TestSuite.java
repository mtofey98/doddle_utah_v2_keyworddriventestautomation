/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author mlopes
 */
public class TC_NextGen_6_Storage_TestSuite
{

    static TestMarshall instance;

    public TC_NextGen_6_Storage_TestSuite()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC_NextGen_6_Storage()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - Multiple Collection - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_06_Storage.xlsx");
        instance.runKeywordDrivenTests();

    }

//    @Test
//    public void sandBox()
//    {
//        try
//        {
//            ReusableFunctionalities.checkStorageRecordsInDatabase("TC_NextGen_6_Storage.xlsx");
//        }
//        catch (Exception e)
//        {
//
//        }
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import static TestSuites.NewMobileTestSuites.TC_NextGen_07_Storage_StoreCheckProcess.instance;
import org.junit.Test;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_08_UnknownParcel {
    
    static TestMarshall instance;

    public TC_NextGen_08_UnknownParcel()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }
    
    @Test
    public void TC_NextGen_08_UnknownParcel()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TC_NextGen_07_Storage_StoreCheckProcess - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_08_UnknownParcel.xlsx");
        instance.runKeywordDrivenTests();

    }
    
}

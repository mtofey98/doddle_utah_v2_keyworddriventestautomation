/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_34_NewDispatchSack
{
    static TestMarshall instance;

    public TC_NextGen_34_NewDispatchSack()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC_NextGen_34_NewDispatchSack()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TC_NextGen_34_NewDispatchSack - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_34_NewDispatchSack.xlsx");
        instance.runKeywordDrivenTests();

    }

}

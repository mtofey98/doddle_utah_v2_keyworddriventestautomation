/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author hduplessis
 */
public class TC_NextGen_04_CarrierDispatch
{

    static TestMarshall instance;

    public TC_NextGen_04_CarrierDispatch()
    {
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
//        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
//        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
//        instance.currentDeviceConfig = Enums.DeviceConfig.Doddle2;
//        instance.currentDevice = Enums.Device.Emulator;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC_NextGen_02_03_FlowTest()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TA38_TA39 - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_04_CarrierDispatch.xlsx");
        instance.runKeywordDrivenTests();

    }
}

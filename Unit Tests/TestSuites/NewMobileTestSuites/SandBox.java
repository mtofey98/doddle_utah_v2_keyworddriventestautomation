/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_30_Return_SingleContainer.validationList;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.popUpMessage;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.junit.Test;

/**
 *
 * @author mtofey
 */
public class SandBox extends BaseClass
{
    

    @Test
    public void GenericTestSuite()
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + "mootest@doddle.test" + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);
            //Must set visible - !
            snakeFrame.setVisible(true);
            snakeFrame.toFront();
            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);

            snakeFrame.setLocation(centerX, centerY);
            snakeFrame.setVisible(true);
            snakeFrame.setLocation(centerX, centerY+75);
            snakeFrame.setLocation(centerX, centerY-50);
            snakeFrame.setLocation(centerX+50, centerY);
            snakeFrame.setLocation(centerX-50, centerY);
            snakeFrame.setLocation(centerX-30, centerY+50);
            snakeFrame.setLocation(centerX-30, centerY-70);
            snakeFrame.setLocation(centerX+125,centerY);
            snakeFrame.setLocation(centerX+20, centerY+80);
            snakeFrame.setLocation(centerX+75, centerY+75);
            snakeFrame.setLocation(centerX-100,centerY+75);
            snakeFrame.repaint();
            snakeFrame.dispose();

            
            
            while (!popUpMessage())
            {
                if (count == 3)
                {
                 
                }

                count++;

                AppiumDriverInstance.pressHardWareButtonbyKeyCode(); //Scan again
            }

            snakeFrame.repaint();
            snakeFrame.dispose();
            

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
        }

    }
}

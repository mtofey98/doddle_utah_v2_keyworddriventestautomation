/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_28_Settings
{
    static TestMarshall instance;

    public TC_NextGen_28_Settings()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Doddle;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TCNextGen_28_Settings()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TCNextGen_28_Settings - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_28_Settings.xlsx");
        instance.runKeywordDrivenTests();

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

/**
 *
 * @author rpeck
 */

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import static TestSuites.NewMobileTestSuites.TC_NextGen_66_NoCust_NoRetailer.instance;
import org.junit.Test;

public class TC_NextGen_67_ExistingCustomerAccount {
    
    static TestMarshall instance;
    
    public TC_NextGen_67_ExistingCustomerAccount()
    {
        //instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        //instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
//        instance.currentDeviceConfig = Enums.DeviceConfig.Doddle2;
//        instance.currentDevice = Enums.Device.Emulator;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC_NextGen_67_ExistingCustomerAccount()
    {
        requiresBrowser = false;
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNetxtGen_67_CollectionExistingAccount.xlsx");
        instance.runKeywordDrivenTests();

    }
}
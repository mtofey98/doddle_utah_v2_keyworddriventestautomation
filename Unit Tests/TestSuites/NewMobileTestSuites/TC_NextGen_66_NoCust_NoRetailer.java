/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_66_NoCust_NoRetailer {
    
    static TestMarshall instance;

    public TC_NextGen_66_NoCust_NoRetailer()
    {
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
//        instance.currentDeviceConfig = Enums.DeviceConfig.Doddle2;
//        instance.currentDevice = Enums.Device.Emulator;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC_NextGen_66_NoCust_NoRetailer()
    {
        requiresBrowser = false;
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_66_CollectionNoCustomerObject_RetailerID.xlsx");
        instance.runKeywordDrivenTests();

    }
}


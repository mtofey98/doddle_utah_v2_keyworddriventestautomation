/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_31_1_ReturnItemsToContainer 
{
    
     static TestMarshall instance;
     
    public TC_NextGen_31_1_ReturnItemsToContainer()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }
    
    @Test
    public void TC22_NextGenReturn()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TC22_NextGenReturn - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_31_1_ReturnItemsToContainer.xlsx");
        instance.runKeywordDrivenTests();

    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author mtofey
 */
public class GenericTestSuite2 
{
    static TestMarshall instance;

    public GenericTestSuite2()
    {

        instance.currentDeviceConfig = Enums.DeviceConfig.zebraMC40;
        instance.currentDevice = Enums.Device.ZebraMC40;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void GenericTestSuite()
    {
        requiresBrowser = false;
        Narrator.logDebug("Doddle - TCNextGen_60_CollectionCustBlock - Test Pack");
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_60_CollectionCustBlock.xlsx");
        instance.runKeywordDrivenTests();

    }
}

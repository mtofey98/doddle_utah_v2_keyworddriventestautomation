/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites.NewMobileTestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_45_MultipleCollections {
 
    static TestMarshall instance;

    public TC_NextGen_45_MultipleCollections()
    {
        instance.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
        instance.currentDevice = Enums.Device.ZebraTC70_Dvt;
//        instance.currentDeviceConfig = Enums.DeviceConfig.Doddle2;
//        instance.currentDevice = Enums.Device.Emulator;
        instance.currentPlatform = Enums.MobilePlatform.Android;
        instance.currentEnvironment = Enums.Environment.DoddleStaging1;
    }

    @Test
    public void TC_NextGen_45_MultipleCollections()
    {
        requiresBrowser = false;
        instance = new TestMarshall("TestPacks\\Mobile\\NewTestSuites\\TCNextGen_CollectionPack.xlsx");
        instance.runKeywordDrivenTests();

    }
}

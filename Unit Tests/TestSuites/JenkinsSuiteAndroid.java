/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.lang.System.out;
import java.util.Properties;
import org.junit.Test;

/**
 *
 * @author fnell
 */
public class JenkinsSuiteAndroid
{

    static TestMarshall instance;

    @Test
    public void executeDoddleTestPack() 
    {

        try{
            
            System.out.println("***************************************");
            System.out.println("JenkinsSuiteAndroid");
            System.out.println("***************************************");
            
            Properties props = System.getProperties();

            String browser = props.getProperty("Browser");

            String testpack = props.getProperty("TestPack");
            
            boolean zipToRemote = Boolean.parseBoolean(props.getProperty("ZipToRemote"));

            String myTest = props.getProperty("myTest");

            String environment = props.getProperty("Environment");

            String device = props.getProperty("Device");

            String deviceConfig = props.getProperty("DeviceConfig");

            out.println("[INFO] Executing tests: Browser - " + browser + ", Environment - " + environment + ", Test Pack - " + testpack + ", MyTest = " + myTest + " Device = " + device + " DeviceConfig = " + deviceConfig);

            ApplicationConfig appConfig = new ApplicationConfig();

            ApplicationConfig.browserType = appConfig.resolveBrowserType(browser);

            instance = new TestMarshall("TestPacks\\" + testpack, appConfig.resolveBrowserType(browser), zipToRemote);

            instance.currentEnvironment = Enums.resolveTestEnvironment(environment);

            instance.currentDevice = Enums.resolveDevice(device);

            instance.currentDeviceConfig = Enums.resolveDeviceConfig(deviceConfig);

            instance.currentPlatform = Enums.MobilePlatform.Android;

            instance.runKeywordDrivenTests();
        }
        catch(Exception e)
        {
            System.out.println("***************************************************************");
            System.out.println("********************"+e.getMessage()+"*************************");
            System.out.println("***************************************************************");
        }
    }
}

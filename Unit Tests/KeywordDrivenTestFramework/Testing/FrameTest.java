/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.junit.Test;

/**
 *
 * @author sbeck
 */
public class FrameTest 
{

    static TestMarshall instance;

    @Test
    public void SQL() throws InterruptedException
    {
        //Create Frame
        JFrame snakeFrame = new JFrame();

        //Can set the default size here
        //snakeFrame.setBounds(100, 200, 800, 800);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int centerX = screenSize.width / 28;
        int centerY = (screenSize.height / 2) + 200;

        snakeFrame.setLocation(centerX, centerY);

        //Must set visible - !
        snakeFrame.setVisible(true);

        //Load image - will be autoSized.
        snakeFrame.add(new JLabel(new ImageIcon("C:\\Users\\gdean@dvt.co.za\\Frameworks\\doddlle_POC_Ref\\Barcodes\\1.png")));
        snakeFrame.pack();
        //Close the frame
          Thread.sleep(5000);
        snakeFrame.dispose();
    }

}

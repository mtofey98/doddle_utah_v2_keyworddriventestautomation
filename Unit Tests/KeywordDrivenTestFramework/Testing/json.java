/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.junit.Test;

/**
 *
 * @author sbeck
 */
public class json
{

    @Test
    public void JSON() throws IOException
    {
        
        HttpURLConnection httpcon;
        String url = "https://stage-apigw.doddle.it";
        String data = request;
        String result = "";
        try
        {
//Connect
            httpcon = (HttpURLConnection) ((new URL(url).openConnection()));
            httpcon.setDoOutput(true);
            httpcon.setRequestProperty("Content-Type", "application/json");
            httpcon.setRequestProperty("Accept", "application/json");
            httpcon.setRequestMethod("POST");
            httpcon.connect();

//Write         
            OutputStream os = httpcon.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(data);
            writer.close();
            os.close();

//Read      
            BufferedReader br = new BufferedReader(new InputStreamReader(httpcon.getInputStream(), "UTF-8"));

            String line = null;
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null)
            {
                sb.append(line);
            }

            br.close();
            result = sb.toString();

        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

  
        public String request = "{noformat}\n"
                + "{\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"countryCode\": \"GB\",\n"
                + "      \"postCode\": \"SW1W 9AY\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"024VIC|Doddle\"\n"
                + "      ],\n"
                + "      \"town\": \"London\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"ASOStest@doddle.com\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"full\": \"ASOS test\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"211169999\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"carrierName\": \"DoddleLogisticsCORE\",\n"
                + "      \"id\": \"HFqriyIoVAjj\",\n"
                + "      \"serviceName\": \"DoddleLate\",\n"
                + "      \"storeID\": \"023FPK\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"ASOS\"\n"
                + "}\n"
                + "{noformat}";
    

}

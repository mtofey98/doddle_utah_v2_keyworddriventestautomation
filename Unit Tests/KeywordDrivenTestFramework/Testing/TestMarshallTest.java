/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author fnell
 */
public class TestMarshallTest {

    static TestMarshall instance;

    public TestMarshallTest() {
        
    }

    @Test
    public void testRunKeywordDrivenTests() throws FileNotFoundException, InterruptedException, IOException
    {
        Narrator.logDebug("runKeywordDrivenTests");
        instance = new TestMarshall("TestPacks\\GMAIL TEST PACK.xlsx", Enums.BrowserType.Chrome);
        instance.currentEnvironment = Enums.Environment.TEST;
        instance.runKeywordDrivenTests();
    }
}

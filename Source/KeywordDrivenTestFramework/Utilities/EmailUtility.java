/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import com.sun.mail.imap.IMAPFolder;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

/**
 *
 * @author szeuch
 */
public class EmailUtility 
{

    public ArrayList readAllEmails(String sender, String subject)
    {
        ArrayList data = new ArrayList();
        Folder folder;        
        Properties props;
        Session session;
        Store store;
        
        try
        {
            props = System.getProperties();
            props.setProperty("mail.store.protocol", "imaps");
            session = Session.getDefaultInstance(props, null);
            store = session.getStore("imaps");
            store.connect("imap.gmail.com", "sintrexautomatiotestingdvt@gmail.com","dvtautomationtesting");
        
            folder = (IMAPFolder) store.getFolder("INBOX");
            folder.open(Folder.READ_WRITE);

            Message[] messages = null;
              
            messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),folder.getMessages());

            for (Message mail : messages)
            {
                if (mail.getFrom()[0].toString().contains(sender) && mail.getSubject().toString().contains(subject))
                {
                    String temp = mail.getContent().toString();
//                    temp.
//                    data.add(mail.getContent().toString());
                    mail.setFlag(Flags.Flag.SEEN, true);
                }
            }
            
            messages = folder.getMessages();
            
            for (Message mail : messages)
            {
                mail.setFlag(Flags.Flag.SEEN, true);
            }
        }
        catch(Exception e)
        {
            System.err.println("[Error]Failed to validate the email - " + e.getMessage());
            return null;
        }        
        return data;
    }
    
   public boolean deleteAllEmails()
    {
        Folder folder;        
        Properties props;
        Session session;
        Store store;
        
        try 
        {
            props = System.getProperties();
            props.setProperty("mail.store.protocol", "imaps");
            session = Session.getDefaultInstance(props, null);
            store = session.getStore("imaps");
            store.connect("imap.gmail.com", "dvttesting1@gmail.com","DVTDoddle123");
        
            folder = (IMAPFolder) store.getFolder("[Gmail]/All Mail");
            folder.open(Folder.READ_WRITE);

            Message[] messages = null;
            

            messages = folder.getMessages();
            
            for (Message mail : messages)
            {
                mail.setFlag(Flags.Flag.DELETED, true);
                folder.expunge();
              
            }
              folder.close(true);
        } 
        catch (Exception e) 
        {
            System.out.println("[Error] - Failed to delete all emails");
            return false;
        }
        
        return true;
    }
   
         public static void delete() throws javax.mail.MessagingException 
   {
        CryptoUtility passwordEncriptor = new CryptoUtility();
      try 
      {
         // get the session object
         Properties properties = new Properties();
         properties.put("mail.store.protocol", "pop3");
         properties.put("mail.pop3s.host", "pop.gmail.com");
         properties.put("mail.pop3s.port", "995");
         properties.put("mail.pop3.starttls.enable", "true");
         Session emailSession = Session.getDefaultInstance(properties);
         emailSession.setDebug(true);

         // create the POP3 store object and connect with the pop server
         Store store = emailSession.getStore("pop3s");

         store.connect("pop.gmail.com", "dvttesting1@gmail.com", passwordEncriptor.decrypt("8JjIa6TmuWNtlEvBva9NX5YCVwaPezAn"));

         // create the folder object and open it
         Folder emailFolder = store.getFolder("INBOX");
         emailFolder.open(Folder.READ_WRITE);

         BufferedReader reader = new BufferedReader(new InputStreamReader(
            System.in));
         // retrieve the messages from the folder in an array and print it
         Message[] messages = emailFolder.getMessages();
         System.out.println("messages.length---" + messages.length);
         for (int i = 0; i < messages.length; i++) {
            Message message = messages[i];

        // set the DELETE flag to true
        message.setFlag(Flags.Flag.DELETED, true);
            
         }
         // expunges the folder to remove messages which are marked deleted
         emailFolder.close(true);
         store.close();

      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}         


    
   
   
    


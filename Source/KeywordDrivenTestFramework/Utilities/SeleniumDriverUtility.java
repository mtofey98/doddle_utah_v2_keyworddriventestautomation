/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.RetrievedTestValues;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.Narrator;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import static java.lang.System.out;
import java.security.*;
import java.net.URL;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author fnell
 * @editir jjoubert
 */
// Contains logic for handling accessor methods and driver calls.
public class SeleniumDriverUtility extends BaseClass
{

    public WebDriver Driver;
    private Enums.BrowserType browserType;
    File fileIEDriver;
    File fileChromeDriver;
    public Boolean _isDriverRunning = false;
    public RetrievedTestValues retrievedTestValues;
    public String DriverExceptionDetail = "";
    TestEntity testData;
    public Object document;
    public String mainWindowsHandle;

    EventFiringWebDriver eventDriver;

    public SeleniumDriverUtility(Enums.BrowserType selectedBrowser)
    {
        retrievedTestValues = new RetrievedTestValues();

        _isDriverRunning = false;
        browserType = selectedBrowser;

        fileIEDriver = new File("IEDriverServer.exe");
        System.setProperty("webdriver.ie.driver", fileIEDriver.getAbsolutePath());

        fileChromeDriver = new File("chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());
    }

    public boolean isDriverRunning()
    {
        return _isDriverRunning;
    }

    public void startDriver()
    {
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
        try
        {
            switch (browserType)
        {
            case IE:
                Driver = new InternetExplorerDriver();
                _isDriverRunning = true;
                break;
            case FireFox:
                Driver = new FirefoxDriver();
                _isDriverRunning = true;
                break;
            case Chrome:
                Driver = new ChromeDriver();
                _isDriverRunning = true;
                break;
            case Safari: ;
                break;
        }
        retrievedTestValues = new RetrievedTestValues();
        Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        Driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        Driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        Driver.manage().window().maximize();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Exception - " + e .getMessage());
        }
        

    }

    public boolean navigateTo(String pageUrl)
    {
        try
        {
            Driver.navigate().to(pageUrl);
        
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" navigating to URL - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
    
    public boolean saveImageUsingURL(String imageURL, String destionationFile)
    {
        try
        {
            LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
            DataColumn barcodeColumn = new DataColumn("", "", Enums.ResultStatus.UNCERTAIN);

//            URL url = new URL(imageURL);
            
            java.net.URLConnection c = new java.net.URL(imageURL).openConnection();
            
            System.setProperty("jsse.enableSNIExtension", "false");
            InputStream fis = c.getInputStream();
            OutputStream fos = new FileOutputStream(destionationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = fis.read(b)) != -1)
            {
                fos.write(b, 0, length);
            }

            fis.close();
            fos.close();

            barcodeColumn.columnValue = convertPNGToBase64(destionationFile);
            barcodeColumn.resultStatus = Enums.ResultStatus.UNCERTAIN;
            DataRow currentRow = new DataRow();
            currentRow.DataColumns.add(barcodeColumn);
            listOfBarcodes.add(currentRow);


        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }
    
    public String convertPNGToBase64(String imageFilePath)
    {
        String base64ReturnString = "";

        try
        {
            out.println("[INFO] Converting screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            out.println("[INFO] Converting completed, image ready for embedding.");
        }
        catch (Exception ex)
        {
            out.println("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }
    
    
    public boolean navigateToTA38Url(String pageUrl)
    {
        try
        {
            Driver.navigate().to(pageUrl);
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            Driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" navigating to URL - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyXpath(String elementXpath)
    {
        try
        {
            Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
//            waitForElementByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyXpathUsingActions(String elementXpath)
    {
        try
        {
            Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
            waitForElementByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions action = new Actions(Driver);
            action.moveToElement(elementToClick);
            action.click(elementToClick);
            action.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean doubleClickElementbyXpath(String elementLinkText)
    {
        try
        {
            //waitForElementByLinkText(elementLinkText);
            Actions act = new Actions(Driver);
            WebElement elementToClick = Driver.findElement(By.xpath(elementLinkText));

            act.doubleClick(elementToClick).perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" double clicking element by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

//                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpath(String elementXpath, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementNoLongerPresentByXpath(String elementId)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) == null)
                    {
                        elementFound = false;
                        break;
                    }
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }

                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpathTimout(String elementId, int timout)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < timout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    SeleniumDriverInstance.pause(1000);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) == null)
                    {
                        elementFound = false;
                        break;
                    }
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }

                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean enterTextByXpath(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
//            elementToTypeIn.clear();
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
    
     public boolean altLeftArrow()
    {
        try
        {
            Robot robot = new Robot();

            robot.keyPress(com.sun.glass.events.KeyEvent.VK_ALT);
            robot.keyPress(com.sun.glass.events.KeyEvent.VK_LEFT);
            robot.keyRelease(com.sun.glass.events.KeyEvent.VK_ALT);
            robot.keyRelease(com.sun.glass.events.KeyEvent.VK_LEFT);

        }
        catch (Exception e)
        {
            System.err.println("Failed to go back to previous window - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        return true;
    }

    public boolean clearAndEnterTextByXpath(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.keyDown(elementToTypeIn, Keys.BACK_SPACE);
            pause(2000);
            typeText.keyUp(elementToTypeIn, Keys.BACK_SPACE);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByXpathUsingActions(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        try
        {
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            WebElement formxpath = Driver.findElement(By.xpath(elementXpath));
            formxpath.click();
            dropDownList.selectByVisibleText(valueToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
    
   public boolean scrollToElement(String elementXpath)
    {
        try
        {
            WebElement element = Driver.findElement(By.xpath(elementXpath));
            ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", element);
            Narrator.logDebug("Scrolled to element - " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Error scrolling to element - " + elementXpath + " - " + e.getStackTrace());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    } 

    public boolean alertHandler()
    {
        try
        {
            Narrator.logDebug("Attempting to click OK in alert pop-up");
            // Get a handle to the open alert, prompt or confirmation
            Alert alert = Driver.switchTo().alert();
            // Get the text of the alert or prompt
            alert.getText();
            // And acknowledge the alert (equivalent to clicking "OK")
            alert.accept();
            Narrator.logDebug("Ok Clicked successfully...proceeding");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking OK in alert pop-up - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public String retrieveTextByXpath(String elementXpath)
    {
        String retrievedText = "";
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToRead = Driver.findElement(By.xpath(elementXpath));
            retrievedText = elementToRead.getText();
            Narrator.logDebug("Text retrieved successfully from element - " + elementXpath);
            return retrievedText;

        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve text from element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveAttributeByXpath(String elementXpath, String Attribute)
    {
        String retrievedAttribute = "";
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToRead = Driver.findElement(By.xpath(elementXpath));
            retrievedAttribute = elementToRead.getAttribute(Attribute);
            Narrator.logDebug("Attribute retrieved successfully from element - " + elementXpath);
            return retrievedAttribute;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve attribute from element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedAttribute;
        }
    }

    public Boolean ValidateByAttribute(String elementXpath, String Attribute, String testData)
    {
        try
        {
            String attribute = retrieveAttributeByXpath(elementXpath, Attribute);
            String data = testData;

            if (!attribute.equals(data))
            {
                Narrator.logError("Failed to validate " + attribute + ", against " + data + ".");
                return false;
            }
            Narrator.logDebug("Validated by attribute value: " + attribute + ", successfully.");

            return true;

        }
        catch (Exception e)
        {
            Narrator.logError("Failed to Validate attribute against TestData - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public Boolean ValidateByText(String elementXpath, String testData)
    {
        try
        {
            String text = retrieveTextByXpath(elementXpath);
            String data = testData;

            if (!text.equals(data))
            {
                Narrator.logError("Failed to validate " + text + ", against " + data + ".");
                return false;
            }
            Narrator.logDebug("Validated by text value: " + text + ", successfully.");

            return true;

        }
        catch (Exception e)
        {
            Narrator.logError("Failed to Validate Text against TestData - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public boolean waitForElementNotVisible(String xpath, int maxWaitCount)
    {
        int timeCounter = 0;
        boolean itemPresent = SeleniumDriverInstance.waitForElementByXpath(xpath);
        while (itemPresent)
        {
            if (timeCounter > maxWaitCount)
            {
                return false;
            }

            //pause
            SeleniumDriverInstance.pause(10000);

            //click the empty space
            //increment max counter
            timeCounter++;
            itemPresent = SeleniumDriverInstance.waitForElementByXpath(xpath);
        }

        return true;
    }

    public boolean switchToFrameByXpath(String frameID)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebElement elementFrame = Driver.findElement(By.xpath(frameID));
                    Driver.switchTo().frame(elementFrame);
                    return true;
                }
                catch (Exception e)
                {
                    //Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to frame by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContent()
    {
        try
        {
            Driver.switchTo().defaultContent();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" switching to default content  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public LinkedList<DataColumn> captureTableInformation(String TableXpath)
    {
        DataRow newRow = new DataRow();

        try
        {
            //Finds the Table
            List<WebElement> Header = Driver.findElements(By.xpath(TableXpath + "//th"));
            List<String> Headers = new ArrayList<>();

            for (int i = 0; i < Header.size(); i++)
            {
                String Head = retrieveTextByXpath(TableXpath + "//th[" + (i + 1) + "]");
                Headers.add(Head);
            }

            List<WebElement> Rows = Driver.findElements(By.xpath(TableXpath + "//tr"));

            for (WebElement Row : Rows)
            {
                List<WebElement> Cells = Row.findElements(By.xpath(TableXpath + "//td"));

                for (int i = 0; i < Cells.size(); i++)
                {

                    DataColumn newColumn = new DataColumn("", "", Enums.ResultStatus.UNCERTAIN);

                    newColumn.columnHeader = Headers.get((i % (Headers.size())));
                    newColumn.columnValue = Cells.get(i).getText();
                    newColumn.resultStatus = Enums.ResultStatus.UNCERTAIN;

                    newRow.DataColumns.add(newColumn);
                }

                return newRow.DataColumns;
            }

        }
        catch (Exception e)
        {
            System.err.println("Error Creating Table" + e);
            this.DriverExceptionDetail = "Error Creating Table";

        }
        return newRow.DataColumns;
    }

    public boolean SaveCookiesToFile(String filepath)
    {
        Set<Cookie> cookieSetReturn;

        Set<Cookie> cookies = Driver.manage().getCookies();

        String path = filepath + "\\Cookies.ser";
        try
        {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(cookies);
            out.close();
            fileOut.close();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to Add Cookies, error - " + e.getMessage());
            return false;
        }
    }

    public boolean AddCookiesFromFile(String filepath)
    {
        Set<Cookie> cookieSetReturn;

        try
        {
            //Reads the Cookie File
            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            cookieSetReturn = (Set<Cookie>) in.readObject();
            in.close();
            fileIn.close();

            for (Cookie getcookies : cookieSetReturn)
            {
                Driver.manage().addCookie(getcookies);

            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to Add Cookies, error - " + e.getMessage());
            return false;
        }
    }

    public void takeScreenShot(String screenShotDescription, boolean isError)
    {
        String imageFilePathString = "";

        if (testCaseId == null)
        {
            return;
        }

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            imageFilePathBuilder.append(this.reportDirectory + "\\");

            relativeScreenShotPath = testCaseId + "\\";

            if (isError)
            {
                relativeScreenShotPath += "FAILED_";
            }
            else
            {
                relativeScreenShotPath += "PASSED_";
            }

            relativeScreenShotPath += testCaseId + "_" + screenShotDescription + ".png";

            imageFilePathBuilder.append(relativeScreenShotPath);

            //imageFilePathBuilder.append(this.generateDateTimeString() + ".png");
            imageFilePathString = imageFilePathBuilder.toString();

            setScreenshotPath(imageFilePathString);

            File screenShot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));

            System.out.println(imageFilePathString.toString());
        }
        catch (Exception e)
        {
            Narrator.logError(" could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    public void takeScreenShotNoId(String filePath)
    {

        try
        {
            SeleniumDriverInstance.pause(3000);
            Robot robo = new Robot();
            java.awt.Rectangle captureSize = new java.awt.Rectangle(679, 165, 561, 313);
            System.out.println(captureSize.toString());

            BufferedImage image = robo.createScreenCapture(captureSize);

            ImageIO.write(image, "png", new File(filePath));


        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public void takeScreenShelfLabel(String filePath)
    {

        try
        {
            SeleniumDriverInstance.pause(3000);
            Robot robo = new Robot();
            java.awt.Rectangle captureSize = new java.awt.Rectangle(679, 165, 561, 313);
            System.out.println(captureSize.toString());

            BufferedImage image = robo.createScreenCapture(captureSize);

            ImageIO.write(image, "png", new File(filePath));


        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public void shutDown()
    {
        retrievedTestValues = null;
        try
        {

            Driver.quit();
//            killChromeOnlyInstance();
        }
        catch (Exception e)
        {
            Narrator.logError(" shutting down driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        
        SeleniumDriverInstance._isDriverRunning = false;
    }
    
    public boolean killChromeOnlyInstance()
    {
        try
        {
            Runtime kill = Runtime.getRuntime();
            System.out.println("========================= SYSTEM JOB PATH ======================");
            System.out.println(System.getProperty("user.dir"));
            for (int i = 0; i < 3; i++)
            {
                kill.exec("cmd.exe /c start " + System.getProperty("user.dir") + "\\killChromeOnly.bat");
            }
        }
        catch (Exception e)
        {
            System.out.println("******************************* BAT FILE EXECUTED UNSUCCESSFULLY *******************************");
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
        
        return true;
    }

    public boolean saveImageUsingActions(String element)
    {
        try
        {
            WebElement Image = SeleniumDriverInstance.Driver.findElement(By.xpath(element));
            Actions action = new Actions(SeleniumDriverInstance.Driver);
            action.contextClick(Image).build().perform();
            
            SeleniumDriverInstance.pause(1500);
            
            action.sendKeys(Keys.CONTROL, "v").build().perform();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Error - " + e.getMessage());
            return false;
        }
        
        return true;
    }
    
    public void copyKeys()
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(Keys.CONTROL, "c");
            action.perform();
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - Contoll + C");

        }
    }

    public boolean downloadImgUsingURL(String imageURL, String destinationFile)
    {
        try
        {
            URL url = new URL(imageURL);
            System.setProperty("jsse.enableSNIExtension", "false");
            InputStream fis = url.openStream();
            OutputStream fos = new FileOutputStream(destinationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = fis.read(b)) != -1)
            {
                fos.write(b, 0, length);
            }

            fis.close();
            fos.close();
            return true;
        }
        catch (Exception e)
        {
            narrator.logError("Failed to download Image, error - " + e.getMessage());
            return false;
        }
    }

    public boolean switchToTabOrWindow()
    {
        try
        {
            String winHandleBefore = SeleniumDriverInstance.Driver.getWindowHandle();
            for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
            {
                if (!winHandle.equals(winHandleBefore))
                {
                    SeleniumDriverInstance.Driver.switchTo().window(winHandle);
                }
            }
        }
        catch (Exception ex)
        {
            Narrator.logError("Could not switch to new tab" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
        return true;
    }

    public boolean switchToWindow(WebDriver driver, String title)
    {
        mainWindowsHandle = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles(); // Gets all the available windows
        for (String handle : handles)
        {
            driver.switchTo().window(handle); // switching back to each window in loop
            if (driver.getTitle().equals(title)) // Compare title and if title matches stop loop and return true
            {
                return true; // We switched to window, so stop the loop and come out of funcation with positive response
            }
        }
        driver.switchTo().window(mainWindowsHandle); // Switch back to original window handle
        return false; // Return false as failed to find window with given title.
    }

    public boolean waitForElementByXpathVisibility(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpathVisibility(String elementXpath, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean SwitchVisibleTab()
    {
        try
        {
            Actions action = new Actions(Driver);
            action.keyDown(Keys.CONTROL);
            action.keyDown(Keys.SHIFT);
            action.keyDown(Keys.TAB);
            action.keyUp(Keys.CONTROL);
            action.keyUp(Keys.SHIFT);
            action.keyUp(Keys.TAB);
            action.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to swap to previous tab, error - " + e.getMessage());
            return false;

        }
    }

    public boolean waitForElementToBeClickableByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be clickable by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }
    //================================================================================Added Custom Methods============================================================================================

    public boolean waitForElementPresentByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));

                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementNoLongerPresentByXpath(String elementClass, Integer timeout)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < timeout)
            {
                try
                {
                    Driver.findElement(By.xpath(elementClass));
                    elementFound = true;
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }
                Thread.sleep(500);
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by Class  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        if (elementFound)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void clearTextByXpath(String elementXpath)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys();
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
        }
        catch (Exception e)
        {
            Narrator.logError(" clearing text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public void pressKey(Keys keyToPress)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(keyToPress);
            action.perform();
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - " + keyToPress);

        }
    }

    public boolean checkForWindow(WebDriver driver, String title, String windowHandle)
    {

        mainWindowsHandle = windowHandle;
        Set<String> handles = driver.getWindowHandles(); // Gets all the available windows
        for (String handle : handles)
        {
            driver.switchTo().window(handle); // switching back to each window in loop
            String til = driver.getTitle();
            if (til.equals(title)) // Compare title and if title matches stop loop and return true
            {
                driver.switchTo().window(mainWindowsHandle);
                return true; // We switched to window, so stop the loop and come out of funcation with positive response

            }
        }
        driver.switchTo().window(mainWindowsHandle); // Switch back to original window handle
        return false; // Return false as failed to find window with given title.
    }

    public String getWindowsHandle(WebDriver driver)
    {
        mainWindowsHandle = driver.getWindowHandle();
        return mainWindowsHandle;
    }

////////////////////////////////////New\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public boolean hoverOverElementByXpathNew(String elementXpath)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);

            Actions hoverTo = new Actions(Driver);
            hoverTo.moveToElement(Driver.findElement(By.xpath(elementXpath)));
            hoverTo.perform();

            Narrator.logInfo("[Info]Text retrieved successfully from element - " + elementXpath);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("[Error] Failed to retrieve text from element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByValueFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        try
        {
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            dropDownList.selectByValue(valueToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by value using Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByIndexFromDropDownListUsingXpath(String elementXpath, Integer indexToSelect)
    {
        try
        {
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            dropDownList.selectByIndex(indexToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to select option from dropdownlist by index using xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementToBeClickableByXpath(String elementXpath, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be clickable by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpathVisibilityMillisecondNew(String elementXpath, Integer milliseconds)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < milliseconds)
            {
                try
                {
                    WebDriverWait waitMSecs = new WebDriverWait(Driver, 0, 1);
                    if (waitMSecs.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (waitMSecs.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean rightClickElementByXpath(String elementXpath)
    {
        this.waitForElementByXpath(elementXpath);
        try
        {
            WebElement elem = SeleniumDriverInstance.Driver.findElement(By.xpath(elementXpath));

            Actions actions = new Actions(SeleniumDriverInstance.Driver);
            actions.moveToElement(elem);
            actions.contextClick(elem);//Right click
            actions.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" right clicking element by xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean ClearTextBoxField(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean OpenNewTabChrome()
    {
        try
        {
            WebElement body = Driver.findElement(By.xpath("//body"));

            body.sendKeys(Keys.CONTROL + "t");

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to open a new tab - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;

        }
    }

}

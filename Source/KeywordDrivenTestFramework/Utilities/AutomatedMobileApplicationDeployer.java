/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import java.io.File;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mtofey
 */
public class AutomatedMobileApplicationDeployer extends BaseClass
{
    public static String errorMessage = "";
    public static boolean conErorr;
    
    public AutomatedMobileApplicationDeployer()
    {
        isLatestBuildDeployed();
    }
    
    public boolean isLatestBuildDeployed()
    {
        File latestBuild = getLatestFilefromPath("C:\\Users\\mtofey\\Documents\\New folder (2)\\Final Dir");
        
        isDeviceConfigUpdated(latestBuild);
        
        //Variables
        boolean checkWiFiStatus = wifiSwitch();
        
        boolean isAppLaunchedAndLoggedIn = false;
        boolean isAppSynched = false;
        boolean isBuildReadyForRegression = false;
        boolean isServerDeadAndAppReady = false;
        
        //Worker methods
        if(checkWiFiStatus)
             isAppLaunchedAndLoggedIn = appLaunchedAndLoggedIn();
        if(isAppLaunchedAndLoggedIn)
            isAppSynched = isBuildSynced();
        if(isAppSynched)
            isBuildReadyForRegression = killAndroidServer();
        if(isBuildReadyForRegression)
            isServerDeadAndAppReady = killAndroidServer();
        
        return isServerDeadAndAppReady;
    }
    
    private static File getLatestFilefromPath(String dirPath)
    {
        try
        {    
            File dir = new File(dirPath);
            //Lists all files in the directory specified
            File[] files = dir.listFiles();

            //Returns throws a NullPointerException if there are no files located in directory
            if (files == null || files.length == 0) 
            {
                throw new NullPointerException("There are no files located in the current directory");
            }

            File lastModifiedFile = files[0];

            //Checks the 'Last Modified' stamp of each file and compares them and returns the newest file
            for (int i = 1; i < files.length; i++) 
            {
               if (lastModifiedFile.lastModified() < files[i].lastModified()) 
               {
                   lastModifiedFile = files[i];
               }
            }

            return lastModifiedFile;
        }
        catch(NullPointerException nullPointer)
        {
            Narrator.logError("Error retrieving the latest file - " + nullPointer.getMessage());
            System.err.println("Error retrieving the latest file - " + nullPointer.getMessage());
            return null;
        }
        catch(Exception e)
        {
            Narrator.logError("Error retrieving the latest file - " + e.getMessage());
            System.err.println("Error retrieving the latest file - " + e.getMessage());
            return null;
        }
    }
    
    private static boolean isDeviceConfigUpdated(File file)
    {
        try
        {
            Enums.DeviceConfig.zebraTC70.ApplicationName = file.getName();
            TestMarshall.currentDeviceConfig = Enums.DeviceConfig.zebraTC70;
            return true;
        }
        catch(Exception e)
        {
            Narrator.logError("Error updating the device config - " + e.getMessage());
            System.err.println("Error updating the device config - " + e.getMessage());
            return false;
        }
    }
    
    //Launches and logs into app
    public boolean appLaunchedAndLoggedIn()
    {
        try
        {
            AppiumDriverInstance = new AppiumDriverUtility();
            
            if(!isLoggedIn())
            {
                narrator.logError("Failed to log into the app - "+errorMessage);
            }
            return true;
        }
        catch(Exception e)
        {
            Narrator.logError("Failed to launch and log into app - " + e.getMessage());
            System.err.println("Failed to launch and log into app - " + e.getMessage());
            return false;
        }
    }
    
    //Called to check if App is synched
    private boolean isBuildSynced()
   {
        try
        {
            int count = 0;
            boolean LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.syncingText(), 15);

            while (LoadingCircle || count < 9)
            {
                try
                {
                    String syncing = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.syncingText());
                    LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.syncingText(), 5);

                    if (syncing.isEmpty())
                    {
                        break;
                    }

                    count++;
                }
                catch (Exception e)
                {
                    narrator.stepPassed("Successfully waited for syncing to be finished - " + e.getMessage());
                    return false;
                }
            }
            
            return true;
        }
        catch(Exception e)
        {
            Narrator.logError("Error syncing latest build - " + e.getMessage());
            System.err.println("Error syncing latest build - " + e.getMessage());
            return false;
        }
    }
    
    //Is being checked initially
    public boolean wifiSwitch()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            errorMessage = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            errorMessage = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            errorMessage = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                errorMessage = "Failed to wait for the wifi button with index to be visible";
                return false;
            }

            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
            String wifiText = element.getAttribute("name");

            System.out.println("");

            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi"))
            {
                Driver.pressKeyCode(AndroidKeyCode.BACK);
                return true;
            }
            
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                errorMessage = "Failed to click the wifi button with index";
                return false;
            }

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            errorMessage = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            errorMessage = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
        {
            errorMessage = "Failed to wait for the wifi to be connected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    //Used in appLaunchedAndLoggedIn();
    public boolean isLoggedIn()
    {

        String temp;

        if (!conErorr)
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                errorMessage = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                errorMessage = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages())
            {
                errorMessage = Shortcuts.error;
                return false;
            }

            String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (!store.equals(narrator.getData("StoreSelection")))
            {
                if (!scrollToSubElement(narrator.getData("StoreSelection")))
                {
                    errorMessage = "Could not find " + narrator.getData("StoreSelection");
                    return false;
                }

            }

            if (!multiplePopUpMessages())
            {
                errorMessage = Shortcuts.error;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                errorMessage = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                errorMessage = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            errorMessage = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            errorMessage = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            errorMessage = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            errorMessage = "Failed to enter the password.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully entered the login details: <br> Username: " + testData.getData("username") + " <br> Password: " + testData.getData("password"));

        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            errorMessage = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                isLoggedIn();

            }
        }

//        if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//           if(!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 4))
//           {
//               errorMessage = "Failed to wait for the progress circe to no longer be present";
//               return false;
//           }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            errorMessage = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

//        int count = 0;
//        boolean LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.syncingText(), 10);
//
//        while (LoadingCircle || count < 3)
//        {
//            try
//            {
//                String syncing = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.syncingText());
//                LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.syncingText(), 5);
//
//                if (syncing.isEmpty())
//                {
//                    break;
//                }
//
//                count++;
//            }
//            catch (Exception e)
//            {
//                narrator.stepFailedWithScreenShot("Successfully waited for syncing to be finished - " + e.getMessage());
//                return false;
//
//            }
//
//        }
        return true;

    }
    //Kills server for regression runs
    private boolean killAndroidServer()
    {
        try
        {
            if (AppiumDriverInstance != null && AppiumDriverInstance.isDriverRunning())
        {
            switch (currentPlatform)
            {
                case Android:
                {
                    AppiumDriverInstance.stopAndroidServer();
                    _isRemote = false;
                    AppiumDriverUtility.isCMDRunning = false;
                    try
                    {
                        Thread.sleep(2000);
                    }
                    catch (Exception e)
                    {
                        Narrator.logError("[ERROR] - " + e.getMessage());
                    }
                    break;
                }
            }
        }
        if (SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.shutDown();
        }
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
    //Used in multiplePopUpMessages();
    public static boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                errorMessage = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }
    
    //Used in Login method
    public static boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 3; i++)
        {
            if (!popUpMessage())
            {
                errorMessage = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    //Used in Login method
    public static boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;
import com.dropbox.core.DbxWriteMode;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.sikuli.script.Key;
/**
 *
 * @author mtofey
 */
public class DropBoxUploaderBeta extends BaseClass
{
    private static DbxRequestConfig config;
    private static String accessToken;
    private static List <String> fileList = new ArrayList<>();
    
    private static String OUTPUT_ZIP_FILE;
    
    //If SOURCE_FOLDER is path then zip files in folder; else if SOURCE_FOLDER is file then zip file 
    private static String SOURCE_FOLDER; 
    
    public DropBoxUploaderBeta()
    {
        
    }
    
    public static void zipAndUploadReportsToDropBox()
    {
        String dropboxUsername, dropboxPassword;
        try
        {
            getLatestReportsFolder(System.getProperty("user.dir")+"\\KeywordDrivenTestReports\\");
            
            String tempKey = "";
            String tempSecret = "";
            
            if(currentDevice == Enums.Device.ZebraTC70_Dvt)
            {
                tempKey = ApplicationConfig.getDropBoxKey();
                tempSecret = ApplicationConfig.getDropBoxSecret();
                dropboxUsername = ApplicationConfig.getDropBoxUsername();
                dropboxPassword = ApplicationConfig.getDropBoxPassword();
            }
            else
            {
                tempKey = ApplicationConfig.getTestbotDropBoxKey();
                tempSecret = ApplicationConfig.getTestbotDropBoxSecret();
                dropboxUsername = ApplicationConfig.getTestbotDropBoxUsername();
                dropboxPassword = ApplicationConfig.getTestbotDropBoxPassword();
            }
            
            final String APP_KEY = tempKey;
            final String APP_SECRET = tempSecret;

            //Request access
            DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);

            //Add config
            config = new DbxRequestConfig(
                "JavaTutorial/1.0", Locale.getDefault().toString());
            DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);

            //Authorize request
            String authorizeUrl = webAuth.start();
            System.out.println("1. Go to: " + authorizeUrl);
            System.out.println("2. Click \"Allow\" (you might have to log in first)");
            System.out.println("3. Copy the authorization code.");
            
            //Using Selenium to get code
            SeleniumDriverInstance = new SeleniumDriverUtility(Enums.BrowserType.Chrome);
            
            SeleniumDriverInstance.startDriver();
            
            SeleniumDriverInstance.navigateTo(authorizeUrl);
            
            String error = "";
            
            if(!logInToDropBox(dropboxUsername,dropboxPassword))
            {
                error = "Failed to log in to DropBox";
                throw new NullPointerException(error);
            }
            
            if(!SeleniumDriverInstance.waitForElementByXpath("//button[@name='allow_access']"))
            {
                error = "Failed to wait for the Allow button.";
                throw new NullPointerException(error);
            }
            
            SeleniumDriverInstance.pause(2500);
            
            if(!SeleniumDriverInstance.clickElementbyXpath("//button[@name='allow_access']"))
            {
                error = "Failed to click on the Allow button.";
                throw new NullPointerException(error);
            }
            
            if(!SeleniumDriverInstance.waitForElementByXpath("//input[@data-token]"))
            {
                SeleniumDriverInstance.pressKey(Keys.F5);
                
                if(!SeleniumDriverInstance.clickElementbyXpath("//button[@name='allow_access']"))
                {
                    error = "Failed to click on the Allow button.";
                    throw new NullPointerException(error);
                }
                
                if(!SeleniumDriverInstance.waitForElementByXpath("//input[@data-token]"))
                {
                    error = "Failed to wait for the access token to appear";
                    throw new NullPointerException(error);
                }
            }
            
            if(!error.isEmpty())
            {
                throw new Exception(error);
            }
            
            String code = SeleniumDriverInstance.retrieveAttributeByXpath("//input[@data-token]","data-token");
            //Authenticates and receives access token
            DbxAuthFinish authFinish = webAuth.finish(code);
            accessToken = authFinish.accessToken;
            
            uploadToDropBox(SOURCE_FOLDER, OUTPUT_ZIP_FILE);
            
            SeleniumDriverInstance.shutDown();
            
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    private static void uploadToDropBox(String fileToZip, String fileNameToUpload)
    {
        try 
        {
            //Gets file to upload
            zipFolder(fileToZip,fileNameToUpload);
            File inputFile = new File(fileNameToUpload);
            FileInputStream inputStream = new FileInputStream(inputFile);
            
            //Initialises client
            DbxClient client = new DbxClient(config, accessToken);
            
            //Prints account that is signed in
            System.out.println("Linked account: " + client.getAccountInfo().displayName);
            
            //Uploads file specified in parameters
            DbxEntry.File uploadedFile = client.uploadFile("/"+fileNameToUpload, DbxWriteMode.add(), inputFile.length(), inputStream);
            System.out.println("Uploaded: " + uploadedFile.toString());
            inputStream.close();
        } 
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        
    }
    
    private static boolean logInToDropBox(String username, String password)
    {
        if(!SeleniumDriverInstance.waitForElementByXpath("//input[@name='login_email']"))
        {
            System.out.println("Failed to wait for the username field to load");
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath("//input[@name='login_email']", username))
        {
            System.out.println("Failed to enter \""+username+"\" as the username");
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath("//input[@name='login_password']", password))
        {
            System.out.println("Failed to enter \""+password+"\" as the password");
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath("//button[@type='submit']"))
        {
            System.out.println("Failed to click on the login button");
            return false;
        }
        
        return true;
    }
    
    private static void zipFolder(String sourceFolder, String outputFolder)
    {
        try
        {
            generateFileList(sourceFolder, new File(sourceFolder));
            zipIt(sourceFolder, outputFolder);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    
    //Zips the folder
    private static void zipIt(String sourceFolder, String zipFileName) {
        byte[] buffer = new byte[1024];
        String source = new File(sourceFolder).getName();
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(zipFileName);
            zos = new ZipOutputStream(fos);

            System.out.println("Output to Zip : " + zipFileName);
            FileInputStream in = null;

            for (String file: fileList) {
                System.out.println("File Added : " + file);
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(sourceFolder + File.separator + file);
                    int len;
                    while ((len = in .read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            zos.closeEntry();
            System.out.println("Folder successfully compressed");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Get's all files in path specified
    private static void generateFileList(String sourceFolder, File node) 
    {
        // add file only
        if (node.isFile()) 
        {
            fileList.add(generateZipEntry(sourceFolder, node.toString()));
        }

        if (node.isDirectory()) 
        {
            String[] subNote = node.list();
            for (String filename: subNote) 
            {
                generateFileList(sourceFolder, new File(node, filename));
            }
        }
    }
    
    private static String generateZipEntry(String sourceFolder,String file) 
    {
        return file.substring(sourceFolder.length() + 1, file.length());
    }
    
    private static void getLatestReportsFolder(String reportsFolder)
    {
        try
        {
            File dir = new File(reportsFolder);
            File[] files = dir.listFiles();
            File lastModified = Arrays.stream(files).filter(File::isDirectory).max(Comparator.comparing(File::lastModified)).orElse(null);
            SOURCE_FOLDER = lastModified.toString();
            OUTPUT_ZIP_FILE = SOURCE_FOLDER.substring(reportsFolder.length(), SOURCE_FOLDER.length())+".zip";
            System.out.println("Last modified folder: "+lastModified);
            System.out.println("DropBox Folder name: "+OUTPUT_ZIP_FILE);
        }
        catch(Exception e) 
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}

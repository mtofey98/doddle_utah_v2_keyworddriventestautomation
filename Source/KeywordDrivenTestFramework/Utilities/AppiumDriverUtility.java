/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.currentDevice;
import static KeywordDrivenTestFramework.Core.BaseClass.currentPlatform;
import static KeywordDrivenTestFramework.Core.BaseClass.testCaseId;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.RetrievedTestValues;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import static java.lang.System.out;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Key;

/**
 *
 * @author drousseau
 */
public class AppiumDriverUtility extends BaseClass
{

//    public static AppiumDriver Driver;
    public static AndroidDriver Driver;
//    public RemoteWebDriver Driver;
    File fileIEDriver;
    public RetrievedTestValues retrievedTestValues;
    public String DriverExceptionDetail = "";
    public String Port = "";
    public String line, newBearer;
    public StringEntity input;
    TestEntity testData;
    public static Boolean _isDriverRunning = false;
    Runtime rt = Runtime.getRuntime();
    public static boolean isCMDRunning = false;

    public AppiumDriverUtility()
    {
        if (_isRemote == false)
        {

            if (isCMDRunning == false)
            {
                this.startServer(currentDevice.ServerURL);

            }
            _isDriverRunning = true;

        }
        else
        {

            Driver = (AndroidDriver) setCapabilitiesRemoteRun(currentDevice.DeviceID, currentDevice.ServerURL);
            _isDriverRunning = true;

        }

    }

    public WebDriver setCapabilitiesWithApk()
    {

        try
        {
            //The APK information is set in the appconfig
            File app = new File(currentDeviceConfig.ApplicationFilePath, currentDeviceConfig.ApplicationName);

            DesiredCapabilities capabilities = new DesiredCapabilities();

            capabilities.setCapability(currentDevice.CapabilityName, currentDevice.DeviceID);
            capabilities.setCapability("deviceName", currentDeviceConfig.deviceName);
            capabilities.setCapability("platformName", currentDeviceConfig.platformName);
            capabilities.setCapability("automationName", currentDeviceConfig.automationName);
            capabilities.setCapability(CapabilityType.VERSION, currentDeviceConfig.Version);
            //Call the following use RunActivity
//          capabilities.setCapability("appPackage", currentDeviceConfig.appPackage);
//          capabilities.setCapability("appActivity", currentDeviceConfig.appActivity);
            capabilities.setCapability("app", app.getAbsolutePath());
            capabilities.setCapability("newCommandTimeout", 600);
            capabilities.setCapability(MobileCapabilityType.NO_RESET, true);

            isCMDRunning = true;
            return new AndroidDriver(new URL(currentDevice.ServerURL), capabilities);
        }
        catch (Exception e)
        {
            Narrator.logError("Error setting Capabilities - " + e.getMessage());
            return null;
        }
    }

    public WebDriver setCapabilitiesWithIpa()
    {

        try
        {
            //The IPA information is set in the appconfig
            File app = new File(currentDeviceConfig.ApplicationFilePath, currentDeviceConfig.ApplicationName);

            DesiredCapabilities capabilities = new DesiredCapabilities();

            capabilities.setCapability(currentDevice.CapabilityName, currentDevice.DeviceID);
            capabilities.setCapability("deviceName", currentDeviceConfig.deviceName);
            capabilities.setCapability("platformName", currentDeviceConfig.platformName);
            capabilities.setCapability("automationName", currentDeviceConfig.automationName);
            capabilities.setCapability(CapabilityType.VERSION, currentDeviceConfig.Version);
            capabilities.setCapability("appPackage", currentDeviceConfig.appPackage);
            capabilities.setCapability("appActivity", currentDeviceConfig.appActivity);
            capabilities.setCapability("app", app.getAbsolutePath());
            capabilities.setCapability("newCommandTimeout", 360);

            return new AndroidDriver(new URL(currentDevice.ServerURL), capabilities);
        }
        catch (Exception e)
        {
            Narrator.logError("Error setting Capabilities - " + e.getMessage());
            return null;
        }
    }

    public boolean takeBarcodeScreenShot(String filePath)
    {

        try
        {
            SeleniumDriverInstance.pause(2000);
            Robot robo = new Robot();
            java.awt.Rectangle captureSize = new java.awt.Rectangle(835, 523, 250, 100);
            System.out.println(captureSize.toString());

            BufferedImage image = robo.createScreenCapture(captureSize);

            ImageIO.write(image, "png", new File(filePath));

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public WebDriver setCapabilitiesRemoteRun(String deviceUDID, String serverURL)
    {

        try
        {
            DesiredCapabilities capabilities = new DesiredCapabilities();

            capabilities.setCapability("udid", deviceUDID);
            capabilities.setCapability("deviceName", deviceUDID);
            capabilities.setCapability("appPackage", currentDeviceConfig.appPackage);
            capabilities.setCapability("appActivity", currentDeviceConfig.appActivity);
            capabilities.setCapability("platformName", currentDevice.platform);
            capabilities.setCapability("newCommandTimeout", 360);
            capabilities.setCapability("onResetTimeout", 360);

            return new AndroidDriver(new URL(serverURL), capabilities)
            {
            };
        }
        catch (Exception e)
        {
            Narrator.logError("Error setting Capabilities - " + e.getMessage());
            return null;
        }
    }

    public boolean takeScreenShotWithCoordinates(String filePath)
    {

        try
        {
            SeleniumDriverInstance.pause(2000);
            Robot robo = new Robot();
            java.awt.Rectangle captureSize = new java.awt.Rectangle(835, 523, 250, 100);
            BufferedImage image = robo.createScreenCapture(captureSize);

            ImageIO.write(image, "png", new File(filePath));

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public boolean startServer(String directory)
    {
        try
        {
            Port = currentDevice.ServerURL;
            String new_dir = directory;
            String[] URLinfo = Port.split(":");
            String[] Portinfo = URLinfo[2].split("/");
            String PortNumber = Portinfo[0];

            //rt.exec("cmd.exe /c cd \"" + new_dir + "\" & start cmd.exe /k \"appium -p \"" + PortNumber + "\" -bp 2251 -U " + currentDevice.DeviceID + "\"");
            rt.exec("cmd /c start C:\\Users\\\"%USERNAME%\"\\AppData\\Roaming\\npm\\appium.cmd -p" + PortNumber + " -bp 2251 -U " + currentDevice.DeviceID);
            pause(10000);
            this.selectPlatform();

        }
        catch (Exception e)
        {
            Narrator.logError("Failed to start server - " + e.getMessage());
        }

        return true;
    }

    public boolean isDriverRunning()
    {
        return _isDriverRunning;
    }

    public void selectPlatform()
    {
        switch (currentPlatform)
        {
            case Android:
                int counter = 1;
                Driver = (AndroidDriver) setCapabilitiesWithApk();
                while (Driver == null && counter < 61)
                {
                    Driver = (AndroidDriver) setCapabilitiesWithApk();
                    pause(500);
                    counter++;

                }
                _isDriverRunning = true;
                break;
            case iOS:
                Driver = (AndroidDriver) setCapabilitiesWithIpa();
                _isDriverRunning = true;
                break;
        }
        retrievedTestValues = new RetrievedTestValues();
    }

    public void Reset()
    {
        try
        {

            Runtime close = Runtime.getRuntime();
            close.exec("taskkill /IM cmd.exe");
        }
        catch (Exception e)
        {
            Narrator.logError("Error restarting the driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }

    }

    public void shutDown()
    {
        retrievedTestValues = null;
        try
        {
            Runtime close = Runtime.getRuntime();
            Driver.quit();
            close.exec("taskkill /IM cmd.exe");

        }
        catch (Exception e)
        {
            Narrator.logError("Error shutting down driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        _isDriverRunning = false;
    }

    public String get(final String url)
    {
        Driver.get(url);
        return Driver.getPageSource();

    }

    public boolean elementGestureTest(String Id)
    {
        //SikuliDriverInstance.Desktop.find(SikuliDriverInstance.ScreenshotDirectory + ImageFilePath).sw;
        MobileElement e = (MobileElement) Driver.findElementById(Id);

        //SikuliDriverInstance.Desktop.find(SikuliDriverInstance.ScreenshotDirectory + ImageFilePath);
        e.swipe(io.appium.java_client.SwipeElementDirection.UP, 2000);
        e.swipe(io.appium.java_client.SwipeElementDirection.DOWN, 2000);
        return true;
    }

    public enum SwipeElementDirection
    {
        /**
         * Up from the center of the lower
         */
        UP
        {
            @Override
            void swipe(AppiumDriver driver, MobileElement element, int duration)
            {
                Point p = element.getCenter();
                Point location = element.getLocation();
                Dimension size = element.getSize();
                driver.swipe(p.getX(), location.getY() + size.getHeight(), p.getX(), location.getY(), duration);
            }
        },
        /**
         * Down from the center of the upper
         */
        DOWN
        {
            @Override
            void swipe(AppiumDriver driver, MobileElement element, int duration)
            {
                Point p = element.getCenter();
                Point location = element.getLocation();
                Dimension size = element.getSize();
                driver.swipe(p.getX(), location.getY(), p.getX(), location.getY() + size.getHeight(), duration);
            }
        },
        /**
         * To the left from the center of the rightmost
         */
        LEFT
        {
            @Override
            void swipe(AppiumDriver driver, MobileElement element, int duration)
            {
                Point p = element.getCenter();
                Point location = element.getLocation();
                Dimension size = element.getSize();
                driver.swipe(location.getX() + size.getWidth(), p.getY(), location.getX(), p.getY(), duration);
            }
        },
        /**
         * To the right from the center of the leftmost
         */
        RIGHT
        {
            @Override
            void swipe(AppiumDriver driver, MobileElement element, int duration)
            {
                Point p = element.getCenter();
                Point location = element.getLocation();
                Dimension size = element.getSize();
                driver.swipe(location.getX(), p.getY(), location.getX() + size.getWidth(), p.getY(), duration);
            }
        };

        void swipe(AppiumDriver driver, MobileElement element, int duration)
        {
        }
    }

//   public boolean swipe() //845,477, 945,525
//    {
//        try
//        {
//            JavascriptExecutor js = (JavascriptExecutor) Driver;
//    HashMap<String, Double> swipeObject = new HashMap<String, Double>();
//    swipeObject.put("startX", 1315.5);
//    swipeObject.put("startY", 978.4);
//    swipeObject.put("endX", 1315.5);
//    swipeObject.put("endY", 596.5);
//    swipeObject.put("duration", 0.20);
//    js.executeScript("mobile: swipe", swipeObject);
//          
//            return true;
//        }
//        catch(Exception e)
//        {
//            System.err.println("Error clicking element by Id - " + e.getMessage());
//            this.DriverExceptionDetail = e.getMessage();
//            return false;      
//        }
//    }
    public boolean scrollDown(String element)

    {
        try
        {
            JavascriptExecutor js = (JavascriptExecutor) Driver;
            System.out.println("swiping to element " + element);
            js.executeScript("mobile: scrollTo", element);
            Driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

//            JavascriptExecutor js = (JavascriptExecutor) Driver;
//            WebElement element = Driver.findElement(By.className(Name));
//            HashMap<String, String> arguments = new HashMap<String, String>();
//            arguments.put("element",((RemoteWebElement) element).getId());
//            js.executeScript("mobile: scrollTo", arguments);
//           JavascriptExecutor js = (JavascriptExecutor) Driver;
//
//        HashMap<String, String> scrollObject = new HashMap<String, String>();
//
//        scrollObject.put("direction", "down");
//
//        scrollObject.put("text", tillLastLink);
//
//        scrollObject.put("element",( (RemoteWebElement) scroll).getId());
//
//        js.executeScript("mobile: scrollTo", scrollObject);
//        
//        WebElement scrollableView = Driver.findElement(By.id("********/broadcast_scroll"));
//
//        scrollDown(scrollableView, "enter text till you want to scroll"); 
            return true;

        }
        catch (Exception e)
        {
            Narrator.logError("Could not find verification text - " + e.getMessage());
            return false;
        }
    }

    public void BarcodeScanner() throws InterruptedException
    {
        //Create Frame
        JFrame snakeFrame = new JFrame();

        //Can set the default size here
        //snakeFrame.setBounds(100, 200, 800, 800);
        java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int centerX = screenSize.width / 2;
        int centerY = (screenSize.height / 2) + 200;

        snakeFrame.setLocation(centerX, centerY);

        //Must set visible - !
        snakeFrame.setVisible(true);

        //Load image - will be autoSized.
        snakeFrame.add(new JLabel(new ImageIcon("C:\\Users\\gdean@dvt.co.za\\Frameworks\\doddlle_POC_Ref\\Barcodes\\1.png")));
        snakeFrame.pack();
        Thread.sleep(10000);
        //Close the frame
        snakeFrame.dispose();
    }

    public boolean verifyIfScreenIsLoaded(String xpath, String verificationText)
    {
        try
        {
            String actionBarTitle = Driver.findElement(By.xpath(xpath)).getText();
            if (actionBarTitle.toUpperCase().trim().equals(verificationText.toUpperCase().trim()))
            {
                Narrator.logDebug("Verification text found...Screen loaded successfully");
                return true;
            }
            else
            {
                Narrator.logError("Could not find verification text - ");
                return false;
            }

        }
        catch (Exception e)
        {
            Narrator.logError("Could not find verification text - " + e.getMessage());
            return false;
        }
    }

    public void scanButton()
    {
        try
        {
            System.out.println("**************************** START SCAN ****************************");
            String[] command =
            {
                "cmd.exe", "/C", "Start", System.getProperty("user.dir") + "\\ZebraTC70_Scan.bat"
            };
            Thread.sleep(500);
            Process p = Runtime.getRuntime().exec(command);
//            p = Runtime.getRuntime().exec(command);
            System.out.println("**************************** END SCAN ****************************");
        }
        catch (Exception e)
        {
            Narrator.logError("Error press the scan button - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
//            return false;
        }
    }

    public void killChromeInstances()
    {
        try
        {
            String command = System.getProperty("user.dir") + "\\killchrome.bat";
            Process process = Runtime.getRuntime().exec(command);
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to kill chrome instances - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

//    public void callScanMethod()
//    {
//        try 
//        {
//            System.out.println("**************************** START SCAN ****************************");
//            String[] command = {"call", "/C", "Start", System.getProperty("user.dir") + "\\ZebraTC70_Scan.bat"};
//            Thread.sleep(500);
//            Process p =  Runtime.getRuntime().exec(command);   
////            p = Runtime.getRuntime().exec(command);
//            System.out.println("**************************** END SCAN ****************************");
//        } 
//        catch (Exception e) 
//        {
//            Narrator.logError("Error press the scan button - " + e.getMessage());
//            this.DriverExceptionDetail = e.getMessage();
////            return false;
//        }
//    }
    public boolean killChromeInstance()
    {
        try
        {
            Runtime kill = Runtime.getRuntime();
            System.out.println("========================= SYSTEM JOB PATH ======================");
            System.out.println(System.getProperty("user.dir"));
            for (int i = 0; i < 3; i++)
            {
                kill.exec("cmd.exe /c start " + System.getProperty("user.dir") + "\\killChrome.bat");
            }
        }
        catch (Exception e)
        {
            System.out.println("******************************* BAT FILE EXECUTED UNSUCCESSFULLY *******************************");
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

        return true;
    }

    public boolean killChromeOnlyInstance()
    {
        try
        {
            Runtime kill = Runtime.getRuntime();
            System.out.println("========================= SYSTEM JOB PATH ======================");
            System.out.println(System.getProperty("user.dir"));
            for (int i = 0; i < 3; i++)
            {
                kill.exec("cmd.exe /c start " + System.getProperty("user.dir") + "\\killChromeOnly.bat");
            }
        }
        catch (Exception e)
        {
            System.out.println("******************************* BAT FILE EXECUTED UNSUCCESSFULLY *******************************");
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

        return true;
    }

    public boolean pressHardWareButtonbyKeyCode()
    {
        try
        {

            Runtime scan = Runtime.getRuntime();
            System.out.println("************************* SYSTEM JOB PATH *************************");
            System.out.println(System.getProperty("user.dir"));

            switch (currentDevice)

            {
                case ZebraMC40:
                {
                    scan.exec("cmd.exe /c start " + System.getProperty("user.dir") + "\\test.bat");
                    System.out.println("******************************* BAT FILE EXECUTED SUCCESSFULLY *******************************");
                    break;
                }
                case ZebraTC70_Doddle:
                {
                    scan.exec("cmd.exe /c start \"" + System.getProperty("user.dir") + "\" ZebraTC70Doddle_Scan.bat");
                    System.out.println("******************************* BAT FILE EXECUTED SUCCESSFULLY *******************************");
                    break;
                }
                case ZebraTC70_Dvt:
                {
                    scan.exec("cmd.exe /c start " + System.getProperty("user.dir") + "\\ZebraTC70Dvt_Scan.bat");
                    System.out.println("******************************* BAT FILE EXECUTED SUCCESSFULLY *******************************");
                    break;
                }
                case ZebraTC51:
                {
                    scan.exec("cmd.exe /c start " + System.getProperty("user.dir") + "\\ZebraTC51_Scan.bat");
                    System.out.println("******************************* BAT FILE EXECUTED SUCCESSFULLY *******************************");
                    break;
                }
                case HoneyWellD75E:
                {
                    scan.exec("cmd.exe /c start " + System.getProperty("user.dir") + "\\HoneyWellD75E_Scan.bat");
                    System.out.println("******************************* BAT FILE EXECUTED SUCCESSFULLY *******************************");
                    break;
                }
            }

            return true;
        }

        catch (Exception e)
        {
            System.out.println("******************************* BAT FILE EXECUTED UNSUCCESSFULLY *******************************");
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public boolean wifiSettings()
    {
        try
        {

            Runtime scan = Runtime.getRuntime();
            scan.exec("cmd.exe /c start " + System.getProperty("user.dir") + "\\network.bat");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementNotVisible(String xpath, int maxWaitCount)
    {
        int timeCounter = 0;
        boolean itemPresent;
        while (itemPresent = AppiumDriverInstance.waitForElementByXpath(xpath, 10))
        {
            if (timeCounter > maxWaitCount)
            {
                return false;
            }
//            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetWithTextView("Loading, please wait...")))
//            {
//
//                return false;
//            }
            //pause
            AppiumDriverInstance.pause(1500);
            //click the empty space
            //increment max counter 
            timeCounter++;
        }

        return true;
    }

    public boolean switchToFrameByXpath(String xpath)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Driver.switchTo().frame(xpath);
                    return true;
                }
                catch (Exception e)
                {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError("Error switching to frame by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToLastDuplicateFrameByXpath(String Xpath)
    {
        int waitCount = 0;
        try
        {
            this.switchToDefaultContent();
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    List<WebElement> iframes = Driver.findElements(By.xpath(Xpath));

                    Driver.switchTo().frame((WebElement) iframes.toArray()[iframes.size() - 1]);
                    return true;
                }
                catch (Exception e)
                {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError("Error switching to last duplicate frame by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContent()
    {
        try
        {
            Driver.switchTo().defaultContent();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error switching to default content  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean moveToElementByXpath(String Xpath)
    {
        try
        {
            Actions moveTo = new Actions(Driver);
            moveTo.moveToElement(Driver.findElement(By.xpath(Xpath)));
            moveTo.perform();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error moving to element - " + Xpath + " - " + e.getStackTrace());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    //modify if needed
    public boolean BarcodeScan()
    {
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(new JLabel(new ImageIcon("C:\\Users\\gdean@dvt.co.za\\Frameworks\\doddlle_POC_Ref\\Barcodes\\1.png")));
            snakeFrame.pack();
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            Thread.sleep(5000);
            snakeFrame.dispose();
        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }
        return true;
    }

    public boolean switchToDefaultContentWhenElementNoLongerVisible(String previousFrameXpath)
    {
        try
        {
            waitForElementNoLongerPresentByXpath(previousFrameXpath);
            Driver.switchTo().defaultContent();
            System.out.println("Successfully switched to default content, current frame handle = " + Driver.getWindowHandle() + ", previous frameId - " + previousFrameXpath);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error switching to default content when element is no longer visible - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementNoLongerPresentByXpath(String elementXpath)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) == null)
                    {
                        elementFound = false;
                        break;
                    }
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }

                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean acceptAlertDialog()
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Driver.switchTo().alert().accept();
                    return true;
                }
                catch (Exception e)
                {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Narrator.logError("Error accepting alert dialog - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByValueFromDropDownListUsingXpath(String Xpath, String valueToSelect)
    {
        try
        {
            this.waitForElementByXpath(Xpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(Xpath)));
            dropDownList.selectByValue(valueToSelect);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error selecting from dropdownlist by value using Name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByTextFromDropDownListUsingXpath(String Xpath, String Text)
    {
        try
        {
            this.waitForElementByXpath(Xpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(Xpath)));
            dropDownList.selectByVisibleText(Text);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error selecting from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void waitUntilElementEnabledByXpath(String ElementXpath)
    {
        try
        {
            int counter = 0;
            boolean isEnabled = false;
            WebElement elementToWaitFor = Driver.findElement(By.xpath(ElementXpath));
            isEnabled = elementToWaitFor.isEnabled();
            while (!isEnabled && counter < 60)
            {
                counter++;
                Thread.sleep(500);
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Error waiting for element to be enabled - " + e.getMessage());
        }

    }

    public boolean checkBoxSelectionByXpath(String elementXpath, boolean mustBeSelected)
    {
        try
        {
            Thread.sleep(2000);
            this.waitForElementByXpath(elementXpath);
            this.waitUntilElementEnabledByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, 60);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement checkBox = Driver.findElement(By.xpath(elementXpath));
            if (checkBox.isSelected() != mustBeSelected)
            {
                checkBox.click();
            }
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error selecting checkbox by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean navigateToPreviousScreen()
    {
        try
        {
            Driver.navigate().back();
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to navigate to the previous screen - " + e.getMessage());
            return false;
        }

    }

    public boolean DVTSignature()
    {
        try
        {
            AppiumDriverInstance.Driver.swipe(70, 290, 70, 410, 10);

            AppiumDriverInstance.Driver.swipe(70, 290, 115, 290, 10);

            AppiumDriverInstance.Driver.swipe(115, 290, 140, 320, 10);

            AppiumDriverInstance.Driver.swipe(140, 320, 140, 380, 10);

            AppiumDriverInstance.Driver.swipe(140, 380, 115, 410, 10);

            AppiumDriverInstance.Driver.swipe(115, 410, 70, 410, 10);

            //V for vendetta
            AppiumDriverInstance.Driver.swipe(170, 290, 205, 410, 10);

            AppiumDriverInstance.Driver.swipe(205, 410, 240, 290, 10);

            //T for Thomas
            AppiumDriverInstance.Driver.swipe(270, 290, 340, 290, 10);

            AppiumDriverInstance.Driver.swipe(305, 290, 305, 410, 10);

            return true;
        }
        catch (Exception e)
        {

            return false;
        }
    }

    public boolean validateElementTextValueByXpath(String elementXpath, String elementText)
    {
        try
        {
            if (waitForElementByXpath(elementXpath))
            {
                WebElement elementToValidate = Driver.findElement(By.xpath(elementXpath));
                String textDetected = elementToValidate.getText();
                if (textDetected.contains(elementText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Error validating element text value by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void WaitUntilDropDownListPopulatedByXpath(String elementXpath)
    {

        try
        {
            this.waitForElementByXpath(elementXpath);
            int waitCount = 0;
            List<WebElement> optionsList;
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
                    optionsList = dropDownList.getOptions();
                    if (optionsList.size() > 0)
                    {
                        break;
                    }
                }
                catch (Exception e)
                {

                }
                Thread.sleep(500);
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError("Error waiting for dropdownlist to be populated by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public boolean clickElementbyXpath(String elementXpath)
    {
        try
        {
            waitForElementByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, 60);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyID(String elementID)
    {
        try
        {
            waitForElementByID(elementID);
            WebDriverWait wait = new WebDriverWait(Driver, 60);
            wait.until(ExpectedConditions.elementToBeClickable(By.id(elementID)));
            WebElement elementToClick = Driver.findElement(By.id(elementID));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByXpath(String elementXPath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXPath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXPath));
            elementToTypeIn.sendKeys(textToEnter);

            return true;
        }
        catch (Exception e)
        {
            System.err.println("Error entering text - " + elementXPath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByID(String elementID, String textToEnter)
    {
        try
        {
            this.waitForElementByID(elementID);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementID));
            elementToTypeIn.sendKeys(textToEnter);

            return true;
        }
        catch (Exception e)
        {
            System.err.println("Error entering text - " + elementID + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpathWithTimeout(String elementXpath, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByID(String elementID)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementID))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by ID '" + elementID + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;

    }

    public boolean waitForElementByID(String elementID, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementID))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by ID '" + elementID + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;

    }

    public boolean waitForElementByIDWithTimeout(String elementID, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementID))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by ID '" + elementID + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpath(String elementXpath, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public void pause(int milisecondsToWait)
    {
        try
        {
            Thread.sleep(milisecondsToWait);
        }
        catch (Exception e)
        {

        }
    }

    public void takeScreenShot(String screenShotDescription, boolean isError)
    {
        String imageFilePathString = "";

        if (testCaseId == null)
        {
            return;
        }

        if (Driver == null)
        {
            return;
        }

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            imageFilePathBuilder.append(AppiumDriverUtility.reportDirectory + "\\");
            // + testCaseId + "\\"

            relativeScreenShotPath = testCaseId + "\\";

            if (isError)
            {
                imageFilePathBuilder.append("FAILED_");

            }
            else
            {
                imageFilePathBuilder.append("PASSED_");

            }

            relativeScreenShotPath += testCaseId + "_" + screenShotDescription + ".png";

            imageFilePathBuilder.append(relativeScreenShotPath);

            if (imageFilePathBuilder.toString().contains("FAILED_"))
            {
                relativeScreenShotPath = "FAILED_" + relativeScreenShotPath;
            }
            else if (imageFilePathBuilder.toString().contains("PASSED_"))
            {
                relativeScreenShotPath = "PASSED_" + relativeScreenShotPath;
            }

            //imageFilePathBuilder.append(this.generateDateTimeString() + ".png");
            imageFilePathString = imageFilePathBuilder.toString();

            setScreenshotPath(imageFilePathString);

            if (AppiumDriverInstance != null && AppiumDriverInstance.isDriverRunning())
            {

                File screenShot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));
            }
        }
        catch (WebDriverException | IOException e)
        {
            Narrator.logError("[Error] could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    public String generateDateTimeString()
    {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");

        return dateFormat.format(dateNow).toString();
    }

    public void CloseChromeInstances() throws IOException
    {
        if (browserType.equals(Enums.BrowserType.Chrome))
        {
            String TASKLIST = "tasklist";
            String KILL = "taskkill /IM ";
            String line;
            String serviceName = "chrome.exe";
            Process p = Runtime.getRuntime().exec(TASKLIST);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            Narrator.logDebug("Closing Chrome tasks...");
            while ((line = reader.readLine()) != null)
            {

                if (line.contains(serviceName))
                {
                    Runtime.getRuntime().exec(KILL + serviceName);
                }
            }
        }
    }

    public boolean TabAndEnterText(String textToEnter)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(Key.TAB, textToEnter);
            action.perform();
            return true;

        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to Tab " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public String retrieveTextByXpath(String xpath)
    {
        String retrievedText = "";
        try
        {
            waitForElementByXpath(xpath);
            WebElement elementToRead = Driver.findElement(By.xpath(xpath));
            retrievedText = elementToRead.getText();
            return retrievedText;
        }
        catch (Exception e)
        {
            System.err.println("Error reading text from element - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveTextByName(String xpath)
    {
        String retrievedText = "";
        try
        {
            waitForElementByXpath(xpath);
            WebElement elementToRead = Driver.findElement(By.xpath(xpath));
            retrievedText = elementToRead.getAttribute("name");
            return retrievedText;
        }
        catch (Exception e)
        {
            System.err.println("Error reading text from element - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveTextByID(String id)
    {
        String retrievedText = "";
        try
        {
            waitForElementByID(id);
            WebElement elementToRead = Driver.findElement(By.id(id));
            retrievedText = elementToRead.getText();
            return retrievedText;
        }
        catch (Exception e)
        {
            System.err.println("Error reading text from element - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public boolean RunActivity(String Package, String Activity)
    {
        try
        {
            Driver.startActivity(Package, Activity);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to run activity, error - " + e.getMessage());
            return false;
        }

    }

    public boolean SetOrientationPortrait()
    {
        try
        {
            ScreenOrientation orientation = Driver.getOrientation();
            if (orientation.value().equals(ScreenOrientation.PORTRAIT))
            {
                narrator.logDebug("Tried to change orientation to Portrait, but Orientation already was Portrait.");
                return true;
            }
            else
            {
                Driver.rotate(ScreenOrientation.PORTRAIT);
                narrator.logDebug("Screen orientation set to portrait.");
                return true;
            }
        }
        catch (Exception e)
        {
            narrator.logError("Failed to change screen orientation to Portrait.");
            return false;
        }
    }

    public boolean SetOrientationLandscape()
    {
        try
        {
            ScreenOrientation orientation = Driver.getOrientation();
            if (orientation.value().equals(ScreenOrientation.LANDSCAPE))
            {
                narrator.logDebug("Tried to change orientation to Landscape, but Orientation already was Landscape.");
                return true;
            }
            else
            {
                Driver.rotate(ScreenOrientation.PORTRAIT);
                narrator.logDebug("Screen orientation set to Landscape.");
                return true;
            }
        }
        catch (Exception e)
        {
            narrator.logError("Failed to change screen orientation to Landscape.");
            return false;
        }
    }

    public boolean scrollToElement(String element)
    {
        try
        {
            Driver.scrollTo(element);
            return true;
        }
        catch (Exception e)
        {
            narrator.logError("Failed to scroll to " + element + ", error - " + e.getMessage());
            return false;
        }
    }

    public boolean tapAtLocation(String x, String y)
    {
        try
        {
            Runtime rt = Runtime.getRuntime();
            rt.exec("cmd.exe /c adb shell input tap " + x + " " + y);
            return true;
        }
        catch (Exception e)
        {
            narrator.logError("Failed to tap at co-ords x: " + x + " y: " + y + " error - " + e.getMessage());
            return false;
        }
    }

    public boolean tapCoordinates(int x, int y)
    {
        try
        {
            AppiumDriverInstance.pause(1000);
            AppiumDriverInstance.Driver.tap(1, x, y, 250);
            System.out.println("");
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Error clicking element by coordinates - x = " + x + ", y = " + y);
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean tapAtLocationWithInt(int x, int y)
    {
        try
        {
            Runtime rt = Runtime.getRuntime();
            rt.exec("cmd.exe /c adb shell input tap " + x + " " + y);
            return true;
        }
        catch (Exception e)
        {
            narrator.logError("Failed to tap at co-ords x: " + x + " y: " + y + " error - " + e.getMessage());
            return false;
        }
    }

    public boolean tapOn(String element)
    {
        new TouchAction((MobileDriver) Driver).tap(Driver.findElement(By.xpath(element))).perform();

        return true;
    }

    public void stopAndroidServer()
    {
        retrievedTestValues = null;
        try
        {
            Runtime close = Runtime.getRuntime();
            Driver.quit();
            close.exec("taskkill /IM cmd.exe");

        }
        catch (Exception e)
        {
            Narrator.logError("Error shutting down driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        _isDriverRunning = false;
    }

    public boolean saveBarcodeASImage(String barcodeName)
    {
        String imageURL = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.GeneratedBarcode())).getAttribute("src");
        String destionationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcodeName + ".png";
        saveImageUsingURL(imageURL, destionationFile);
        return true;
    }

    public boolean saveImageUsingURL(String imageURL, String destionationFile)
    {
        try
        {
            LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
            DataColumn barcodeColumn = new DataColumn("", "", Enums.ResultStatus.UNCERTAIN);

            URL url = new URL(imageURL);
            System.setProperty("jsse.enableSNIExtension", "false");
            InputStream fis = url.openStream();
            OutputStream fos = new FileOutputStream(destionationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = fis.read(b)) != -1)
            {
                fos.write(b, 0, length);
            }

            fis.close();
            fos.close();

            barcodeColumn.columnValue = convertPNGToBase64(destionationFile);
            barcodeColumn.resultStatus = Enums.ResultStatus.UNCERTAIN;
            DataRow currentRow = new DataRow();
            currentRow.DataColumns.add(barcodeColumn);
            listOfBarcodes.add(currentRow);

        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    public String convertPNGToBase64(String imageFilePath)
    {
        String base64ReturnString = "";

        try
        {
            out.println("[INFO] Converting screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            out.println("[INFO] Converting completed, image ready for embedding.");
        }
        catch (Exception ex)
        {
            out.println("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }

    public boolean clearBarcodeFolder()
    {
        File index = new File(System.getProperty("user.dir") + "\\Barcodes");

        String[] entries = index.list();
        for (String s : entries)
        {
            File currentFile = new File(index.getPath(), s);
            currentFile.delete();
        }

        return true;
    }

    public boolean putStoreConfigMultipleContainers()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.multipleContainerStoreConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);
            narrator.stepPassed(line);

            if (!line.contains("MULTIPLE_CONTAINERS"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            System.out.println("Exception - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean putStoreConfigSingleContainer()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.singleContainerStoreConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);
            narrator.stepPassed(line);

            if (!line.contains("SINGLE_CONTAINER"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            System.out.println("Exception - " + e.getMessage());
            return false;
        }

        return true;
    }
}

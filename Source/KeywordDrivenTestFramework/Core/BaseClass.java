/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Core;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Reporting.ReportGenerator;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import KeywordDrivenTestFramework.Utilities.AutoItDriverUtility;
import KeywordDrivenTestFramework.Utilities.DataBaseUtility;
import KeywordDrivenTestFramework.Utilities.EmailUtility;
import KeywordDrivenTestFramework.Utilities.OtpSmsUtility;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 *
 * @author fnell
 */
// All tests inherit from base class, 
// Base class contains initialisations of all neccessary utilities and 
// entities
public class BaseClass
{

    public static List<TestEntity> testDataList;
    public static Enums.BrowserType browserType;
    public static ReportGenerator reportGenerator;

    public static SeleniumDriverUtility SeleniumDriverInstance;
    public static SikuliDriverUtility SikuliDriverInstance;
    public static DataBaseUtility DataBaseInstance;
    public static AutoItDriverUtility AutoItInstance;
    public static EmailUtility EmailUtilInstance;
    public static OtpSmsUtility otpSms;
    public static ApplicationConfig appConfig = new ApplicationConfig();
    private DateTime startTime, endTime;
    private Duration testDuration;
    public static String testCaseId;
    public static String reportDirectory;
    public static String currentTestDirectory;
    public static Enums.Environment currentEnvironment;

    //Appium-specific config
    public static Enums.Database currentDatabase;
    public static Enums.Device currentDevice;
    public static Enums.DeviceConfig currentDeviceConfig;
    public static Enums.MobilePlatform currentPlatform;
    public static AppiumDriverUtility AppiumDriverInstance;
    public static boolean requiresBrowser = true; //For appium set this false in the @Test
    public static boolean _isRemote = false; //For Remote Testing set to True in @Test
    //
    
    public static boolean copyToNetwork = false;

    public static String inputFilePath;
    public static String screenshotPath;
    public static String relativeScreenShotPath;
    public static TestEntity testData;

    public static Narrator narrator;

    public static String getRelativeScreenshotPath()
    {
        return "./" + relativeScreenShotPath;
    }

    public static void setScreenshotPath(String screenshotPath)
    {
        BaseClass.screenshotPath = screenshotPath;
    }

    public BaseClass()
    {
        System.setProperty("java.awt.headless", "false");
    }

    public void setStartTime()
    {
        this.startTime = new DateTime();
    }

    public long getTotalExecutionTime()
    {
        this.endTime = new DateTime();
        testDuration = new Duration(this.startTime, this.endTime);
        return testDuration.getStandardSeconds();
    }

    public String resolveScenarioName()
    {
        String isolatedFileNameString;
        String[] splitFileName;
        String[] inputFileNameArray;
        String resolvedScenarioName = "";

        // Get file name from inputFilePath (remove file extension)
        inputFileNameArray = inputFilePath.split("\\.");
        isolatedFileNameString = inputFileNameArray[0];
        if (isolatedFileNameString.contains("/"))
        {
            inputFileNameArray = isolatedFileNameString.split("/");
        }
        else if (isolatedFileNameString.contains("\\"))
        {
            inputFileNameArray = isolatedFileNameString.split("\\\\");
        }

        isolatedFileNameString = inputFileNameArray[inputFileNameArray.length - 1];

        splitFileName = isolatedFileNameString.split("(?=\\p{Upper})");

        for (String word : splitFileName)
        {
            resolvedScenarioName += word + " ";
        }

        return resolvedScenarioName.trim();
    }

    public String retrieveTestParameterUsingTestCaseId(String testCaseId, String parameterName)
    {
        String defaultReturn = "parameter not found";
        for (TestEntity testEntity : this.testDataList)
        {
            if (testEntity.TestCaseId.toUpperCase().equals(testCaseId.toUpperCase()))
            {
                if (testEntity.TestParameters.containsKey(parameterName))
                {
                    return testEntity.TestParameters.get(parameterName);
                }
                else
                {
                    return defaultReturn;
                }
            }
        }
        return defaultReturn;
    }

    public void pause(int milisecondsToWait)
    {
        try
        {
            Thread.sleep(milisecondsToWait);
        }
        catch (Exception e)
        {

        }
    }

    public String generateDateTimeString()
    {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");

        return dateFormat.format(dateNow).toString();
    }

}

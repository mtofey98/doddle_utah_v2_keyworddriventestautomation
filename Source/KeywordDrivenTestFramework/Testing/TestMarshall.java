
/* Changed made by James Joubert
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.DataBaseInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.browserType;
import static KeywordDrivenTestFramework.Core.BaseClass.inputFilePath;
import static KeywordDrivenTestFramework.Core.BaseClass.reportDirectory;
import static KeywordDrivenTestFramework.Core.BaseClass.reportGenerator;
import static KeywordDrivenTestFramework.Core.BaseClass.testCaseId;
import static KeywordDrivenTestFramework.Core.BaseClass.testDataList;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Reporting.ReportGenerator;
import KeywordDrivenTestFramework.Reporting.TestReportEmailerUtility;
import KeywordDrivenTestFramework.Testing.Mobile.CouchBaseValidator;
import KeywordDrivenTestFramework.Testing.Mobile.CouchBaseValidator_2;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_02_Login;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_03_Carrier;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_04_CarriersDispatch;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_05_Storage_AmazonParcels;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_06_Storage;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_07_Storage_StoreCheckProcess;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_08_UnknownParcel;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_09_RepeatingStorageProcess;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_10_ReturningAParcelToStorage_ParcelAtCollection;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_11_ReturningParcelToStorage_MissingParcel;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_12_Collection_Code;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_13_Collection_DoddleID;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_14_Collection_EmailAddress;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_15_Collection_UPI;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_16_Collection_OrderID;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_17_Collection_ConfirmingCustomerIdentity;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_18_ProcessingCustomerParcels;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_23_1_PreBookedReturn;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_23_Return_PreBookedReturn;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_28_Settings;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_29_Return_TestPrintout;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_30_Return_SingleContainer;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_31_1_MoveReturnItemToContainerWorkflow;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_31_Return_ExistingDispatchSack;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_32_Return_ItemTooLarge;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_33_Return_BoxIsFull;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_34_Return_NewDispatchSack;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_45_MultipleCollectionCode_NoCustomerBlock;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_46_MultipleCollectionCode_CustomerBlock;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_65;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_64_1;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_66_CollectionNoRetailerIDAndCustomerObject;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_67_ExistingCustomerAccount;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_52_Collection_Search_By_Code_With_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_53_Collection_Search_By_Email_With_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_54_Collection_Search_By_DoddleID_With_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_55_Collection_Search_By_DoddleID_With_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_56_Collection_Search_By_Email_With_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_57_Collection_Search_By_Email_With_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_58_Collection_Search_By_UPI_WithOut_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_59_Collection_Search_By_UPI_With_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_60_Collection_Search_By_Email_With_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_61_Collection_Search_By_OrderID_WithOut_Cust_Block;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_62;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_63;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_64;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_65_1;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_66_CollectionNoRetailerIDAndCustomerObject;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_67_ExistingCustomerAccount;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.CustomerRefundFlow;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.DoddleCheckIn;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.DoddleGenerateBarCode;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.DoddleLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.DoddleLoginGeneralFunctionality;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.DoddleScanningVolumeTest;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.MobileDoddleLogout;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.NewDoddleLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RFNDN1NonExistingDoddleID;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RFNDN2NonExistingEmail;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RFNDN3NonExistingUPI;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RTRNT10ReturnPreBookedReturn;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RTRNT11InvalidRetailerReturn;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RTRNT14CancelBooking;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RTRNT5WifiOff;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RTRNT6WifiOn;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RTRNT7MultipleItemReturnWifiOff;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.RTRNT7MultipleItemReturnWifiOn;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA19Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA21Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA22Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA23Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA24Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA25Payload_IntegratedCollectionWithPostCode;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA26Payload_IntegratedCollectionWithSignature;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA29Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA30Payload_IntegratedCollectionWithSignature;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA31Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA32Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA33Payload_IntegratedCollectionWithPostCode;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA34Payload_IntergratedCollection;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA35Payload_IntegratedCollectionWithPostCode;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA36Payload_IntegratedCollectionWithCollectionCode;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA38DataArtReturnsSingleContainerLater;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA38DataArtReturnsSingleContainerNow;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA38DataArtTestReturnFlowWifiOff;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA38DataArtTestReturnFlowWifiOn;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA39CorrectStoreLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA39DeviceScreenOffLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA39IncorrectEmailLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA39IncorrectPasswordLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA39IncorrectStoreLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA39WifiOffLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA46DataArtReturnsBoxIsFull;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA47DataArtReturnsItemTooLarge;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TCNextGen45MultipleCollections;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TCNextGen47RetakePhoto;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TCNextGen50ParcelAuthentication;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TC_NextGen_03_TestClass;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.BookAShipment;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.CustomerLoginGeneralFunctionality;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.DoddleUnlimitedAccountCreation;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.EditPersonalDetails;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.ForgottenPasswordFlow;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses.DoddleBarcodeSaveTest;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses.DoddleLogout;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses.DoddleNavigateToPage;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses.DoddleSignIn;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses.DoddleSignUp;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.ParcelTrackingFunctionality;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.PayAsYouDoddleAccountRegistration;
import KeywordDrivenTestFramework.Testing.Web.TestClasses.UpgradeFromPAYDToUnlimited;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.isCMDRunning;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import KeywordDrivenTestFramework.Utilities.AutoItDriverUtility;
import KeywordDrivenTestFramework.Utilities.CSVReportUtility;
import KeywordDrivenTestFramework.Utilities.DataBaseUtility;
import KeywordDrivenTestFramework.Utilities.DropBoxUploaderBeta;
import KeywordDrivenTestFramework.Utilities.EmailUtility;
import KeywordDrivenTestFramework.Utilities.ExcelReaderUtility;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import com.relevantcodes.extentreports.ExtentReports;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.zeroturnaround.zip.ZipUtil;

/**
 *
 * @author fnell
 * @editor jmacauley
 */

public class TestMarshall extends BaseClass
{

    // Handles calling test methods based on test parameters , instantiates Selenium Driver object   
    ExtentReports extentReports;
    ExcelReaderUtility excelInputReader;
    CSVReportUtility cSVReportUtility;
    TestReportEmailerUtility reportEmailer;
    //James removed(Moved to Narrator)
    //PrintStream errorOutputStream;
    //PrintStream infoOutputStream;
    private String dateTime;
    private int totalIgnore = 0;
    public static int count = 0;
    public static String status;
    public static String line;
    public static String configStatus;
    public static String config;
    public static String[] singleContainerConfigList, multipleContainerConfigList;
    MobileDoddleLogout log = new MobileDoddleLogout();
    String networkReportZip;

    public TestMarshall()
    {
        inputFilePath = ApplicationConfig.InputFileName();
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        EmailUtilInstance = new EmailUtility();
        DataBaseInstance = new DataBaseUtility();
        AutoItInstance = new AutoItDriverUtility();

        this.generateReportDirectory();

    }

    public TestMarshall(String inputFilePathIn)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        EmailUtilInstance = new EmailUtility();
        DataBaseInstance = new DataBaseUtility();
        AutoItInstance = new AutoItDriverUtility();

        this.generateReportDirectory();

    }

    public TestMarshall(String inputFilePathIn, Enums.BrowserType browserTypeOverride)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = browserTypeOverride;
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        EmailUtilInstance = new EmailUtility();
        DataBaseInstance = new DataBaseUtility();
        AutoItInstance = new AutoItDriverUtility();

        this.generateReportDirectory();

    }

    public TestMarshall(String inputFilePathIn, Enums.BrowserType browserTypeOverride, boolean isCopyToNetwork)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = browserTypeOverride;
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        EmailUtilInstance = new EmailUtility();
        DataBaseInstance = new DataBaseUtility();
        AutoItInstance = new AutoItDriverUtility();
        copyToNetwork = isCopyToNetwork;

        this.generateReportDirectory();

    }

    public void runKeywordDrivenTests()
    {

        int numberOfTest = 0;

        testDataList = loadTestData(inputFilePath);

        if (testDataList.size() < 1)
        {
            Narrator.logError("Test data object is empty - spreadsheet not found or is empty");
        }
        else
        {
            //CREATE NEW REPORT FILE
            reportGenerator.creatingNewTextFile();
            //James added // Steve edited
            if (requiresBrowser == true)
            {
                CheckBrowserExists();
            }
            Narrator.takeScreenShot(true, "init");
            // Each case represents a test keyword found in the excel spreadsheet

//            singleContainerConfigList = new String[]
//            {
//                "Single Container Later", "Existing Dispatch Sack", "Item Too Large", "Box Is Full"
//            };
//            multipleContainerConfigList = new String[]
//            {
//                "TC-NextGen-22 New Return"
//            };
            for (TestEntity testData : testDataList)
            {

                try
                {
                    checkConfigFromTextFile(testData);

                    numberOfTest++;
                    testCaseId = testData.TestCaseId;
                    // Make sure browser is not null - could have thrown an exception and terminated
                    //James added // Steve edited
                    if (requiresBrowser == true)
                    {
                        CheckBrowserExists();
                    }
                    // Skip test methods and test case id's starting with ';'
                    if (!testData.TestMethod.startsWith(";") && !testData.TestCaseId.startsWith(";"))
                    {
                        Narrator.logDebug("Executing test - " + testData.TestMethod + " | " + numberOfTest + " of " + testDataList.size());
                        switch (testData.TestMethod)
                        {

//==================================================Web========================================================================
                            case "DoddleNavigate":
                            {
                                ensureNewBrowserInstance();
                                DoddleNavigateToPage doddleNavigateToPage = new DoddleNavigateToPage(testData);
                                reportGenerator.addResult(doddleNavigateToPage.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "DoddleSignUp":
                            {
                                DoddleSignUp doddleSignUp = new DoddleSignUp(testData);
                                reportGenerator.addResult(doddleSignUp.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "DoddleSignIn":
                            {
                                DoddleSignIn doddleSignIn = new DoddleSignIn(testData);
                                reportGenerator.addResult(doddleSignIn.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "DoddleLogout":
                            {
                                DoddleLogout doddleLogout = new DoddleLogout(testData);
                                reportGenerator.addResult(doddleLogout.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "EditPersonalDetails":
                            {
                                EditPersonalDetails editPersonalDetails = new EditPersonalDetails(testData);
                                reportGenerator.addResult(editPersonalDetails.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "CustomerLoginGeneralFunctionality":
                            {
                                CustomerLoginGeneralFunctionality customerLoginGeneralFunctionality = new CustomerLoginGeneralFunctionality(testData);
                                reportGenerator.addResult(customerLoginGeneralFunctionality.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "ForgottenPasswordFlow":
                            {
                                ForgottenPasswordFlow forgottenPasswordFlow = new ForgottenPasswordFlow(testData);
                                reportGenerator.addResult(forgottenPasswordFlow.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "PayAsYouDoddleAccountRegistration":
                            {
                                PayAsYouDoddleAccountRegistration payAsYouDoddleAccountRegistration = new PayAsYouDoddleAccountRegistration(testData);
                                reportGenerator.addResult(payAsYouDoddleAccountRegistration.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "ParcelTrackingFunctionality":
                            {
                                ParcelTrackingFunctionality parcelTrackingFunctionality = new ParcelTrackingFunctionality(testData);
                                reportGenerator.addResult(parcelTrackingFunctionality.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "DoddleUnlimitedAccountCreation":
                            {
                                DoddleUnlimitedAccountCreation doddleUnlimitedAccountCreation = new DoddleUnlimitedAccountCreation(testData);
                                reportGenerator.addResult(doddleUnlimitedAccountCreation.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "UpgradeFromPAYDToUnlimited":
                            {
                                UpgradeFromPAYDToUnlimited upgradeFromPAYDToUnlimited = new UpgradeFromPAYDToUnlimited(testData);
                                reportGenerator.addResult(upgradeFromPAYDToUnlimited.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "BookAShipment":
                            {
                                BookAShipment bookAShipment = new BookAShipment(testData);
                                reportGenerator.addResult(bookAShipment.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "GenerateBarcode":
                            {
                                ensureNewBrowserInstance();
                                DoddleGenerateBarCode doddleGenerateBarCode = new DoddleGenerateBarCode(testData);
                                reportGenerator.addResult(doddleGenerateBarCode.executeTest());
                                numberOfTest++;
                                break;

                            }

                            case "SaveBarcode":
                            {
                                DoddleBarcodeSaveTest save = new DoddleBarcodeSaveTest(testData);
                                reportGenerator.addResult(save.executeTest());
                                numberOfTest++;
                                break;
                            }
//======================================================================Mobile================================================================================

                            case "Login":
                            {
                                stopAppiumServerIfExists();
                                DoddleLogin doddleLogin = new DoddleLogin(testData);
                                reportGenerator.addResult(doddleLogin.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA_39_Login":
                            {
                                //stopAppiumServerIfExists();
                                NewDoddleLogin newDoddleLogin = new NewDoddleLogin(testData);
                                reportGenerator.addResult(newDoddleLogin.executeTest());
                                numberOfTest++;
                                break;

                            }

                            case "Logout":
                            {
                                MobileDoddleLogout mobileDoddleLogout = new MobileDoddleLogout(testData);
                                reportGenerator.addResult(mobileDoddleLogout.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "DoddleLoginGeneralFunctionality":
                            {
                                DoddleLoginGeneralFunctionality doddleLoginGeneralFunctionality = new DoddleLoginGeneralFunctionality(testData);
                                reportGenerator.addResult(doddleLoginGeneralFunctionality.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "Checkin":
                            {
                                DoddleCheckIn doddleCheckIn = new DoddleCheckIn(testData);
                                reportGenerator.addResult(doddleCheckIn.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "VolumeTest":
                            {
                                DoddleScanningVolumeTest doddleScanningVolumeTest = new DoddleScanningVolumeTest(testData);
                                reportGenerator.addResult(doddleScanningVolumeTest.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "TA19Payload_IntergratedCollection":
                            {
                                TA19Payload_IntergratedCollection aSOSIntergratedCollection = new TA19Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA21Payload_IntergratedCollection":
                            {
                                TA21Payload_IntergratedCollection aSOSIntergratedCollection = new TA21Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA22Payload_IntergratedCollection":
                            {
                                TA22Payload_IntergratedCollection aSOSIntergratedCollection = new TA22Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA23Payload_IntergratedCollection":
                            {
                                TA23Payload_IntergratedCollection aSOSIntergratedCollection = new TA23Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA29Payload_IntergratedCollection":
                            {
                                TA29Payload_IntergratedCollection aSOSIntergratedCollection = new TA29Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA24Payload_IntergratedCollection":
                            {
                                TA24Payload_IntergratedCollection aSOSIntergratedCollection = new TA24Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA31Payload_IntergratedCollection":
                            {
                                TA31Payload_IntergratedCollection aSOSIntergratedCollection = new TA31Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA32Payload_IntergratedCollection":
                            {
                                TA32Payload_IntergratedCollection aSOSIntergratedCollection = new TA32Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA34Payload_IntergratedCollection":
                            {
                                TA34Payload_IntergratedCollection aSOSIntergratedCollection = new TA34Payload_IntergratedCollection(testData);
                                reportGenerator.addResult(aSOSIntergratedCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA26Payload_IntergratedCollectionWithSignature":
                            {

                                TA26Payload_IntegratedCollectionWithSignature payload_IntegratedCollectionWithSignature = new TA26Payload_IntegratedCollectionWithSignature(testData);
                                reportGenerator.addResult(payload_IntegratedCollectionWithSignature.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA30Payload_IntergratedCollectionWithSignature":
                            {

                                TA30Payload_IntegratedCollectionWithSignature payload_IntegratedCollectionWithSignature = new TA30Payload_IntegratedCollectionWithSignature(testData);
                                reportGenerator.addResult(payload_IntegratedCollectionWithSignature.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA25Payload_IntergratedCollectionWithPostCode":
                            {

                                TA25Payload_IntegratedCollectionWithPostCode payload_IntegratedCollectionWithPostCode = new TA25Payload_IntegratedCollectionWithPostCode(testData);
                                reportGenerator.addResult(payload_IntegratedCollectionWithPostCode.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA33Payload_IntergratedCollectionWithPostCode":
                            {

                                TA33Payload_IntegratedCollectionWithPostCode payload_IntegratedCollectionWithPostCode = new TA33Payload_IntegratedCollectionWithPostCode(testData);
                                reportGenerator.addResult(payload_IntegratedCollectionWithPostCode.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA35Payload_IntergratedCollectionWithPostCode":
                            {

                                TA35Payload_IntegratedCollectionWithPostCode payload_IntegratedCollectionWithPostCode = new TA35Payload_IntegratedCollectionWithPostCode(testData);
                                reportGenerator.addResult(payload_IntegratedCollectionWithPostCode.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TA36Payload_IntergratedCollectionWithCollectionCode":
                            {

                                TA36Payload_IntegratedCollectionWithCollectionCode cUSTOMERCOLLECTIONCODE_TESTCLASS = new TA36Payload_IntegratedCollectionWithCollectionCode(testData);
                                reportGenerator.addResult(cUSTOMERCOLLECTIONCODE_TESTCLASS.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TCNextGen45MultipleCollections":
                            {
                                TCNextGen45MultipleCollections nextgen = new TCNextGen45MultipleCollections(testData);
                                reportGenerator.addResult(nextgen.executeTest());
                                numberOfTest++;
                                count++;
                                break;
                            }

                            case "IncorrectStoreLogin":
                            {
                                TA39IncorrectStoreLogin incorrect = new TA39IncorrectStoreLogin(testData);
                                reportGenerator.addResult(incorrect.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "CorrectStoreLogin":
                            {
                                stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                AppiumDriverInstance._isDriverRunning = false;
                                TA39CorrectStoreLogin correct = new TA39CorrectStoreLogin(testData);
                                reportGenerator.addResult(correct.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "IncorrectEmailLogin":
                            {
                                TA39IncorrectEmailLogin email = new TA39IncorrectEmailLogin(testData);
                                reportGenerator.addResult(email.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "IncorrectPassword":
                            {
                                TA39IncorrectPasswordLogin password = new TA39IncorrectPasswordLogin(testData);
                                reportGenerator.addResult(password.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "WifiOffLogin":
                            {
                                TA39WifiOffLogin wifi = new TA39WifiOffLogin(testData);
                                reportGenerator.addResult(wifi.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "DeviceScreenOff":
                            {
                                TA39DeviceScreenOffLogin screenOff = new TA39DeviceScreenOffLogin(testData);
                                reportGenerator.addResult(screenOff.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "DataArtTestReturnFlowWifiOff":
                            {
                                TA38DataArtTestReturnFlowWifiOff dataArtTestWifiOff = new TA38DataArtTestReturnFlowWifiOff(testData);
                                reportGenerator.addResult(dataArtTestWifiOff.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "DataArtTestReturnFlowWifiOn":
                            {
                                stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                AppiumDriverInstance._isDriverRunning = false;
                                TA38DataArtTestReturnFlowWifiOn dataArtTestWifiOn = new TA38DataArtTestReturnFlowWifiOn(testData);
                                reportGenerator.addResult(dataArtTestWifiOn.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "DataArtReturnsSingleContainerNow":
                            {
                                stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                AppiumDriverInstance._isDriverRunning = false;
                                
                                TA38DataArtReturnsSingleContainerNow dataArtTest = new TA38DataArtReturnsSingleContainerNow(testData);
                                reportGenerator.addResult(dataArtTest.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "DataArtReturnsSingleContainerLater":
                            {
                                stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                Shortcuts.putStoreSystemUIConfigSingleContainer();
                                TA38DataArtReturnsSingleContainerLater dataArtTest = new TA38DataArtReturnsSingleContainerLater(testData);
                                reportGenerator.addResult(dataArtTest.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "DataArtReturnsBoxIsFull":
                            {
                                TA46DataArtReturnsBoxIsFull fullBox = new TA46DataArtReturnsBoxIsFull(testData);
                                reportGenerator.addResult(fullBox.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "DataArtReturnsItemTooLarge":
                            {
                                TA47DataArtReturnsItemTooLarge tooLarge = new TA47DataArtReturnsItemTooLarge(testData);
                                reportGenerator.addResult(tooLarge.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "CustomerRefundFlow":
                            {
                                CustomerRefundFlow refund = new CustomerRefundFlow(testData);
                                reportGenerator.addResult(refund.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RTRN_T5_WifiOff":
                            {
                                RTRNT5WifiOff wifiOff = new RTRNT5WifiOff(testData);
                                reportGenerator.addResult(wifiOff.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RTRN_T6_WifiOn":
                            {
                                RTRNT6WifiOn wifiOn = new RTRNT6WifiOn(testData);
                                reportGenerator.addResult(wifiOn.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RTRN_T7_MultipleItemReturnWifiOn":
                            {
                                RTRNT7MultipleItemReturnWifiOn multiple = new RTRNT7MultipleItemReturnWifiOn(testData);
                                reportGenerator.addResult(multiple.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RTRN_T7_MultipleItemReturnWifiOff":
                            {
                                RTRNT7MultipleItemReturnWifiOff multiple = new RTRNT7MultipleItemReturnWifiOff(testData);
                                reportGenerator.addResult(multiple.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RTRN_T11_InvalidRetialerReturn":
                            {
                                RTRNT11InvalidRetailerReturn invalid = new RTRNT11InvalidRetailerReturn(testData);
                                reportGenerator.addResult(invalid.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RTRN_T14_CancelBooking":
                            {
                                RTRNT14CancelBooking cancel = new RTRNT14CancelBooking(testData);
                                reportGenerator.addResult(cancel.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RTRN_T10_ReturnPreBookedReturn":
                            {
                                RTRNT10ReturnPreBookedReturn prebooked = new RTRNT10ReturnPreBookedReturn(testData);
                                reportGenerator.addResult(prebooked.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RFND_N1_NonExistingDoddleID":
                            {
                                RFNDN1NonExistingDoddleID doddleID = new RFNDN1NonExistingDoddleID(testData);
                                reportGenerator.addResult(doddleID.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RFND_N2_NonExistingEmail":
                            {
                                RFNDN2NonExistingEmail email = new RFNDN2NonExistingEmail(testData);
                                reportGenerator.addResult(email.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RFND_N3_NonExistingUPI":
                            {
                                RFNDN3NonExistingUPI upi = new RFNDN3NonExistingUPI(testData);
                                reportGenerator.addResult(upi.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "Couch_Base_Validator":
                            {
                                CouchBaseValidator Couch_Base_Validator = new CouchBaseValidator(testData);
                                reportGenerator.addResult(Couch_Base_Validator.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "Couch_Base_Validator_2":
                            {
                                CouchBaseValidator_2 Couch_Base_Validator_2 = new CouchBaseValidator_2(testData);
                                reportGenerator.addResult(Couch_Base_Validator_2.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC-NextGen-03":
                            {
                                TC_NextGen_03_TestClass TC_NextGen_03_TestClass = new TC_NextGen_03_TestClass(testData);
                                reportGenerator.addResult(TC_NextGen_03_TestClass.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "RetakePhoto":
                            {
                                TCNextGen47RetakePhoto retake = new TCNextGen47RetakePhoto(testData);
                                reportGenerator.addResult(retake.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "ParcelAuthentication":
                            {
                                TCNextGen50ParcelAuthentication parcel = new TCNextGen50ParcelAuthentication(testData);
                                reportGenerator.addResult(parcel.executeTest());
                                reportGenerator.addResult(parcel.executeTest());
                                numberOfTest++;
                                break;
                            }

                            //===================================New Mobile Test Classes==============================================================
                            case "TC_NextGen_06":
                            {
                                Shortcuts.putPreadvisedSkipPhotoConfigFalse();
                                stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                AppiumDriverInstance._isDriverRunning = false;
                                TC_NextGen_06_Storage storage = new TC_NextGen_06_Storage(testData);
                                reportGenerator.addResult(storage.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "StoreCheckProcess":
                            {

                                TC_NextGen_07_Storage_StoreCheckProcess storage = new TC_NextGen_07_Storage_StoreCheckProcess(testData);
                                reportGenerator.addResult(storage.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_09":
                            {
                                TC_NextGen_09_RepeatingStorageProcess repeatingStorage = new TC_NextGen_09_RepeatingStorageProcess(testData);
                                reportGenerator.addResult(repeatingStorage.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_10":
                            {
                                TC_NextGen_10_ReturningAParcelToStorage_ParcelAtCollection returnParcelToStorage = new TC_NextGen_10_ReturningAParcelToStorage_ParcelAtCollection(testData);
                                reportGenerator.addResult(returnParcelToStorage.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_11":
                            {
                                TC_NextGen_11_ReturningParcelToStorage_MissingParcel returnMissingParcelToStorage = new TC_NextGen_11_ReturningParcelToStorage_MissingParcel(testData);
                                reportGenerator.addResult(returnMissingParcelToStorage.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_17":
                            {
                                TC_NextGen_17_Collection_ConfirmingCustomerIdentity confirmCustomerID = new TC_NextGen_17_Collection_ConfirmingCustomerIdentity(testData);
                                reportGenerator.addResult(confirmCustomerID.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_18":
                            {
                                TC_NextGen_18_ProcessingCustomerParcels proccessCustomerPacrcels = new TC_NextGen_18_ProcessingCustomerParcels(testData);
                                reportGenerator.addResult(proccessCustomerPacrcels.executeTest());
                                numberOfTest++;
                                break;
                            }

                            //Added By Hyran
                            case "TC_NextGen_02_Login":
                            {
                                
                                stopAppiumServerIfExists();
                                TC_NextGen_02_Login carrier = new TC_NextGen_02_Login(testData);
                                reportGenerator.addResult(carrier.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "TC_NextGen_03_Carrier":
                            {
                                //stopAppiumServerIfExists();
                                TC_NextGen_03_Carrier carrier = new TC_NextGen_03_Carrier(testData);
                                reportGenerator.addResult(carrier.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "TC_NextGen_04_CarrierDispatch":
                            {
                                //stopAppiumServerIfExists();
                                TC_NextGen_04_CarriersDispatch carrier2 = new TC_NextGen_04_CarriersDispatch(testData);
                                reportGenerator.addResult(carrier2.executeTest());
                                numberOfTest++;
                                break;
                            }
                             case "TC_NextGen_62":
                            {
                                Shortcuts.putPreadvisedSkipPhotoConfigTrue();
                                //stopAppiumServerIfExists();
                                TC_NextGen_62 cr = new TC_NextGen_62(testData);
                                reportGenerator.addResult(cr.executeTest());
                                numberOfTest++;
                                break;
                            }
                              case "TC_NextGen_63":
                            {
                                //stopAppiumServerIfExists();
                                TC_NextGen_63 cr3 = new TC_NextGen_63(testData);
                                reportGenerator.addResult(cr3.executeTest());
                                numberOfTest++;
                                break;
                            }
                            //Added By Jody
                            case "AmazonParcels":
                            {
                                
                                TC_NextGen_05_Storage_AmazonParcels amazoness = new TC_NextGen_05_Storage_AmazonParcels(testData);
                                reportGenerator.addResult(amazoness.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_28_Settings":
                            {
                                //stopAppiumServerIfExists();
                                TC_NextGen_28_Settings settings = new TC_NextGen_28_Settings(testData);
                                reportGenerator.addResult(settings.executeTest());
                                numberOfTest++;
                                break;
                            }
                             case "TC_NextGen_23_1_PreBookedReturn":
                            {
                                 Shortcuts.putStoreSystemUIConfigSingleContainer();
                               // stopAppiumServerIfExists();
                               // AppiumDriverInstance.killChromeInstance();
                                //isCMDRunning = false;
                               // AppiumDriverInstance._isDriverRunning = false;
                                TC_NextGen_23_1_PreBookedReturn preBook23_1 = new TC_NextGen_23_1_PreBookedReturn(testData);
                                reportGenerator.addResult(preBook23_1.executeTest());
                                numberOfTest++;
                                break;
                            }
                            case "TestPrintout":
                            {
                                TC_NextGen_29_Return_TestPrintout testPrintout = new TC_NextGen_29_Return_TestPrintout(testData);
                                reportGenerator.addResult(testPrintout.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "Return":
                            {
                                Shortcuts.putStoreSystemUIConfigMultipleContainers();
                                TC_NextGen_22_Return TC_NextGen_22_Return = new TC_NextGen_22_Return(testData);
                                reportGenerator.addResult(TC_NextGen_22_Return.executeTest());
                                SeleniumDriverInstance.shutDown();
                                numberOfTest++;
                                break;
                            }

                            case "PreBookedReturn":
                            {
                                Shortcuts.putStoreSystemUIConfigMultipleContainers();
                                TC_NextGen_23_Return_PreBookedReturn Return = new TC_NextGen_23_Return_PreBookedReturn(testData);
                                reportGenerator.addResult(Return.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_12_Code":
                            {
                                TC_NextGen_12_Collection_Code codeCollection = new TC_NextGen_12_Collection_Code(testData);
                                reportGenerator.addResult(codeCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_13_DoddleID":
                            {
                                TC_NextGen_13_Collection_DoddleID doddleIDCollection = new TC_NextGen_13_Collection_DoddleID(testData);
                                reportGenerator.addResult(doddleIDCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_14_EmailAddress":
                            {
                                TC_NextGen_14_Collection_EmailAddress emailCollection = new TC_NextGen_14_Collection_EmailAddress(testData);
                                reportGenerator.addResult(emailCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_15_UPI":
                            {
                                TC_NextGen_15_Collection_UPI upiCollection = new TC_NextGen_15_Collection_UPI(testData);
                                reportGenerator.addResult(upiCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_16_OrderID":
                            {
                                TC_NextGen_16_Collection_OrderID orderIDCollection = new TC_NextGen_16_Collection_OrderID(testData);
                                reportGenerator.addResult(orderIDCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "SingleContainer":
                            {
                                TC_NextGen_30_Return_SingleContainer singleContainer = new TC_NextGen_30_Return_SingleContainer(testData);
                                reportGenerator.addResult(singleContainer.executeTest());
                                SeleniumDriverInstance.shutDown();
                                numberOfTest++;
                                break;
                            }

                            case "ExistingDispatchSack":
                            {
                                TC_NextGen_31_Return_ExistingDispatchSack existing = new TC_NextGen_31_Return_ExistingDispatchSack(testData);
                                reportGenerator.addResult(existing.executeTest());
                                SeleniumDriverInstance.shutDown();
                                numberOfTest++;
                                break;

                            }

                            case "ItemTooLarge":
                            {
                                Shortcuts.putStoreSystemUIConfigSingleContainer();
                                TC_NextGen_32_Return_ItemTooLarge tooLarge = new TC_NextGen_32_Return_ItemTooLarge(testData);
                                reportGenerator.addResult(tooLarge.executeTest());
                                SeleniumDriverInstance.shutDown();
                                numberOfTest++;
                                break;
                            }

                            case "BoxIsFull":
                            {   
                                Shortcuts.putStoreSystemUIConfigSingleContainer();
                                TC_NextGen_33_Return_BoxIsFull fullBox = new TC_NextGen_33_Return_BoxIsFull(testData);
                                reportGenerator.addResult(fullBox.executeTest());
                                SeleniumDriverInstance.shutDown();
                                numberOfTest++;
                                break;
                            }

                            case "NewDispatchSack":
                            {
                                Shortcuts.putStoreSystemUIConfigSingleContainer();
                                TC_NextGen_34_Return_NewDispatchSack newSack = new TC_NextGen_34_Return_NewDispatchSack(testData);
                                reportGenerator.addResult(newSack.executeTest());
                                SeleniumDriverInstance.shutDown();
                                numberOfTest++;
                                break;
                            }

                            case "TC45_MultipleCollectionsAmazonCode":
                            {
                                TC_NextGen_45_MultipleCollectionCode_NoCustomerBlock amazonCodeMultipleCollection = new TC_NextGen_45_MultipleCollectionCode_NoCustomerBlock(testData);
                                reportGenerator.addResult(amazonCodeMultipleCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC46_MultipleCollectionsCode":
                            {
                                TC_NextGen_46_MultipleCollectionCode_CustomerBlock codeMultipleCollection = new TC_NextGen_46_MultipleCollectionCode_CustomerBlock(testData);
                                reportGenerator.addResult(codeMultipleCollection.executeTest());
                                numberOfTest++;
                                break;
                            }

                             case "TC_NextGen_65":
                            {
                                Shortcuts.putPreadvisedSkipPhotoConfigTrue();
                                stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                AppiumDriverInstance._isDriverRunning = false;
                                TC_NextGen_65 TC65 = new TC_NextGen_65(testData);
                                 reportGenerator.addResult(TC65.executeTest());
                                 numberOfTest++;
                                break;
                            }
                              case "TC_NextGen_65_1":
                            {
                                Shortcuts.putPreadvisedSkipPhotoConfigFalse();
                                stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                AppiumDriverInstance._isDriverRunning = false;
                                TC_NextGen_65_1 TC651 = new TC_NextGen_65_1(testData);
                                 reportGenerator.addResult(TC651.executeTest());
                                 numberOfTest++;
                                break;
                            }
                             case "TC_NextGen_64_1":
                            {
                                Shortcuts.putPreadvisedSkipPhotoConfigTrue();
                                 stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                AppiumDriverInstance._isDriverRunning = false;
                                TC_NextGen_64_1 TC641 = new TC_NextGen_64_1(testData);
                                 reportGenerator.addResult(TC641.executeTest());
                                 numberOfTest++;
                                break;
                            }
                                case "TC_NextGen_64":
                            {
                                Shortcuts.putPreadvisedSkipPhotoConfigTrue();
                                 stopAppiumServerIfExists();
                                AppiumDriverInstance.killChromeInstance();
                                isCMDRunning = false;
                                AppiumDriverInstance._isDriverRunning = false;
                                TC_NextGen_64 TC64 = new TC_NextGen_64(testData);
                                 reportGenerator.addResult(TC64.executeTest());
                                 numberOfTest++;
                                break;
                            }
                            /*
                                @mtofey added the following
                            */
                            case "TC_NextGen_51":
                            {
                                TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            

                            case "TC66_NoCustomerObjectAndRetialerID":
                            {
                                TC_NextGen_66_CollectionNoRetailerIDAndCustomerObject TC_NextGen_66_CollectionNoRetailerIDAndCustomerObject = new TC_NextGen_66_CollectionNoRetailerIDAndCustomerObject(testData);
                                reportGenerator.addResult(TC_NextGen_66_CollectionNoRetailerIDAndCustomerObject.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_52":
                            {
                                TC_NextGen_52_Collection_Search_By_Code_With_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_52_Collection_Search_By_Code_With_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            

                            case "TC67_ExistingCustomerAccount":
                            {
                                TC_NextGen_67_ExistingCustomerAccount TC_NextGen_67_ExistingCustomerAccount = new TC_NextGen_67_ExistingCustomerAccount(testData);
                                reportGenerator.addResult(TC_NextGen_67_ExistingCustomerAccount.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_53":
                            {
                                TC_NextGen_53_Collection_Search_By_Email_With_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_53_Collection_Search_By_Email_With_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            
                            case "TC_NextGen_54":
                            {
                                TC_NextGen_54_Collection_Search_By_DoddleID_With_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_54_Collection_Search_By_DoddleID_With_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            
                            case "TC_NextGen_55":
                            {
                                TC_NextGen_55_Collection_Search_By_DoddleID_With_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_55_Collection_Search_By_DoddleID_With_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            
                            case "TC_NextGen_56":
                            {
                                TC_NextGen_56_Collection_Search_By_Email_With_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_56_Collection_Search_By_Email_With_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            
                            case "TC_NextGen_57":
                            {
                                TC_NextGen_57_Collection_Search_By_Email_With_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_57_Collection_Search_By_Email_With_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            
                            case "TC_NextGen_58":
                            {
                                TC_NextGen_58_Collection_Search_By_UPI_WithOut_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_58_Collection_Search_By_UPI_WithOut_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            
                            case "TC_NextGen_59":
                            {
                                TC_NextGen_59_Collection_Search_By_UPI_With_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_59_Collection_Search_By_UPI_With_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }

                            case "TC_NextGen_60":
                            {
                                TC_NextGen_60_Collection_Search_By_Email_With_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_60_Collection_Search_By_Email_With_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }
                            
                            case "TC_NextGen_61":
                            {
                                TC_NextGen_61_Collection_Search_By_OrderID_WithOut_Cust_Block TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block = new TC_NextGen_61_Collection_Search_By_OrderID_WithOut_Cust_Block(testData);
                                reportGenerator.addResult(TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block.executeTest());
                                numberOfTest++;
                                break;
                            }

                            
                            case "TC_NextGen_08_UnknownParcel":
                            {
                                TC_NextGen_08_UnknownParcel TC_NextGen_08_UnknownParcel = new TC_NextGen_08_UnknownParcel(testData);
                                reportGenerator.addResult(TC_NextGen_08_UnknownParcel.executeTest());
                                numberOfTest++;
                                break;
                            }
                            
                            case "TC_NextGen_31_1_MoveReturnItemsToContainer":
                            {
                                TC_NextGen_31_1_MoveReturnItemToContainerWorkflow TC_NextGen_31_1_MoveReturnItemToContainerWorkflow = new  TC_NextGen_31_1_MoveReturnItemToContainerWorkflow(testData);
                                reportGenerator.addResult(TC_NextGen_31_1_MoveReturnItemToContainerWorkflow.executeTest());
                                numberOfTest++;
                                break;
                            }

                        }

                        if (reportGenerator.currentResult.testStatus == Enums.ResultStatus.FAIL)
                        {                           
                            log.Logout();
                        }

                        reportTextFile(numberOfTest);
                        Narrator.logDebug("Continuing to next test method");
                    }

                    //James added
                    if (testData.TestMethod.startsWith(";") || testData.TestCaseId.startsWith(";"))
                    {
                        totalIgnore++;
                        reportGenerator.writeToFile4(numberOfTest, testDataList.size(), totalIgnore);

                    }

                }
                catch (Exception | Error e)
                {
                    e.printStackTrace();
                    System.out.println("[Error] An Exception was thrown in the TestMarshall - " + e.getMessage());
                }
            }

            if (SeleniumDriverInstance != null && SeleniumDriverInstance.isDriverRunning())
            {
                SeleniumDriverInstance.shutDown();
            }
            if (AppiumDriverInstance != null && AppiumDriverInstance.isDriverRunning())
            {
                AppiumDriverInstance.shutDown();
            }

            reportGenerator.generateTestReport();
            reportEmailer = new TestReportEmailerUtility(reportGenerator.testResults);
            if(copyToNetwork)
                copyReportsDirectoryToNetworkDrive();
            reportEmailer.SendResultsEmail();
            saveReportDirectoryName();
//            DropBoxUploaderBeta.zipAndUploadReportsToDropBox();

            //James removed(Moved to Narrator)
            //this.flushOutputStreams();
            //James added
            //Narrator.finishFile();
        }
    }

    private List<TestEntity> loadTestData(String inputFilePath)
    {
        return excelInputReader.getTestDataFromExcelFile(inputFilePath);
    }

    public static void CheckBrowserExists()
    {
        if (SeleniumDriverInstance == null)
        {
            SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
            SeleniumDriverInstance.startDriver();
        }

        if (!SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.startDriver();
        }
    }

    public static void ensureNewBrowserInstance()
    {
        if (SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.shutDown();
        }
        SeleniumDriverInstance.startDriver();
    }

    public String generateDateTimeString()
    {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
        dateTime = dateFormat.format(dateNow).toString();
        return dateTime;
    }

    public void generateReportDirectory()
    {
        reportDirectory = ApplicationConfig.ReportFileDirectory() + resolveScenarioName() + "_" + this.generateDateTimeString();
        String[] reportsFolderPathSplit = this.reportDirectory.split("\\\\");
        this.currentTestDirectory = ApplicationConfig.ReportFileDirectory() + "\\" + reportsFolderPathSplit[reportsFolderPathSplit.length - 1];
        this.networkReportZip = reportsFolderPathSplit[reportsFolderPathSplit.length-1]+".zip";
    }
    
    public void copyReportsDirectoryToNetworkDrive()
    {
        String rundate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String networkFolderPath = ApplicationConfig.networkReportDirectory()+ new SimpleDateFormat("MMM").format(Calendar.getInstance().getTime())+" "+rundate.substring(0,4) + "\\"+ rundate + "\\";
        
        System.out.println("");
        try
        {
            File networkReportFolder = new File(networkFolderPath);
            networkReportFolder.mkdirs();
            
            ZipUtil.pack(new File(this.reportDirectory), new File(networkFolderPath+this.networkReportZip));
            
            System.out.println("Report folder zipped and copied to the network folder with file path - "+ networkFolderPath + this.networkReportZip);
        }
        catch(Exception | Error e)
        {
            System.err.println("Unable to copy the report directory to the network path '"+ networkFolderPath +"', fault - "+e.getMessage());
        }
    }

    public void saveReportDirectoryName()
    {
        try
        {
            FileUtils.writeStringToFile(new File(System.getProperty("User.dir") + "ReportList.txt"), reportDirectory);
        }
        catch (Exception e)
        {
            System.out.println("Unable to save file");
        }
    }

    public void reportTextFile(int numberOfTests)
    {
        reportGenerator.writeToFile4(numberOfTests, testDataList.size(), totalIgnore);
    }

    public static void stopAppiumServerIfExists()
    {
        if (AppiumDriverInstance != null && AppiumDriverInstance.isDriverRunning())
        {
            switch (currentPlatform)
            {
                case Android:
                {
                    AppiumDriverInstance.stopAndroidServer();
                    _isRemote = false;
                    AppiumDriverUtility.isCMDRunning = false;
                    try
                    {
                        Thread.sleep(2000);
                    }
                    catch (Exception e)
                    {
                        Narrator.logError("[ERROR] - " + e.getMessage());
                    }
                    break;
                }
            }
        }
        if (SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.shutDown();
        }
    }

    public static void writeConfigToTextFile(String config)
    {
        String fileName = "ContainerConfig.txt";

        try
        {
            FileWriter fw = new FileWriter(fileName);

            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(config);
            bw.close();
            System.out.println("Wrote " + config + " to file");
        }
        catch (IOException e)
        {
            System.out.println("Error writing to file '" + fileName + "'");
        }
    }

    public static void checkConfigFromTextFile(TestEntity dataTest)
    {
        String fileName = "ContainerConfig.txt";
        line = null;

        try
        {
            FileReader fr = new FileReader(fileName);

            BufferedReader br = new BufferedReader(fr);

            while ((line = br.readLine()) != null)
            {
                System.out.println(line);
                configStatus = line;
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("unable to file '" + fileName + "'");

        }
        catch (IOException e)
        {
            System.out.println("Error reading file '" + fileName + "'");

        }

        config = dataTest.getData("containerConfigRequired");

        switch (config)
        {
            case "SINGLE_CONTAINER":
            {
                if (!configStatus.contains("SINGLE_CONTAINER"))
                {
                    Shortcuts.putStoreSystemUIConfigSingleContainer();
                    writeConfigToTextFile("SINGLE_CONTAINER");
                    break;
                }

                break;
            }

            case "MULTIPLE_CONTAINERS":
            {

                if (!configStatus.contains("MULTIPLE_CONTAINERS"))
                {
                    Shortcuts.putStoreSystemUIConfigMultipleContainers();
                    writeConfigToTextFile("MULTIPLE_CONTAINERS");
                    break;
                }

                break;
            }
            
            case "":
            {
                System.out.println("Config update not required");
                break;
            }

            default:
            {
                System.out.println("Could'nt update container config");
                break;
            }
        }

    }
}

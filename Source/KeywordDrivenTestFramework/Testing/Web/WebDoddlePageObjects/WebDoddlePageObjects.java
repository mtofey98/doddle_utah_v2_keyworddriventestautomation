/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author gdean
 */
public class WebDoddlePageObjects extends BaseClass
{

    public static String DoddleURL()
    {
        // Use ENUM
        return currentEnvironment.PageUrl;
    }
    public static String iframe()
    {
        return "//iframe[contains(@src, 'doddlesupport')]";
    }
    public static String CouchBaseValidatorURL()
    {
        // Use ENUM
        return "http://tmpstage-couchbaseadmin-445592012.eu-west-1.elb.amazonaws.com:4985/_admin/db/doddle_stores_sync/channels/store_456DVT";
    }
    public static String doddleSupportURL()
    {
        return "https://stage-help.doddle.com/access/unauthenticated?return_to=https%3A%2F%2Fdoddlesupport1462524635.zendesk.com%2Fagent%2Fdashboard";
    }
    public static String CouchBaseValidator_2URL()
    {
        // Use ENUM
        return "http://tmpstage-couchbaseadmin-445592012.eu-west-1.elb.amazonaws.com:4985/_admin/db/doddle_stores_sync/documents/ITEM_RETURN::2e11d9bb-8204-4c5c-b978-904e02fedc03";
    }
    public static String newBarcodeGenURL(String barcode)
    {
        return "https://barcode.tec-it.com/barcode.ashx?translate-esc=off&data="+barcode+"&code=Code128&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=000000&bgcolor=FFFFFF&qunit=Mm&quiet=0";
    }
    
    public static String CouchBaseValidatorURLRawDocRetrieve()
    {
        // Use ENUM
        return "//span[contains(text(),'ITEM_RETURN')]";
    }
    public static String CouchBaseValidatorURLRawDoc(String EntryName)
    {
        // Use ENUM
        return "http://tmpstage-couchbaseadmin-445592012.eu-west-1.elb.amazonaws.com:4985/doddle_stores_sync/"+EntryName+"";
    }
    public static String CouchBaseValidator_2URLRawDoc()
    {
        // Use ENUM
        return "http://tmpstage-couchbaseadmin-445592012.eu-west-1.elb.amazonaws.com:4985/doddle_stores_sync/ITEM_RETURN::2e11d9bb-8204-4c5c-b978-904e02fedc03";
    }
    public static String CouchBaseValidatorHeader()
    {
      
        return "//a[@class='watched']";
    }
    public static String CouchBaseValidatorClickFirst(String Entry)
    {
      
        return "//li[contains(@data-reactid, '"+Entry+"')]";
    }//li[contains(@data-reactid, 'ITEM_RETURN')]
    public static String CouchBaseValidatorReturnSTID()
    {
      
        return "//div[16]//pre";
    }
    public static String CouchBaseValidatorReturnLableValue()
    {
      
        return "//div[@id='main']//pre[contains(text(),'labelValue')]";
    }
    public static String CouchBaseValidatorReturnURL()
    {
      
        return "//div[@id='main']//pre[contains(text(),'url')]";
    }
    
    public static String itemReturnNumber()
    {
        return "//h4//span//span[@data-reactid][1]";
    }
    
    public static String CouchBaseValidatorReturnStatus()
    {
      
        return "//pre[contains(text(),'status')]";
    }
    public static String CouchBaseValidatorReturnRetailerSelected(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnHANDOVER_FROM_CUSTOMER(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String doddleSupportUsername()
    {
        return "//input[@id='user_email']";
    }
    public static String doddleSupportPassword()
    {
        return "//input[@id='user_password']"; 
              
    }
    public static String whatIsThisText()
    {
        return "//span[text()='What is this?']";
    }
    public static String submitAsDropDown()
    {
//        return "//div[@class='pane section ticket-resolution-footer-pane']//button[@data-toggle='dropdown']";
//        return "//button[@data-ember-action='4543']//..//button[@data-toggle]";
        return "//div[@class='ember-view btn-group dropup ticket_submit_buttons status_button']//..//button[@class='btn btn-inverse dropdown-toggle']";
    }
    public static String doddleSupportDash()
    {
        return "//div[@title='Support']";
    }
    public static String submitAsSolvedOption()
    {
        return "//strong[text()='Solved']";
    }
    public static String newPhotoReqCheckbox()
    {
        return "//div[@class='ember-view property_box ticket_properties']//div[@class='ember-view form_field checkbox-field custom_field_114094420194']//input[@type='checkbox']";
    }
    public static String ticketSubject()
    {
        return "//table[@class='filter_tickets main']//tbody[@class='ember-view']";
    }
    public static String ticketPage()
    {
        return "//div[@id='wrapper']";
    }
    public static String sortByID()
    {
        return "//div[@class='dashboard-top-panel']/..//tr[@class='ember-view sorting']//th[text()='ID']";
    }
    public static String latestSubjectTicket()
    {
        return "//tr[@class='ember-view regular in-focus new']";
    }
    public static String doddleSupportSignInBtn()
    {
        return "//input[@value='Sign in']";
    }
    public static String doddleSupportSearchIcon()
    {
        return "//a[@class='search-icon']";
    }
    public static String doddleSupportSearchBar()
    {
        return "//div[contains(@class, 'searchmenu-root')]";
    }
    
    public static String searchResult()
    {
        return "//div[@class='zd-menu-footer']";
    }
    public static String CouchBaseValidatorReturn456DVT(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturndvt_automation(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnSECURITY_SCAN_PASSED(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnADDED_RETAILER_REFS(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnADDED_RETURN_ITEMISATION(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnADDED_CUSTOMER_DETAILS(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnRETURN_ROUTING_DETERMINED(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnADDED_RETURN_LABEL(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnCHECKOUT_COMPLETED(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorReturnAT_COLLECTION_POINT(String value)
    {
      
        return "//pre[contains(text(),'"+value+"')]";
    }
    public static String CouchBaseValidatorClickdown()
    {
      
        return "//div[@id='main']//div[@style='display: block; bottom: 0px;']";
        //return "//div[@style='display: block; bottom: 0px;']";
    }
    public static String CouchBaseValidatorClickRawDoc()
    {
      
        return "//a[@data-reactid='.0.0.0.1.1.5.0']";
    }
    public static String CouchBaseValidatorClickRawDocWait()
    {
      
        return "//input[@data-reactid='.0.0.0.1:1.2.1.1']";
    }

    public static String doddleLogo()
    {
        return "//h1/a/img[@alt='Doddle logo']";
    }

    public static String BarCodeGenURL()
    {
        return "http://nextgen-load-preadvice.appspot.com/";
    }

    public static String barcodeGeneratorURL(String randomNumber)
    {
        return "https://www.barcodesinc.com/generator/image.php?code=" + randomNumber + "&style=196&type=C128B&width=250&height=100&xres=1&font=5";
    }
    
    public static String couchbaseValidationsURL()
    {
        return "http://tmpstage-couchbaseadmin-445592012.eu-west-1.elb.amazonaws.com:4985/_admin/db/doddle_stores_sync";
       
    }
    
    public static String storeDVTLink()
    {
        return "//a[text()='store_456DVT']";
    }
    
    public static String collectionCode()
    {
        return "//div[@class='CodeMirror-lines']//pre[contains(text(), 'referenceId')]";
    }
    
    public static String rawDocumentData()
    {
        return "//pre[@style]";
    }
    
    public static String rawDocumentURL()
    {
        return "//a[text()='Raw document URL']";
    }

    public static String couchbaseUsersLink()
    {
        return "//a[text()='Users']";
    }
    
    public static String itemCollectionList()
    {
        return "//a[contains(text(), 'ITEM_COLLECTION')]";
    }
    
    public static String itemReturnList()
    {
        return "//a[contains(text(), 'ITEM_RETURN')]";
    }
    
    public static String storeTaskList()
    {
        return "//a[contains(text(), 'STORE_TASK')]";
    }
    
    public static String numberOfDocumentsInList()
    {
        return "//div[@class='ListDocs']//strong//span[@data-reactid='.0.0.0.1.0.0.0']";
    }
    
    public static String openBracketCharacter()
    {
        return "//pre[text()='{']";
    }
    
    public static String dvtAutomationLink()
    {
        return "//a[@href='/_admin/db/doddle_stores_sync/users/DVT_Automation']";
    }
    
    public static String store_456DVTLink()
    {
        return "//a[@href='/_admin/db/doddle_stores_sync/channels/store_456DVT']";
    }
    
    public static String highlightedLinksList()
    {
        return "//li[@class='false']";
    }
    
    //--------------Generic PageObjects--------------//
    public static String AWithText(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String doddleLogout()
    {
        return "//a[text()='LOG OUT']";
    }

    public static String gmailSignout()
    {
        return "//a[text()='Sign out']";
    }

    public static String doddleSignUp()
    {
        return "//a[text()='SIGN UP']";
    }

    public static String AwithClass(String text)
    {
        return "//a[@class ='" + text + "']";
    }

    public static String ButtonWithText(String text)
    {
        return "//button[text()='" + text + "']";
    }

    public static String createMyDoddleAccountButton()
    {
        return "//button[text()='Create my Doddle account']";
    }

    public static String InputWithId(String text)
    {
        return "//input[@id = '" + text + "']";
    }

    public static String emailsignin()
    {
        return "//input[@id = 'signIn']";
    }

    public static String passwordValidation()
    {
        return "//input[@id = 'Passwd']";
    }

    public static String nextButton()
    {
        return "//input[@id = 'next']";
    }
    public static String firstItemLoad()
    {
        return"//a[contains(text(),'LOAD')]";
    }
    public static String eventContainer()
    {
        return"//div[@class ='CodeMirror-code']//pre[contains(text(),'\"eventType\": \"ADMINSTRATIVELY_VOIDED\"')]";
    }
    public static String containerStatus()
    {
        return "//div[@class ='CodeMirror-code']//pre[contains(text(),'\"newStatus\": \"VOID\"')]";
    }
    public static String allText()
    {
    return "//div[@class ='CodeMirror-code']//pre";    
    }
    public static String emailValidation()
    {
        return "//input[@id = 'Email']";
    }

    public static String mobileNumberSignup()
    {
        return "//input[@id = 'mobile_number']";
    }

    public static String postalCodeSignup()
    {
        return "//input[@id = 'register-postcode-lookup_search_postcode']";
    }

    public static String passwordTextfield()
    {
        return "//input[@id = 'password']";
    }

    public static String emailTextField()
    {
        return "//input[@id = 'email']";
    }

    public static String lastNameSignUpTextField()
    {
        return "//input[@id = 'name_lastName']";
    }

    public static String firstNameSignUpTextField()
    {
        return "//input[@id = 'name_firstName']";
    }

    public static String WelcomeToDoddleEmail()
    {
        return "//b[contains(text(),'Welcome to Doddle DVTJohn, we look forward to meeting you.')]";
    }

    public static String NewEmail()
    {
        return "//div[text()='1 new']";
    }

    public static String listContainsText(String text)
    {
        return "//li[contains(text(),'" + text + "')]";
    }

    public static String InputWithName(String text)
    {
        return "//input[@name='" + text + "']";
    }

    public static String passwordTextbox()
    {
        return "//input[@name='password']";
    }

    public static String emailTextbox()
    {
        return "//input[@name='email']";
    }

    public static String trackingCode()
    {
        return "//input[@name='trackingCode']";
    }

    public static String termsAndConditionsCheckbox()
    {
        return "//div[@id='field-termsConditions']//div[@class='checkbox-box']";
    }

    public static String doddleID()
    {
        return "//p[@class='intro-text']//small[contains(text(),'')]";
    }

    public static String RadioButton()
    {
        return "//input[@name = 'filename']";
    }

    public static String Stores()
    {
        return "//input[@name='storeid']";
    }

    public static String AirmoneyRadioButton()
    {
        return "//input[contains(@value, 'airmoney.json')]";
    }

    public static String AmazonRadioButton()
    {
        return "//input[contains(@value, 'amazon.json')]";
    }

    public static String AsosRadioButton()
    {
        return "//input[contains(@value, 'asos.json')]";
    }

    public static String CityForexRadioButton()
    {
        return "//input[contains(@value, 'cityforex.json')]";
    }

    public static String DHLRadioButton()
    {
        return "//input[contains(@value, 'dhl.json')]";
    }

    public static String FarfetchRadioButton()
    {
        return "//input[contains(@value, 'farfetch.json')]";
    }

    public static String HawesCurtisRadioButton()
    {
        return "//input[contains(@value, 'hawescurtis.json')]";
    }

    public static String HobbsRadioButton()
    {
        return "//input[contains(@value, 'hobbs.json')]";
    }

    public static String MissGuidedRadioButton()
    {
        return "//input[contains(@value, 'missguided.json')]";
    }

    public static String MooRadioButton()
    {
        return "//input[contains(@value, 'moo.json')]";
    }

    public static String NespressoRadioButton()
    {
        return "//input[contains(@value, 'nespresso.json')]";
    }

    public static String NewlookRadioButton()
    {
        return "//input[contains(@value, 'newlook.json')]";
    }

    public static String ShoeAholicsRadioButton()
    {
        return "//input[contains(@value, 'shoeaholics.json')]";
    }

    public static String TmlewinRadioButton()
    {
        return "//input[contains(@value, 'tmlewin.json')]";
    }

    public static String WiggleRadioButton()
    {
        return "//input[contains(@value, 'wiggle.json')]";
    }
    //---------Doddle Barcode "File" Page Objects----------//

    //---------Doddle Barcode "Store" Page Objects----------//
    public static String EWSRadioButton()
    {
        return "//input[contains(@value, '987EWS')]";
    }

    public static String FPKRadioButton()
    {
        return "//input[contains(@value,'023FPK')]";
    }

    public static String SubmitButton()
    {
        return "//button";
    }

    public static String GeneratedBarcode()
    {
        return "//img";
    }
//======================================Doodle Generic Page Objects=====================================

    public static String SpanWithTextXpath(String text)
    {
        return "//span[text()='" + text + "']";
    }

    public static String DivWithIDSpanXpath(String text)
    {
        return "//Div[@id='" + text + "']//span";
    }

    public static String DivWithIDSpanWithText(String text, String Description)
    {
        return "//Div[@id='" + text + "']//span[text()='" + Description + "']";
    }

    public static String DivWithIdDivXpath(String text)
    {
        return "//Div[@id='" + text + "']//div";
    }

    public static String DivWithTextSpanXpath(String text)
    {
        return "//div[text()='" + text + "']//div";
    }

    public static String DownloadFileNameXpath(String text)
    {
        return "//span[text()='" + text + "']/../..//div[5]//span";
    }

    public static String SpanContainsTextXpath(String text)
    {
        return "//span[contains(text(), '" + text + "')]";
    }

    public static String BContainsText(String text)
    {
        return "//b[contains(text(), '" + text + "')]";
    }

    public static String welcomeToDoddleEmailVal()
    {
        return "//b[contains(text(), 'Welcome to Doddle DVTJohn')]";
    }

    public static String TdContainsInput(String text)
    {
        return "//td[contains(text(),'" + text + "')]/..//input";
    }

    public static String SpanContainsClassXpath(String text)
    {
        return "//span[contains(@class, '" + text + "')]";
    }

    public static String UncheckedEmailheader()
    {
        return "//div[@class='baseIL cvIL']//img[contains(@class,'-unchk-png')]//..//..//div[text()='Doddle Password Reset']";
    }

    public static String AWithTextXpath(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String TdWithIDWithA(String text)
    {
        return "//td[@id='" + text + "']//a";
    }

    public static String TdWithName(String text)
    {
        return "//td[@name='" + text + "']";
    }

    public static String AContainsOnclickTextXpath(String text)
    {
        return "//a[contains(@onclick,'" + text + "')]";
    }

    public static String AContainsTextXpath(String text)
    {
        return "//a[contains(text(), '" + text + "')]";
    }
    
     public static String signInButton()
    {
        return "//a[contains(text(), 'SIGN IN')]";
    }

    public static String logoutBtn()
    {
        return "//a[contains(text(), 'LOG OUT')]";
    }

    public static String AContainsTitle(String text)
    {
        return "//a[contains(@title, '" + text + "']";
    }

    public static String HWithText(String text, String text1)
    {
        return "//h" + text + "[text()='" + text1 + "']";
    }

    public static String H4WithText(String text1)
    {
        return "//h4[text()='" + text1 + "']";
    }

    public static String ThWithText(String text)
    {
        return "//th[text()='" + text + "']";
    }

    public static String locationVal()
    {
        return "//th[text()='Location']";
    }

    public static String statusVal()
    {
        return "//th[text()='Status']";
    }

    public static String date()
    {
        return "//th[text()='Date']";
    }

    public static String LiContainsTextXpath(String text)
    {
        return "//li[contains(text(), '" + text + "')]";
    }

    public static String LiWithIdwithAXpath(String text)
    {
        return "//li[@id='" + text + "']//a";
    }

    public static String LiContainsTextXpath2(String text)
    {
        return "//li[contains(text(), '" + text + "')]";
    }

    public static String LiWithTextXpath(String text)
    {
        return "//li[text()= '" + text + "']";
    }

    public static String LiWithIdXpath(String text)
    {
        return "//li[@id= '" + text + "']";
    }

    public static String LiWithClassXpath(String text)
    {
        return "//li[@class= '" + text + "']";
    }

    public static String MarqueeContainsTextXpath(String text)
    {
        return "//marquee[contains(text(),'" + text + "')]";
    }

    public static String TdWithTextXpath(String text)
    {
        return "//td[text()='" + text + "']";
    }

    public static String OptionWithTextXpath(String text)
    {
        return "//option[text()='" + text + "']";
    }

    public static String OptionWithTitleXpath(String text)
    {
        return "//option[@title='" + text + "']";
    }

    public static String OptionContainsTextXpath(String text)
    {
        return "//option[contains(text(),'" + text + "')]";
    }

    public static String TdContainsTextXpath(String text)
    {
        return "//td[contains(text(), '" + text + "')]";
    }

    public static String TdContainsStyle(String text)
    {
        return "//td[contains(@style, '" + text + "')]";
    }

    public static String doddleColourVal()
    {
        return "//td[contains(@style, 'color:#ffffff;font-size:20px')]";
    }

    public static String doddleEmailTitle()
    {
        return " //a[contains(@title,'dvttesting1@gmail.com')]";
    }

    public static String ImgWithIdXpath(String text)
    {
        return "//img[@id='" + text + "']";
    }

    public static String ImgWithTitlepath(String text)
    {
        return "//img[@title='" + text + "']";
    }

    public static String TdContainsTitleXpath(String text)
    {
        return "//td[contains(@title,'" + text + "')]";
    }

    public static String ImgWithNameXpath(String text)
    {
        return "//img[@name='" + text + "']";
    }

    public static String ColumnSortStatusIcon(String text)
    {
        return "//div[text()='" + text + "']/..//div[3]";
    }

    public static String ImgContainsSrcAndNameXpath(String src, String name)
    {
        return "//img[contains(@src,'" + src + "')][contains(@name,'" + name + "')]";
    }

    public static String ImgContainsTitle(String text)
    {
        return "//img[contains(@title,'" + text + "')]";
    }

    public static String ImgWithSrc(String text)
    {
        return "//img[@src='" + text + "']";
    }

    public static String ImgContainsSrc(String text)
    {
        return "//img[contains(@src, '" + text + "')]";
    }

    public static String InputWithValueXpath(String text)
    {
        return "//input[@value='" + text + "']";
    }

    public static String InputWithIdXpath(String text)
    {
        return "//input[@id='" + text + "']";
    }

    public static String persistantcookie()
    {
        return "//input[@id='PersistentCookie']";
    }

    public static String InputWithClassXpath(String text)
    {
        return "//input[@class='" + text + "']";
    }

    public static String InputContainsValueXpath(String text)
    {
        return "//input[contains(@value, '" + text + "')]";
    }

    public static String InputContainsIdXpath(String text)
    {
        return "//input[contains(@id, '" + text + "')]";
    }

    public static String InputWithText(String text)
    {
        return "//input[text()='" + text + "']";
    }

    public static String InputWithType(String text)
    {
        return "//input[@type='" + text + "']";
    }

    public static String NobrContainsTextXpath(String text)
    {
        return "//nobr[contains(text(), '" + text + "')]";
    }

    public static String PContainsTextXpath(String text)
    {
        return "//p[contains(text(), '" + text + "')]";
    }

    public static String sorryMsgVal()
    {
        return "//p[contains(text(), 'Sorry')]";
    }

    public static String SelectWithIdXpath(String text)
    {
        return "//select[@id='" + text + "']";
    }

    public static String SelectWithIdOptionXpath(String text)
    {
        return "//select[@id='" + text + "']//option";
    }

    public static String SelectWithIDByoptionTextXpath(String DDownXpath, String txtToSelect)
    {
        return "//select[@id = '" + DDownXpath + "']//option[text() = '" + txtToSelect + "']";
    }

    public static String SelectWithIDByoptionContainsTextXpath(String DDownXpath, String txtToSelect)
    {
        return "//select[@id = '" + DDownXpath + "']//option[contains(text(),'" + txtToSelect + "')]";
    }

    public static String SelectWithNameXpath(String text)
    {
        return "//select[@name='" + text + "']";
    }

    public static String ListItemWithIdToAXpath(String text)
    {
        return "//li[@id='" + text + "']//a";
    }

    public static String AWithTitleXpath(String text)
    {
        return "//a[@title='" + text + "']";
    }

    public static String DivWithTextXpath(String text)
    {
        return "//div[text()='" + text + "']";
    }

    public static String oneNewEmail()
    {
        return "//div[text()='1 new']";
    }

    public static String gmailpromotionsTab()
    {
        return "//div[text()='Promotions']";
    }

    public static String DoddleIDVal()
    {
        return "//td[text()='Doddle ID']";
    }

    public static String DivContainsTextXpath(String text)
    {
        return "//div[contains(text(), '" + text + "')]";
    }

    public static String DivWithClassDivWithText(String tabclass, String text)
    {
        return "//div[@class='" + tabclass + "']//div[text()= '" + text + "']";
    }

    public static String ButtonWithTextXpath(String text)
    {
        return "//button[text()='" + text + "']";
    }

    public static String trackButton()
    {
        return "//button[text()='Track']";
    }

    public static String ButtonWithIDXpath(String text)
    {
        return "//button[@id='" + text + "']";
    }

    public static String ButtonWithName(String text)
    {
        return "//button[@name='" + text + "']";
    }

    public static String ButtonWithClass(String text)
    {
        return "//button[@class='" + text + "']";
    }

    public static String signinBtn()
    {
        return "//button[@class='btn btn-action']";
    }

    public static String ButtonWithTitleXpath(String text)
    {
        return "//button[@title='" + text + "']";
    }

    public static String ButtonContainsOnClickXpath(String text)
    {
        return "//button[contains(@onclick,'" + text + "')]";
    }

    public static String FontContainsTextXpath(String text)
    {
        return "//font[contains(text(), '" + text + "')]";
    }

    public static String FontWithTextXpath(String text)
    {
        return "//font[text()='" + text + "']";
    }

    public static String TdWithId(String text)
    {
        return "//td[@id='" + text + "']";//font
    }

    public static String TdWithClass(String text)
    {
        return "//td[@class='" + text + "']";
    }

    public static String TdWithTitle(String text)
    {
        return "//td[@title='" + text + "']";
    }

    public static String CompleteReport_ReportNameTextXpath(String text)
    {
        return "//div[@id='" + text + "']/span";
    }

    public static String DivWithId(String text)
    {
        return "//div[@id='" + text + "']";
    }

    public static String FieldsetWithClass(String text)
    {
        return "//fieldset[@class='" + text + "']";
    }

    public static String DivContainsId(String text)
    {
        return "//div[contains(@id, '" + text + "')]";
    }

    public static String DivWithClass(String text)
    {
        return "//div[@class='" + text + "']";
    }

    public static String DivWithArialabel(String text)
    {
        return "//div[@aria-label='" + text + "']";
    }

    public static String DivContainsClass(String text)
    {
        return "//div[contains(@class, '" + text + "')]";
    }

    public static String DivContainsClassSpan(String text)
    {
        return "//div[contains(@class, '" + text + "')]//span";
    }

    public static String bContainsText(String text)
    {
        return "//b[contains(text(), '" + text + "')]";
    }

    public static String TextAreaWithId(String text)
    {
        return "//textarea[@id='" + text + "']";
    }

    public static String IframeContainsNameXpath(String text)
    {
        return "//iframe[contains(@name , '" + text + "')]";
    }

    public static String IframeByID(String text)
    {
        return "//iframe[@id='" + text + "']";
    }

    public static String MainTabsClose(String text)
    {
        return "//li[contains(@tabname,'" + text + "')]//a//..//span";
    }

    public static String MainTabClose(String text)
    {
        return "//li[@tabname = '" + text + "']//span";
    }

    public static String LiWithTabname(String text)
    {
        return "//li[@tabname = '" + text + "']";
    }

    public static String tablewithIdXpath(String text)
    {
        return "//table[@id='" + text + "']";
    }

    public static String AcontainsClass(String text)
    {
        return "//a[contains(@class,'" + text + "')]";
    }

    public static String btnSignin()
    {
        return "//a[contains(@class,'btn--signin')]";
    }

    public static String AwithID(String text)
    {
        return "//a[@id,'" + text + "']";
    }

    public static String DivWithIDandAwithID(String text, String text2)
    {
        return "//div[@id='" + text + "']//a[@id='" + text2 + "']";
    }

    public static String StrongWithClass(String text)
    {
        return "//strong[@class='" + text + "']";
    }

    public static String StrongWithText(String text)
    {
        return "//strong[text()='" + text + "']";
    }

    public static String InboxSubjectXpath(String text)
    {
        return "//div[@class='baseIL cvIL']//div[text()='" + text + "']";
    }

    public static String GmailDeleteEmail()
    {
        return "//div[not(contains(@style,'display: none'))]/div[@title='Delete']";
    }

    public static String locationSearchField()
    {
        return "//input[@id = 'location-search-field']";
    }

    public static String getQuoteBtn()
    {
        return "//a[text()='Get quote']";
    }

    public static String emailSignInBtn()
    {
        return "//input[@value='Sign in']";
    }

    public static String usernameField()
    {
        return "//input[@id='username']";
    }

    public static String passwordField()
    {
        return "//input[@id='password']";
    }

    public static String deleteText()
    {
        return "//div[@id='divToolbarButtondelete']//a[@id='delete']";
    }

    public static String doddleSignInBtn()
    {
        return "//a[text()='SIGN IN']";
    }

    public static String submitSignInBtn()
    {
        return "//button[@name='login-submit']";
    }

    public static String forgotYourPasswordLink()
    {
        return "//a[text()='Forgotten your password?']";
    }

    public static String resetMyPasswordBtn()
    {
        return "//button[@id='submit']";
    }

    public static String doddleSupportText()
    {
        return "//div[@id='divSenderList'][text()='Doddle Support']";
    }

    public static String passwordRecoveryEmail()
    {
        return "//input[@id='email']";
    }

    public static String forgottenPasswordMessage()
    {
        return "//strong[@class='email']";
    }

    public static String hereTextLink()
    {
        return "//a[text()='here']";
    }

    public static String newPasswordField()
    {
        return "//input[@id='password']";
    }

    public static String confirmNewPasswordField()
    {
        return "//input[@id='password_confirmation']";
    }

    public static String saveNewPasswordBtn()
    {
        return "//button[@id='submit']";
    }

    public static String signIn()
    {
        return "//a[text()='Sign in']";
    }

    public static String emailSignInPage()
    {
        return "//input[@name='email']";
    }

    public static String emailField()
    {
        return "//input[@name='email']";
    }

    public static String passwordNameField()
    {
        return "//input[@name='password']";

    }

    public static String passwordChangedText()
    {
        return "//div[text()='Your password has been changed']";
    }

    public static String sendAParcelBtn()
    {
        return "//a[text()='SEND A PARCEL']";
    }

    public static String whereAreYouTextBox()
    {
        return "//input[@name='search_key']";
    }

    public static String nearestStoreDropdown()
    {
        return "//div[@class='pac-container pac-logo']";
    }

    public static String nearestStoreDistance()
    {
        return "//span[text()='7.1km']";
    }

    public static String sendViaSurbitonBtn()
    {
        return "//a[text()='Send via Surbiton']";
    }

    public static String postalCodeDestination()
    {
        return "//input[@name='dom_postcode']";
    }

    public static String length()
    {
        return "//input[@name='long']";
    }

    public static String description()
    {
        return "//input[@name='description']";
    }

    public static String weight()
    {
        return "//input[@name='weight']";
    }

    public static String height()
    {
        return "//input[@name='tall']";
    }

    public static String width()
    {
        return "//input[@name='wide']";
    }

    public static String value()
    {
        return "//input[@name='value']";
    }

    public static String whatWillItCoseBtn()
    {
        return "//input[@class='btn btn-action']";
    }

    public static String pricingOptions()
    {
        return "//h5[text()='Next Day']";
    }

    public static String pricingOptionsBefore()
    {
        return "//h5[text()='Next Day Before 10:30']";
    }

    public static String chooseButtonSubmit()
    {
        return "//input[@type='submit']";
    }

    public static String recipientCountry()
    {
        return "//input[@name='recipient_county']";
    }

    public static String recipientCity()
    {
        return "//input[@name='recipient_city']";
    }

    public static String recipientAddress2()
    {
        return "//input[@name='recipient_add2']";
    }

    public static String recipientAddress1()
    {
        return "//input[@name='recipient_add1']";
    }

    public static String recipientCompany()
    {
        return "//input[@name='recipient_company']";
    }

    public static String recipientName()
    {
        return "//input[@name='recipient_name']";
    }

    public static String recipientEmail()
    {
        return "//input[@name='recipient_email']";
    }

    public static String recipientPhone()
    {
        return "//input[@name='recipient_phone']";
    }

    public static String enterAddressManually()
    {
        return "//a[text()='enter the address manually']";
    }

    public static String checkoutBtn()
    {
        return "//input[@value='Go to checkout']";
    }

    public static String continueBtn()
    {
        return "//input[@value='Continue']";
    }

    public static String TsAndCsCheckbox()
    {
        return "//div[@class='checkbox-box']";
    }

    public static String registerBtn()
    {
        return "//a[text()='register here']";
    }

    public static String firstNameText()
    {
        return "//input[@name='firstName']";
    }

    public static String lastNameText()
    {
        return "//input[@name='lastName']";
    }

    public static String emailText()
    {
        return "//input[@name='email']";
    }

    public static String passwordText()
    {
        return "//input[@name='password']";
    }

    public static String createMyAccountBtn()
    {
        return "//input[@value='Create my account']";
    }

    public static String brainTreeIframe()
    {
        return "braintree-dropin-frame";
    }

    public static String cvvField()
    {
        return "//input[@id='cvv']";
    }

    public static String creditCardNumber()
    {
        return "//input[@id = 'credit-card-number']";
    }

    public static String postalCodeField()
    {
        return "//input[@id = 'postal-code']";
    }

    public static String expirationDate()
    {
        return "//input[@id = 'expiration']";
    }

    public static String payNowBtn()
    {
        return "//input[@value='Pay now']";
    }

    public static String cvvEntryField()
    {
        return "//h5[text()='Payment complete!']";
    }

    public static String gmailEmailField()
    {
        return "//input[@id = 'Email']";
    }

    public static String signInBtn()
    {
        return "//input[@id = 'signIn']";
    }

    public static String passwdField()
    {
        return "//input[@id = 'Passwd']";
    }

    public static String nextBtn()
    {
        return "//input[@id = 'next']";
    }

    public static String promotionsTab()
    {
        return "//div[text()='Primary']";
    }

    public static String emailNewTag()
    {
        return "//div[text()='new']";
    }
    
    public static String load()
    {
        return "//a[contains(text(), 'LOAD')]";
    }

}

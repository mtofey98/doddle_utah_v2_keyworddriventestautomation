/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;

/**
 *
 * @author gdean
 */
public class ParcelTrackingFunctionality extends BaseClass
{

    int counter = 1;

    public ParcelTrackingFunctionality(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String email = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {

        if (!TrackParcel())
        {
            return narrator.testFailed("Failed to validate that a user can not log in with the incorrect details - " + error);
        }

        return narrator.finalizeTest("Successfully Tested the Parcel tracking functionality and that it works as expected");
    }

    public boolean TrackParcel()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.trackingCode()))
        {
            error = "Failed to wait for tracking number textbox.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.trackingCode(), testData.getData("TrackingNumber")))
        {
            error = "Failed to enter tracking number of: " + testData.getData("TrackingNumber") + " into the tracking number textbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.trackButton()))
        {
            error = "Failed to click track button.";
            return false;
        }
//validating that the tracking screen appears and is displaying tracking code and details of parcel
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.H4WithText("Tracking code: " + testData.getData("TrackingNumber"))))
        {
            error = "Failed to wait for tracking page to appear and to display tracking code.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.date()))
        {
            error = "Failed to wait for the Date label.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.date()))
        {
            error = "Failed to wait for the Time label.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.statusVal()))
        {
            error = "Failed to wait for the Stauts label.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.locationVal()))
        {
            error = "Failed to wait for the location label.";
            return false;
        }
        Narrator.stepPassed("Successfully tested the parcel tracking functionality.");
        return true;
    }
}

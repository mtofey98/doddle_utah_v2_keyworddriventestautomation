/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;

/**
 *
 * @author gdean
 */
public class CustomerLoginGeneralFunctionality extends BaseClass
{

    int counter = 1;

    public CustomerLoginGeneralFunctionality(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String email = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {
      
        if (!NegativeLogin())
        {
            return narrator.testFailed("Failed to validate that a user can not log in with the incorrect details - " +error);
        }
            if (!PositiveLogin())
        {
            return narrator.testFailed("Failed to log in with the correct details - " + error);
        }
        return narrator.finalizeTest("Successfully Tested that a user can not log in with incorrect username and or password combination");
    }

    public boolean NegativeLogin()
    {
    if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.btnSignin()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.emailTextbox()))
        {
            error = "Failed to wait for sign in page.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.emailTextbox(), testData.getData("email")))
        {
            error = "Failed to enter email of: " + testData.getData("email") + ", to the email field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordTextbox(), testData.getData("Password")))
        {
            error = "Failed to enter password of: " + testData.getData("Password") + ", to the password field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.signinBtn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.sorryMsgVal()))
        {
        error = "Failed to validate that the incorrect cridential error message appeared when entering the incorrect data and trying to sign in";
        return false;
        }
             Narrator.stepPassed("Successfully Validated that using the incorrect credentials gives the expected results");
    return true;
    }
      public boolean PositiveLogin()
    {
     if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.emailTextbox(), testData.getData("CorrectEmail")))
        {
            error = "Failed to enter firstname of: " + testData.getData("CorrectEmail") + ", to the firstname field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordTextbox(), testData.getData("CorrectPassword")))
        {
            error = "Failed to enter password of: " + testData.getData("CorrectPassword") + ", to the password field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.signinBtn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.logoutBtn()))
        {
            error = "Failed to validate that the user account page was loaded after sign in attempt.";
            return false;
        }
             Narrator.stepPassed("Successfully validated that a customer can log in with the correct credentials.");
    return true;
    }
}

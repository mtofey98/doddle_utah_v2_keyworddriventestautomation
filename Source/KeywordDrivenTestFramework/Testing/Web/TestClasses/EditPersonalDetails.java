/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 */
public class EditPersonalDetails extends BaseClass
{
    Narrator narrator;
    int counter = 1;

    public EditPersonalDetails(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String email = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {
   
        if (!EditPersonalDetails())
        {
            return narrator.testFailed("Failed to Sign into Doddle Account - " +error);
        }
            if (!TestTeardown())
        {
            return narrator.testFailed("Failed to reset the account details to Default - " +error);
        }

        return narrator.finalizeTest("Successfully Signed Into existing Doddle Account");
    }

    public boolean EditPersonalDetails()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AWithTextXpath("ACCOUNT")))
        {
            error = "Failed to click the Account button";
            return false;
        }

        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("fullName"), ""))
        {
            error = "Failed to clear Full Name text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("fullName"), testData.getData("Full Name")))
        {
            error = "Failed to enter text into Full Name text field.";
            return false;
        }
        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("line1"), ""))
        {
            error = "Failed to clear Adress Line1 text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("line1"), testData.getData("AdressL1")))
        {
            error = "Failed to enter text into Address line 1 text field.";
            return false;
        }
        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("line2"), ""))
        {
            error = "Failed to clear Adress Line 2 text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("line2"), testData.getData("AddressL2")))
        {
            error = "Failed to enter text into Address line 2 text field.";
            return false;
        }
        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("email"), ""))
        {
            error = "Failed to clear email text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("email"), testData.getData("email")))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }
        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("mobile.number"), ""))
        {
            error = "Failed to clear Mobile number text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("mobile.number"), testData.getData("mobile")))
        {
            error = "Failed to enter text into mobile text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.ButtonWithClass("btn btn-action")))
        {
            error = "Failed to click on the Update Details button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AContainsTextXpath("MAIN")))
        {
            error = "Failed to click on the Main button.";
            return false;
        }
        //Automation is to fast to see screen change. Pause added for visual confirmation
        SeleniumDriverInstance.pause(1000);
        
             if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AContainsTextXpath("ACCOUNT")))
        {
            error = "Failed to click on the account button.";
            return false;
        }
//===============Validate that the data was updated successfully=========================

        WebElement name;
        try
        {
            name = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.InputWithName("fullName")));
            String temp = name.getAttribute("value").toString();
            if (!temp.contains(testData.getData("FullName")))
            {
                error = "Failed to compare extracted element to test pack data";
                return false;

            }
        } catch (Exception e)
        {
            error = "Failed to extract text from textbox";
            return false;
        }

        WebElement AddressL1;
        try
        {
            AddressL1 = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.InputWithName("line1")));
            String temp = AddressL1.getAttribute("value").toString();
            if (!temp.contains(testData.getData("AdressL1")))
            {
                error = "Failed to compare extracted element to test pack data";
                return false;

            }
        } catch (Exception e)
        {
            error = "Failed to extract text from textbox";
            return false;
        }

        WebElement AddressL2;
        try
        {
            AddressL2 = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.InputWithName("line2")));
            String temp = AddressL2.getAttribute("value").toString();
            if (!temp.contains(testData.getData("AddressL2")))
            {
                error = "Failed to compare extracted element to test pack data";
                return false;

            }
        } catch (Exception e)
        {
            error = "Failed to extract text from textbox";
            return false;
        }
        WebElement email;

        try
        {
            email = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.InputWithName("email")));
            String temp = email.getAttribute("value").toString();
            if (!temp.contains(testData.getData("email")))
            {
                error = "Failed to compare extracted element to test pack data";
                return false;

            }
        } catch (Exception e)
        {
            error = "Failed to extract text from textbox";
            return false;
        }

        WebElement mobilePhone;

        try
        {
            mobilePhone = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.InputWithName("mobile.number")));
            String temp = mobilePhone.getAttribute("value").toString();
            if (!temp.contains(testData.getData("mobile")))
            {
                error = "Failed to compare extracted element to test pack data";
                return false;

            }
        } catch (Exception e)
        {
            error = "Failed to extract text from textbox";
            return false;
        }
narrator.stepPassed("Successfully Validated that the personal details have beem changed");
        return true;
    }
    public boolean TestTeardown()
    {
       if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("fullName"), ""))
        {
            error = "Failed to clear Full Name text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("fullName"), testData.getData("Fulll Name Default")))
        {
            error = "Failed to enter text into Full Name text field.";
            return false;
        }
        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("line1"), ""))
        {
            error = "Failed to clear Adress Line1 text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("line1"), testData.getData("AdressL1 Default")))
        {
            error = "Failed to enter text into Address line 1 text field.";
            return false;
        }
        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("line2"), ""))
        {
            error = "Failed to clear Adress Line 2 text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("line2"), testData.getData("AddressL2 Default")))
        {
            error = "Failed to enter text into Address line 2 text field.";
            return false;
        }
        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("email"), ""))
        {
            error = "Failed to clear email text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("email"), testData.getData("email Default")))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }
        if (!SeleniumDriverInstance.ClearTextBoxField(WebDoddlePageObjects.InputWithName("mobile.number"), ""))
        {
            error = "Failed to clear Mobile number text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("mobile.number"), testData.getData("mobile Default")))
        {
            error = "Failed to enter text into mobile text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.ButtonWithClass("btn btn-action")))
        {
            error = "Failed to click on the Update Details button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AContainsTextXpath("MAIN")))
        {
            error = "Failed to click on the Main button.";
            return false;
        }
             if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AContainsTextXpath("ACCOUNT")))
        {
            error = "Failed to click on the account button.";
            return false;
        }
             narrator.stepPassed("Successfully set the Account details to default after testing");
    return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.EmailUtility;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 */
public class PayAsYouDoddleAccountRegistration extends BaseClass
{
    int counter = 1;
    String error = "";
    public static String email = "";
    public static String doddleID = "";

    public PayAsYouDoddleAccountRegistration(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    public TestResult executeTest()
    {
        if (!payAsYouDoddleAccountCreation())
        {
            return narrator.testFailed("Failed to create a pay as you doddle account - " + error);
        }
        if (!validateEmailRecieved())
        {
            return narrator.testFailed("Failed to validate that an email was sent to verify your account was created - " + error);
        }

        return narrator.finalizeTest("Successfully created a pay as you Doddle account and Validated that the User ID in the welcome email match.");
    }

    public boolean payAsYouDoddleAccountCreation()
    {
        try
        {
            EmailUtility.delete();
        } catch (Exception e)
        {
            error = "Failed to delete the emails in the gmail inbox";
            return false;
        }

        //Creates an email Address
        Date date = new Date();
        email = date.toString();

        //Remove : from string
        email = email.replaceAll(":", "");

        //Remove empty spaces from string
        email = email.replaceAll(" ", "");

        String[] emailcon = testData.getData("email").split("@");
        String finalemail = emailcon[0] + "+" + email + "@" + emailcon[1];

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.doddleSignUp()))
        {
            error = "Failed to click sign up button.";
            return false;
        }

        //Enter details
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.firstNameSignUpTextField(), testData.getData("FirstName")))
        {
            error = "Failed to enter firstname of: " + testData.getData("Firstname") + ", to the firstname field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.lastNameSignUpTextField(), testData.getData("LastName")))
        {
            error = "Failed to enter Lastname of: " + testData.getData("Lastname") + ", to the Lastname field.";
            return false;
        }

        //Enters email address
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.emailTextField(), finalemail))
        {
            error = "Failed to enter email of: " + finalemail + ", to the email field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordTextfield(), testData.getData("Password")))
        {
            error = "Failed to enter password of: " + testData.getData("Password") + ", to the password field.";
            return false;
        }
        //Types Postal Code
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.postalCodeSignup(), testData.getData("postalCode")))
        {
            error = "Failed to enter postcode of: " + testData.getData("postalCode") + ", to the postcode field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.mobileNumberSignup(), testData.getData("MobileNumber")))
        {
            error = "Failed to enter mobile number : " + testData.getData("MobileNumber") + ", to the mobile number field.";
            return false;
        }

        //Selects the address
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.listContainsText(testData.getData("address"))))
        {
            error = "Failed to wait for the address: " + testData.getData("address") + ", to load.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.listContainsText(testData.getData("address"))))
        {
            error = "Failed to select the address with: " + testData.getData("address");
            return false;
        }

        //Accepts Ts&Cs
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.termsAndConditionsCheckbox()))
        {
            error = "Failed to click Terms and Condtions Checkbox.";
            return false;
        }

        //Clicks Create Account
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.createMyDoddleAccountButton()))
        {
            error = "Failed to click Create account button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.doddleID()))
        {
            error = "Failed to wait for the Doddle account to be created.";
            return false;
        }

        //Retrieves the Doddle ID
        doddleID = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.doddleID());
        doddleID = doddleID.substring(doddleID.lastIndexOf(" ")).replace(" ", "");

        Narrator.stepPassed("Signed up with Doodle ID: " + doddleID + " successfully.");
        return true;
    }

    public boolean validateEmailRecieved()
    {
        if (!SeleniumDriverInstance.navigateTo(testData.getData("emailURL")))
        {
            error = "Failed to navigete to the selected URL";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.emailValidation(), testData.getData("email")))
        {
            error = "Failed to enter email of: " + testData.getData("email") + ", to the email field for gmail.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.nextButton()))
        {
            error = "Failed to click on the next button ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordValidation(), testData.getData("Password")))
        {
            error = "Failed to enter password of: " + testData.getData("Password") + ", to the password field for gmail.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.persistantcookie()))
        {
            error = "Failed to untick the 'Stay signed in tickbox'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.emailsignin()))
        {
            error = "Failed to click on the Sign In button ";
            return false;
        }
        //clicks on the
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.gmailpromotionsTab()))
        {
            error = "Failed to wait for the Promotions tab within gmail ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecondNew(WebDoddlePageObjects.oneNewEmail(), 300))
        {
            error = "Failed to wait for the 'Welcome do doddle' email to appear in the inbox";
            return false;
        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.gmailpromotionsTab()))
//        {
//            error = "Failed to click on the Promotions tab within gmail  ";
//            return false;
//        }

        //Need to wait for the email to come into the gmail inbox. NOTE!!! Spaming the refresh button does not help.
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(WebDoddlePageObjects.welcomeToDoddleEmailVal(), 300))
        {
            error = "Failed to wait for the 'Welcome do doddle' email to appear in the inbox";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.welcomeToDoddleEmailVal()))
        {
            error = "Failed to click or the 'Welcome to doddle' conformation email to open it.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.doddleColourVal()))
        {
            error = "Failed to wait or the email te open.";
            return false;
        }
        WebElement DoddleIDVal = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.doddleColourVal()));
        try
        {
            String temp = DoddleIDVal.getText();
            if (!temp.contains(doddleID))
            {
                error = "Failed to validate that email and and the my doddle, doddle ID is the same";
                return false;
            }
        } catch (Exception e)
        {
            error = "Failed to extract text from email";
            return false;
        }

        SeleniumDriverInstance.pause(10000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.doddleEmailTitle()))
        {
            error = "Failed to click or the account button top right corner of gmail.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.gmailSignout()))
        {
            error = "Failed to wait for the Sign out button to appear";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.gmailSignout()))
        {
            error = "Failed to click on the sign out button.";
            return false;
        }
        //validate that the user is signed out
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.nextButton()))
        {
            error = "Failed to validate that the user is signed out of gmail. ";
            return false;
        }

        if (!SeleniumDriverInstance.navigateTo(testData.getData("DoddleURL")))
        {
            error = "Failed to navigete to the selected URL";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.doddleLogout()))
        {
            error = "Failed to wait for the Delete button.";
            return false;
        }
        //When stepping into the method delete() you will see we are using java mail API to programatically
        //Delete all emails within a given gmail account.
        try
        {
            EmailUtility.delete();
        } catch (Exception e)
        {
            error = "Failed to delete the emails in the gmail inbox";
            return false;
        }
        Narrator.stepPassed("Successfully navigated to the gmail and validated that the 'Welcome to doddle' email was sent.");
        Narrator.stepPassed("Successfully validated that the Doddle email in the email and the doddle email on the user account matches.");
        return true;
    }

}

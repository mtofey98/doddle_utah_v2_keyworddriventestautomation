/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import java.util.Date;

/**
 *
 * @author gdean
 */
public class UpgradeFromPAYDToUnlimited extends BaseClass
{

    int counter = 1;

    public UpgradeFromPAYDToUnlimited(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String email = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {

        if (!CreatePAYDAccount())
        {
            return narrator.testFailed("Failed to create a pay as you doddle account." + error);
        }
        if (!UpgradeToDoddleUnlimitedAccount())
        {
            return narrator.testFailed("Failed to create a pay as you doddle account." + error);
        }
        return narrator.finalizeTest("Successfully created a pay as you doddle account and upgraded the account to a Doddle Unlimited account.");
    }

    public boolean CreatePAYDAccount()
    {
      //When stepping into the method delete() you will see we are using java mail API to programatically
        //Delete all emails within a given gmail account.
        try
        {
         EmailUtilInstance.delete();   
        }
        catch (Exception e)
        {
            error = "Failed to delete the emails in the gmail inbox";
            return false;
        }
        //Creates an email Address
        Date date = new Date();
        email = date.toString();

        //Remove : from string
        email = email.replaceAll(":", "");

        //Remove empty spaces from string
        email = email.replaceAll(" ", "");

        String[] emailcon = testData.getData("email").split("@");
        String finalemail = emailcon[0] + "+" + email + "@" + emailcon[1];

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AWithText("SIGN UP")))
        {
            error = "Failed to click sign up button.";
            return false;
        }
   if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.InputWithId("name_firstName")))
        {
            error = "Failed to wait for firstname of: " + testData.getData("Firstname") + ", to the firstname field.";
            return false;
        }
        //Enter details
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("name_firstName"), testData.getData("FirstName")))
        {
            error = "Failed to enter firstname of: " + testData.getData("Firstname") + ", to the firstname field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("name_lastName"), testData.getData("LastName")))
        {
            error = "Failed to enter Lastname of: " + testData.getData("Lastname") + ", to the Lastname field.";
            return false;
        }

        //Enters email address
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("email"), finalemail))
        {
            error = "Failed to enter email of: " + finalemail + ", to the email field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("password"), testData.getData("Password")))
        {
            error = "Failed to enter password of: " + testData.getData("Password") + ", to the password field.";
            return false;
        }

        //Types Postal Code
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("register-postcode-lookup_search_postcode"), testData.getData("postalCode")))
        {
            error = "Failed to enter postcode of: " + testData.getData("postalCode") + ", to the postcode field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("mobile_number"), testData.getData("MobileNumber")))
        {
            error = "Failed to enter mobile number : " + testData.getData("MobileNumber") + ", to the mobile number field.";
            return false;
        }

        //Selects the address
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.listContainsText(testData.getData("address"))))
        {
            error = "Failed to wait for the address: " + testData.getData("address") + ", to load.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.listContainsText(testData.getData("address"))))
        {
            error = "Failed to select the address with: " + testData.getData("address");
            return false;
        }

        //Accepts Ts&Cs
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.termsAndConditionsCheckbox()))
        {
            error = "Failed to click Terms and Condtions Checkbox.";
            return false;
        }

        //Clicks Create Account
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.ButtonWithText("Create my Doddle account")))
        {
            error = "Failed to click Create account button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.doddleID()))
        {
            error = "Failed to wait for the Doddle account to be created.";
            return false;
        }

        //Retrieves the Doddle ID
        doddleID = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.doddleID());
        doddleID = doddleID.substring(doddleID.lastIndexOf(" ")).replace(" ", "");

        narrator.stepPassed("Signed up with Doodle ID: " + doddleID + " successfully.");
        return true;
    }

    public boolean UpgradeToDoddleUnlimitedAccount()
    {
          if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.AWithText("MyDoddle")))
        {
            error = "Failed to click on the 'MyDoddle' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AWithText("MyDoddle")))
        {
            error = "Failed to click on the 'MyDoddle' button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.AWithText("ACCOUNT")))
        {
            error = "Failed to wait for the 'Account' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AWithText("ACCOUNT")))
        {
            error = "Failed to click on the 'Account' button";
            return false;
        }
              if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.AWithText("Account type")))
        {
            error = "Failed to click on the 'Account type' button";
            return false;
        }
           if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AWithText("Account type")))
        {
            error = "Failed to click on the 'Account type' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AWithText("Account type")))
        {
            error = "Failed to click on the 'Account type' button";
            return false;
        }
        //server side issue when clicking the upgrade button witout the pause.
        //error appears. the pause is a quick fix
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.SpanWithTextXpath("Upgrade")))
        {
            error = "Failed to click on the 'Upgrade' button";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath("braintree-dropin-frame"))
        {
            error = "Failed to switch the the Brain Tree Iframe";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.InputWithIdXpath("cvv")))
        {
            error = "Failed to wait for the 'cvv' field of the credit card entry field to be visible.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.InputWithId("credit-card-number")))
        {
            error = "Failed to enter card number of: " + testData.getData("CardNumber") + " into the card number field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(WebDoddlePageObjects.InputWithId("credit-card-number"), testData.getData("CardNumber")))
        {
            error = "Failed to enter card number of: " + testData.getData("CardNumber") + " into the card number field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(WebDoddlePageObjects.InputWithId("expiration"), testData.getData("ExpireDate")))
        {
            error = "Failed to enter postcode of: " + testData.getData("ExpireDate") + " into the expiration date field ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(WebDoddlePageObjects.InputWithId("cvv"), testData.getData("Cvv")))
        {
            error = "Failed to enter postcode of: " + testData.getData("postalCode") + " into the cvv field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(WebDoddlePageObjects.InputWithId("postal-code"), testData.getData("postalCode")))
        {
            error = "Failed to enter postcode of: " + testData.getData("postalCode") + " into the postal code field.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to default content";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.ButtonWithClass("btn btn-action")))
        {
            error = "Failed to wait for the 'Upgrade' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.ButtonWithClass("btn btn-action")))
        {
            error = "Failed to click on the 'Upgrade' button";
            return false;
        }
        //Validate that the transacction was successfull and that the account was upgraded
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.StrongWithText("Doddle Unlimited")))
        {
            error = "Failed validate that the account was upgraded. No Verivication message was displayed";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.SpanWithTextXpath("Downgrade")))
        {
            error = "Failed validate that the account was upgraded. Could not find the 'Downgrade' button.";
            return false;
        }
        //Truckates gmail inbox
      //When stepping into the method delete() you will see we are using java mail API to programatically
        //Delete all emails within a given gmail account.
        try
        {
         EmailUtilInstance.delete();   
        }
        catch (Exception e)
        {
            error = "Failed to delete the emails in the gmail inbox";
            return false;
        }
        return true;
    }

}

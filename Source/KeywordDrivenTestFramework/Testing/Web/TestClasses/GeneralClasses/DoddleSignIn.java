/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SikuliDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.util.Date;

/**
 *
 * @author gdean
 */
public class DoddleSignIn extends BaseClass
{

    int counter = 1;

    public DoddleSignIn(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String email = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the GMail URL
        if (!DoddleSignIn())
        {
            return narrator.testFailed("Failed to Sign into Doddle Account");
        }

        return narrator.finalizeTest("Successfully Signed Into existing Doddle Account");
    }

    public boolean DoddleSignIn()
    {
        //Clicks the Sign Ip button
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AcontainsClass("btn--signin")))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.InputWithName("email")))
        {
            error = "Failed to wait for sign in page.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("email"), testData.getData("email")))
        {
            error = "Failed to enter firstname of: " + testData.getData("email") + ", to the firstname field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithName("password"), testData.getData("Password")))
        {
            error = "Failed to enter password of: " + testData.getData("Password") + ", to the password field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.ButtonWithClass("btn btn-action")))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.AContainsTextXpath("LOG OUT")))
        {
            error = "Failed to validate that the user account page was loaded after sign in attempt.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Logged into doddle account");
        testData.extractParameter("Username: " + testData.getData("email") + "", " password: " + testData.getData("Password") + "", "");
        return true;
    }
}

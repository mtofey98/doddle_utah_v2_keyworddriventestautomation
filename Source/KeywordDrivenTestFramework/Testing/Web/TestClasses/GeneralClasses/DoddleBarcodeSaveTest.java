/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;

/**
 *
 * @author fnell
 */
public class DoddleBarcodeSaveTest extends BaseClass
{

    String barcode = MobileDoddlePageObjects.barcode();
    String [] barcodeType = {MobileDoddlePageObjects.barcode(),MobileDoddlePageObjects.rand()};
    String [] folderName = {"Barcodes", "ShelfLabels"};
    String error = "";
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    
    public DoddleBarcodeSaveTest(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;
    }
    
    public TestResult executeTest()
    {

//        int i = 0;
//        
//        while (i < 2)
//        {            
//            if (!save.saveBarcode(barcode, barcodeType[i], folderName[i]))
//            {
//                error = "Failed to generate and save barcode from URL";
//                return narrator.testFailedScreenshot("Error - " + error);
//            }
//            
//            i++;
//        }
        
        return narrator.finalizeTest("");
    }
}

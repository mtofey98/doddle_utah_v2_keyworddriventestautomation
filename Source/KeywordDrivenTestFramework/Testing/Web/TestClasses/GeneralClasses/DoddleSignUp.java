/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;

/**
 *
 * @author gdean
 */
public class DoddleSignUp extends BaseClass
{

    int counter = 1;

    public DoddleSignUp(TestEntity testData)
    {
        narrator = new Narrator(testData);
       this.testData = testData;

    }

       String error = "";
    public static String email = "";
    public static String doddleID = "";
    
    
    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the GMail URL
        if (!DoddleSignUp())
        {
            return narrator.testFailed("Failed to Sign up for a Doddle Account");
        }

        return narrator.finalizeTest("Successfully Signed up for a Doddle Account");
    }

    public boolean DoddleSignUp()
    {
        //Clicks the Sign Up button
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AWithText("SIGN UP")))
        {
            error = "Failed to click sign up button.";
            return false;
        }

//        Enter details
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.InputWithId("name_firstName")))
        {
            error = "Failed to wait for firstname text field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(WebDoddlePageObjects.InputWithId("name_firstName")))
        {
            error = "Failed to wait for firstname text field to be visible.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(WebDoddlePageObjects.InputWithId("name_firstName")))
        {
            error = "Failed to wait for firstname text field to be clickable.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("name_firstName"),testData.getData("FirstName")))
        {
            error = "Failed to enter firstname of: " + testData.getData("FirstName") + ", to the firstname field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("name_lastName"), testData.getData("LastName")))
        {
            error = "Failed to enter Lastname of: " + testData.getData("LastName") + ", to the Lastname field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("email"), testData.getData("email")))
        {
            error = "Failed to enter email of: " + testData.getData("email") + "into the email field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("password"), testData.getData("Password")))
        {
            error = "Failed to enter password of: " + testData.getData("Password") + ", to the password field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("mobile_number"), testData.getData("MobileNumber")))
        {
            error = "Failed to enter mobile number of: " + testData.getData("MobileNumber") + ", to the mobile field.";
            return false;
        }

        //Types Postal Code
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.InputWithId("register-postcode-lookup_search_postcode"), testData.getData("postalCode")))
        {
            error = "Failed to enter postcode of: " + testData.getData("postalCode") + ", to the postcode field.";
            return false;
        }

        //Selects the address
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.InputWithIdXpath("register-postcode-lookup_filled_address")))
        {
            error = "Failed to wait for the address: " + testData.getData("address") + ", to load.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.LiContainsTextXpath(testData.getData("address"))))
        {
            error = "Failed to select the address with: " + testData.getData("address");
            return false;
        }

        //Accepts Ts&Cs
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.termsAndConditionsCheckbox()))
        {
            error = "Failed to click Terms and Condtions Checkbox.";
            return false;
        }

        //Clicks Create Account
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.ButtonWithText("Create my Doddle account")))
        {
            error = "Failed to click Create account button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.doddleID()))
        {
            error = "Failed to wait for the Doddle account to be created.";
            return false;
        }
        
        //Retrieves the Doddle ID
        doddleID = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.doddleID());
        doddleID = doddleID.substring(doddleID.lastIndexOf(" "));
        
        
        narrator.stepPassedWithScreenShot("Signed up with Doodle ID: " + doddleID +" successfully.");
        
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.AWithText("LOG OUT")))
        {
            error = "Failed to click the LOG OUT button.";
            return false;
        }
       
narrator.stepPassedWithScreenShot("Successfully Created a Doddle account and and recieved Doddle ID");
        return true;
    }
}

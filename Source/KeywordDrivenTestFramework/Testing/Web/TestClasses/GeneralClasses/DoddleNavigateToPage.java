/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;

/**
 *
 * @author fnell
 */
public class DoddleNavigateToPage extends BaseClass
{

    int counter = 1;
    String error = "";

    public DoddleNavigateToPage(TestEntity testData)
    {
        narrator = new Narrator(testData);

    }

    public TestResult executeTest()
    {
        // This step will Launch the browser 
        if (!NavigateToDoddlePage())
        {
            return narrator.testFailed("Doddle webpage - " + error);
        }

        return narrator.finalizeTest("Successfully navigated to the Doddle home page");
    }

    public boolean NavigateToDoddlePage()
    {

        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.DoddleURL()))
        {
            error = "Failed to navigate to doddle website.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.doddleLogo()))
        {
            error = "Failed to find doddle logo.";
            return false;
        }


        return true;
    }
}

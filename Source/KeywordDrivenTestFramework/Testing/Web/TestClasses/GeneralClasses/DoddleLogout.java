/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses.GeneralClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;

/**
 *
 * @author gdean
 */
public class DoddleLogout extends BaseClass
{

    int counter = 1;

    public DoddleLogout(TestEntity testData)
    {
         narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String email = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the GMail URL
        if (!DoddleLogout())
        {
            return narrator.testFailed("Failed to Sign into Doddle Account");
        }

        return narrator.finalizeTest("Successfully Signed Into existing Doddle Account");
    }

    public boolean DoddleLogout()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.logoutBtn()))
        {
            error = "Failed to wait for the log out button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.logoutBtn()))
        {
            error = "Failed to click on the log out Button ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.signInButton()))
        {
            error = "Failed to wait for the sign In button to be visible";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Successfully logged out of doddle account");
        return true;
    }
}

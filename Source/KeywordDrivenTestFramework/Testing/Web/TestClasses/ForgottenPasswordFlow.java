/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author gdean
 */
public class ForgottenPasswordFlow extends BaseClass
{

    int counter = 1;

    public ForgottenPasswordFlow(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String email = "";
    public static String doddleID = "";
    String originalHandle = "";

    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the GMail URL
        if (!Validatepage())
        {
            return narrator.testFailed("Failed to Validate that the Doddle page was loaded successfully - " + error);
        }
        if (!TestStartup())
        {
            return narrator.testFailed("Failed to ensure that the System is at default before starting the testing process - " + error);
        }
        if (!PasswordRecovery())
        {
            return narrator.testFailed("Failed to retrieve forgotten password - " + error);
        }
        if (!PasswordChangedValidation())
        {
            return narrator.testFailed("Failed to validate that the password has changed - " + error);
        }
        if (!TestTeardown())
        {
            return narrator.testFailed("Failed to set email account to default after testing the system - " + error);
        }
        return narrator.finalizeTest("Successfully Retrieved forgotten password and validated that the password has been changed ");
    }

    public boolean Validatepage()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.locationSearchField()))
        {
            error = "Failed to wait for the Doddle home page search bar to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.getQuoteBtn()))
        {
            error = "Failed to wait for the Doddle Home page 'Get Quote' Button to be visible";
            return false;
        }
        return true;
    }

    public boolean TestStartup()
    {
        //When stepping into the method delete() you will see we are using java mail API to programatically
        //Delete all emails within a given gmail account.
        try
        {
            EmailUtilInstance.delete();
        }
        catch (Exception e)
        {
            error = "Failed to delete the emails in the gmail inbox" + e.getMessage();
            return false;
        }
        if (!SeleniumDriverInstance.navigateTo(testData.getData("emailURL")))
        {
            error = "Failed to navigate to the selected URL";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.emailSignInBtn()))
        {
            error = "Failed to wait for the login to email account page to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.usernameField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " in the username textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordField(), testData.getData("Oldpassword")))
        {
            error = "Failed to enter password : " + testData.getData("Oldpassword") + " in the password textbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.emailSignInBtn()))
        {
            error = "Failed to click on the login button email.";
            return false;
        }
        //Validating that no email regarding the pasword reset is visible within the email and if it is it will run a script to delete the email
        //befor starting the test
        if (SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecondNew(WebDoddlePageObjects.DivWithTextXpath(testData.getData("doddleEmailSubject")), 10))
        {
            int count = 0;
            for (int i = 0; i < 5; i++)
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.deleteText()))
                {
                    error = "Failed to click on the the doddle email subject to open correct email";
                    return false;
                }
                count++;

                SeleniumDriverInstance.pause(500);
            }
        }
        return true;
    }

    public boolean PasswordRecovery()
    {
        if (!SeleniumDriverInstance.navigateTo(testData.getData("WebsiteURL")))
        {
            error = "Failed to navigete to the Doddle staging website URL";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.doddleSignInBtn()))
        {
            error = "Failed to click on the sign in button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.submitSignInBtn()))
        {
            error = "Failed to wait for the sign in button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.forgotYourPasswordLink()))
        {
            error = "Failed to click on the 'Forgotten your password' link to recieve a password recovery email";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.resetMyPasswordBtn()))
        {
            error = "Failed to wait for the 'Reset Password' button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordRecoveryEmail(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("email") + "";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.resetMyPasswordBtn()))
        {
            error = "Failed to click on the 'Reset my password' button.";
            return false;
        }
        //Validate that the 'message' stating an email has been sent to your email has appeared or not
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.forgottenPasswordMessage()))
        {
            error = "Failed to validate that the 'Restore Forgotten password email has been sent' message has appeared ";
            return false;
        }
        if (!SeleniumDriverInstance.navigateTo(testData.getData("emailURL")))
        {
            error = "Failed to navigete to the selected URL";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.DivWithTextXpath(testData.getData("doddleEmailSubject"))))
        {
            error = "Failed to validate that an email has appeared in email inbox to reset forgotten password";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(WebDoddlePageObjects.DivWithTextXpath(testData.getData("doddleEmailSubject"))))
        {
            error = "Failed to validate that an email has appeared in email inbox to reset forgotten password";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(WebDoddlePageObjects.DivWithTextXpath(testData.getData("doddleEmailSubject"))))
        {
            error = "Failed to validate that an email has appeared in email inbox to reset forgotten password";
            return false;
        }
        //static pause
        //after waiting for the email to come in we need to give outlook second or 3 to proccess the email in inbox
        //Reason beeing the framework is to fast for the inbox
        SeleniumDriverInstance.pause(5000);
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(WebDoddlePageObjects.doddleSupportText()))
        {
            error = "Failed to click on the the doddle email subject to open correct email";
            return false;
        }

        originalHandle = SeleniumDriverInstance.Driver.getWindowHandle();

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.hereTextLink()))
        {
            error = "Failed to click on the the link 'here' to reset password.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.newPasswordField(), testData.getData("NewPassword")))
        {
            error = "Failed to enter password : " + testData.getData("NewPassword") + " into the new password texttbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.confirmNewPasswordField(), testData.getData("NewPassword")))
        {
            error = "Failed to enter confirm password : " + testData.getData("password_confirmation") + " into the new confirm password texttbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.saveNewPasswordBtn()))
        {
            error = "Failed to click on the the link 'Save password' button.";
            return false;
        }
        return true;
    }

    public boolean PasswordChangedValidation()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.signIn()))
        {
            error = "Failed to click on the the sign in button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.emailSignInPage()))
        {
            error = "Failed to wait for sign in page.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.emailField(), testData.getData("Email")))
        {
            error = "Failed to enter firstname of: " + testData.getData("Email") + ", to the firstname field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordNameField(), testData.getData("NewPassword")))
        {
            error = "Failed to enter password of: " + testData.getData("NewPassword") + ", to the password field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.submitSignInBtn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.logoutBtn()))
        {
            error = "Failed to validate that the user account page was loaded after sign in attempt.";
            return false;
        }
        return true;
    }

    public boolean TestTeardown()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.logoutBtn()))
        {
            error = "Failed to click the Log out button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.signIn()))
        {
            error = "Failed to click on the sign in button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.submitSignInBtn()))
        {
            error = "Failed to wait for the sign in button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.forgotYourPasswordLink()))
        {
            error = "Failed to click on the 'Forgotten your password' link to recieve a password recovery email";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.resetMyPasswordBtn()))
        {
            error = "Failed to wait for the 'Reset Password' button to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.emailField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("email") + "";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.resetMyPasswordBtn()))
        {
            error = "Failed to click on the 'Reset my password' button.";
            return false;
        }

        //Validate that the 'message' stating an email has been sent to your email has appeared or not
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.forgottenPasswordMessage()))
        {
            error = "Failed to validate that the 'Restore Forgotten password email has been sent' message has appeared ";
            return false;
        }

        //collecting parent Handle to call upon new tab
        for (String handle : SeleniumDriverInstance.Driver.getWindowHandles())
        {
            if (!handle.equals(originalHandle))
            {
                SeleniumDriverInstance.Driver.switchTo().window(handle);
                SeleniumDriverInstance.Driver.close();
            }
        }
        SeleniumDriverInstance.Driver.switchTo().window(originalHandle);

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(WebDoddlePageObjects.UncheckedEmailheader()))
        {
            error = "Failed to wait for the unchecked doddle email to come in";
            return false;
        }

        SeleniumDriverInstance.pressKey(Keys.DELETE);

        SeleniumDriverInstance.Driver.navigate().refresh();

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.passwordChangedText()))
        {
            error = "Failed to validate that an email has appeared in email inbox to reset forgotten password";
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.DELETE);
//giving the tab a second o open.
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(WebDoddlePageObjects.DivWithTextXpath(testData.getData("doddleEmailSubject"))))
        {
            error = "Failed to validate that an email has appeared in email inbox to reset forgotten password";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.hereTextLink()))
        {
            error = "Failed to click on the the link 'here' to reset password.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to Switch to active tab";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.passwordField()))
        {
            error = "Failed to wait for the password field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.confirmNewPasswordField()))
        {
            error = "Failed to enter confirm password : " + testData.getData("password_confirmation") + " into the confirm password texttbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordField(), testData.getData("Oldpassword")))
        {
            error = "Failed to enter password : " + testData.getData("NewPassword") + " into the password texttbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.confirmNewPasswordField(), testData.getData("Oldpassword")))
        {
            error = "Failed to enter confirm password : " + testData.getData("password_confirmation") + " into the confirm password texttbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.saveNewPasswordBtn()))
        {
            error = "Failed to click on the the link 'Save password' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.signIn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.signIn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.emailField()))
        {
            error = "Failed to wait for sign in page.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(WebDoddlePageObjects.emailField()))
        {
            error = "Failed to wait for sign in page to be visible.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(WebDoddlePageObjects.emailField()))
        {
            error = "Failed to wait for sign in page to be clickable.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.emailField(), testData.getData("Email")))
        {
            error = "Failed to enter firstname of: " + testData.getData("Email") + ", to the firstname field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordNameField(), testData.getData("Oldpassword")))
        {
            error = "Failed to enter password of: " + testData.getData("Oldpassword") + ", to the password field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.submitSignInBtn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.logoutBtn()))
        {
            error = "Failed to validate that the user account page was loaded after sign in attempt.";
            return false;
        }
        //When stepping into the method delete() you will see we are using java mail API to programatically
        //Delete all emails within a given gmail account.
        try
        {
            EmailUtilInstance.delete();
        }
        catch (Exception e)
        {
            error = "Failed to delete the emails in the gmail inbox";
            return false;
        }

        return true;
    }

}

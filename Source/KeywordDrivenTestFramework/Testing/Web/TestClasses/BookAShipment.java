/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Web.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.EmailUtilInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 */
public class BookAShipment extends BaseClass
{

    int counter = 1;

    public BookAShipment(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String email = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {
        if (!Quote())
        {
            return narrator.testFailed("Failed to get a quote for desired order - " + error);
        }
        if (!Price())
        {
            return narrator.testFailed("Failed to get a price. - " + error);
        }
        if (!Address())
        {
            return narrator.testFailed("Failed to create a Doddle Unlimited account - " + error);
        }
        if (!ReviewOrder())
        {
            return narrator.testFailed("Failed to create a Doddle Unlimited account - " + error);
        }
        if (!Payment())
        {
            return narrator.testFailed("Failed to create a Doddle Unlimited account - " + error);
        }
           if (!emailValidation())
        {
            return narrator.testFailed("Failed to validate that a reciept email was sent.- " + error);
        }

       
        return narrator.finalizeTest("Successfully sent a parcel. System works as expceted.");

    }

    public boolean Quote()
    {
        //When stepping into the method delete() you will see we are using java mail API to programatically
        //Delete all emails within a given gmail account.
        try
        {
         EmailUtilInstance.delete();   
        }
        catch (Exception e)
        {
            error = "Failed to delete the emails in the gmail inbox";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.sendAParcelBtn()))
        {
            error = "Failed to wait for the 'Send parcel' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.sendAParcelBtn()))
        {
            error = "Failed to click on the 'Send parcel' button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.whereAreYouTextBox()))
        {
            error = "Failed to wait for the 'Where are you' textbox";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.whereAreYouTextBox(), testData.getData("PostalCode")))
        {
            error = "Failed to click on the 'Send parcel' button";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.nearestStoreDropdown()))
        {
            error = "Failed to wait for the Nearest Store from dropdown.";
            return false;
        }
         
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.nearestStoreDropdown()))
        {
            error = "Failed to click on the Nearest Store from dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.nearestStoreDistance()))
        {
            error = "Failed to wait for the Nearest Store.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.nearestStoreDistance()))
        {
            error = "Failed to click on the Nearest Store.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.sendViaSurbitonBtn()))
        {
            error = "Failed to wait for the 'Send via Subiton' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.sendViaSurbitonBtn()))
        {
            error = "Failed to click on the 'Send via Subiton' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.postalCodeDestination(), testData.getData("SendingToPostalCode")))
        {
            error = "Failed to enter Destination Postal code of : " + testData.getData("SendingToPostalCode") + " into the destination postal code textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.length(), testData.getData("Lenght")))
        {
            error = "Failed to enter Lenght: " + testData.getData("Lenght") + " into the lengh textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.width(), testData.getData("width")))
        {
            error = "Failed to enter width: " + testData.getData("width") + " into the width textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.height(), testData.getData("Height")))
        {
            error = "Failed to enter Height: " + testData.getData("Height") + " into the Height textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.weight(), testData.getData("Weight")))
        {
            error = "Failed to enter Weight: " + testData.getData("Weight") + " into the Weight textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.description(), testData.getData("ItemDescrip")))
        {
            error = "Failed to enter ItemDescrip: " + testData.getData("ItemDescrip") + " into the Description textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.value(), testData.getData("Price")))
        {
            error = "Failed to enter value: " + testData.getData("Price") + " into the value textbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.whatWillItCoseBtn()))
        {
            error = "Failed to click on the 'What will it cost?' button.";
            return false;
        }
        Narrator.stepPassed("Successfully entered the parcel details and requested a quote");
        return true;
    }

    public boolean Price()
    {
        //the number indecates the size of h. e.g. h1, h2, h3, h4, h5 followed by the string to use
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.pricingOptions()))
        {
            error = "Failed to wait for the pricing options to be visible";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecondNew(WebDoddlePageObjects.pricingOptionsBefore(), 10))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.pricingOptionsBefore()))
            {
                error = "Failed to wait for the pricing options to be visible";
                return false;
            }
        }
        //if desired a different shipping option can be chosen using the method below.
        //Change the integer to a number corresponding to the button you want to click
        // 0 = top most button. 1 = Button below top most. 2 = Button below the middle button.
        List<WebElement> chooseButton;
        try
        {
            chooseButton = SeleniumDriverInstance.Driver.findElements(By.xpath(WebDoddlePageObjects.chooseButtonSubmit()));
            chooseButton.get(0).click();
        } catch (Exception e)
        {
            error = "Failed to extract the choose buttons xpaths and add them to a list";
            return false;
        }
        Narrator.stepPassed("Successfully got payment details and selected a shipping method");
        return true;
    }

    public boolean Address()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.recipientName()))
        {
            error = "Failed to wait for the 'Recipient' textbox to be visible";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.recipientName(), testData.getData("NameOfReciever")))
        {
            error = "Failed to enter a recipient name into the textbox ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.recipientEmail(), testData.getData("RecieverEmail")))
        {
            error = "Failed to enter recipient email into the textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.recipientPhone(), testData.getData("RecieverPhoneNUmber")))
        {
            error = "Failed to enter recipient phone number into the textbox";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.enterAddressManually()))
        {
            error = "Failed to click on the option 'Enter address manually'";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.recipientCompany()))
        {
            error = "Failed to enterr Recipient company name";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.recipientCompany(), testData.getData("CompanyName")))
        {
            error = "Faield to enter recipient name";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.recipientAddress1(), testData.getData("AddressL1")))
        {
            error = "Failed to enter recipients street name";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.recipientAddress2(), testData.getData("AddressL2")))
        {
            error = "Failed to enter recipients house/appartment number";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.recipientCity(), testData.getData("city")))
        {
            error = "Failed to enter recipients city";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.recipientCountry(), testData.getData("Country")))
        {
            error = "Failed to enter recipients country";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.continueBtn()))
        {
            error = "Failed to click on the coninue button after entering all the details";
            return false;
        }
        Narrator.stepPassed("Successfully entered the Recievers Address and details to recieve the package");
        return true;
    }

    public boolean ReviewOrder()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.checkoutBtn()))
        {
            error = "Failed to wait for the checkout button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.TsAndCsCheckbox()))
        {
            error = "Failed to tick the checkbox to agree to terms and conditions";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.checkoutBtn()))
        {
            error = "Failed to click on the checkkout button";
            return false;
        }
        //Creates an email Address
        Date date = new Date();
        email = date.toString();

        //Remove : from string
        email = email.replaceAll(":", "");

        //Remove empty spaces from string
        email = email.replaceAll(" ", "");

        String[] emailcon = testData.getData("email").split("@");
        String finalemail = emailcon[0] + "+" + email + "@" + emailcon[1];
        //Need to create a new account everytime we do the process. Reason:
        //Cards that get linked to the accounts acan not be deleted and thus can not run in regression.

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.registerBtn()))
        {
            error = "Failed to click on the register button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.firstNameText()))
        {
            error = "Failed to wait for the name textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.firstNameText(), testData.getData("SenderName")))
        {
            error = "Failed to enter name : " + testData.getData("SenderName") + " into the name textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.lastNameText(), testData.getData("SenderLastName")))
        {
            error = "Failed to enter last name : " + testData.getData("SenderLastName") + " into the last name textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.emailText(), finalemail))
        {
            error = "Failed to enter email : " + finalemail + " into the email textbox";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwordText(), testData.getData("password")))
        {
            error = "Failed to enter password : " + testData.getData("password") + " into the password textbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.TsAndCsCheckbox()))
        {
            error = "Failed to tick the terms and conditions tickbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.createMyAccountBtn()))
        {
            error = "Failed to click on the 'Create my Account'.";
            return false;
        }
        testData.extractParameter("Email Generated = " + finalemail + ". Password used = " + testData.getData("password") + ".", "", "");
        testData.extractParameter("Name of Sneder :" + testData.getData("password") + ".", "Lastname of sender: " + testData.getData("SenderLastName") + ".", "");
        Narrator.stepPassed("Successfully created a new account and logged in with the new account");

        return true;
    }

    public boolean Payment()
    {
        if (!SeleniumDriverInstance.switchToFrameByXpath(WebDoddlePageObjects.brainTreeIframe()))
        {
            error = "Failed to switch the the Brain Tree Iframe";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.cvvField()))
        {
            error = "Failed to wait for the 'cvv' field of the credit card entry field to be visible.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.creditCardNumber()))
        {
            error = "Failed to enter card number of: " + testData.getData("CardNumber") + " into the card number field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(WebDoddlePageObjects.creditCardNumber(), testData.getData("CardNumber")))
        {
            error = "Failed to enter card number of: " + testData.getData("CardNumber") + " into the card number field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(WebDoddlePageObjects.expirationDate(), testData.getData("ExpireDate")))
        {
            error = "Failed to enter postcode of: " + testData.getData("ExpireDate") + " into the expiration date field ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(WebDoddlePageObjects.cvvField(), testData.getData("Cvv")))
        {
            error = "Failed to enter postcode of: " + testData.getData("postalCode") + " into the cvv field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(WebDoddlePageObjects.postalCodeField(), testData.getData("postalCode")))
        {
            error = "Failed to enter postcode of: " + testData.getData("postalCode") + " into the postal code field.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch to default content";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.payNowBtn()))
        {
            error = "Failed to click on the 'Pay now' button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.cvvEntryField()))
        {
            error = "Failed to wait for the 'cvv' field of the credit card entry field to be visible.";
            return false;
        }
        
        testData.extractParameter("Card number used :"+testData.getData("CardNumber")+".", "Expire Date :"+testData.getData("ExpireDate")+".", "cvv used: "+testData.getData("cvv")+"");
        Narrator.stepPassed("Successfully used a credit card to pay for the parcel to be sent.");
        return true;
    }
    
    public boolean emailValidation()
    {
     if (!SeleniumDriverInstance.navigateTo(testData.getData("emailURL")))
        {
            error = "Failed to navigete to the selected URL";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.gmailEmailField(), testData.getData("email")))
        {
            error = "Failed to enter email of: " + testData.getData("email") + ", to the email field for gmail.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.nextBtn()))
        {
            error = "Failed to click on the next button ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.passwdField(), testData.getData("password")))
        {
            error = "Failed to enter password of: " + testData.getData("password") + ", to the password field for gmail.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.signInBtn()))
        {
            error = "Failed to click on the Sign In button ";
            return false;
        }
        //clicks on the
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.promotionsTab()))
        {
            error = "Failed to wait for the Promotions tab within gmail ";
            return false;
        }
          if (!SeleniumDriverInstance.waitForElementByXpathVisibilityMillisecondNew(WebDoddlePageObjects.emailNewTag(),500))
        {
            error = "Failed to wait for the new tag to appear when recieving an email ";
            return false;
        }
          
             if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.emailNewTag()))
        {
            error = "Failed to click on the new tag. ";
            return false;
        }
       

        //Need to wait for the emailto come into the gmail inbox. NOTE!!! Spaming the refresh button does not help.
            if(!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.DoddleIDVal()))   
        {
            error = "Failed to wait on the email to open";
            return false;
        }
        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.navigateTo(testData.getData("DoddleURL")))
        {
            error = "Failed to navigete to the selected URL";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.logoutBtn()))
        {
            error = "Failed to wait for the logout button.";
            return false;
        }
         //When stepping into the method delete() you will see we are using java mail API to programatically
        //Delete all emails within a given gmail account.
        try
        {
         EmailUtilInstance.delete();   
        }
        catch (Exception e)
        {
            error = "Failed to delete the emails in the gmail inbox";
            return false;
        }
    return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return.validationList;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_67_ExistingCustomerAccount extends BaseClass {
    
    
    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    //barcodes
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    //***************

    //API call body
    StringEntity input;
    
    public static String[] validationsList;
    String value;
    
        public TC_NextGen_67_ExistingCustomerAccount(TestEntity testData) {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();     
        }
        
     public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
            }
            
            collectionNoCustomerAccount();

            executeBarCode();
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to set the Barcode settings - " + error);
            }
            //TODO    
                   
            return narrator.finalizeTest("Successfully completed collection with existing customer account");
        }
        else
        {
            executeBarCode();
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to set the Barcode settings - " + error);
            }

            collectionNoCustomerAccount();
            //TODO

            executeBarCode();
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to set the Barcode settings - " + error);
            }
            
            return narrator.finalizeTest("Successfully completed collection with existing customer account");
        }                     //TODo
    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode("DC" + MobileDoddlePageObjects.barcode(), MobileDoddlePageObjects.rand()))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();

            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, SettersAndGetters.getGeneratedBarCode(), storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }
    
    public boolean collectionNoCustomerAccount()
    {
        if (!ReusableFunctionalities.initiateCollection_DoddleID())
        {
            error = "Failed to initiate collection with Doddle ID";
            return false;
        }
        
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        
        if (!ReusableFunctionalities.initiateExistingCustomerAccount())
        {
            error = "Failed to initiate collection with email address";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the back arrow butotn";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to clicke the back arrow";
            return false;
        }
        

        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
        //Shortcuts.newBearer();

        Shortcuts.preAadviceCollectionParcelItemID(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), SettersAndGetters.getNewBearer());

        if (!SaveBarcodesFromURL.saveBarcode(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }
        
        if (!ReusableFunctionalities.initiateExistingCustomerAccount())
        {
            error = "Failed to initiate collection with email address";
            return false;
        }
                         
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }
        
        int count = 0;

        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            if (count == 10)
            {
                error = "Failed to wait for the ok button";
                return false;
            }

            count++;
        }
        
        Shortcuts.doCollectionsAPICall(SettersAndGetters.getBarCodeRefID(), SettersAndGetters.getNewBearer());
        
       if(!Shortcuts.line.contains("\"customerId\":\"XXX11111\",\"email\":\"mootest@doddle.test\""))
        {
            error = "\"customerId\": \"XXX11111\" was not found in the ITEM_COLLECTION record";
            return false;
        }
       
        if(!Shortcuts.changeAPIResponseColor("\"customerId\":\"XXX11111\",\"email\":\"mootest@doddle.test\""))
        {
            error = "\"customerId\": \"XXX11111\" was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Customer object added" + Shortcuts.line);
        
        if(!Shortcuts.line.contains("\"payment\":{\"currency\":\"GBP\""))
        {
            error = "\"customerId\": \"XXX11111\" was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("\"payment\":{\"currency\":\"GBP\""))
        {
            error = "Payments was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Payments object added" + Shortcuts.line);
                  
        if (!Shortcuts.line.contains("\"eventType\": \"NOID_PROCESS_ENRICHED_CUSTOMER_AND_PRICE\""))
        {
            error = "\"eventType\": \"NOID_PROCESS_ENRICHED_CUSTOMER_AND_PRICE\" was not found";
            return false;
        }
                     
        if(!fullFlow())
        {
            error = "Unable to complete the collection process";
            return false;
        }
        
        return true;
    }
    
    public boolean fullFlow()
    {
        
        if (!ReusableFunctionalities.CollectionProcess())
        {
            error = "Failed to initiate collection with email address";
            return false;
        }
        
        if (!ReusableFunctionalities.collectionOptionPassport())
        {
            error = "Failed to do the collection process with passport";
            return false;
        }
        
        try
        {
            AppiumDriverInstance.Driver.swipe(70, 290, 70, 410, 10);

            AppiumDriverInstance.Driver.swipe(70, 290, 115, 290, 10);

            AppiumDriverInstance.Driver.swipe(115, 290, 140, 320, 10);

            AppiumDriverInstance.Driver.swipe(140, 320, 140, 380, 10);

            AppiumDriverInstance.Driver.swipe(140, 380, 115, 410, 10);

            AppiumDriverInstance.Driver.swipe(115, 410, 70, 410, 10);

            //V for vendetta
            AppiumDriverInstance.Driver.swipe(170, 290, 205, 410, 10);

            AppiumDriverInstance.Driver.swipe(205, 410, 240, 290, 10);

            //T for Thomas
            AppiumDriverInstance.Driver.swipe(270, 290, 340, 290, 10);

            AppiumDriverInstance.Driver.swipe(305, 290, 305, 410, 10);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return false;
        }

        Narrator.stepPassedWithScreenShot("Customer Signature");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.signatureNextButton()))
        {
            error = "Failed to wait for the next step";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.signatureNextButton()))
        {
            error = "Failed to wait for the next step";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to wait for the proceed to payment button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to click the proceed to payment button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked proceed to payment button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to wait for the 'Proceed to Payment' button to be touchable";
            return false;       
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnParcelToStorage()))
        {
            error = "Failed to click the return to storage button";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.yesButton()))
        {
            error = "Failed to wait for the pop up";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Returning parcel to storage");
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.yesButton()))
        {
            error = "Failed to click the yes button in the pop up";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to wait for the collection tile";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully returned parcels to storage");
        
        
        Shortcuts.doCollectionsAPICall(SettersAndGetters.getBarCodeRefID(), SettersAndGetters.getNewBearer());
        
        if(!Shortcuts.line.contains("\"REJECTED_BY_CUSTOMER\""))
        {
            error = "Failed to add \"REJECTED_BY_CUSTOMER\" in couchbase";
            return false;
        }
       
        if(!Shortcuts.changeAPIResponseColor("\"REJECTED_BY_CUSTOMER\""))
        {
            error = "\"customerId\": \"XXX11111\" was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Customer object added" + Shortcuts.line);
            
  return true;
    }
}
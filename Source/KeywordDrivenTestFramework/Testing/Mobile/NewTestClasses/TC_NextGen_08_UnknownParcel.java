/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.barcode;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_08_UnknownParcel extends BaseClass
{
    
    String error = "";
    public static TestEntity currentData;

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    //barcodes
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String amazonBarcode = MobileDoddlePageObjects.storedAmazonBarcode;
    //***************

    //API call body
    StringEntity input;
    //***************

    public TC_NextGen_08_UnknownParcel(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }
    
     public TestResult executeTest()
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
            }

            if (!unknownParcel())
            {
                return narrator.testFailedScreenshot("Failed to scan unknown barcode - " + Shortcuts.error);
            }
            
            if (!preAdvicedParcel())
            {
                return narrator.testFailedScreenshot("Failed to scan preadviced barcode - " + Shortcuts.error);
            }
            
            return narrator.finalizeTest("Successfully scanned a known and unknown parcel");
        }
        else
        {
            if (!unknownParcel())
            {
                return narrator.testFailedScreenshot("Failed to scan unknown barcode - " + Shortcuts.error);
            }
            
            if (!preAdvicedParcel())
            {
                return narrator.testFailedScreenshot("Failed to scan preadviced barcode - " + Shortcuts.error);
            }
            
            return narrator.finalizeTest("Successfully scanned a known and unknown parcel");
        }
    }
  
    
    public boolean unknownParcel()
    {
        String parcel = MobileDoddlePageObjects.barcode();
        String shelf = MobileDoddlePageObjects.rand();
           
        if (!SaveBarcodesFromURL.saveBarcode(parcel, shelf))
        {
            error = "Failed to set up the bar code";
            return false;
        }
        
        if (!Shortcuts.scanAndRescanBarcode(parcel))
        {
            error = "Failed to scan the barcode";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Scanned and unknown barcode" + parcel);
        
        return true;
    }
    
    public boolean preAdvicedParcel()
    {
        String parcel = MobileDoddlePageObjects.barcode();
        String shelf = MobileDoddlePageObjects.rand();
        
        Shortcuts.preAadviceCollectionParcelExpectedStatus(parcel, shelf, newBearer);
        
        if (!SaveBarcodesFromURL.saveBarcode(parcel, shelf))
        {
            error = "Failed to set up the bar code";
            return false;
        }
        
        Narrator.stepPassed("Preadvice barcode = " + parcel);
        
        if (!Shortcuts.scanBarcode(parcel))
        {
            error = "Failed to scan the barcode";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemStatus()))
        {
            error = "Failed to wait for item status : EXPECTED";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Scanned preadvised barcode - " + parcel);
        
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }
              
      return true;
    }
}
    


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author mtofey
 */
public class TC_NextGen_16_Collection_OrderID extends BaseClass {

    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    //barcodes
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    ArrayList<String> collectionOrderIDArr = new ArrayList<>();
    //***************

    //API call body
    StringEntity input;

    //holds vale for order ID
    public static String orderID = "";

    //***************
    public TC_NextGen_16_Collection_OrderID(TestEntity testData) {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
    }

    public TestResult executeTest() {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1)) {
            if (!ReusableFunctionalities.Login()) {
                return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);

            }
            executeBarCode();

            if (!ReusableFunctionalities.initiateCollection_OrderID()) {
                return narrator.testFailedScreenshot("Failed to intiate a Collection with Order ID - " + Shortcuts.error);
            }

            if (!ReusableFunctionalities.CollectionProcess()) {
                return narrator.testFailed("Failed to do the collection process -" + error);
            }

            if (!ReusableFunctionalities.collectionOptionPassport()) {
                return narrator.testFailed("Failed to select the passport option -" + error);
            }

            if (!ReusableFunctionalities.collectionEndProcess()) {
                return narrator.testFailed("Failed to complete the collection process -" + error);
            }
            return narrator.finalizeTest("Successfully intiated a Collection with Order ID");

        } else {
            executeBarCode();

            if (!ReusableFunctionalities.initiateCollection_OrderID()) {
                return narrator.testFailedScreenshot("Failed to intiate a Collection with Order ID - " + Shortcuts.error);
            }

            if (!ReusableFunctionalities.CollectionProcess()) {
                return narrator.testFailed("Failed to do the collection process -" + error);
            }

            if (!ReusableFunctionalities.collectionOptionPassport()) {
                return narrator.testFailed("Failed to select the passport option -" + error);
            }

            if (!ReusableFunctionalities.collectionEndProcess()) {
                return narrator.testFailed("Failed to complete the collection process -" + error);
            }
            return narrator.finalizeTest("Successfully intiated a Collection with Order ID");
        }

    }

    public TestResult executeBarCode() {
        if (!Shortcuts.newBearer()) {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode)) {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal"))) {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode) {
        try {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();

            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, SettersAndGetters.getGeneratedBarCode(), storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            orderID = line.replaceAll("[-+.^:,\"]", "");
            Pattern pattern = Pattern.compile("orderID(.*)parcels.*[^0-9].*");
            Matcher matcher = pattern.matcher(orderID);

            while (matcher.find()) {
                System.out.println(matcher.group(1));
                collectionOrderIDArr.add(matcher.group(1));
            }
            orderID = collectionOrderIDArr.get(0);

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied")) {
                narrator.testFailed(line);
                return false;
            }
        } catch (Exception e) {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;

/**
 *
 * @author mtofey
 */
public class TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block extends BaseClass
{

    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();

    public TC_NextGen_51_Collection_Search_By_Code_No_Cust_Block(TestEntity testData)
    {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
            }

            if (!executePreCondition())
            {
                return narrator.testFailed("Failed to execute the test precondition");
            }

            if (!workerMethod())
            {
                return narrator.testFailed("Failed to search for a customer, with customer block, using collection code");
            }

            return narrator.finalizeTest("Successfully searched for a collection using collection code");
        } else
        {
            if (!executePreCondition())
            {
                return narrator.testFailed("Failed to execute the test precondition");
            }

            if (!workerMethod())
            {
                return narrator.testFailed("Failed to search for a customer, with customer block, using collection code");
            }

            return narrator.finalizeTest("Successfully searched for a collection using collection code");

        }
    }

    public boolean executePreCondition()
    {
        try
        {
            if (!ReusableFunctionalities.createCollectionBarcodeWithOutCustomerBlock())
            {
                error = ReusableFunctionalities.error;
                return false;
            }
            
            SaveBarcodesFromURL.saveLabel(SettersAndGetters.getBarCodeRefID());

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
            {
                error = "Failed to click on the collections tile";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Clicked the collection tile");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionCodeButton()))
            {
                error = "Failed to wait for the collection code button to appear";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionCodeButton()))
            {
                error = "Failed to click on the collections code button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully clicked the collection code button");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterCollectionCodeField()))
            {
                error = "Failed to wait for the Collection Code input field to appear";
                return false;
            }
            return true;
        } catch (Exception e)
        {
            error = e.getMessage();
            narrator.logFailure("Failed to execute precondition");
            narrator.logError(e.getMessage());
            System.out.println("Failed to execute precondition");
            return false;
        }
    }

    public boolean workerMethod()
    {
        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to scan the barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreCollectionCodesMessage()))
        {
            error = "Failed to wait for the any more collection codes message";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Scanned collection code");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesNoButton()))
        {
            error = "Faile to click the no button on popuop";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Faield to wait for the correect button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to click the correct button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the ok button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu(), 120))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }

        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to dashboard";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully navigated back to the dashboard");

        Shortcuts.doCollectionsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

        if (!Shortcuts.line.contains("\"searchType\":\"referenceId\""))
        {
            error = "\"searchType\": \"referenceId\" was not found in the ITEM_COLLECTION record";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("\"searchType\":\"referenceId\""))
        {
            error = "could not change API response colour :" + Shortcuts.changeFailedAPIResponseColor("\"searchType\":\"referenceId\"");
            return false;
        }

        Narrator.stepPassed(Shortcuts.line);

        return true;
    }
}

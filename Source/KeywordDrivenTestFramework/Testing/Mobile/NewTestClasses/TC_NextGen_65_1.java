/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.CollectionProcess;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.backToDashBoard;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.checkCarriersRecordsInDatabase;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.collectionEndProcess;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.collectionOptionPassport;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.moreCollectionCodesNo;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.searchWithCollectionCodes;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.line;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.http.client.methods.HttpGet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_65_1 extends BaseClass
{

    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String amazonBarcode = "";
    String amazonShelf = "";
    String amazonBarcode2 = "";
    String amazonShelf2 = "";
    String amazonBarcode3 = "";
    String amazonShelf3 = "";
    static int run = 0;
    static int timeout = 15;
    static String temp = "";
    static List<WebElement> parcelListSize;
    static List<String> parcelList = new ArrayList();
    static Set<String> finalBarcodes = new HashSet<>();
    static int listSizeBefore = 0;
    static int listSize = 0;
    //API call body
    StringEntity input;

    public TC_NextGen_65_1(TestEntity testData)
    {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {

        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
            {

                if (!ReusableFunctionalities.Login())
                {
                    return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
                }

                TC_NextGen_03_Carrier cr = new TC_NextGen_03_Carrier(testData);

                //===================01============
                String bb1 = MobileDoddlePageObjects.barcode();
                String shelf = MobileDoddlePageObjects.rand();
                cr.executeBarCode(bb1, shelf);
                writeToExcel2(bb1, "TC_NextGen_3_Carriers_Bar2Copy.xlsx", "TC_NextGen_3_Carriers2Copy.xlsx");

                if (!ReusableFunctionalities.carrierProcessExecution())
                {
                    return narrator.testFailedScreenshot("Failed to do the Carrier process - " + Shortcuts.error);
                }
                ReusableFunctionalities.createCollectionBarcodeWithOutCustomerBlock65(SettersAndGetters.getGeneratedBarCode());
                SaveBarcodesFromURL.saveLabel(SettersAndGetters.getBarCodeRefID());

                Shortcuts.doCollectionsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                if (!Shortcuts.line.contains("\"retailerId\""))
                {
                    error = "retailer was not found in the ITEM_COLLECTION record";
                    return narrator.testFailed(error);
                }

                if (!Shortcuts.changeAPIResponseColor("\"retailerId\""))
                {
                    error = "could not change API response colour :" + Shortcuts.changeFailedAPIResponseColor("\"retailerId\"");
                    return narrator.testFailed(error);
                }
                Narrator.stepPassed(Shortcuts.line);
                
                if (!StorageProccess(bb1, shelf))
                {
                    return narrator.testFailedScreenshot("Failed to do Storage process - ");
                }

                if (!checkCarriersRecordsInDatabase("TC_NextGen_3_Carriers_Bar2Copy.xlsx"))
                {
                    return narrator.testFailedScreenshot("Failed to Validate Carrier Parcel- " + Shortcuts.error);
                }
//             if (!ReusableFunctionalities.checkRecordsInDatabase("TC_NextGen_6_Validate.xlsx"))
//            {
//                return narrator.testFailedScreenshot("Failed check the records on the Database - " + Shortcuts.error);
//            }

//            if (!collection())
//            {
//                return narrator.testFailedScreenshot("Failed to do collections- " + Shortcuts.error);
//            }
                // ReusableFunctionalities.Logout();
                return narrator.finalizeTest("Successfully completed TC_NextGen_65.1");
            }
        }

        return narrator.testFailedScreenshot("Failed to wait for 'Version Text'");
    }

    public boolean collection()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Failed to wait for the 'other search options' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Successfully clicked the 'other search options' button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Other search options window");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to wait for the 'UPI' option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to touch the 'UPI' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the UPI option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter UPI field";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter UPI field";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to click on the enter the UPI in the UPI Field";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to click on the enter the UPI in the UPI Field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterUPIField(), SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to enter UPI in the 'UPI' field";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered UPI ");
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;
        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcel()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcel()))
        {
            error = "Failed to touch the ok button";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }
        try
        {
            String bcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.expectedText());

            if (bcode.equals(SettersAndGetters.getGeneratedShelfCode()))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.photoOfParcel()))
                {
                    error = "Failed to touch photo of parcel";
                    return false;
                }

                Narrator.stepPassedWithScreenShot("No Photo");
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.photoParcelBackButton()))
                {
                    error = "Failed to touch photo of parcel";
                    return false;
                }
            }
            if (!(bcode.equals(SettersAndGetters.getGeneratedShelfCode())))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.lostParcelButton()))
                {
                    error = "Failed to touch photo of parcel";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertOkButton()))
                {
                    error = "Failed to touch photo of parcel";
                    return false;
                }

            }
            loop();
        } catch (Exception e)
        {
            error = e.getMessage();
            return false;
        }

        ReusableFunctionalities.backToDashBoard();

        Narrator.stepPassedWithScreenShot("");
        return true;
    }

    public boolean loop()
    {
        while (run < timeout)
        {
            parcelListSize = AppiumDriverInstance.Driver.findElements(By.xpath(MobileDoddlePageObjects.retakePhotoItemBarcode()));
            for (int i = 0; i < parcelListSize.size(); i++)
            {
                temp = parcelListSize.get(i).getText();
                parcelList.add(temp.trim());
            }
            for (int i = 0; i < parcelList.size(); i++)
            {
                finalBarcodes.add(parcelList.get(i));

            }
            listSizeBefore = listSize;
            listSize = finalBarcodes.size();
            try
            {
                //AppiumDriverInstance.Driver.swipe(237, 509, 237, 262, 2000);

                String bcode2 = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.expectedText());
                System.out.println(bcode2 + " Validate Against" + SettersAndGetters.getGeneratedShelfCode());

                if ((bcode2.equals(SettersAndGetters.getGeneratedShelfCode())))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.photoOfParcel()))
                    {
                        error = "Failed to touch photo of parcel";
                        return false;
                    }

                    Narrator.stepPassedWithScreenShot("No Photo for this parcel Screen Appears");
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.photoParcelBackButton()))
                    {
                        error = "Failed to touch photo of parcel";
                        return false;
                    }
                    return true;

                }
                if (!(bcode2.equals(SettersAndGetters.getGeneratedShelfCode())))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.lostParcelButton()))
                    {
                        error = "Failed to touch photo of parcel";
                        return false;
                    }
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertOkButton()))
                    {
                        error = "Failed to touch photo of parcel";
                        return false;
                    }
                }
            } catch (Exception e)
            {
                error = "Failed to scroll to the desired co-ordinates - " + e.getMessage();
                return false;
            }

            if ((listSizeBefore == listSize))
            {
                run++;
            }

        }
        return true;
    }

    public boolean getStoreSystemUIConfig(String newBearer)
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet storeConfigPut = new HttpGet("https://stage-apigw.doddle.it/v2/stores/456DVT");

        try
        {

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input.setContentType("application/json");
            HttpResponse response2 = client.execute(storeConfigPut);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

//            if (!line.contains("SINGLE_CONTAINER"))
//            {
//                narrator.testFailed(line);
//                return false;
//            }
            Narrator.stepPassed(line);
        } catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean validateStore(String fileName)
    {
        String currentValue;
        String textToBeChecked = "";
        String keyValue = "";
        Shortcuts.databaseRecordsLabels(fileName);
        Map<String, String> recordInDatabase = SettersAndGetters.getDatabaseRecords();//getting the Map with different values
        Set<String> keyData = SettersAndGetters.getDatabaseRecords().keySet();//getting only the keys for the Map
        List<String> keyOfRecordInDatabase = new ArrayList<>(keyData);//putting the map keys in a list to be used on the Map

        getStoreSystemUIConfig(SettersAndGetters.getNewBearer());
        String bearerValue = SettersAndGetters.getAPIResponse();

        for (int i = 0; i < keyOfRecordInDatabase.size(); i++)
        {
            switch (recordInDatabase.get(keyOfRecordInDatabase.get(i)))
            {
                case "placeholder":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                case "hiddenValue":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                default:
                {
                    currentValue = recordInDatabase.get(keyOfRecordInDatabase.get(i));
                    if ((keyOfRecordInDatabase.get(i).contains(" ")))
                    {
                        String[] tempRecord = keyOfRecordInDatabase.get(i).split(" ");
                        keyValue = tempRecord[0];
                    } else
                    {
                        keyValue = keyOfRecordInDatabase.get(i);
                    }

                    textToBeChecked = "\"" + keyValue + "\"" + ":" + "\"" + currentValue + "\"";
                    break;
                }
            }

            if (!bearerValue.contains(textToBeChecked))
            {
                error = "Failed to add \"" + textToBeChecked + "\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(textToBeChecked))
            {
                error = "Failed to update text: \"'" + textToBeChecked + "'\" to green color";
                return false;
            }

        }

        Narrator.stepPassed("Carrier added successfully - " + Shortcuts.line);

        return true;
    }

    public boolean StorageProccess(String bb, String shelf)
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }
        SettersAndGetters.setGeneratedBarCode(bb);
        SettersAndGetters.setGeneratedShelfCode(shelf);
        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }
        //Narrator.stepFailedWithScreenShot("TAKE PHOTO SCREEN STILL HERE");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }
        if (!Shortcuts.scanAndRescanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan shelf barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
            {
                error = "Failed to click on the pause storage button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to click the back to dashboard button";
                return false;
            }
        } else if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.finishStorageButton()))
        {
            error = "Failed to touch the 'Pause storage' button.";
            return false;
        }

        Narrator.stepPassed("Successfully retrieved parcel from carrier, scanned and stored parcel.");
        return true;
    }

    public void writeToExcel2(String barcode, String fName, String fName2)
    {
        try
        {
            //Get the excel file.
            FileInputStream file = new FileInputStream(new File(System.getProperty("user.dir") + "//DatabaseRecordsLabels//" + fName2));

            //Get workbook for XLS file.
            XSSFWorkbook yourworkbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook.
            //If there have >1 sheet in your workbook, you can change it here IF you want to edit other sheets.
            XSSFSheet sheet1 = yourworkbook.getSheetAt(0);

            // Get the row of your desired cell.
            // Let's say that your desired cell is at row 2.
            Row row = sheet1.getRow(4);
            // Get the column of your desired cell in your selected row.
            // Let's say that your desired cell is at column 2.
            Cell column = row.getCell(1);
            // If the cell is String type.If double or else you can change it.
            String updatename = column.getStringCellValue();
            //New content for desired cell.
            updatename = barcode;
            //Print out the updated content.
            //System.out.println(updatename);
            //Set the new content to your desired cell(column).
            column.setCellValue(updatename);
            //Close the excel file.
            file.close();
            //Where you want to save the updated sheet.
            FileOutputStream out
                    = new FileOutputStream(new File(System.getProperty("user.dir") + "//DatabaseRecordsLabels//" + fName));
            yourworkbook.write(out);
            out.close();

        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}

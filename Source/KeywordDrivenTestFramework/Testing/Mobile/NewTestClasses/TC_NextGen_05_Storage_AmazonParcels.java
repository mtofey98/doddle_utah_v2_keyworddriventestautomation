/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import KeywordDrivenTestFramework.Utilities.CryptoUtility;
import io.appium.java_client.MobileElement;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_05_Storage_AmazonParcels extends BaseClass
{

    String name = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    String error = "";
    Narrator narrator;
    StringEntity input;
    String[] lines;
    String[] liness;
    String regex;

//==========================Veribles for custom methods=====================
    int Run = 0;
    int timeout = 8;
    int ListSize = 0;
    int ListSizeBefore = 0;
    String temp = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    List<MobileElement> parcels;
    public static String parcel;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.amazonBarcode();
    String incorrectBarcode = "AA-123-" + MobileDoddlePageObjects.randomThreeDigitNo() + "-" + MobileDoddlePageObjects.randomThreeDigitNo();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String imageURL = "";
    String destinationFile = "";
    List<WebElement> parcelListSize;
    Set<String> finalBarcodes = new HashSet<>();
    public String newBearer = "";
    String line = "";
    String apiLine;
    int run = 0;
    int listSizeBefore = 0;
    int listSize = 0;
    public boolean conErorr;
    String value;
    String itemReturn;
    int i = 0;
    public static URL url;
    String apiResponse;
    String itemID;
    int notificationBellBefore, notificationBellAfter;
    CryptoUtility crypt = new CryptoUtility();

    public static TestEntity currentData;
    Shortcuts shortcuts = new Shortcuts();
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();

//==========================================================================
    public TC_NextGen_05_Storage_AmazonParcels(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        this.currentData = testData;
    }

    public TestResult executeTest()
    {

        String email = testData.getData("Email");
        String cellNumber = testData.getData("CellNo");
        String storeID = testData.getData("StoreID");
        String cardNo = testData.getData("cardNo");
        String postalCode = testData.getData("postal");

        if (!Shortcuts.newBearer())
        {
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + error);
        }

        if (!save.saveBarcodes(barcode, incorrectBarcode))
        {
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            Login();

            if (!carrierProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
            }
        } else
        {
            if (!carrierProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
            }
        }

        return narrator.finalizeTest("Successfully completed the storage process with amazon parcels");
    }

    public boolean carrierProcess()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

//        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 3))
//            {
//                error = "Failed to wait for the 'Loading, Please wait...' to be no longer visible";
//                return false;
//            }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        for (int j = 0; j < 8; j++)
        {
            if (!shortcuts.scanBarcode(incorrectBarcode))
            {
                error = "Failed to scan incorrect barcode";
                return false;
            }

            if (j == 3)
            {
                Narrator.stepPassedWithScreenShot("Blocking message");
                AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.warningNoButton());
            }

            if (j == 7)
            {
                AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.warningYesButton());
                break;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
        }

        Narrator.stepPassedWithScreenShot("Barcode was accepted and scanned successfully");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the back arrow button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back button";
            return false;
        }

        if (!shortcuts.scanBarcode(barcode))
        {
            error = "Failed to scan amazon barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Amazon barcode successfully scanned");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean Login()
    {
        if (!conErorr)
        {
//            if (!(ReusableFunctionalities.checkWifiIsOn()))
//            {
//                error = "Failed to switch wifi-on " + Shortcuts.error;
//                return false;
//            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (!store.equals(narrator.getData("StoreSelection")))
            {
                if (!scrollToSubElement(narrator.getData("StoreSelection")))
                {
                    error = "Could not find " + narrator.getData("StoreSelection");
                    return false;
                }

            }

            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                Login();

            }
        }

//        if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//           if(!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 4))
//           {
//               error = "Failed to wait for the progress circe to no longer be present";
//               return false;
//           }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            } else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        } catch (Exception e)
        {
            return false;
        }

    }

}

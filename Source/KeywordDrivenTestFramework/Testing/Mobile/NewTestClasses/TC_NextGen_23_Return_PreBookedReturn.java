/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return.LabelValue;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return.validationList;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return.wifiStatus;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_30_Return_SingleContainer.error;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA38DataArtTestReturnFlowWifiOff;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import static java.lang.System.out;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.isSeleniumFailure;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.retrieveItemIDFromCouchBase;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.LabelValue;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import java.io.IOException;
import java.util.LinkedHashSet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_23_Return_PreBookedReturn extends BaseClass
{

    Narrator narrator;
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();

    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String barcodeName = "";
    public String newBearer = "";
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public static boolean wifiStatus;
    public boolean restart;
    public boolean conErorr;
    boolean testPassed = true;
    //==========================Veribles for custom methods=====================
//    static boolean restart = false;
    String line = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String temp2;
    String text;
    String correctBarcode;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL;
    String destinationFile;
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    WebElement barcodeVal;
    String value;
    String itemReturn;
    String specificCaseText;
    String retailer;
    String clientVersion;
    String deviceIdentifier;
    String storeID;
    String orderID;
    String email;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();
    public String[] validationList;
    String printer = Shortcuts.getPrinter();

    //==========================================================================
    public TC_NextGen_23_Return_PreBookedReturn(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
            {
                if (!ReusableFunctionalities.Login())
                {
                    return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!initiatePreBookedReturns())
                {
                    return narrator.testFailedScreenshot("Failed to do the preBooked returns process" + error + Shortcuts.error);
                }

                if (!ReturnSummaryFlow())
                {
                    return narrator.testFailedScreenshot("Unable to do the return summary flow" + error + Shortcuts.error);
                }

                if (!ReusableFunctionalities.returnsBackToDashboard())
                {
                    return narrator.testFailedScreenshot("Failed to cancel return" + error + Shortcuts.error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!preBookedReturnFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the pre booked return flow");
                }

                if (!ReturnSummaryFlow())
                {
                    return narrator.testFailedScreenshot("Unable to do the return summary flow" + error + Shortcuts.error);
                }

                if (!completeReturnsWithGeneratedLabel())
                {
                    return narrator.testFailedScreenshot("Unable to complete the returns process" + error + Shortcuts.error);
                }

                if (!ReusableFunctionalities.returnsBackToDashboard())
                {
                    return narrator.testFailedScreenshot("Failed to cancel return" + error + Shortcuts.error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!preBookedReturnFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the pre booked return flow");
                }

                if (!completeReturnsWithTempLabel())
                {
                    return narrator.testFailedScreenshot("Unable to complete the returns process" + error + Shortcuts.error);
                }

                if (!ReusableFunctionalities.returnsBackToDashboard())
                {
                    return narrator.testFailedScreenshot("Failed to cancel return" + error + Shortcuts.error);
                }

                if (!specificCaseFlow())
                {
                    return narrator.testFailedScreenshot("Failed doing the specific case flow" + error + Shortcuts.error);
                }

                if (!dashboardScan())
                {
                    return narrator.testFailedScreenshot("Failed doing the dashboard scan process" + error + Shortcuts.error);
                }

                if (!despatchSelectedContatinersFlow())
                {
                    return narrator.testFailedScreenshot("Failed doing the 'depsatch selected contatiner flow'" + error + Shortcuts.error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!initiatePreBookedReturns())
                {
                    return narrator.testFailedScreenshot("Failed to do the preBooked returns process" + error + Shortcuts.error);
                }

                if (!cancellingReturns())
                {
                    return narrator.testFailedScreenshot("Failed to cancel the returns process - " + error + "" + Shortcuts.error);
                }

                return narrator.finalizeTest("Successfully completed the pre booked returns process with multiple container configuration");
            } else
            {
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!initiatePreBookedReturns())
                {
                    return narrator.testFailedScreenshot("Failed to do the preBooked returns process" + error + Shortcuts.error);
                }

                if (!ReturnSummaryFlow())
                {
                    return narrator.testFailedScreenshot("Unable to do the return summary flow" + error + Shortcuts.error);
                }

                if (!ReusableFunctionalities.returnsBackToDashboard())
                {
                    return narrator.testFailedScreenshot("Failed to cancel return" + error + Shortcuts.error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!preBookedReturnFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the pre booked return flow");
                }

                if (!ReturnSummaryFlow())
                {
                    return narrator.testFailedScreenshot("Unable to do the return summary flow" + error + Shortcuts.error);
                }

                if (!completeReturnsWithGeneratedLabel())
                {
                    return narrator.testFailedScreenshot("Unable to complete the returns process" + error + Shortcuts.error);
                }

                if (!ReusableFunctionalities.returnsBackToDashboard())
                {
                    return narrator.testFailedScreenshot("Failed to cancel return" + error + Shortcuts.error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!preBookedReturnFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the pre booked return flow");
                }

                if (!completeReturnsWithTempLabel())
                {
                    return narrator.testFailedScreenshot("Unable to complete the returns process" + error + Shortcuts.error);
                }

                if (!ReusableFunctionalities.returnsBackToDashboard())
                {
                    return narrator.testFailedScreenshot("Failed to cancel return" + error + Shortcuts.error);
                }

                if (!specificCaseFlow())
                {
                    return narrator.testFailedScreenshot("Failed doing the specific case flow" + error + Shortcuts.error);
                }

                if (!dashboardScan())
                {
                    return narrator.testFailedScreenshot("Failed doing the dashboard scan process" + error + Shortcuts.error);
                }

                if (!despatchSelectedContatinersFlow())
                {
                    return narrator.testFailedScreenshot("Failed doing the 'depsatch selected contatiner flow'" + error + Shortcuts.error);
                }

                if (!ReusableFunctionalities.returnsBackToDashboard())
                {
                    return narrator.testFailedScreenshot("Failed doing the cancelling of the returns" + error + Shortcuts.error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return" + Shortcuts.error);
                }

                if (!initiatePreBookedReturns())
                {
                    return narrator.testFailedScreenshot("Failed to do the preBooked returns process" + error + Shortcuts.error);
                }

                if (!cancellingReturns())
                {
                    return narrator.testFailedScreenshot("Failed to cancel the returns process - " + error + "" + Shortcuts.error);
                }

                return narrator.finalizeTest("Successfully completed the pre booked returns process with multiple container configuration");
            }

        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }

    }

    public boolean initiatePreBookedReturns()
    {
        validationList = new String[]
        {
            "BOOKING_MADE", "EXPECTED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to wait for the returns tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        //Enter Email
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the pre booked return button");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to click the email field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter the email in the search field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        Narrator.stepPassedWithScreenShot("enter email - " + testData.getData("Email"));

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "failed to search using email";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Searched returns by entering email");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "failed to click the back button";
            return false;
        }

        //Scan Email
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the email text field";
            return false;
        }

        if (!SaveBarcodesFromURL.saveLabel(testData.getData("Email")))
        {
            error = "Failed to save the email barcode";
            return false;
        }

        Narrator.stepPassed("Scanning barcode - " + testData.getData("Email"));

        if (!Shortcuts.scanAndRescanBarcode(testData.getData("Email")))
        {
            error = "Failed to scan email";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Searched returns by scanning email");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "failed to click the back button";
            return false;
        }

        //Scan reference code   
        Narrator.stepPassed("Scanning barcode - " + SettersAndGetters.getBarCodeRefID());

        if (!SaveBarcodesFromURL.saveLabel(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to save the email barcode";
            return false;
        }

        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to scan email";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "failed to click the back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to click the doddle email text field";
            return false;
        }

        // Enter reference code
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to scan the barcode";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        Narrator.stepPassedWithScreenShot("Enter reference ID - " + SettersAndGetters.getBarCodeRefID());

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to click the search collection button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully selected a booking");

        validationList = new String[]
        {
            "BOOKING_RETRIEVED", "HANDOVER_FROM_CUSTOMER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean ReturnSummaryFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to find the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to find click the back arrow button";
            return false;
        }

        Narrator.stepPassed("tapped the back button");

        validationList = new String[]
        {
            "SELECTED_IN_ERROR", "EXPECTED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        Narrator.stepPassedWithScreenShot("Select the booking screen appeared");

        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.barCodeRefID))
        {
            error = "Failed to scan email";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.editReferences()))
        {
            error = "Failed to find the 3 dots button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.editReferences()))
        {
            error = "Failed to click on the edit reference button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.orderID()))
        {
            error = "Failed to find the order ID field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.orderID(), testData.getData("orderIdEdit")))
        {
            error = "Failed to find the order ID field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        Narrator.stepPassedWithScreenShot("Order ID updated - " + testData.getData("orderIdEdit"));

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newLookEditButton()))
        {
            error = "Failed to find the edit button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newLookEditButton()))
        {
            error = "Failed to click the edit button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the edit button");

        validationList = new String[]
        {
            "ADDED_RETAILER_REFS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 2))
        {
            Narrator.stepPassedWithScreenShot("Back button is not present");
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerBlockEdit()))
        {
            error = "Failed to find the 3 dots button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerBlockEdit()))
        {
            error = "Failed to click the 3 dots button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Customer edit screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.skipEdit()))
        {
            error = "Failed to find the skip button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.skipEdit()))
        {
            error = "Failed to click the 3 dots button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the skip button");

        validationList = new String[]
        {
            "SKIPPED_CUSTOMER_DETAILS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerBlockEdit()))
        {
            error = "Failed to find the 3 dots button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Customer edit screen");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerBlockEdit()))
        {
            error = "Failed to click the 3 dots button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the doddle email text field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("editEmail")))
        {
            error = "Failed to enter - " + testData.getData("editedMootest@doddle.edit") + " in the email field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        Narrator.stepPassedWithScreenShot("Customer edit screen");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the search button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Could not load the return summary screen";
            return false;
        }

        validationList = new String[]
        {
            "ADDED_CUSTOMER_DETAILS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean completeReturnsWithGeneratedLabel()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Faield to click the confirm and print label button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected the confrim and print label option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to wait for the printer option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to click the dvt printer";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to wait for select printer button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to click the select printer button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 2))
        {
            Narrator.stepPassedWithScreenShot("Printer error");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
            {
                error = "Failed to dismiss the error message";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
            {
                error = "Failed to click the retry button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
            {
                error = "Failed to click the retry button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("clicked retry");
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 6))
        {
            Narrator.stepFailedWithScreenShot("Could not complete returns with a generated label - 'Failed to connect screen displayed'");
            return false;
        } else
        {
            if (!generatedLabelFlow())
            {
                error = "Failed to do the generated label flow";
                return false;
            }

        }

        return true;
    }

    public boolean completeReturnsWithTempLabel()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!ReusableFunctionalities.wifiOff())
        {
            error = "Faield to turn off the wifi";
            return false;
        }

        wifiStatus = false;

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 8))
        {
            Narrator.stepPassedWithScreenShot("Successfully validated scan a temp label screen");

        } else
        {
            error = "Failed to validate temp label screen";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
        {
            Narrator.stepPassed("Back button not present");
        }

        if (!save.saveShelfLabel(temp))
        {
            error = "Failed to save shelfLabel";
            return false;
        }

        if (!ReusableFunctionalities.scanShelfBarcode(temp))
        {
            error = "Failed to scan temp label barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 8))
        {
            error = "App Crash!";
            return false;
        }

        if (!ReusableFunctionalities.wifiOn())
        {
            error = "Failed to turn the wifi on";
            return false;
        }

        if (!retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retrieve item ID from couchbase";
            return false;
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED", "ADDED_RETURN_LABEL"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 5))
        {
            error = "Failed because of app crash";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 2))
        {
            Narrator.stepPassed("Back button not present");
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!ReusableFunctionalities.returnsBackToDashboard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        validationList = new String[]
        {
            "CHECKOUT_COMPLETED", "AT_COLLECTION_POINT"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        AppiumDriverInstance.pause(1500);

        if (!ReusableFunctionalities.checkWifiIsOn())
        {
            error = "Failed to switch wifi on";
            return false;
        }

        wifiStatus = true;

        return true;
    }

    public boolean generatedLabelFlow()
    {
        String incorrectBarcode = MobileDoddlePageObjects.barcode();

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the 'scan the printed label' screen to appear";
            return false;
        }

        if (!SaveBarcodesFromURL.saveLabel(incorrectBarcode))
        {
            error = "Failed to save the incorrect barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(incorrectBarcode))
        {
            error = "Failed to scan incorrect barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Scanned incorrect barcode");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to wait for the try again button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to click the try again button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the scan the printed label text";
            return false;
        }

        try
        {

            Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

            String bearer = SettersAndGetters.getNewBearer();
            String apiResponse = SettersAndGetters.getAPIResponse();

            //Getting LabelValue
            apiResponse = SettersAndGetters.getAPIResponse();
            regex = "\"labelValue\":\"";
            lines = apiResponse.split(regex);
            liness = lines[1].split("\",\"url\":");
            apiResponse = liness[0];
            LabelValue = apiResponse;
            SettersAndGetters.setReturnsLabelValue(LabelValue);
            System.out.println(LabelValue);

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve the label value from couchbase";
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed(3)";
            return false;
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the done button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the 'any more returns' message to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the no button";
            return false;
        }

        validationList = new String[]
        {
            "CHECKOUT_COMPLETED", "AT_COLLECTION_POINT"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLaterButton(), 2))
        {
            error = "Tap now/later buttons occurred, container config failed to change";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
        {
            error = "Failed to wait for the returns back to dashboard buton";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
        {
            error = "Failed to click the returns back to dashboard buton";
            return false;
        }

        return true;
    }

    public boolean tempLabelFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
        {
            Narrator.stepPassed("Back button not present");
        }

        if (!save.saveShelfLabel(temp))
        {
            error = "Failed to save shelfLabel";
            return false;
        }

        if (!ReusableFunctionalities.scanShelfBarcode(temp))
        {
            error = "Failed to scan temp label barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 8))
        {
            error = "App Crash!";
            return false;
        }

        if (!ReusableFunctionalities.wifiOn())
        {
            error = "Failed to turn the wifi on";
            return false;
        }

        if (!retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retrieve item ID from couchbase";
            return false;
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED", "ADDED_RETURN_LABEL"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 5))
        {
            error = "Failed because of app crash";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 2))
        {
            Narrator.stepPassed("Back button not present");
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLaterButton(), 2))
            {
                error = "Tap now/later buttons appeared, container config failed to change";
                return false;
            }

            error = "failed to wait for the returns back to dashboard button";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        validationList = new String[]
        {
            "CHECKOUT_COMPLETED", "AT_COLLECTION_POINT"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        AppiumDriverInstance.pause(1500);

        if (!ReusableFunctionalities.checkWifiIsOn())
        {
            error = "Failed to switch wifi on";
            return false;
        }

        wifiStatus = true;

        return true;
    }

    public boolean specificCaseFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicking on the storage tile");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (SettersAndGetters.getReturnsLabelValue() == null)
        {
            error = "Failed to scan the returns label value";
            return false;
        }

        if (!ReusableFunctionalities.scan(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan barcode in the storage process";
            return false;
        }

        try
        {
            specificCaseText = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnsStoragePopupAlert());
            Narrator.stepPassedWithScreenShot("Successfully scanned the barcode");
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve popup text";
            return false;
        }

        if (specificCaseText.contains("This is a return!"))
        {
            Narrator.stepPassedWithScreenShot("Successfully validated returns barcode in storage process");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton());
        } else
        {
            error = "Empty string";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the back arrow button after popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back arrow button after popup";
            return false;
        }

        return true;
    }

    public boolean dashboardScan()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the checkin button";
            return false;
        }

        AppiumDriverInstance.pause(3000);

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            if (SettersAndGetters.getReturnsLabelValue() == null)
            {
                error = "Could not print label value";
                return false;
            } else
            {
                error = "Failed to scan label value";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Scanned on dashboard");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moveToCarrierContainerButton(), 10))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moveToCarrierContainerButton()))
            {
                error = "Failed to click on the move to carrier container button";
                return false;
            }
            Narrator.stepPassedWithScreenShot("Select move contatiner to carrier");

            if (!ReusableFunctionalities.allocatingContainer())
            {
                error = "Failed to put return in container";
                return false;
            }
        } else
        {
            error = "App took too long to display layout";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to find the allocate new container button";
            return false;
        }

        if (!Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan the container barcode";
            return false;
        }

        validationList = new String[]
        {
            "SCANNED_TO_DESPATCH_CONTAINER", "AT_GOODS_OUT"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabaseUsingLabelValue(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }
        return true;
    }

    public boolean despatchSelectedContatinersFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch the checkin button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Other") + ".";
            return false;
        }

        narrator.stepPassedWithScreenShot("Carrier selection");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to click Other tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Checking available containers");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!ReusableFunctionalities.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan newly allocated container";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchOfCage()))
        {
            error = "Failed to wait for the 'Confirm dispatch of x cages' button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected container- " + SettersAndGetters.getGeneratedShelfCode() + " " + "Ready for despatch");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchOfCage()))
        {
            error = "Failed to touch on the 'Confirm dispatch of x cages' button ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Dispatch parcel to carrier completed");

        validationList = new String[]
        {
            "DESPATCHED", "SENT_TO_RETAILER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabaseUsingLabelValue(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean specificCaseCon1321Flow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Touched menu");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to wait for the go back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to click the go back button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the go back button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Tapped on go back button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked on menu button and selected another flow");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
            {
                error = "Failed to click the cancel all returns button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Selected reason customer changed mind option");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
            {
                error = "Failed to touch the 'yes' button on the popup message.";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
            {
                error = "Failed to click the more returns no button";
                return false;
            }
        }

        validationList = new String[]
        {
            "VOID", "CANCELLED_HANDOVER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;

    }

    public boolean preadvicePreBookReturns()
    {
        retailer = testData.getData("retailerId");
        clientVersion = testData.getData("clientVersion");
        storeID = testData.getData("storeId");
        orderID = testData.getData("orderId");
        deviceIdentifier = testData.getData("deviceIdentifier");
        email = testData.getData("Email");

        Shortcuts.returnsBearerToken();
        Shortcuts.prebookReturn(clientVersion, deviceIdentifier, email, storeID, retailer, orderID);

        Shortcuts.newBearer();

        return true;
    }

    public boolean preBookedReturnFlow()
    {
        validationList = new String[]
        {
            "BOOKING_MADE", "EXPECTED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to wait for the returns tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        //Enter Email
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        //Scan reference code   
        Narrator.stepPassed("Scanning barcode - " + SettersAndGetters.getBarCodeRefID());

        if (!SaveBarcodesFromURL.saveLabel(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to save the email barcode";
            return false;
        }

        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to scan email";
            return false;
        }

        return true;
    }

    public boolean cancellingReturns()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Faield to click the confirm and print label button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected the confrim and print label option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to wait for the printer option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to click the dvt printer";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to wait for select printer button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to click the select printer button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 2))
        {
            Narrator.stepPassedWithScreenShot("Printer error");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
            {
                error = "Failed to dismiss the error message";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
            {
                error = "Failed to click the retry button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
            {
                error = "Failed to click the retry button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("clicked retry");
        }

        String incorrectBarcode = MobileDoddlePageObjects.barcode();

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the 'scan the printed label' screen to appear";
            return false;
        }

        if (!SaveBarcodesFromURL.saveLabel(incorrectBarcode))
        {
            error = "Failed to save the incorrect barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(incorrectBarcode))
        {
            error = "Failed to scan incorrect barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Scanned incorrect barcode");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to wait for the try again button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to click the try again button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the scan the printed label text";
            return false;
        }

        try
        {

            Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

            String bearer = SettersAndGetters.getNewBearer();
            String apiResponse = SettersAndGetters.getAPIResponse();

            //Getting LabelValue
            apiResponse = SettersAndGetters.getAPIResponse();
            regex = "\"labelValue\":\"";
            lines = apiResponse.split(regex);
            liness = lines[1].split("\",\"url\":");
            apiResponse = liness[0];
            LabelValue = apiResponse;
            SettersAndGetters.setReturnsLabelValue(LabelValue);
            System.out.println(LabelValue);

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve the label value from couchbase";
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!specificCaseCon1321Flow())
        {
            error = "Failed to cancel the returns";
            return false;
        }

        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return.LabelValue;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return.validationList;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_23_Return_PreBookedReturn.wifiStatus;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_30_Return_SingleContainer.error;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_31_Return_ExistingDispatchSack.onscreenDPSBarcode;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_31_Return_ExistingDispatchSack.validationList;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_33_Return_BoxIsFull.dispatchSackBarcode;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_33_Return_BoxIsFull.validationList;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_34_Return_NewDispatchSack.dispatchSackBarcode;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_34_Return_NewDispatchSack.validationList;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.retrieveItemIDFromCouchBase;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanShelfBarcode;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author hduplessis
 */
public class TC_NextGen_23_1_PreBookedReturn extends BaseClass
{

    Narrator narrator;
    static String onscreenDPSBarcode = "";
    static String boxIsFullBarcode = "";
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    String value;
    String itemReturn;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    static String dispatchSackBarcode;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();
    Shortcuts shortcut = new Shortcuts();
    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String DCBarcode = "";
    public String barcodeName = "";
    public String noContainersText;
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public boolean restart;
    public boolean conErorr;
    //==========================Veribles for custom methods=====================
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String text;
    String correctBarcode;
    String newBearer, line;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL = "";
    String destinationFile = "";
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    String temp2 = "";
    public static String[] validationList;
    WebElement barcodeVal;
    public List<MobileElement> itemHolder;
    List<MobileElement> containerList;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();

    String specificCaseText;
    String retailer;
    String clientVersion;
    String deviceIdentifier;
    String storeID;
    String orderID;
    String email;
    String printer = Shortcuts.getPrinter();

    //==========================================================================
    public TC_NextGen_23_1_PreBookedReturn(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
            {
                if (!ReusableFunctionalities.Login())
                {
                    return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                }

                // 1st flow cancelling 
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return - " + error);
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return - " + error);
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess - " + error);
                }

                if (!tapNowFlow())
                {
                    return narrator.testFailedScreenshot("Failed to tap the now button - " + error);
                }

                if (!cancelDespatch())
                {
                    return narrator.testFailedScreenshot("Failed to do the tap later process with single container - " + error);
                }

                //2nd Flow scan existing despatch            
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return - " + error);
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

                if (!tapNowFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the tap now flow" + error);
                }

                if (!existingDispatchScanBarcode())
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with existing dispatch sack - " + error);
                }

                //3rd Flow item too large
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

                if (!itemTooLarge())
                {
                    return narrator.testFailedScreenshot("Failed to do the item too large returns proccess" + error);
                }

                //4th Flow Box is full
                if (!checkContainers())
                {
                    return narrator.testFailedScreenshot("Failed to check containers - " + error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!boxIsFullFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the box is full flow" + error);
                }

                if (!checkContainers())
                {
                    return narrator.testFailedScreenshot("Failed to do the returns process with box is full option - " + error);
                }

                //5th Flow new despatch sack        
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

                if (!tapNowFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the tap now flow" + error);
                }

                if (!newDispatchSackScanBarcode())
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with existing dispatch sack - " + error);
                }

                //6th Flow Cancelling
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

            } else
            {
                // 1st flow cancelling 
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

                if (!tapLaterFlow())
                {
                    return narrator.testFailedScreenshot("Failed to tap the now button");
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

                if (!tapNowFlow())
                {
                    return narrator.testFailedScreenshot("Failed to tap the now button - " + error);
                }

                if (!cancel())
                {
                    return narrator.testFailedScreenshot("Failed to cancel the returns" + error);
                }

                //2nd Flow scan existing despatch            
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

                if (!tapNowFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the tap now flow" + error);
                }

                if (!existingDispatchScanBarcode())
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with existing dispatch sack - " + error);
                }

                //3rd Flow item too large
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

                if (!itemTooLarge())
                {
                    return narrator.testFailedScreenshot("Failed to do the item too large returns proccess" + error);
                }

                //4th Flow Box is full
                if (!checkContainers())
                {
                    return narrator.testFailedScreenshot("Failed to check containers - " + error);
                }

                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the prebooked returns flow" + error);
                }

                if (!boxIsFullFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the box is full flow" + error);
                }

                if (!checkContainers())
                {
                    return narrator.testFailedScreenshot("Failed to do the returns process with box is full option - " + error);
                }

                //5th Flow new despatch sack        
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

                if (!tapNowFlow())
                {
                    return narrator.testFailedScreenshot("Failed to do the tap now flow" + error);
                }

                if (!newDispatchSackScanBarcode())
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with existing dispatch sack - " + error);
                }

                //6th Flow Cancelling
                if (!preadvicePreBookReturns())
                {
                    return narrator.testFailedScreenshot("Failed to preadvise a pre booked return");
                }

                if (!ReusableFunctionalities.initiatePreBookedReturn())
                {
                    return narrator.testFailedScreenshot("Failed to initiate pre booked return");
                }

                if (!completeReturns())
                {
                    return narrator.testFailedScreenshot("Failed to complete the returns proccess");
                }

            }
            return narrator.finalizeTest("Successfully completed TC_NEXTGEN 23.1 (Single Container)");
        } //return narrator.finalizeTest("Successfully completed TC_NEXTGEN 23.1 (Single Container)");
        else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    public boolean tapLaterFlow()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLaterButton()))
        {
            error = "Failed to wait for the returns later button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsLaterButton()))
        {
            error = "Failed to clicks the returns later button";
            return false;
        }

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase" + ReusableFunctionalities.error;
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        validationList = new String[]
        {
            "ASSIGN_TO_CONTAINER_DEFERRED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean existingDispatchSackCancel()
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
        {
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
            {
                error = "Failed to wait for the returns done button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo()))
        {
            Narrator.stepPassedWithScreenShot("Before clicking cancel");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo()))
            {
                error = "Failed to click the parcel cancel button";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Successfully cancelled the parcel dispatch");

        return true;
    }

    public boolean existingDispatchScanBarcode()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeUpANewDispatchSackLayout(), 2))
        {
            temp2 = MobileDoddlePageObjects.rand();
            SaveBarcodesFromURL.saveShelfLabel(temp2);
            Shortcuts.scanShelfBarcode(temp2);

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to find the done button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the done button";
                return false;
            }
        }

        if (!save.saveReturnsBarcode(temp))
        {
            error = "Failed to save barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan incorrect barcode";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchErrorSack()))
            {
                error = "Failed to wait for the error message";
                return false;
            }

            String errorMessage = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchErrorSack());

            if (!errorMessage.contains("Please scan dispatch sack"))
            {
                error = "Failed to validate error message";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
            {
                error = "Failed to click the ok button";
                return false;
            }

            onscreenDPSBarcode = "";
            onscreenDPSBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnSingleContainerBarcode());
            int index = onscreenDPSBarcode.indexOf(".");
            onscreenDPSBarcode = onscreenDPSBarcode.substring(index + 1).trim();

            if (!save.saveReturnsBarcode(onscreenDPSBarcode))
            {
                error = "Failed to generate onscreen barcode";
                return false;
            }

        } catch (Exception e)
        {
            error = "Failed to retrieve barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(onscreenDPSBarcode))
        {
            error = "Failed to scan barocode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the back button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully scanned the despatch sack - " + onscreenDPSBarcode);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back button";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!ReusableFunctionalities.scanBarcode(onscreenDPSBarcode))
        {
            error = "Failed to scan barocode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
        {
            error = "Failed to wait for the dispatch returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
        {
            error = "Failed to click the dispatch returns done button";
            return false;
        }

        return true;
    }

    public boolean tapNowFlow()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard(), 2))
        {
            error = "Container config failed to change, config currently on MULTIPLE_CONTAINERS";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the returns now button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to clicks the returns now button";
            return false;
        }

        return true;
    }

    public boolean cancel()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
            {
                error = "Failed to click the cancel button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo()))
            {
                error = "Failed to click the cancel button";
                return false;
            }
        }

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase" + ReusableFunctionalities.error;
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        validationList = new String[]
        {
            "ASSIGN_TO_CONTAINER_DEFERRED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean itemTooLarge()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            Narrator.stepPassedWithScreenShot("Creating new despatch sack - " + SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsItemTooLarge()))
        {
            error = "Failed to wait for the item too large button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemTooLarge()))
        {
            error = "Failed to click the item too large button";
            return false;
        }

        try
        {
            AppiumDriverInstance.pause(1000);

            String itemDescription = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnsItemTooLargeDescription2());

            Narrator.stepPassedWithScreenShot("Item Description : " + itemDescription);
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to validate item too large description";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemTooLargeCloseButton()))
        {
            error = "failed to click the close button";
            return false;
        }

        return true;
    }

    //Box is full Methods*
    public boolean boxIsFullFlowCancel()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {

            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            Narrator.stepPassedWithScreenShot("Creating new despatch sack - " + SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
        {
            error = "Failed to wait for the box is full button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
        {
            error = "Failed to click the box is full button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.sealTheDispatchSackText()))
        {
            error = "Failed to wait for the dispatch layout";
            return false;
        }

        String sackToSeal = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.boxIsFullBarcode());

        if (!SaveBarcodesFromURL.saveShelfLabel(sackToSeal))
        {
            error = "Failed to save shelfLabel";
            return false;
        }

        if (!ReusableFunctionalities.scanShelfBarcode(sackToSeal))
        {
            error = "Failed to scan temp label barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Completed box is full process");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.boxIsFullCancelButton()))
        {
            error = "Failed to click the dispatch cancel button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully canceled the despatch process");

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean boxIsFullFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {

            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            Narrator.stepPassedWithScreenShot("Creating new despatch sack - " + SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
        {
            error = "Failed to wait for the box is full button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked box is full");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
        {
            error = "Failed to click the box is full button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.sealTheDispatchSackText()))
        {
            error = "Failed to wait for the dispatch layout";
            return false;
        }

        String sackToSeal = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.boxIsFullBarcode());

        if (!SaveBarcodesFromURL.saveShelfLabel(sackToSeal))
        {
            error = "Failed to save shelfLabel";
            return false;
        }

        if (!ReusableFunctionalities.scanShelfBarcode(sackToSeal))
        {
            error = "Failed to scan temp label barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Completed box is full process");

        validationList = new String[]
        {
            "CONTAINER_FULL", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkLoadRecordsInDatabase(validationList))
        {
            error = "Failed to validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
        {
            error = "Failed to put the parcel in the despatch sack";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked on the cancel button");

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase" + ReusableFunctionalities.error;
            return false;
        }

        validationList = new String[]
        {
            "ASSIGN_TO_CONTAINER_DEFERRED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean newDispatchSackScanBarcode()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tasksNotificationBell()))
        {
            error = "Failed to click the notification bell";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the toast notification bell");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.completeTaskButton()))
        {
            error = "Failed to wait for the complete task button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.completeTaskButton()))
        {
            error = "Failed toclick the complete task button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked complete task button");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBoxIsFull(), 3))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
            {
                error = "Failed to click the box is full button";
                return false;
            }

            try
            {
                dispatchSackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchSackBarcode());
            } catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to retrieve dispatch sack barcode";
                return false;
            }

            if (!save.saveReturnsBarcode(dispatchSackBarcode))
            {
                error = "Failed to save dispatch sack barcode";
                return false;
            }

            if (!ReusableFunctionalities.scanBarcode(dispatchSackBarcode))
            {
                error = "Failed to scan barcode";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeUpANewDispatchSackLayout()))
        {
            error = "Failed to wait for the new despatch sack layout";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Make up a new despatch screen");

        if (!save.saveReturnsBarcode(temp))
        {
            error = "Failed to save dispatch sack barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 15))
            {
                error = "Failed to wait for the progress cicle to be no longer present";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Successfully created a new despatch sack - " + temp);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to wait for the done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to click the done button";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsFinishDispatchDoneButton()))
        {
            error = "Failed to wait for the done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsFinishDispatchDoneButton()))
        {
            error = "Failed to click the done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.closeTaskButton()))
        {
            error = "Failed to wait for the clost task button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Return no longer present in tasks, it was successfully put in a despatch sack");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.closeTaskButton()))
        {
            error = "Failed to click the clost task button";
            return false;
        }

        if (!ReusableFunctionalities.returnsBackToDashboard())
        {
            error = "Failed to navigate to dashboard";
            return false;
        }

        return true;
    }

    public boolean cancelDespatch()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
        {
            error = "Failed to wait for the cancel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
        {
            error = "Failed to click the cancel button";
            return false;
        }

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            "ASSIGN_TO_CONTAINER_DEFERRED"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully cancelled the pre booked return");

        return true;
    }

    public boolean checkContainers()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'carriers' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("DispatchCarrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.manageContainersBtn()))
        {
            error = "Failed to click Manage Containers";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.manageContainersBtn()))
        {
            error = "Failed to click Manage Containers";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.removeContainers()))
        {
            error = "Failed to wait 'Remove Containers' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.removeContainers()))
        {
            error = "Failed to touch 'Remove Containers' button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.noContainersText(), 2))
        {

            for (int i = 0; i < 4; i++)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
                    {
                        error = "Failed to click back Arrow";
                        return false;
                    }
                }

            }
        } else
        {
            try
            {
//                itemHolder.add((MobileElement) AppiumDriverInstance.Driver.findElements(By.xpath(MobileDoddlePageObjects.containerItemHolder())));
                itemHolder = AppiumDriverUtility.Driver.findElements(By.xpath(MobileDoddlePageObjects.containerItemHolder()));
            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

            for (int i = 0; i < itemHolder.size(); i++)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemContainerSelectButton(i)))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemContainerSelectButton(i)))
                    {
                        error = "Failed to click the select button";
                        return false;
                    }
                }
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmDeletionOfContainers()))
            {
                error = "Failed to wait for the remove container button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmDeletionOfContainers()))
            {
                error = "Failed to click the remove container button";
                return false;
            }

            for (int i = 0; i < 4; i++)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
                    {
                        error = "Failed to click back Arrow";
                        return false;
                    }
                }
            }
        }

        return true;
    }
    //* Box is full methods end

    public boolean newDispatchSackScanBarcode2()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBoxIsFull(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
            {
                error = "Failed to click the box is full button";
                return false;
            }

            try
            {
                dispatchSackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchSackBarcode());
            } catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to retrieve dispatch sack barcode";
                return false;
            }

            if (!save.saveReturnsBarcode(dispatchSackBarcode))
            {
                error = "Failed to save dispatch sack barcode";
                return false;
            }

            if (!ReusableFunctionalities.scanBarcode(dispatchSackBarcode))
            {
                error = "Failed to scan barcode";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeUpANewDispatchSackLayout()))
            {
                error = "Failed to wait for the new despatch sack layout";
                return false;
            }

            if (!save.saveReturnsBarcode(temp))
            {
                error = "Failed to save dispatch sack barcode";
                return false;
            }

            if (!ReusableFunctionalities.scanBarcode(temp))
            {
                error = "Failed to scan barcode";
                return false;
            }

        } else
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeUpANewDispatchSackLayout()))
            {
                error = "Failed to wait for the new despatch sack layout";
                return false;
            }

            if (!save.saveReturnsBarcode(temp))
            {
                error = "Failed to save dispatch sack barcode";
                return false;
            }

            if (!ReusableFunctionalities.scanBarcode(temp))
            {
                error = "Failed to scan barcode";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to wait for the done button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the done button";
                return false;
            }

            AppiumDriverInstance.pause(1000);

            if (!ReusableFunctionalities.scanBarcode(temp))
            {
                error = "Failed to scan barcode";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to wait for the done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to click the done button";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsFinishDispatchDoneButton()))
        {
            error = "Failed to wait for the done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsFinishDispatchDoneButton()))
        {
            error = "Failed to click the done button";
            return false;
        }

        return true;
    }

    //Cancel Returns
    public boolean cancelReturnUsingAll()
    {

        if (!cancelReturn())
        {
            error = "Failed to do the returns process";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to wait for the do you want to cancel CURRENT return or ALL returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to click the cancel button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the cancel returns all button");

        Narrator.stepPassed("Successfully selected ALL, cancel returns booking screen appeared");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        Narrator.stepPassed("Selected more returns yes");

        AppiumDriverInstance.pause(3000);

        if (!cancelReturn())
        {
            error = "Failed to do the returns process";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to wait for the do you want to cancel CURRENT return or ALL returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to click the cancel button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the cancel returns all button");

        Narrator.stepPassed("Successfully selected ALL, cancel returns booking screen appeared");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected more returns no");

        validationList = new String[]
        {
            "VOID", "CANCELLED_HANDOVER", "reason"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean cancelReturn()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton(), 1))
        {
            if (!ReusableFunctionalities.returnsBackToDashboard())
            {
                error = "Failed to navigate back to dashboard";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
            {
                error = "Failed to touch 'Returns' button.";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to wait for the 'New Rerurn' button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to touch on the 'New Return' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), 2))
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
            {
                error = "Failed to wait for the  'Customer Acc Number' textfield";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
            {
                error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
                return false;
            }
            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
            {
                error = "Failed to touch the 'RMA Number' textfield";
                return false;
            }
            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
            {
                error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to wait for the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to click the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
            {
                error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
            {
                error = "Failed to touch and expand the 'reason' dropdown";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to wait for the 'Reasons'list to appear.";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to touch the 'Reason' for item return";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField(), 2))
            {

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
                {
                    error = "Failed to wait for the  Enter Carrier order id textfield";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
                {
                    error = "Failed to touch the  Enter Carrier order id textfield";
                    return false;
                }
                if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
                {
                    error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
                    return false;
                }

                Driver.pressKeyCode(AndroidKeyCode.BACK);

                narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
                {
                    error = "Failed to wait for the confirm and print label button";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
                {
                    error = "Failed to touch the  'Search' Button.";
                    return false;
                }
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 15))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 2))
            {
                Narrator.stepPassedWithScreenShot("Printer error");

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
                {
                    error = "Failed to dismiss the error message";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
                {
                    error = "Failed to click the retry button";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
                {
                    error = "Failed to click the retry button";
                    return false;
                }

                Narrator.stepPassedWithScreenShot("clicked retry");
            }

            try
            {
                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
            {
                error = "Failed to wait for the  'Customer Acc Number' textfield";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
            {
                error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
                return false;
            }
            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
            {
                error = "Failed to touch the 'RMA Number' textfield";
                return false;
            }
            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
            {
                error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to wait for the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to click the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
            {
                error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
            {
                error = "Failed to touch and expand the 'reason' dropdown";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to wait for the 'Reasons'list to appear.";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to touch the 'Reason' for item return";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            AppiumDriverInstance.pause(1000);

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to touch Menu";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Returns Popup");

        return true;
    }

    public boolean preBookedReturnFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsTile()))
        {
            error = "Failed to wait for the returns tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsTile()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to wait the PreBook Button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to click the PreBook Button";
            return false;
        }
        //TODO:
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the pre booked return button");

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter the email in the search field";
            return false;
        }
        //TODO: select preadvice return

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to wait for the printer option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to click the dvt printer";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to wait for select printer button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to click the select printer button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 2))
        {
            Narrator.stepPassedWithScreenShot("Printer error");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
            {
                error = "Failed to dismiss the error message";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
            {
                error = "Failed to click the retry button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
            {
                error = "Failed to click the retry button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("clicked retry");
        }

        //TODO:G2- Label Scan
        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed(3)";
            return false;
        }

        return true;
    }

    public boolean completeReturns()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Faield to click the confirm and print label button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected the confrim and print label option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to wait for the printer option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to click the dvt printer";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to wait for select printer button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to click the select printer button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 2))
        {
            Narrator.stepPassedWithScreenShot("Printer error");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
            {
                error = "Failed to dismiss the error message";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
            {
                error = "Failed to click the retry button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
            {
                error = "Failed to click the retry button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("clicked retry");
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 6))
        {
            if (!tempLabelFlow())
            {
                error = "Failed to do the temp label flow";
                return false;
            }
        } else
        {
            if (!generatedLabelFlow())
            {
                error = "Failed to do the generated label flow";
                return false;
            }

        }

        return true;
    }

    public boolean generatedLabelFlow()
    {
        String incorrectBarcode = MobileDoddlePageObjects.barcode();

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the 'scan the printed label' screen to appear";
            return false;
        }

        if (!SaveBarcodesFromURL.saveLabel(incorrectBarcode))
        {
            error = "Failed to save the incorrect barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(incorrectBarcode))
        {
            error = "Failed to scan incorrect barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Scanned incorrect barcode");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to wait for the try again button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to click the try again button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the scan the printed label text";
            return false;
        }

        try
        {

            Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

            String bearer = SettersAndGetters.getNewBearer();
            String apiResponse = SettersAndGetters.getAPIResponse();

            //Getting LabelValue
            apiResponse = SettersAndGetters.getAPIResponse();
            regex = "\"labelValue\":\"";
            lines = apiResponse.split(regex);
            liness = lines[1].split("\",\"url\":");
            apiResponse = liness[0];
            LabelValue = apiResponse;
            SettersAndGetters.setReturnsLabelValue(LabelValue);
            System.out.println(LabelValue);

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve the label value from couchbase";
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed(3)";
            return false;
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the done button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the 'any more returns' message to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the no button";
            return false;
        }

        validationList = new String[]
        {
            "CHECKOUT_COMPLETED", "AT_COLLECTION_POINT"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        return true;
    }

    public boolean cancelStoringReturn()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo()))
        {
            error = "Failed to find the cancel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo()))
        {
            error = "Failed to click the despatch button";
            return false;
        }

        return true;
    }

    public boolean tempLabelFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
        {
            Narrator.stepPassed("Back button not present");
        }

        if (!SaveBarcodesFromURL.saveShelfLabel(temp))
        {
            error = "Failed to save shelfLabel";
            return false;
        }

        if (!ReusableFunctionalities.scanShelfBarcode(temp))
        {
            error = "Failed to scan temp label barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 8))
        {
            error = "App Crash!";
            return false;
        }

        if (!ReusableFunctionalities.wifiOn())
        {
            error = "Failed to turn the wifi on";
            return false;
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED", "ADDED_RETURN_LABEL"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 5))
        {
            error = "Failed because of app crash";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 2))
        {
            Narrator.stepPassed("Back button not present");
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLaterButton(), 2))
            {
                error = "Tap now/later buttons appeared, container config failed to change";
                return false;
            }

            error = "failed to wait for the returns back to dashboard button";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        validationList = new String[]
        {
            "CHECKOUT_COMPLETED", "AT_COLLECTION_POINT"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        AppiumDriverInstance.pause(1500);

        if (!ReusableFunctionalities.checkWifiIsOn())
        {
            error = "Failed to switch wifi on";
            return false;
        }

        wifiStatus = true;

        return true;
    }

    public boolean preadvicePreBookReturns()
    {
        retailer = testData.getData("retailerId");
        clientVersion = testData.getData("clientVersion");
        storeID = testData.getData("storeId");
        orderID = testData.getData("orderId");
        deviceIdentifier = testData.getData("deviceIdentifier");
        email = testData.getData("Email");

        Shortcuts.returnsBearerToken();
        Shortcuts.prebookReturn(clientVersion, deviceIdentifier, email, storeID, retailer, orderID);

        Shortcuts.newBearer();

        return true;
    }

    public boolean retrieveContainers()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'carriers' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("DispatchCarrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchParcel(), 2))
        {

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
            {
                error = "Failed to touch on the DHL tile.";
                return false;
            }

            for (int j = 0; j < 3; j++)
            {
                Driver.swipe(237, 509, 237, 262, 2000);
                containerList.addAll(AppiumDriverUtility.Driver.findElements(By.xpath(MobileDoddlePageObjects.containerID())));
            }
        }

        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }

        return true;
    }

    public boolean cancellingReturns()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to wait for the go back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to click the go back button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Tapped on go back button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked on menu button and selected another flow");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
            {
                error = "Failed to click the cancel all returns button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Selected reason customer changed mind option");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
            {
                error = "Failed to touch the 'yes' button on the popup message.";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
            {
                error = "Failed to click the more returns no button";
                return false;
            }
        }

        validationList = new String[]
        {
            "VOID", "CANCELLED_HANDOVER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean specificCaseCON1589()
    {

        if (!preadvicePreBookReturns())
        {
            error = "Failed to preadvice a pre booked return";
            return false;
        }

        if (!ReusableFunctionalities.initiatePreBookedReturn())
        {
            error = "Failed to do the prebooked returns process";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Faield to click the confirm and print label button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected the confrim and print label option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to wait for the printer option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
        {
            error = "Failed to click the dvt printer";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to wait for select printer button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to click the select printer button";
            return false;
        }

        String incorrectBarcode = MobileDoddlePageObjects.barcode();

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the 'scan the printed label' screen to appear";
            return false;
        }

        if (!SaveBarcodesFromURL.saveLabel(incorrectBarcode))
        {
            error = "Failed to save the incorrect barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(incorrectBarcode))
        {
            error = "Failed to scan incorrect barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Scanned incorrect barcode");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to wait for the try again button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to click the try again button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the scan the printed label text";
            return false;
        }

        try
        {

            Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

            String bearer = SettersAndGetters.getNewBearer();
            String apiResponse = SettersAndGetters.getAPIResponse();

            //Getting LabelValue
            apiResponse = SettersAndGetters.getAPIResponse();
            regex = "\"labelValue\":\"";
            lines = apiResponse.split(regex);
            liness = lines[1].split("\",\"url\":");
            apiResponse = liness[0];
            LabelValue = apiResponse;
            SettersAndGetters.setReturnsLabelValue(LabelValue);
            System.out.println(LabelValue);

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve the label value from couchbase";
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed(3)";
            return false;
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the done button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the 'any more returns' message to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the no button";
            return false;
        }

        if (!preadvicePreBookReturns())
        {
            error = "Failed to preadvice a pre booked return";
            return false;
        }

        if (!ReusableFunctionalities.initiatePreBookedReturn())
        {
            error = "Failed to do the prebooked returns process";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        //Scan reference code   
        Narrator.stepPassed("Scanning barcode - " + SettersAndGetters.getBarCodeRefID());

        if (!SaveBarcodesFromURL.saveLabel(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to save the email barcode";
            return false;
        }

        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to scan email";
            return false;
        }

        return true;
    }
}

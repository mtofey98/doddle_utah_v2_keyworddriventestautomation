/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;

/**
 *
 * @author mtofey
 */
public class TC_NextGen_59_Collection_Search_By_UPI_With_Cust_Block extends BaseClass
{

    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();

    public TC_NextGen_59_Collection_Search_By_UPI_With_Cust_Block(TestEntity testData)
    {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
            }

            if (!executePreCondition())
            {
                return narrator.testFailed("Failed to execute the test precondition");
            }
            
            if (!workerMethod())
            {
                return narrator.testFailed("Failed to search for a customer, with customer block, using collection code");
            }

            return narrator.finalizeTest("Successfully searched for a collection using collection code");
        }
        else
        {
            if (!executePreCondition())
            {
                return narrator.testFailed("Failed to execute the test precondition");
            }
            
            if (!workerMethod())
            {
                return narrator.testFailed("Failed to search for a customer, with customer block, using collection code");
            }

            return narrator.finalizeTest("Successfully searched for a collection using collection code");

        }
    }

    public boolean executePreCondition()
    {
        try
        {
           if(!ReusableFunctionalities.createCollectionBarcodeWithCustomerBlock())
           {
               error = ReusableFunctionalities.error;
               return false;
           }
           
           SaveBarcodesFromURL.saveLabel(SettersAndGetters.getGeneratedBarCode());
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
            {
                error = "Failed to click on the collections tile";
                return false;
            }
            
            if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
            {
                error = "Failed to wait for the other search options button";
                return false;
            }
            
            if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
            {
                error = "Failed to wait for the other search options button";
                return false;
            }

            if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionUPI()))
            {
                error = "Failed to wait for the UPI button to appear";
                return false;
            }

            if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionUPI()))
            {
                error = "Failed to click on the UPI button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected UPI");
            
            if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
            {
                error = "Failed to wait for the UPI input field to appear";
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            error = e.getMessage();
            narrator.logFailure("Failed to execute precondition");
            narrator.logError(e.getMessage());
            System.out.println("Failed to execute precondition");
            return false;
        }
    }

    public boolean workerMethod()
    {
        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan the barcode";
            return false;
        }

         Narrator.stepPassed("Sucesffully scanned barcode");
        
                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu(), 120))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if(!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to dashboard";
            return false;
        }
        
        Shortcuts.doCollectionsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

        if(!Shortcuts.line.contains("\"searchType\":\"labelValue\""))
        {
            error = "\"searchType\":\"labelValue\" was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("\"searchType\":\"labelValue\""))
        {
            error = "could not change API response colour :" + Shortcuts.changeFailedAPIResponseColor("\"searchType\":\"labelValue\"");
            return false;
        }
        
        Narrator.stepPassed(Shortcuts.line);

        return true;
    }
}

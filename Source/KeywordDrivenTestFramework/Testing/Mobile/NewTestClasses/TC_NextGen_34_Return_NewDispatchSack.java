/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_34_Return_NewDispatchSack extends BaseClass
{

    Narrator narrator;
    static String onscreenDPSBarcode = "";
    static String boxIsFullBarcode = "";
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    String value;
    String itemReturn;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    static String dispatchSackBarcode;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();
    Shortcuts shortcut = new Shortcuts();
    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String DCBarcode = "";
    public String barcodeName = "";
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public boolean restart;
    public boolean conErorr;
    //==========================Veribles for custom methods=====================
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String text;
    String correctBarcode;
    String newBearer, line;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL = "";
    String destinationFile = "";
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    String temp2 = "";
    public static String[] validationList;
    WebElement barcodeVal;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();

    //==========================================================================
    public TC_NextGen_34_Return_NewDispatchSack(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
         if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to do the login process");
            }

            if (!ReusableFunctionalities.singleContainerExclusivePoshTotty())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
            }

            if (!newDispatchSackScanBarcodeCancel())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process making a new dispatch sack - " + error);
            }
          
            validationList = new String[]
            {
                "CREATED", "ACTIVE", "ASSIGN_TO_CONTAINER_DEFERRED"
            };

            if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error);
            }

            validationList = new String[]
            {
                ""
            };
            
             return narrator.finalizeTest("Successfully completed the 'Make up a new Despatch Sack process'");
        }
        else
        {            
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
            {
                return narrator.testFailedScreenshot("Failed to click the cancel button");
            }

            validationList = new String[]
            {
                "CREATED", "ACTIVE", "ASSIGN_TO_CONTAINER_DEFERRED"
            };

            if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error);
            }

            validationList = new String[]
            {
                ""
            };

            if (!ReusableFunctionalities.singleContainerExclusivePoshTotty())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
            }

            if (!newDispatchSackScanBarcodeCancel())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process making a new dispatch sack - " + error);
            }        
        }

        return narrator.finalizeTest("Successfully completed the 'Make up a new Despatch Sack process'");
        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    public boolean newDispatchSackScanBarcodeCancel()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Clicked the now button");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBoxIsFull(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
            {
                error = "Failed to click the box is full button";
                return false;
            }

            try
            {
                dispatchSackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchSackBarcode());
            }
            catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to retrieve dispatch sack barcode";
                return false;
            }

            if (!save.saveReturnsBarcode(dispatchSackBarcode))
            {
                error = "Failed to save dispatch sack barcode";
                return false;
            }

            if (!ReusableFunctionalities.scanBarcode(dispatchSackBarcode))
            {
                error = "Failed to scan barcode";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeUpANewDispatchSackLayout()))
        {
            error = "Failed to wait for the new despatch sack layout";
            return false;
        }

        if (!save.saveReturnsBarcode(temp))
        {
            error = "Failed to save dispatch sack barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan barcode";
            return false;
        }

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkLoadRecordsInDatabase(validationList))
        {
           error = "failed to do validations";
           return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to wait for the done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to click the done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsItemTooLarge()))
        {
            error = "Failed to load the put return in container screen";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Put return in container screen appeared");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo()))
        {
            error = "Failed to click the cancel button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Cancelled the return");


        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        validationList = new String[]
        {
            "ASSIGN_TO_CONTAINER_DEFERRED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };           
            
        return true;
    }
    
    public boolean newDispatchSackScanBarcode()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Clicked the now button");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBoxIsFull(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
            {
                error = "Failed to click the box is full button";
                return false;
            }

            try
            {
                dispatchSackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchSackBarcode());
            }
            catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to retrieve dispatch sack barcode";
                return false;
            }

            if (!save.saveReturnsBarcode(dispatchSackBarcode))
            {
                error = "Failed to save dispatch sack barcode";
                return false;
            }

            if (!ReusableFunctionalities.scanBarcode(dispatchSackBarcode))
            {
                error = "Failed to scan barcode";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeUpANewDispatchSackLayout()))
        {
            error = "Failed to wait for the new despatch sack layout";
            return false;
        }

        if (!save.saveReturnsBarcode(temp))
        {
            error = "Failed to save dispatch sack barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan barcode";
            return false;
        }

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkLoadRecordsInDatabase(validationList))
        {
           error = "failed to do validations";
           return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to wait for the done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to click the done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsItemTooLarge()))
        {
            error = "Failed to load the put return in container screen";
            return false;
        }
        
        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan barcode";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsFinishDispatchDoneButton()))
        {
            error = "Failed to wait for the done button";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Scanned the new despatch sack");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsFinishDispatchDoneButton()))
        {
            error = "Failed to click the done button";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Clicked done");
        
        return true;
    }

    public boolean cancelDespatch()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo(), 2))
        {       
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
            {
                error = "Failed to click the box is full button";
                return false;
            }
            
            try
            {
                dispatchSackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchSackBarcode());
            } 

            catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to retrieve dispatch sack barcode";
                return false;
            }

            if (!save.saveReturnsBarcode(dispatchSackBarcode))
            {
                error = "Failed to save dispatch sack barcode";
                return false;
            }

            if (!ReusableFunctionalities.scanBarcode(dispatchSackBarcode))
            {
                error = "Failed to scan barcode";
                return false;
            }
        }
        
        Narrator.stepPassedWithScreenShot("Make up a new despatch sack layout");
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
        {
            error = "Failed to wait for the done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
        {
            error = "Failed to click the done button";
            return false;
        }
        
        Narrator.stepPassed("Touch cancel button");
        
        Narrator.stepPassedWithScreenShot("Successfully cancelled the creation of a new despatch sack");
        
        return true;
    }
    
    public boolean makeUpNewDepsatchSack()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo(), 2))
        {       
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
            {
                error = "Failed to click the box is full button";
                return false;
            }
            
            try
            {
                dispatchSackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchSackBarcode());
            } 

            catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to retrieve dispatch sack barcode";
                return false;
            }

            if (!save.saveReturnsBarcode(dispatchSackBarcode))
            {
                error = "Failed to save dispatch sack barcode";
                return false;
            }

            if (!ReusableFunctionalities.scanBarcode(dispatchSackBarcode))
            {
                error = "Failed to scan barcode";
                return false;
            }
            
            Narrator.stepPassedWithScreenShot("Successfully sealed the despatch sack");
        }
        
        Narrator.stepPassedWithScreenShot("Make up a new despatch sack layout");
        
        if (!save.saveReturnsBarcode(temp))
        {
            error = "Failed to save dispatch sack barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 15))
            {
                error = "Failed to wait for the progress cicle to be no longer present";
                return false;
            }
        }
        
        Narrator.stepPassedWithScreenShot("Successfully created a new despatch sack - " + temp);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to wait for the done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
        {
            error = "Failed to click the done button";
            return false;
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo(), 2))
        {
            Narrator.stepPassedWithScreenShot("Put return in conatiner screen");
        }

        AppiumDriverInstance.pause(1000);

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan barcode";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsFinishDispatchDoneButton()))
        {
            error = "Failed to wait for the done button";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Scanned the new despatch sack");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsFinishDispatchDoneButton()))
        {
            error = "Failed to click the done button";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Clicked done");
        
        
        return true;
    }

}

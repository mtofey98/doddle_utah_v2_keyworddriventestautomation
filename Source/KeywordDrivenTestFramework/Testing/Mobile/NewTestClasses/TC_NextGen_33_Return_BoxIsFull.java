/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanShelfBarcode;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import io.appium.java_client.MobileElement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_33_Return_BoxIsFull extends BaseClass
{

    Narrator narrator;
    static String onscreenDPSBarcode = "";
    static String boxIsFullBarcode = "";
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    String value;
    String itemReturn;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    static String dispatchSackBarcode;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();
    Shortcuts shortcut = new Shortcuts();
    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String DCBarcode = "";
    public String barcodeName = "";
    public String noContainersText;
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public boolean restart;
    public boolean conErorr;
    //==========================Veribles for custom methods=====================
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String text;
    String correctBarcode;
    String newBearer, line;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL = "";
    String destinationFile = "";
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    String temp2 = "";
    public static String[] validationList;
    WebElement barcodeVal;
    public List<MobileElement> itemHolder;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();

    //==========================================================================
    public TC_NextGen_33_Return_BoxIsFull(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
         if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {
        if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login" + error + Shortcuts.error);
            }
                
            if (!checkContainers())
            {
                return narrator.testFailedScreenshot("Failed to check the containers - " + error);
            }

            if (!ReusableFunctionalities.singleContainerExclusivePoshTotty()&& ReusableFunctionalities.isSeleniumFailure == false)
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
            }
            
            else if (ReusableFunctionalities.isSeleniumFailure)
            {
                return narrator.seleniumTestFailedScreenshot("Failed to retrieve item IDs" + ReusableFunctionalities.error);
            }

            if (!newDispatchSackCancel())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process making a new dispatch sack - " + error);
            }

            validationList = new String[]
            {
                "CREATED", "ACTIVE"
            };

            if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
            {
                return narrator.testFailedScreenshot("Failed to do the validations - " + ReusableFunctionalities.error);
            }

            validationList = new String[]
            {
                ""
            };

            validationList = new String[]
            {
                "ASSIGN_TO_CONTAINER_DEFERRED"
            };

            if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
            {
                return narrator.testFailedScreenshot("Failed to do the validations - " + ReusableFunctionalities.error);
            }

            validationList = new String[]
            {
                ""
            };

            if (!boxIsFullFlow())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process making a new dispatch sack - " + error);
            }
        }
        else
        {
            if (!checkContainers())
            {
                return narrator.testFailedScreenshot("Failed to check the containers - " + error);
            }

            if (!ReusableFunctionalities.singleContainerExclusivePoshTotty() && ReusableFunctionalities.isSeleniumFailure == false)
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
            }
            
            else if (ReusableFunctionalities.isSeleniumFailure)
            {
                return narrator.seleniumTestFailedScreenshot("Failed to retrieve item IDs" + ReusableFunctionalities.error);
            }

            if (!newDispatchSackCancel())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process making a new dispatch sack - " + error);
            }

            validationList = new String[]
            {
                "CREATED", "ACTIVE"
            };

            if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
            {
                return narrator.testFailedScreenshot("Failed to do the validations - " + ReusableFunctionalities.error);
            }

            validationList = new String[]
            {
                ""
            };

            validationList = new String[]
          
            {
                "ASSIGN_TO_CONTAINER_DEFERRED"
            };

            if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
            {
                return narrator.testFailedScreenshot("Failed to do the validations - " + ReusableFunctionalities.error);
            }

            validationList = new String[]
            {
                ""
            };

            if (!boxIsFullFlow())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process making a new dispatch sack - " + error);
            }
        }

        return narrator.finalizeTest("Successfully completed box is full flow");
        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    public boolean newDispatchSackCancel()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {
        
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
            
            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());
            
            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());
            
            Narrator.stepPassedWithScreenShot("Creating new despatch sack - " + SettersAndGetters.getGeneratedShelfCode());
            
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }
            
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }    

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
        {
            error = "Failed to wait for the box is full button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
        {
            error = "Failed to click the box is full button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.sealTheDispatchSackText()))
        {
            error = "Failed to wait for the dispatch layout";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Box is full present");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.boxIsFullCancelButton()))
        {
            error = "Failed to click the dispatch cancel button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully cancelled the sealing of depsatch sack");

        return true;
    }
    
    public boolean boxIsFullFlow()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tasksNotificationBell()))
        {
            error = "Failed to click the notification bell";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.completeTaskButton()))
        {
            error = "Failed to wait for the complete task button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.completeTaskButton()))
        {
            error = "Failed toclick the complete task button";
            return false;
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {
        
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
            
            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());
            
            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());
            
            Narrator.stepPassedWithScreenShot("Creating new despatch sack - " + SettersAndGetters.getGeneratedShelfCode());
            
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }
            
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }    

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
        {
            error = "Failed to wait for the box is full button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBoxIsFull()))
        {
            error = "Failed to click the box is full button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.sealTheDispatchSackText()))
        {
            error = "Failed to wait for the dispatch layout";
            return false;
        }
        
        try
        {
            dispatchSackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchSackBarcode());
        } 

        catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve dispatch sack barcode";
            return false;
        }

        if (!save.saveReturnsBarcode(dispatchSackBarcode))
        {
            error = "Failed to save dispatch sack barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(dispatchSackBarcode))
        {
            error = "Failed to scan barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully sealed the despatch sack");

        validationList = new String[]
        {
            "CONTAINER_FULL", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkLoadRecordsInDatabase(validationList))
        {
            error = "Failed to validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        
        return true;
    }

    public boolean checkContainers()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'carriers' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("DispatchCarrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.manageContainersBtn()))
        {
            error = "Failed to click Manage Containers";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.manageContainersBtn()))
        {
            error = "Failed to click Manage Containers";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.removeContainers()))
        {
            error = "Failed to wait 'Remove Containers' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.removeContainers()))
        {
            error = "Failed to touch 'Remove Containers' button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.noContainersText(), 2))
        {

            for (int i = 0; i < 4; i++)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
                    {
                        error = "Failed to click back Arrow";
                        return false;
                    }
                }

            }
        }
        else
        {
            try
            {
//                itemHolder.add((MobileElement) AppiumDriverInstance.Driver.findElements(By.xpath(MobileDoddlePageObjects.containerItemHolder())));
                itemHolder = AppiumDriverUtility.Driver.findElements(By.xpath(MobileDoddlePageObjects.containerItemHolder()));
            }
            catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

            for (int i = 0; i < itemHolder.size(); i++)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemContainerSelectButton(i)))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemContainerSelectButton(i)))
                    {
                        error = "Failed to click the select button";
                        return false;
                    }
                }
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmDeletionOfContainers()))
            {
                error = "Failed to wait for the remove container button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmDeletionOfContainers()))
            {
                error = "Failed to click the remove container button";
                return false;
            }

            for (int i = 0; i < 4; i++)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
                    {
                        error = "Failed to click back Arrow";
                        return false;
                    }
                }
            }
        }

        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author mlopes
 */
public class TC_NextGen_09_RepeatingStorageProcess extends BaseClass
{

    String error = "";
    Narrator narrator;
    StringEntity input;

//==========================Variables for custom methods=====================
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();

    boolean testPassed = true;
    public static TestEntity currentData;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();

//==========================================================================
    public TC_NextGen_09_RepeatingStorageProcess(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        this.currentData = testData;
    }

    public TestResult executeTest()
    {
        //Added by Manuel Lopes
        executeBarCode();
        if (testPassed == false)
        {
            return narrator.testFailedScreenshot("Failed to seet the Barcode settings - " + error);
        }

        if (!ReusableFunctionalities.carrierProcessExecution())
        {
            return narrator.testFailedScreenshot("Failed to do the Carrier process - " + Shortcuts.error);
        }

        if (!StorageProccess())
        {
            return narrator.testFailedScreenshot("Failed to do the Storage Repeating process - " + Shortcuts.error);
        }

        if (!ReusableFunctionalities.checkRecordsInDatabase("TC_NextGen_09.xlsx"))
        {
            return narrator.testFailedScreenshot("Failed check the records on the Database - " + Shortcuts.error);
        }
        return narrator.finalizeTest("Successfully completed Storage Repeating Process test");
    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();
            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, barcode, storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean StorageProccess()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (!Shortcuts.rescanBarcodeAndWait(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.numberOfParcelsLabel(), 5))
        {
            error = "Failed to display the number of parcels passed from Goods-in Process";
            return false;
        }

        String numberOfParcels = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.numberOfParcelsLabel());
        Narrator.stepPassed("Successfully displayed the number of Parcels passed from Goods-in Process with the value of " + numberOfParcels);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }
        if (!Shortcuts.scanAndRescanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan shelf barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
            {
                error = "Failed to click on the pause storage button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to click the back to dashboard button";
                return false;
            }
        }
        else if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.finishStorageButton()))
        {
            error = "Failed to touch the 'Pause storage' button.";
            return false;
        }

        Narrator.stepPassed("Successfully retrieved parcel from carrier, scanned and stored parcel.");
        
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard" + ReusableFunctionalities.error;
            return false;
        }
        
        return true;
    }
}

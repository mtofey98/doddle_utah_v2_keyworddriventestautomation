/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_32_Return_ItemTooLarge extends BaseClass
{

    Narrator narrator;
    static String onscreenDPSBarcode = "";
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    String value;
    String itemReturn;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();
    Shortcuts shortcut = new Shortcuts();
    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String DCBarcode = "";
    public String barcodeName = "";
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public boolean restart;
    public boolean conErorr;
    //==========================Veribles for custom methods=====================
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String text;
    String correctBarcode;
    String newBearer, line;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL = "";
    String destinationFile = "";
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    String temp2 = "";
    public static String[] validationList;
    WebElement barcodeVal;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();

    //==========================================================================
    public TC_NextGen_32_Return_ItemTooLarge(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
         if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login");
            }
            
            if (!ReusableFunctionalities.singleContainerExclusivePoshTotty() && ReusableFunctionalities.isSeleniumFailure == false)
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
            }
            
            else if (ReusableFunctionalities.isSeleniumFailure)
            {
                return narrator.seleniumTestFailedScreenshot("Failed to retrieve item IDs" + ReusableFunctionalities.error);
            }

            if (!itemTooLarge())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process with item too large - " + error);
            }
        }
        else
        {
            if (!ReusableFunctionalities.singleContainerExclusivePoshTotty() && ReusableFunctionalities.isSeleniumFailure == false)
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
            }
            
            else if (ReusableFunctionalities.isSeleniumFailure)
            {
                return narrator.seleniumTestFailedScreenshot("Failed to retrieve item IDs" + ReusableFunctionalities.error);
            }

            if (!itemTooLarge())
            {
                return narrator.testFailedScreenshot("Failed to do the Returns process with item too large - " + error);
            }
        }

        return narrator.finalizeTest("Successfully completed the return parcel test with wifi on");
        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    public boolean itemTooLarge()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the now button";
            return false;
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {            
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
            
            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());
            
            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());
            
            Narrator.stepPassedWithScreenShot("Creating new despatch sack - " + SettersAndGetters.getGeneratedShelfCode());
            
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }
            
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }
        

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsItemTooLarge()))
        {
            error = "Failed to wait for the item too large button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemTooLarge()))
        {
            error = "Failed to click the item too large button";
            return false;
        }

        try
        {
            AppiumDriverInstance.pause(1000);

            String itemDescription = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnsItemTooLargeDescription2());

            Narrator.stepPassedWithScreenShot("Item Description : " + itemDescription);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to validate item too large description";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemTooLargeCloseButton()))
        {
            error = "failed to click the close button";
            return false;
        }

        return true;
    }

}

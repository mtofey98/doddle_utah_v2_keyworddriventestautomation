/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.CollectionProcess;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.backToDashBoard;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.collectionEndProcess;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.collectionOptionPassport;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.moreCollectionCodesNo;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.searchWithCollectionCodes;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_45_MultipleCollectionCode_NoCustomerBlock extends BaseClass {

    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String amazonBarcode = "";
    String amazonShelf = "";
    String amazonBarcode2 = "";
    String amazonShelf2 = "";
    String amazonBarcode3 = "";
    String amazonShelf3 = "";

    //API call body
    StringEntity input;

    public TC_NextGen_45_MultipleCollectionCode_NoCustomerBlock(TestEntity testData) {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
    }

    public TestResult executeTest() {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1)) {
            if (!ReusableFunctionalities.Login()) {
                return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
            }
            executeBarCode();
            
            if (!ReusableFunctionalities.checkHintsScreenIsOn())
            {
                return narrator.testFailedScreenshot("Failed to ensure that the hints screen is activated");
            }

            if(!MultipleCollectionCodesNoCustomerBlock())
            {
                return narrator.testFailedScreenshot("Failed to do the collection process -" + error);
            }

            return narrator.finalizeTest("Successfully completed collection process with multiple collection codes without customer block");
        } else {
            executeBarCode();

            if (!ReusableFunctionalities.checkHintsScreenIsOn())
            {
                return narrator.testFailedScreenshot("Failed to ensure that the hints screen is activated");
            }
            
            if(!MultipleCollectionCodesNoCustomerBlock())
            {
                return narrator.testFailedScreenshot("Failed to do the collection process -" + error);
            }

            return narrator.finalizeTest("Successfully completed collection process with multiple collection codes without customer block");
        }

    }

    public TestResult executeBarCode() {
        if (!Shortcuts.newBearer()) {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode)) {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal"))) {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode) {
        try {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();

            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, SettersAndGetters.getGeneratedBarCode(), storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied")) {
                narrator.testFailed(line);
                return false;
            }
        } catch (Exception e) {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean MultipleCollectionCodesNoCustomerBlock() {

        String nonExistingCollectionCode = testData.getData("IncorrectBarcode");
        //Creates a new parcel without the customer block being present in the couchbase
        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.amazonBarcode());
        SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
        amazonBarcode = SettersAndGetters.getGeneratedBarCode();
        amazonShelf = SettersAndGetters.getGeneratedShelfCode();
        Shortcuts.preAadviceCollectionParcelSettingReferenceIDNoCustomerBlock(amazonBarcode, amazonShelf, newBearer);
        String collectionCode = SettersAndGetters.getBarCodeRefID();
        executeBarCode();

        //Creates a second parcel with a different collection Code
        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.amazonBarcode());
        SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
        amazonBarcode2 = SettersAndGetters.getGeneratedBarCode();
        amazonShelf2 = SettersAndGetters.getGeneratedShelfCode();
        Shortcuts.preAadviceCollectionParcelSettingReferenceIDNoCustomerBlock(amazonBarcode2, amazonShelf2, newBearer);
        String collectionCode2 = SettersAndGetters.getBarCodeRefID();
        executeBarCode();

        //Creates a third parcel with different collection code
        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.amazonBarcode());
        SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
        amazonBarcode3 = SettersAndGetters.getGeneratedBarCode();
        amazonShelf3 = SettersAndGetters.getGeneratedShelfCode();
        Shortcuts.preAadviceCollectionParcelSettingReferenceIDNoCustomerBlock(amazonBarcode3, amazonShelf3, newBearer);
        String collectionCode3 = SettersAndGetters.getBarCodeRefID();

        if (!SaveBarcodesFromURL.saveBarcode(nonExistingCollectionCode, MobileDoddlePageObjects.rand())) {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(amazonBarcode, amazonShelf)) {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(collectionCode, SettersAndGetters.getGeneratedShelfCode())) {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(amazonBarcode2, amazonShelf2)) {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(collectionCode2, SettersAndGetters.getGeneratedShelfCode())) {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(amazonBarcode3, amazonShelf3)) {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(collectionCode3, SettersAndGetters.getGeneratedShelfCode())) {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile())) {

            error = "Failed to click the collection tile";
            return false;
        }

        narrator.stepPassedWithScreenShot("Collections");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionCodeButton())) {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionCodeButton())) {
            error = "Failed to touch the 'Collection Code' option.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected collection code option");

        if (!Shortcuts.scanBarcode(collectionCode)) {
            error = "Failed to scan barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("Scanned collection code");
        SettersAndGetters.setGeneratedBarCode(amazonBarcode);
        moreCollectionCodesNo();
        backToDashBoard();

        //search collection code
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile())) {
            error = "Failed to click the collection tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionCodeButton())) {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionCodeButton())) {
            error = "Failed to touch the 'Collection Code' option.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected collection code option with search");

        if (!Shortcuts.scanBarcode(collectionCode)) {
            error = "Failed to scan barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("Scanned collection code");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreCollectionCodesYesButton())) {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }

        narrator.stepPassedWithScreenShot("Select yes option");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesYesButton())) {
            error = "Failed to select the no button";
            return false;
        }

        searchWithCollectionCodes();
        backToDashBoard();

        //scan two different collection codes
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile())) {
            error = "Failed to click the collection tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionCodeButton())) {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionCodeButton())) {
            error = "Failed to touch the 'Collection Code' option.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected collection code option with different collection codes");

        if (!Shortcuts.scanBarcode(collectionCode)) {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreCollectionCodesYesButton())) {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }

        narrator.stepPassedWithScreenShot("Scanned first collection code");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesYesButton())) {
            error = "Failed to select the no button";
            return false;
        }

        if (!Shortcuts.scanBarcode(collectionCode2)) {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreCollectionCodesYesButton())) {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesYesButton())) {
            error = "Failed to select the no button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Scanned second collection code");

        //Scan same collection code twice
        if (!Shortcuts.scanBarcode(collectionCode)) {
            error = "Failed to scan barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("scan duplicate collection code");

        if (!Shortcuts.scanBarcode(nonExistingCollectionCode)) {
            error = "Failed to scan barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("Scan incorrect collection code");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage())) {
            error = "Failed to wait an alert to popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage())) {
            error = "Failed to wait an alert to popup";
            return false;
        }

        if (!Shortcuts.scanBarcode(collectionCode3)) {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreCollectionCodesYesButton())) {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Scanned third collection code");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesYesButton())) {
            error = "Failed to select the no button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.deleteCollectionCode())) {
            error = "Failed to wait for the delete icon";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.deleteCollectionCode())) {
            error = "Failed to remove the collection code";
            return false;
        }

        narrator.stepPassedWithScreenShot("Deleted a collection code from list");

        //Full flow with multiple Collections
        searchWithCollectionCodes();
        CollectionProcess();
        collectionOptionPassport();
        collectionEndProcess();

        return true;
    }
}

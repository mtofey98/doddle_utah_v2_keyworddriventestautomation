/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mtofey
 */
public class TC_NextGen_60_Collection_Search_By_Email_With_Cust_Block extends BaseClass
{
    public static TestEntity currentData;
    
      String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    
    
    public TC_NextGen_60_Collection_Search_By_Email_With_Cust_Block(TestEntity testData)
    {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
    }
    
    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1)) 
        {
           if (!ReusableFunctionalities.Login()) 
           {
               return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
           }
           
           if(!executePreCondition())
           {
               return narrator.testFailed("Failed to execute the test precondition");
           }
           
           if(!workerMethod())
           {
               return narrator.testFailed("Failed to execute the worker method");
           }

           return narrator.finalizeTest("Successfully searched for a collection using collection code");
        } 
        else 
        {
            if(!executePreCondition())
            {
                return narrator.testFailed("Failed to execute the test precondition");
            }

           if(!workerMethod())
           {
               return narrator.testFailed("Failed to execute the worker method");
           }
    
            return narrator.finalizeTest("Successfully searched for a collection using collection code");

        }
    }
    
    public boolean executePreCondition()
    {
        try
        {
            if(!ReusableFunctionalities.createCollectionBarcodeWithCustomerBlock())
            {
                error = ReusableFunctionalities.error;
                return false;
            }
            
            SaveBarcodesFromURL.saveLabel(SettersAndGetters.getBarCodeRefID());
            if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
            {
                error = "Failed to click on the collections tile";
                return false;
            }

            if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
            {
                error = "Failed to wait for the email button to appear";
                return false;
            }

            if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
            {
                error = "Failed to click on the email button";
                return false;
            }

            if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
            {
                error = "Failed to wait for the Email Address input field to appear";
                return false;
            }
            
             if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
            {
                error = "Failed to click the Email Address input field to appear";
                return false;
            }
             
             Narrator.stepPassedWithScreenShot("Successfully selected email");

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("Email")))
            {
                error = "Failed to enter the Email Address into the input field";
                return false;
            }
            
            Narrator.stepPassedWithScreenShot("Successfully entered email");

            WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
            int xLocation = element.getLocation().x + 70;
            int yLocation = element.getLocation().y + 40;

            if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
            {
                error = "Failed to tap at desired location";
                return false;
            }

            return true;
        }
        catch(Exception e)
        {
            error = e.getMessage();
            narrator.logFailure("Failed to execute precondition");
            narrator.logError(e.getMessage());
            System.out.println("Failed to execute precondition");
            return false;
        }
    }

    public boolean workerMethod()
    {
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the Correct button to appear";
            return false;
        }

        Shortcuts.doCollectionsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

        if(!Shortcuts.line.contains("\"searchType\":\"email\""))
        {
            error = "\"searchType\":\"email\" was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("\"searchType\":\"email\""))
        {
            error = "could not change API response colour :" + Shortcuts.changeFailedAPIResponseColor("\"searchType\":\"email\"");
            return false;
        }
        
        Narrator.stepPassed(Shortcuts.line);
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click on the BACK ARROW button";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionDoddleID()))
        {
            error = "Failed to wait for the collection code button to appear";
            return false;
        }

        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionDoddleID()))
        {
            error = "Failed to click on the collections code button";
            return false;
        }

        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterID()))
        {
            error = "Failed to wait for the Doddle ID input field to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterID()))
        {
            error = "Failed to click the Doddle ID input field to appear";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Successfully clicked Doddle ID");

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterID(), testData.getData("DoddleID")))
        {
            error = "Failed to enter the Doddle ID";
            return false;
        }
         
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the Correct button to appear";
            return false;
        }
                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu(), 120))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to dashboard";
        }

        Shortcuts.doCollectionsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());
        
        if(!Shortcuts.line.contains("\"searchType\":\"customerId\""))
        {
            error = "\"searchType\":\"customerId\" was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("\"searchType\":\"customerId\""))
        {
            error = "\"searchType\":\"customerId\" was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        Narrator.stepPassed(Shortcuts.line);
        
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mtofey
 */
public class TC_NextGen_62 extends BaseClass
{

    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();

    public TC_NextGen_62(TestEntity testData)
    {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
            }
                AppiumDriverInstance.pause(400000);
            if (!executePreCondition())
            {
                return narrator.testFailed("Failed to execute the test precondition");
            }

            if (!workerMethod())
            {
                return narrator.testFailed("Failed to execute the worker method");
            }

            return narrator.finalizeTest("Successfully Completed TC_NextGen_62");
        } else
        {
             AppiumDriverInstance.pause(400000);
            if (!executePreCondition())
            {
                return narrator.testFailed("Failed to execute the test precondition");
            }

            if (!workerMethod())
            {
                return narrator.testFailed("Failed to execute the worker method");
            }

            return narrator.finalizeTest("Successfully searched for a collection using collection code");

        }
    }

    public boolean executePreCondition()
    {
        try
        {
            SettersAndGetters.setGeneratedBarCode(barcode);
            SettersAndGetters.setGeneratedShelfCode(shelfBarcode);
            Shortcuts.newBearer();
            ReusableFunctionalities.preAadviceCollectionParcelSettingOrderID(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), newBearer);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
            {
                error = "Failed to click on the collections tile";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Clicked the collection tile");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
            {
                error = "Failed to wait for the 'other search options' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
            {
                error = "Successfully clicked the 'other search options' button";
                return false;
            }

            narrator.stepPassedWithScreenShot("Selected other search options");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionOrderID()))
            {
                error = "Failed to wait for the Order ID button to appear";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionOrderID()))
            {
                error = "Failed to click on the Order ID button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterOrderIDField()))
            {
                error = "Failed to wait for the Order ID input field to appear";
                return false;
            }
            return true;
        } catch (Exception e)
        {
            error = e.getMessage();
            narrator.logFailure("Failed to execute precondition");
            narrator.logError(e.getMessage());
            System.out.println("Failed to execute precondition");
            return false;
        }
    }

    public boolean workerMethod()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterOrderIDField()))
        {
            error = "Failed to wait for the Order ID input field to appear";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterOrderIDField(), SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to enter the Order ID";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Entered the order ID inside the order ID field");

        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to find the correct button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to click the correct button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Faield to wait for the correect button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the correct button");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to click the correct button";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu(),60))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to dashboard";
        }

//        Narrator.stepPassedWithScreenShot("Navigated back to the dashboard");
        Shortcuts.doCollectionsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

        if (!Shortcuts.line.contains("retailerOrderId"))
        {
            error = "\"searchType\": \"retailerOrderId\" was not found in the ITEM_COLLECTION record";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("retailerOrderId"))
        {
            error = "\"retailerOrderId\" was not found in the ITEM_COLLECTION record";
            return false;
        }

        if (!Shortcuts.line.contains("\"customer\""))
        {
            error = "\"customer\"" + "was not found in the ITEM_COLLECTION record";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("\"customer\""))
        {
            error = "\"customer\"" + "was not found in the ITEM_COLLECTION record";
            return false;
        }
        Narrator.stepPassed(Shortcuts.line);

        return true;
    }
}

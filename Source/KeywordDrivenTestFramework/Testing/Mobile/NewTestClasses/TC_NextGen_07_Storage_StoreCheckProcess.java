/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import KeywordDrivenTestFramework.Utilities.CryptoUtility;
import io.appium.java_client.MobileElement;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_07_Storage_StoreCheckProcess extends BaseClass
{

    String name = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    String error = "";
    Narrator narrator;
    StringEntity input;
    String[] lines;
    String[] liness;
    String regex;

//==========================Veribles for custom methods=====================
    int Run = 0;
    int timeout = 8;
    int ListSize = 0;
    int ListSizeBefore = 0;
    String temp = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    List<MobileElement> parcels;
    public static String parcel;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String [] barcode = {MobileDoddlePageObjects.barcode(),MobileDoddlePageObjects.barcode(),MobileDoddlePageObjects.barcode()};
    String incorrectBarcode = "AA-123-" + MobileDoddlePageObjects.randomThreeDigitNo() + "-" + MobileDoddlePageObjects.randomThreeDigitNo();
    String barcodeNotFound = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String imageURL = "";
    String destinationFile = "";
    List<WebElement> parcelListSize;
    Set<String> finalBarcodes = new HashSet<>();
    public String newBearer = "";
    String line = "";
    String apiLine;
    int run = 0;
    int listSizeBefore = 0;
    int listSize = 0;
    public boolean conErorr;
    String value;
    String itemReturn;
    int i = 0;
    public static URL url;
    String apiResponse;
    String itemID;
    int notificationBellBefore, notificationBellAfter;
    CryptoUtility crypt = new CryptoUtility();

    public static TestEntity currentData;
    Shortcuts shortcuts = new Shortcuts();
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();

//==========================================================================
    public TC_NextGen_07_Storage_StoreCheckProcess(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        this.currentData = testData;
    }

    public TestResult executeTest()
    {

        String email = testData.getData("Email");
        String cellNumber = testData.getData("CellNo");
        String storeID = testData.getData("StoreID");
        String cardNo = testData.getData("cardNo");
        String postalCode = testData.getData("postal");

        if(!executeBarcode())
        {
            return narrator.testFailedScreenshot("Failed to seet the barode settings - " + error);
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 2))
        {
            Login();
            
            if(!carrierProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
            }
            
        }
        else
        {
            if(!carrierProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
            }
            
            if (!StorageProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Storage process - " + error);
            }
            
            if (!StorageCheckProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Storage process - " + error);
            }
        }

        return narrator.finalizeTest("Successfully completed the stock check process");
    }
    
    public boolean executeBarcode()
    {
        if (!Shortcuts.newBearer())
        {
            error = "Failed to retrieve the new bearer token - " + error;
        }

        if (!save.saveMultipleBarcodes(barcode[0], barcode[1], barcode[2], barcodeNotFound, shelfBarcode))
        {
            error = "Failed to generate and save the barcodes - " + error;
        }
        
        //        for (int j = 0; j < 3; j++)
//        {
//            if (!restCall(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue(), email, cellNumber, barcode[j], randomNumber, storeID, cardNo, postalCode))
//            {
//                return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
//            }
//        }
        
        return true;
    }
    
     public boolean carrierProcess()
    {
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

//        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 3))
//            {
//                error = "Failed to wait for the 'Loading, Please wait...' to be no longer visible";
//                return false;
//            }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }
        
        AppiumDriverInstance.pause(1000);
        
        if (!shortcuts.scanBarcode(barcode[0]))
        {
            error = "Failed to scan the first carrier barcode";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.hintButtonClose()))
        {
            error = "Failed to wait for the close hint button to be visible";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.hintButtonClose()))
        {
            error = "Failed to click the close hint button";
            return false;
        }
        
        if (!shortcuts.scanBarcode(barcode[1]))
        {
            error = "Failed to scan the second carrier barcode";
            return false;
        }
        
        if (!shortcuts.scanBarcode(barcode[2]))
        {
            error = "Failed to scan the third carrier barcode";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Barcodes successfully scanned");
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }
       
        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");
        
        return true;
    }
     
     public boolean StorageProcess()
     {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (!shortcuts.scanBarcode(barcode[0]))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }
        
        if(!shortcuts.scanShelfBarcode(shelfBarcode))
        {
            error = "Failed to scan the shelfBarcode";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeNextParcelButton()))
        {
            error = "Failed to wait for the store the next parcel button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeNextParcelButton()))
        {
            error = "Failed click the store the next parcel button";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }
        
        if (!shortcuts.scanBarcode(barcode[1]))
        {
            error = "Failed to scan barcode";
            return false;
        }
         
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }
        
        if(!shortcuts.scanShelfBarcode(shelfBarcode))
        {
            error = "Failed to scan the shelfBarcode";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeNextParcelButton()))
        {
            error = "Failed to wait for the store the next parcel button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeNextParcelButton()))
        {
            error = "Failed click the store the next parcel button";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }
        
        if (!shortcuts.scanBarcode(barcode[2]))
        {
            error = "Failed to scan barcode";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }
        
        if(!shortcuts.scanShelfBarcode(shelfBarcode))
        {
            error = "Failed to scan the shelfBarcode";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton()))
        {
            error = "Failed to wait for the pause storage button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
        {
            error = "Failed click the pause storage button";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
        {
            error = "Failed to wait for the storage back to dashboard button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
        {
            error = "Failed to click the storage back to dashboard button";
            return false;
        }
        
         return true;
     }
    
     public boolean StorageCheckProcess()
    {
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed click the storage tile";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button to be visible";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to click the store check button";
            return false;
        }

        if (!shortcuts.scanShelfBarcode(shelfBarcode))
        {
            error = "Failed to scan shelf barcode";
            return false;
        }

        AppiumDriverInstance.pause(1000);
        
        Narrator.stepPassedWithScreenShot("Parcels on this shelf:");
        
        for (int k = 0; k < 3; k++)
        {
            
            if (!shortcuts.scanBarcode(barcode[k]))
            {
                error = "Failed to scan the first carrier barcode";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully scanned parcels on shelf");
        }
        
        AppiumDriverInstance.pause(5000);
        
        //validate couchbase
        //======================================================================================================================
        Shortcuts.doStorageLabelValueAPICall(barcode[0], Shortcuts.newBearer);
        
        //ClientVersion
        if (!Shortcuts.line.contains("\"clientVersion\""))
        {
            error = "Failed to add \"clientVersion\" event in CouchBase";
            return false;
        }
        
        if (!Shortcuts.changeAPIResponseColor("\"clientVersion\""))
        {
            error = "Failed to update text: \"clientVersion\" to green color";
            return false;
        }
        
        //dateTime
        if (!Shortcuts.line.contains("\"dateTime\""))
        {
            error = "Failed to add \"dateTime\" event in CouchBase";
            return false;
        }
        
        if (!Shortcuts.changeAPIResponseColor("\"dateTime\""))
        {
            error = "Failed to update text:\"dateTime\" to green color";
            return false;
        }
        
        //deviceIdentifier
        if (!Shortcuts.line.contains("\"deviceIdentifier\""))
        {
            error = "Failed to add \"deviceIdentifier\" event in CouchBase";
            return false;
        }
        
        if (!Shortcuts.changeAPIResponseColor("\"deviceIdentifier\""))
        {
            error = "Failed to update text: \"deviceIdentifier\" to green color";
            return false;
        }
        
        //eventType
        if (!Shortcuts.line.contains("\"STOCK_CHECK_CONFIRM\""))
        {
            error = "Failed to add \"STOCK_CHECK_CONFIRM\" event in CouchBase";
            return false;
        }
        
        if (!Shortcuts.changeAPIResponseColor("\"STOCK_CHECK_CONFIRM\""))
        {
            error = "Failed to update text: \"STOCK_CHECK_CONFIRM\" to green color";
            return false;
        }
        
        //location
        if (!Shortcuts.line.contains("\"location\""))
        {
            error = "Failed to add \"location\" event in CouchBase";
            return false;
        }
        
        if (!Shortcuts.changeAPIResponseColor("\"location\""))
        {
            error = "Failed to update text: \"location\" to green color";
            return false;
        }
        
        //shelfID
        if (!Shortcuts.line.contains("\""+shelfBarcode+"\""))
        {
            error = "Failed to add \""+shelfBarcode+"\" event in CouchBase";
            return false;
        }
        
        if (!Shortcuts.changeAPIResponseColor("\""+shelfBarcode+"\""))
        {
            error = "Failed to update text: \""+shelfBarcode+"\" to green color";
            return false;
        }
        
        
        //storeID
        if (!Shortcuts.line.contains("\"456DVT\""))
        {
            error = "Failed to add \"456DVT\" event in CouchBase";
            return false;
        }
        
        if (!Shortcuts.changeAPIResponseColor("\"456DVT\""))
        {
            error = "Failed to update text: \"456DVT\" to green color";
            return false;
        }
        
        
        //staffID
        if (!Shortcuts.line.contains("\"dvt_automation\""))
        {
            error = "Failed to add \"dvt_automation\" event in CouchBase";
            return false;
        }
        
        if (!Shortcuts.changeAPIResponseColor("\"dvt_automation\""))
        {
            error = "Failed to update text:\"dvt_automation\" to green color";
            return false;
        }
        
        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        //======================================================================================================================
        
        if(!shortcuts.storeCheckScanBarcode(barcode[0]))
        {
            error = "Failed to scan barcode previously scanned barcode";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Barcode already checked");
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertOkButton()))
        {
            error = "Failed to click the barcode already checked ok button";
            return false;
        }
        
        if(!shortcuts.storeCheckScanBarcode(barcodeNotFound))
        {
            error = "Failed to scan barcode not on shelf";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Barcode not found on shelf");
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertOkButton()))
        {
            error = "Failed to click the barcode already checked ok button";
            return false;
        }
      
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.endStoreCheckButton()))
        {
            error = "Failed to wait for the end store check button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.endStoreCheckButton()))
        {
            error = "Failed to click the end store check button";
            return false;
        }
 
        Narrator.stepPassed("Successfully ended the store check process");
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dialogueRightOption()))
        {
            error = "Failed to wait for the ok popup button";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dialogueRightOption()))
        {
            error = "Failed to click the popup - ok button";
            return false;
        }
        
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard" + ReusableFunctionalities.error;
            return false;
        }
        
        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean Login()
    {
        if (!conErorr)
        {
//            if (!(ReusableFunctionalities.checkWifiIsOn()))
//            {
//                error = "Failed to switch wifi-on " + Shortcuts.error;
//                return false;
//            }
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (!store.equals(narrator.getData("StoreSelection")))
            {
                if (!scrollToSubElement(narrator.getData("StoreSelection")))
                {
                    error = "Could not find " + narrator.getData("StoreSelection");
                    return false;
                }

            }

            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                Login();

            }
        }

//        if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//           if(!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 4))
//           {
//               error = "Failed to wait for the progress circe to no longer be present";
//               return false;
//           }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }
    
    public boolean restCall(String header, String headerValue, String email, String cellNumber, String barcodeName, String randomNumber, String storeID, String cardNo, String postalCode)
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost preadvicePost = new HttpPost("https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        tokenPost.addHeader(header, headerValue);
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());
        
        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            
            line = rd.readLine();
            
            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");
            
            line = liness[0];
            newBearer = line;
            
            preadvicePost.addHeader(header, "Bearer " + newBearer);
            
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, barcodeName, storeID));
            input.setContentType("application/json");
            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));
            
            line = rd.readLine();
            
            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }
        
        return true;
    }

}

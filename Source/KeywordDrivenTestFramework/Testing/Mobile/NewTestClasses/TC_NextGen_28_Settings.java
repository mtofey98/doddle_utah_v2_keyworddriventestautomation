/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanBarcode;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;

/**
 *
 * @author hduplessis
 */
public class TC_NextGen_28_Settings extends BaseClass
{

    String error = "";
    Narrator narrator;
    StringEntity input;

//==========================Variables for custom methods=====================
    String randomNumber = MobileDoddlePageObjects.randomNo();
    //String shelfBarcode = MobileDoddlePageObjects.rand();
    String bCode = MobileDoddlePageObjects.barcode();
    boolean testPassed = true;
    public static TestEntity currentData;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();

//==========================================================================
    public TC_NextGen_28_Settings(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {         
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1)) 
        {
            //ReusableFunctionalities.checkWifiIsOn();
          
            if (!ReusableFunctionalities.Login())
            {
                error = "Failed to do the Login process";
                return narrator.testFailedScreenshot("Failed to do the login process");
            }

            //Added by Hyran DuPlessis
            if (!settings())
            {
                return narrator.testFailedScreenshot("Failed to do the Settings process - " + Shortcuts.error);
            }
            return narrator.finalizeTest("Successfully completed Setting Screen test");
        }
        
        else
        {            
            //ReusableFunctionalities.checkWifiIsOn();
            //Added by Hyran DuPlessis
            if (!settings())
            {
                return narrator.testFailedScreenshot("Failed to do the Settings process - " + Shortcuts.error);
            }
        }

        return narrator.finalizeTest("Successfully completed Setting Screen test");
    }

    public boolean settings()
    {

        for (int i = 0; i < 2; i++)
        {
            if (!onOffToggle())
            {
                error = "Failed to toggle";
                return false;
            }
            if (!carrierSettings())
            {
                error = "Failed to do carrier settings";
                return false;
            }
            if (!collectionSettings())
            {
                error = "Failed to do collection settings";
                return false;
            }

            if (!ReusableFunctionalities.backToDashBoard())
            {
                error = "Failed to go to dashboard";
                return false;
            }

        }
        return true;
    }

    public boolean carrierSettings()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to wait for the Carriers Tile.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Carriers Tile' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the other to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to click Other tile.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Purple draw a line notification screen doesn't appear");

        TC_NextGen_03_Carrier cr = new TC_NextGen_03_Carrier(testData);
        AppiumDriverInstance.pause(1000);
        //===================01============
        String bb1 = MobileDoddlePageObjects.barcode();

        cr.executeBarCode(bb1, MobileDoddlePageObjects.rand());

        cr.writeToExcel(bb1, "TC_NextGen_3_Carriers_Bar.xlsx", "TC_NextGen_3_Carriers.xlsx");
        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            return false;
        }
        narrator.stepPassedWithScreenShot("Purple first scanned notification screen appear");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton(), 60))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to touch on the 'Store parcels later' button.";
            return false;
        }

        return true;
    }

    public boolean collectionSettings()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to wait for the Collection Tile button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Failed to wait for the 'other search options' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Successfully clicked the 'other search options' button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Other search options window");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to wait for the 'UPI' option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to touch the 'UPI' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the UPI option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter UPI field";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter UPI field";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to click on the enter the UPI in the UPI Field";
            return false;
        }
        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode("DPS" + MobileDoddlePageObjects.barcode());

        Shortcuts.preAadviceCollectionParcel(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), newBearer);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to click on the enter the UPI in the UPI Field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterUPIField(), SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to enter UPI in the 'UPI' field";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered UPI ");

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.okButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Purple hint screen");
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to touch the ok button";
                return false;
            }
        }
          if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu(),60))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }

//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton())) {
//            error = "Failed to touch the ok button";
//            return false;
//        }
        return true;

    }

    public boolean onOffToggle()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to touch Menu";
            return false;
        }
//         if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton())) {
//            error = "Failed to wait for the settings button";
//            return false;
//        }
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton())) {
//            error = "Failed to touch Menu";
//            return false;
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.settingsButton()))
        {
            error = "Failed to wait for the settings button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.settingsButton()))
        {
            error = "Failed click settings";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to wait for the ok pop-up button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to click pop-up";
            return false;
        }      
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkBox()))
        {
            error = "Failed to wait for the check box ";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkBox()))
        {
            error = "Failed to click Checkbox";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Clicked Activate hint screens");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click Back Arrow";
            return false;
        }
        

        return true;
    }
}

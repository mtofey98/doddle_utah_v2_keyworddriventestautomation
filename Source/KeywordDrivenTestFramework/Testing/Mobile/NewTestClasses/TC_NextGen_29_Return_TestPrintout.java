/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA38DataArtTestReturnFlowWifiOff;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import static java.lang.System.out;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_29_Return_TestPrintout extends BaseClass
{

    Narrator narrator;
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();

    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String barcodeName = "";
    public String newBearer = "";
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public boolean restart;
    public boolean conErorr;
    boolean testPassed = true;
    //==========================Veribles for custom methods=====================
//    static boolean restart = false;
    String line = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String temp2;
    String text;
    String correctBarcode;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL;
    String destinationFile;
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    WebElement barcodeVal;
    String value;
    String itemReturn;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();

    //==========================================================================
    public TC_NextGen_29_Return_TestPrintout(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {

            switch (testData.TestCaseId)
            {
                case "TC-NextGen-29 Test Printout":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        ReusableFunctionalities.Login();
                        if (!testPrintout())
                        {
                            return narrator.testFailedScreenshot("Failed to do the test printout process - " + error);
                        }

                    } else
                    {
                        if (!testPrintout())
                        {
                            return narrator.testFailedScreenshot("Failed to do the test printout process - " + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed the returns make a test printout test with wifi on.");
                }

                case "TC-NextGen-29 WifiOff":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        ReusableFunctionalities.Login();
                        if (!ReusableFunctionalities.wifiOff())
                        {
                            return narrator.testFailedScreenshot("Failed to turn the wifi off - " + ReusableFunctionalities.error);
                        }

                        if (!testPrintOutWifiOff())
                        {
                            return narrator.testFailedScreenshot("Failed to do the test printout process with wifi off - " + error);
                        }

                    } else
                    {
                        if (!ReusableFunctionalities.wifiOff())
                        {
                            return narrator.testFailedScreenshot("Failed to turn the wifi off - " + ReusableFunctionalities.error);
                        }

                        if (!testPrintOutWifiOff())
                        {
                            return narrator.testFailedScreenshot("Failed to do the test printout process with wifi off - " + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed the returns make a test printout test.");
                }

            }

            return narrator.finalizeTest("Successfully completed the returns make a test printout test.");
        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    public boolean testPrintout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the dashboard icon";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click the dashboard help desk icon";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.settingsButton()))
        {
            error = "Failed to wait for the settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.settingsButton()))
        {
            error = "Failed to click the settings button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInYes()))
        {
            error = "Failed to wait for the settings popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkInYes()))
        {
            error = "Failed to click the settings popup";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeATestPrintout()))
        {
            error = "Failed to wait for the make a test printout button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.makeATestPrintout()))
        {
            error = "Failed to click the make a test printout button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelRecieptPrintout()))
        {
            error = "Failed to cancel the receipt printout";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelRecieptPrintout()))
        {
            error = "Failed to click the receipt printout";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeATestPrintout()))
        {
            error = "Failed to wait for the make a test printout button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.makeATestPrintout()))
        {
            error = "Failed to click the make a test printout button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.SettingsPrinterOption()))
        {
            error = "Failed to wait for the settings dvt printer option button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.SettingsPrinterOption()))
        {
            error = "Failed to click the settings dvt printer option button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.settingsSelectDvtPrinter()))
        {
            error = "Failed to click the select printer button";
            return false;
        }

        AppiumDriverInstance.pause(10000);

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelRecieptPrintout(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelRecieptPrintout()))
            {
                error = "Failed click the cancel receipt button";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Successfully made a test printout");

        return true;
    }

    public boolean testPrintOutWifiOff()
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
            {
                error = "Failed to click the back arrow button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the dashboard icon";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click the dashboard help desk icon";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.settingsButton()))
        {
            error = "Failed to wait for the settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.settingsButton()))
        {
            error = "Failed to click the settings button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInYes()))
        {
            error = "Failed to wait for the settings popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkInYes()))
        {
            error = "Failed to click the settings popup";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.makeATestPrintout()))
        {
            error = "Failed to wait for the make a test printout button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.makeATestPrintout()))
        {
            error = "Failed to click the make a test printout button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.SettingsPrinterOption()))
        {
            error = "Failed to wait for the settings dvt printer option button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.SettingsPrinterOption()))
        {
            error = "Failed to click the settings dvt printer option button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.settingsSelectDvtPrinter()))
        {
            error = "Failed to click the select printer button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully validated unable to make a test printout with wifi off | unable to resolve hostname");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelRecieptPrintout()))
        {
            error = "Failed to click the cancel button";
            return false;
        }

        if (!ReusableFunctionalities.wifiOn())
        {
            error = "Failed to turn on wifi";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the dashboard icon";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click the dashboard help desk icon";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the dashboard icon";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click the dashboard help desk icon";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInYes()))
        {
            error = "Failed to wait for the settings popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkInYes()))
        {
            error = "Failed to click the settings popup";
            return false;
        }

        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import static java.lang.System.out;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_30_Return_SingleContainer extends BaseClass
{

    Narrator narrator;
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    static String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    String value;
    String itemReturn;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();
    Shortcuts shortcut = new Shortcuts();
    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String DCBarcode = "";
    public String barcodeName = "";
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public boolean restart;
    public boolean conErorr;
    //==========================Veribles for custom methods=====================
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String text;
    String correctBarcode;
    String newBearer, line;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL = "";
    String destinationFile = "";
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    String temp2 = "";
    public static String[] validationList;
    WebElement barcodeVal;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();

    //==========================================================================
    public TC_NextGen_30_Return_SingleContainer(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    TC_NextGen_30_Return_SingleContainer()
    {

    }

    public TestResult executeTest()
    {
        if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {
            switch (this.testData.TestCaseId)
            {
                case "TC-NextGen-30 Single Container Later":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        ReusableFunctionalities.Login();

                        if (!ReusableFunctionalities.singelContainer() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                        }

                        if (!tapLaterFlow())
                        {
                            return narrator.testFailedScreenshot("Failed to do the tap later process with single container - " + error);
                        }

                    } else
                    {
                        if (!ReusableFunctionalities.singelContainer() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                        }

                        if (!tapLaterFlow())
                        {
                            return narrator.testFailedScreenshot("Failed to do the tap later process with single container - " + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed return execution Process test by tapping later.");
                }

                case "TC-NextGen-30 Single Container Now":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        ReusableFunctionalities.Login();

                        if (!ReusableFunctionalities.singelContainer() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                        }

                        if (!tapNowFlow())
                        {
                            return narrator.testFailedScreenshot("Failed to do the tap now process with single container - " + error);
                        }

                        if (!cancel())
                        {
                            return narrator.testFailedScreenshot("Failed to do the cancellation process" + error);
                        }

                    } else
                    {
                        if (!ReusableFunctionalities.singelContainer() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                        }

                        if (!tapNowFlow())
                        {
                            return narrator.testFailedScreenshot("Failed to do the tap now process with single container - " + error);
                        }

                        if (!cancel())
                        {
                            return narrator.testFailedScreenshot("Failed to do the cancellation process" + error);
                        }
                    }

                    return narrator.finalizeTest("Successfully completed return execution Process test by tapping now.");
                }
            }

            return narrator.finalizeTest("Successfully completed the return parcel test with wifi on");

        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    public static boolean tapNowFlow()
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard(), 2))
        {
            error = "Container config failed to change";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the returns now button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to clicks the returns now button";
            return false;
        }

        return true;
    }

    public boolean tapLaterFlow()
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard(), 2))
        {
            error = "Container config failed to change";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLaterButton()))
        {
            error = "Failed to wait for the returns later button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsLaterButton()))
        {
            error = "Failed to clicks the returns later button";
            return false;
        }

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase" + ReusableFunctionalities.error;
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        validationList = new String[]
        {
            "ASSIGN_TO_CONTAINER_DEFERRED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean cancel()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButton()))
            {
                error = "Failed to click the cancel button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo()))
            {
                error = "Failed to click the cancel button";
                return false;
            }
        }

        return true;
    }

}

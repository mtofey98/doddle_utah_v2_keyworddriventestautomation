/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mlopes
 */
public class TC_NextGen_18_ProcessingCustomerParcels extends BaseClass
{

    public static TestEntity currentData;

    String error = "";
    public String retailersName;
    public String barcodeValue;

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    //barcodes
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    //***************

    //API call body
    StringEntity input;

    //***************
    public TC_NextGen_18_ProcessingCustomerParcels(TestEntity testData)
    {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 2))
        {
         
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to do the login process");
            }
            
            executeBarCode();
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to seet the Barcode settings - " + error);
            }

            if (!initiateCollectionByEmailAddress_DisconfirmID())
            {
                return narrator.testFailedScreenshot("Failed to Disconfirm the Customer ID - " + error);
            }
            
            if (!ReusableFunctionalities.initiateCollection_Code())
            {
                return narrator.testFailedScreenshot("Failed to Initiate Collection with Email - " + Shortcuts.error);
            }

            if (!processCustomerParcels())
            {
                return narrator.testFailedScreenshot("Failed to process the customer Parcels - " + Shortcuts.error);
            }

            return narrator.finalizeTest("Successfully processed the customers Parcels");
            
        }
        
        else
        {
            executeBarCode();
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to seet the Barcode settings - " + error);
            }
            
            if (!initiateCollectionByEmailAddress_DisconfirmID())
            {
                return narrator.testFailedScreenshot("Failed to Disconfirm the Customer ID - " + error);
            }
            
            if (!ReusableFunctionalities.initiateCollection_Code())
            {
                return narrator.testFailedScreenshot("Failed to Initiate Collection with Email - " + Shortcuts.error);
            }

            if (!processCustomerParcels())
            {
                return narrator.testFailedScreenshot("Failed to process the customer Parcels - " + Shortcuts.error);
            }

            return narrator.finalizeTest("Successfully processed the customers Parcels");
        }
    }    

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();
            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, barcode, storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean initiateCollectionByEmailAddress_DisconfirmID() //added by Manuel Lopes
    {

        Shortcuts.cleanCollections();
        
        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode("DPS" + MobileDoddlePageObjects.barcode());

        Shortcuts.preAadviceCollectionParcel(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), newBearer);

        if (!SaveBarcodesFromURL.saveBarcode(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to touch the 'Email' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the email option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfull entered email address");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionDisconfirmCustomerID()))
        {
            error = "Failed to laod the \"No\" button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionDisconfirmCustomerID()))
        {
            error = "Failed to click on \"No\" button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleBackButton()))
        {
            error = "Failed to laod the \"Back\" button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleBackButton()))
        {
            error = "Failed to click on \"Back\" button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        return true;
    }

    public boolean processCustomerParcels()
    {
        int count = 0;
        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.expectedText()))
        {
            if (count == 10)
            {
                error = "Failed to load the List of Parcels";
                return false;
            }

            count++;
        }
        count = 0;

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to load the \"Pick Parcels From Storage\" button";
            return false;
        }

        boolean purpleParcelFound = false;

        int attempt = 0;
        
        while (!purpleParcelFound)
        {            
            if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.photoOfParcelLabel(),2))
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.forgetTodayButton(),2))
                {
                   purpleParcelFound = true;
                }
                
                else 
                {
                    AppiumDriverInstance.Driver.swipe(240, 595, 240, 300, 2000);
                }
            }
            
            else
            {                  
               AppiumDriverInstance.Driver.swipe(240, 595, 240, 300, 2000);
               attempt++;

               if (attempt == 20)
               {
                   error = "Failed to find purple parcel";
                   return false;
               }

            }
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerValue()))
        {
            error = "Failed to find the Retailer value";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retakePhotoItemBarcode()))
        {
            error = "Failed to find the Barcode value";
            return false;
        }

        try
        {
            retailersName = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.retailerValue());
            barcodeValue = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.retakePhotoItemBarcode());

        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve text - " + e.getMessage();
            return false;
        }

        Narrator.stepPassedWithScreenShot("Suucessfuly validated a customer Parcel with Retailer Name as \"" + retailersName + "\" and Barcode Value as \"" + barcodeValue + "\"");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.photoOfParcelLabel()))
        {
            error = "Failed to click on the Photo Parcel button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.photoParcelBackButton()))
        {
            error = "Failed to load the Back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.photoParcelBackButton()))
        {
            error = "Failed to click on the Back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.forgetTodayButton()))
        {
            error = "Failed to click on the Forget Today button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.reinstateButton()))
        {

            Narrator.stepPassedWithScreenShot("Successfully clicked on the Forget Today Button");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.reinstateButton()))
            {
                error = "Failed to click on the Reinstate Button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.forgetTodayButton()))
            {
                error = "Failed to click on the Reinstate Button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully clicked on the Reinstate Button");
        }
        else
        {
            error = "Failed to click on the Reinstate Button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to click the \"Pick Parcels From Storage\" button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }


        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.lostParcelButton()))
        {
            error = "Failed to find the Lost Parcel button ";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.lostParcelButton()))
        {
            error = "Failed to click on the Lost Parcel button ";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.alertOkButton()))
        {
            error = "Failed to load the confirmation alert dialog for Lost Parcel";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked on the Lost Parcel Button");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertOkButton()))
        {
            error = "Failed to click on the Yes option";
            return false;
        }

        if (!ReusableFunctionalities.loopBarcodeCollection())
        {
            error = "Failed to create the barcodes " + Shortcuts.error;
            return false;
        }

        if (!ReusableFunctionalities.loopBarcodeScan())
        {
            error = "Failed to save the barcodes " + Shortcuts.error;
            return false;
        }

        String[] items =
        {
            "labelValue", "retailerOrderId", "customer", "AT_GOODS_IN", "alerts", "eventType", "status"
        };


        if (!ReusableFunctionalities.checkCollectionsRecordsInDatabase(items))
        {
            error = "Failed to do validations";
            return false;
        }
//        });

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to wait for the 'Boom! Another happy customer' message.'";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to click the 'Return to customer' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.yesCheckoutBtn()))
        {
            error = "Failed to wait for the yes - checkout btn to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.yesCheckoutBtn()))
        {
            error = "Failed to touch the yes - checkout btn";
            return false;
        }
        narrator.stepPassedWithScreenShot("Complete scanning proccess");

        if (!ReusableFunctionalities.collectionOptionPassport())
        {
            error = "Failed";
            return false;
        }

        if (!ReusableFunctionalities.collectionEndProcess())
        {
            error = "Failed to make dvt signature";
            return false;
        }

        return true;
    }
}

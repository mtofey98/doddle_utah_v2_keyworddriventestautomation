/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.DoddleLogin;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.MobileDoddleLogout;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA38DataArtTestReturnFlowWifiOff;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import static java.lang.System.out;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.retrieveItemIDFromCouchBase;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanShelfBarcode;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.validationList;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.value;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_22_Return extends BaseClass
{
    // static TestMarshall instance;

    Narrator narrator;
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    public static String LabelValue;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();

    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String barcodeName = "";
    public String newBearer = "";
    public String specificCaseText = "";
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public static boolean wifiStatus;
    public boolean restart;
    public boolean conErorr;
    boolean testPassed = true;
    //==========================Veribles for custom methods=====================
//    static boolean restart = false;
    String line = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String temp2;
    String text;
    String correctBarcode;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL;
    String destinationFile;
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    String itemCode;
    String itemreason;
    String doddleEmail;
    WebElement barcodeVal;
    String value;
    String itemReturn;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();
    MobileDoddleLogout log = new MobileDoddleLogout();
    String deviceBeingUsed = null;

    public static String[] validationList;

    //==========================================================================
    public TC_NextGen_22_Return(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        ReusableFunctionalities.error = "";

        if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {
            //Added by Jody Macauley
            switch (this.testData.TestCaseId)
            {
                case "TC-NextGen-22 New Return":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + error + "");
                        }

                        if (!ReusableFunctionalities.returnsExecution() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process - " + error);
                        }
                    } else
                    {
                        if (!ReusableFunctionalities.returnsExecution() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process - " + error);
                        }
                    }

                    return narrator.finalizeTest("Successfully completed return execution Process test");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Security Scan":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login");
                        }

                        if (!ReusableFunctionalities.returnsExecution() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        }

                        if (!securityScanFailed())
                        {
                            return narrator.testFailedScreenshot("Failed to do the Security Scan failed process - " + Shortcuts.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Couchbase failure" + ReusableFunctionalities.error);
                        }

                        if (!securityScanPassed())
                        {
                            return narrator.testFailedScreenshot("Failed to do the Security Scan passed process - " + Shortcuts.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.testFailedScreenshot("Couchbase failure" + ReusableFunctionalities.error + error);
                        }

                    } else
                    {
                        if (!securityScanFailed())
                        {
                            return narrator.testFailedScreenshot("Failed to do the Security Scan failed process - " + Shortcuts.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Couchbase failure" + ReusableFunctionalities.error);
                        }

                        if (!securityScanPassed())
                        {
                            return narrator.testFailedScreenshot("Failed to do the Security Scan passed process - " + Shortcuts.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Couchbase failure" + ReusableFunctionalities.error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed security scan Process test");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Return References":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login");
                        }

                        if (!returnReferences() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error);
                        }
                    } else
                    {

                        if (!returnReferences() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the return references process - " + Shortcuts.error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed return references process test");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Items":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!returnReferences() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        }

                        if (!items() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the items process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the items process - " + Shortcuts.error + error);
                        }
                    } else
                    {
                        if (!items() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the items process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.testFailedScreenshot("Failed to do the items process - " + Shortcuts.error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed return items process test");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Search Customer":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login");
                        }

                        if (!returnReferences() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        }

                        if (!searchCustomer() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.testFailedScreenshot("Failed to do the search customer process - " + Shortcuts.error + error);
                        }
                    } else
                    {
                        if (!searchCustomer() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do search customer process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the search customer process - " + Shortcuts.error + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed search customer process test");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 CON-840":
                {

                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!itemsCON() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the items con-840 test - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the items con-840 test - " + error);
                        }
                    } else
                    {
                        if (!itemsCON() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the items con-840 test - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the items con-840 test - " + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed specific case with items CON-840 process test");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Return Summary":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!itemsCON() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the return summary - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the items con-840 test - " + error);
                        }

                        if (!returnSummary() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the returns summary process test - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the returns summary process test - " + error);
                        }
                    } else
                    {
                        if (!returnSummary() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the returns summary process test - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the returns summary process test - " + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed returns summary process test");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Temp Label Mock Build":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!tempLabelMockBuild() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the temp label solution mock build test - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the temp label solution mock build test - " + error);
                        }

                    } else
                    {
                        if (!tempLabelMockBuild() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the temp label solution mock build test - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the temp label solution mock build test - " + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed returns process solution mock build");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Label Didnt Print":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!labelDidntPrint() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the label didnt print process test - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the label didnt print process test - " + error);

                        }
                    } else
                    {
                        if (!labelDidntPrint() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the label didnt print process test - " + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the label didnt print process test - " + error);

                        }

                    }

                    return narrator.finalizeTest("Successfully completed temp label didn't return process");
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Temp Label":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!tempLabel() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Temp Label process - " + Shortcuts.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do temp label returns - " + error);
                        }
                    } else
                    {
                        if (!tempLabel() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Temp Label process - " + Shortcuts.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do temp label returns - " + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed returns temp label process");
                }
                //===========================================================================================================================================          
                case "TC-NextGen-22 Specific Case for temp label":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!wifiStatus)
                        {
                            ReusableFunctionalities.checkWifiIsOn();
                            wifiStatus = true;
                        }

                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!specificCaseTempLabel() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns specific case for temp label - " + error + " " + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the returns with specific case - " + error);
                        }
                    } else
                    {
                        if (!wifiStatus)
                        {
                            ReusableFunctionalities.checkWifiIsOn();
                            wifiStatus = true;
                        }

                        if (!specificCaseTempLabel() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the Returns specific case for temp label - " + error + " " + ReusableFunctionalities.error);
                        }

                        if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the returns with specific case - " + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed returns specific case temp label process");
                }
                //===========================================================================================================================================          
                case "TC-NextGen-22 Cancel Return Using Menu (Current)":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!wifiStatus)
                        {
                            ReusableFunctionalities.checkWifiIsOn();
                            wifiStatus = true;
                        }

                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!cancelReturnUsingCurrent() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the cancelation of return using current - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the cancelation of return using current - " + ReusableFunctionalities.error + error);
                        }
                    } else
                    {
                        if (!wifiStatus)
                        {
                            ReusableFunctionalities.checkWifiIsOn();
                            wifiStatus = true;
                        }

                        if (!cancelReturnUsingCurrent() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the cancelation of return using current - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the cancelation of return using current - " + ReusableFunctionalities.error + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed cancelation of returns using current");
                }

                //===========================================================================================================================================         
                case "TC-NextGen-22 Cancel Return Using Menu (All)":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to login" + ReusableFunctionalities.error);
                        }

                        if (!cancelReturnUsingAll() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the cancelation of return using all - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the cancelation of return using all - " + ReusableFunctionalities.error + error);
                        }
                    } else
                    {
                        if (!cancelReturnUsingAll() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do the cancelation of return using all - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the cancelation of return using all - " + ReusableFunctionalities.error + error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed cancelation of returns using all");
                }
                //===========================================================================================================================================         
                case "TC-NextGen-22 Specific Case CON-1589":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {

                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to do the login process - " + ReusableFunctionalities.error);
                        }

                        if (!specificCaseCON1589() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do specific case returns process - " + ReusableFunctionalities.error + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        }

                    } else
                    {
                        if (!specificCaseCON1589() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed returns process solution mock build");
                }
//===========================================================================================================================================
                case "TC_NextGen-22 Specific Case":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {

                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot(("Failed to do the login process" + error));
                        }

                        if (!ReusableFunctionalities.addParceltoCollectionPointFromReturns() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to add returns flow parcel to collection point" + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the specific case CON 1589 precondition");
                        }

                        if (!specificCase() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        }

                    } else
                    {
                        if (!ReusableFunctionalities.addParceltoCollectionPointFromReturns() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to add returns flow parcel to collection point" + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the specific case CON 1589 precondition");
                        }

                        if (!specificCase() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed returns process solution mock build");
                }
                //===========================================================================================================================================          
                case "TC-NextGen-22 Dashboard Scan":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {

                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to do the login process");
                        }

                        if (!ReusableFunctionalities.addParceltoCollectionPointFromReturns() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to add returns flow parcel to collection point" + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the specific case precondition for dashboard scan");
                        }

                        if (!specificCase() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do specific case CON1589 process - " + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the specific case CON1589 process" + error + ReusableFunctionalities.error);
                        }

                        if (!dashboardScan())
                        {
                            return narrator.testFailed("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        }

                        return narrator.finalizeTest("Successfully completed dashboard scan process" + ReusableFunctionalities.error);
                    } else
                    {
                        if (!dashboardScan())
                        {
                            return narrator.testFailed("Failed to do specific case returns process - " + error + ReusableFunctionalities.error);
                        }

                    }

                    return narrator.finalizeTest("Successfully completed completed dashboard scan process" + ReusableFunctionalities.error);
                }
//===========================================================================================================================================
                case "TC-NextGen-22 Despatch Selected Container(s)":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        if (!ReusableFunctionalities.Login())
                        {
                            return narrator.testFailedScreenshot("Failed to do the login process");
                        }

                        if (!ReusableFunctionalities.addParceltoCollectionPointFromReturns() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to add returns flow parcel to collection point" + error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the specific case precondition for dashboard scan");
                        }

                        if (!specificCase() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do specific case CON1589 process - " + error + ReusableFunctionalities.error);
                        }

                        if (!dashboardScan())
                        {
                            return narrator.testFailedScreenshot("Failed to do the dashboard scan precondition" + error + ReusableFunctionalities.error);
                        }

                        if (!despatchSelectedContainers() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do Despatch Selected Containers - " + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do Despatch Selected Container - " + error + ReusableFunctionalities.error);
                        }
                    } else
                    {
                        if (!despatchSelectedContainers() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.testFailedScreenshot("Failed to do Despatch Selected Containers - " + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do Despatch Selected Container - " + error + ReusableFunctionalities.error);
                        }

                    }

                    return narrator.finalizeTest("Succesffullt completed returns prcess solution mock build");
                }

                case "TC-NextGen-22 Specific Case CON-1321":
                {
                    if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
                    {
                        ReusableFunctionalities.Login();

                        if (!CON1321() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the specific case return process - " + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.testFailed("Failed to do specific case return process - " + error + ReusableFunctionalities.error);
                        }
                    } else
                    {
                        if (!CON1321() && ReusableFunctionalities.isSeleniumFailure == false)
                        {
                            return narrator.seleniumTestFailedScreenshot("Failed to do the specific case return process - " + error + ReusableFunctionalities.error);
                        } else if (ReusableFunctionalities.isSeleniumFailure)
                        {
                            return narrator.testFailed("Failed to do specific case return process - " + error + ReusableFunctionalities.error);
                        }

                    }

                    return narrator.finalizeTest("Succesffullt completed returns prcess solution mock build");
                }

            }

            return narrator.finalizeTest("Successfully completed the return parcel test with wifi on");
        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    //===============================================================================================================================================================
    public boolean securityScanFailed()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.securityScanFailed()))
        {
            if (!ReusableFunctionalities.returnsExecution())
            {
                error = ReusableFunctionalities.error;
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.securityScanFailed()))
        {
            error = "Failed to wait for the 'security scanned failed' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.securityScanFailed()))
        {
            error = "Failed to click the security scan failed screen";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemFailedScan()))
        {
            Narrator.stepPassedWithScreenShot("Unable to accept returns. Item failed security checks");
        }

        if (!retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retrieve item ID from couchbase";
            return false;
        }
        //2nd Couchbase Validation
        validationList = new String[]
        {
            "SECURITY_SCAN_FAILED", "REJECTED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public boolean securityScanPassed()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanFailedDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dialogueRightOption()))
            {
                error = "Failed toclick the any more returns message";
                return false;
            }

        } else
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
            {
                error = "Failed to touch 'Returns' button.";
                return false;
            }
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to wait for the 'New Rerurn' button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to touch on the 'New Return' button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(1000);

        if (!retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retrieve item ID from couchbase";
            return false;
        }

        //3rd Couchbase Validation
        validationList = new String[]
        {
            "SECURITY_SCAN_PASSED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!ReusableFunctionalities.returnsBackToDashboard())
        {
            error = "Failed to navigate back to the dashboard" + ReusableFunctionalities.error;
            return false;
        }

        return true;
    }

    public boolean returnReferences()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to click on the return tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the new return button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to click the new return button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan passed screen");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        Narrator.stepPassed("References are disables. No back button");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerLogo()))
        {
            Narrator.stepPassedWithScreenShot("Successfully validated return retailer logo on return references screen");
        }

        for (int i = 0; i < 3; i++)
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnReferencesHelpIcon(i)))
            {
                error = "Failed to click the help icon";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Help text " + (i + 1));

            try
            {
                Driver.pressKeyCode(AndroidKeyCode.BACK);
            } catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to click the back button";
                return false;
            }

            if (i == 2)
            {
                break;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = ReusableFunctionalities.error;
            return false;
        }

        validationList = new String[]
        {
            "ADDED_RETAILER_REFS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        return true;
    }

    public boolean items()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the back arrow button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back arrow button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnReferencesHelpIcon(0)))
        {
            error = "Failed to wait for the icon";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnReferencesHelpIcon(0)))
        {
            error = "Failed to click the help icon";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Help text");

        try
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to click the back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsAddNewItemBtn()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsItemIncrease(), 2))
        {
            AppiumDriverInstance.Driver.swipe(240, 500, 240, 300, 2000);
        }

        for (int i = 0; i < 4; i++)
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemIncrease()))
            {
                error = "Failed to click the item increase button";
                return false;
            }

        }
        Narrator.stepPassedWithScreenShot("Successfully increased quantity");

        for (int i = 0; i < 4; i++)
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemDecrease()))
            {
                error = "Failed to click the item decrease button";
                return false;
            }
        }
        Narrator.stepPassedWithScreenShot("Successfully decreased quantity");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsItemDeleteButton(), 2))
        {
            AppiumDriverInstance.Driver.swipe(240, 500, 240, 300, 2000);
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemDeleteButton()))
        {
            error = "Failed to click the delete button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully undable to delete first item from list");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsAddNewItemBtn()))
        {
            error = "Failed to click the add new item button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Added new item");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode2")))
        {
            error = "Failed to enter " + testData.getData("ItemCode2") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason2"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason2"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsItemDeleteButton(), 2))
        {
            AppiumDriverInstance.Driver.swipe(240, 500, 240, 300, 2000);
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemDeleteButton()))
        {
            error = "Failed to click the delete button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to wait for the item return next button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to click the item return next button";
            return false;
        }

        validationList = new String[]
        {
            "ADDED_RETURN_ITEMISATION"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean searchCustomer()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back arrow button";
            return false;
        }

        validationList = new String[]
        {
            "ADDED_RETURN_ITEMISATION"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to wait for the item return next button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to click the item return next button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully tapped the contact search button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to wait for the cancel return button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to click the cancel return button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
            {
                error = "Failed to click the more returns no button";
                return false;
            }
        }

        return true;
    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveShelfLabels(shelfLabelName, containerShelfLabel))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean Return()
    {

        int count = 0;
        boolean LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.syncingText(), 5);

        while (LoadingCircle || count < 5)
        {
            try
            {
                String syncing = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.syncingText());
                LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.syncingText(), 5);

                if (syncing.isEmpty())
                {
                    break;
                }

                count++;
            } catch (Exception e)
            {
                narrator.stepPassed("Successfully waited for syncing to be finished - " + e.getMessage());
                return false;

            }

        }

        if (!ReusableFunctionalities.retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retrieve itemID from couchbase";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        validationList = new String[]
        {
            "RETAILER_SELECTED", "HANDOVER_FROM_CUSTOMER", ""
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        validationList = new String[]
        {
            "SECURITY_SCAN_PASSED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        validationList = new String[]
        {
            "ADDED_RETAILER_REFS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        AppiumDriverInstance.pause(2000);

        validationList = new String[]
        {
            "ADDED_CUSTOMER_DETAILS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 30))
            {
                error = "Failed to wait for the progress circe to no longer be present";
                return false;
            }
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 2))
        {
            error = "Failed because temp label screen appeared";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected DVT Printer");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            try
            {
                if (!ReusableFunctionalities.retrieveItemIDFromCouchBase())
                {
                    ReusableFunctionalities.isSeleniumFailure = true;
                    error = "Failed to retrieve item ID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the scan the printed label text";
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!shortcuts.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed(3)";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
        {
            error = "Failed to wait for the returns back to dashboard buton";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
        {
            error = "Failed to click the returns back to dashboard buton";
            return false;
        }

        validationList = new String[]
        {
            "CHECKOUT_COMPLETE"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean itemsCON()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("ReturnRetailer") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'carrier' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("AccountNumber") + "'into the 'Customer Name' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

//================================================================================================================
        //item code 1
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode1")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason1"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason1"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        Narrator.stepPassedWithScreenShot("First return details");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to wait for next button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsAddNewItemBtn()))
        {
            error = "Failed to click the add new item button";
            return false;
        }

        //================================================================================================================
        //item code 2
        Narrator.stepPassedWithScreenShot("First return details");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode2")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason2"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason2"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Second return details");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the doddle email field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the email text field";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.errorAlertTitle()))
        {
            Narrator.stepPassedWithScreenShot("Error Message");
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
        {
            error = "Failed to click the alert error ok button";
            return false;
        }

        return true;
    }

    public boolean ReturnsToCarrierContainer()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.forceStartSync()))
        {
            error = "Failed to click on the syncing indicator";
            return false;
        }

        AppiumDriverInstance.pause(3000);

        if (!shortcuts.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan barcode in dashboard scan";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 10))
            {
                error = "Failed to wait for the progress circle to be no longer present";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.barcodeVal()))
        {
            error = "Failed to wait for barcode";
            return false;
        }

        try
        {
            barcodeVal = AppiumDriverInstance.Driver.findElement(By.id("com.doddle.concession:id/collection_item_upi"));
        } catch (Exception e)
        {
            System.out.println("Error - " + e.getMessage());
            return false;
        }

        temp = barcodeVal.getText().trim();

        if (!temp.equals(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to validate that the barcodes are the same. Barcode generated and parcel being returned is does not have the same barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("retailer and Customer Details");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moveToCarrierContainerButton()))
        {
            error = "Failed to touch the 'Move to Carrier Container' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to wait for the 'Allocate new Container for carrier' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to touch the 'Allocate new Container for carrier' button.";
            return false;
        }

        if (!shortcuts.scanShelfBarcode(containerShelfLabel))
        {
            error = "Failed to scan container shelf label";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.containersForDispatchText()))
        {
            error = "Failed to wait for the 'scan Container For Dispatch' button.";
            return false;
        }
        if (!shortcuts.scanShelfBarcode(containerShelfLabel))
        {
            error = "Failed to scan container shelf label";
            return false;
        }

        return true;

    }

    public boolean DispatchReturn()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("DispatchCarrier")))
        {
            error = "Failed to search for " + testData.getData("DispatchCarrier") + ".";
            return false;
        }
        try
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        } catch (Exception e)
        {
            error = "Failed to click the harware back button " + e.getMessage();
            return false;

        }

        narrator.stepPassedWithScreenShot("Carrier selection");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.selectCarrier(testData.getData("DispatchCarrier"))))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }
        narrator.stepPassedWithScreenShot("dipatch options selection");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        if (!shortcuts.scanShelfBarcode(containerShelfLabel))
        {
            error = "Failed to scan the container shelf label";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchOfCage()))
        {
            error = "Failed to wait for the 'Confirm dispatch of x cages' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchOfCage()))
        {
            error = "Failed to touch on the 'Confirm dispatch of x cages' button ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Dispatch parcel to carrier completed");

        File index = new File(System.getProperty("user.dir") + "\\Barcodes");

        String[] entries = index.list();
        for (String s : entries)
        {
            File currentFile = new File(index.getPath(), s);
            currentFile.delete();
        }

        return true;
    }

    public boolean returnSummary()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
            {
                error = "Failed to touch 'Returns' button.";
                return false;
            }
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to wait for the 'New Rerurn' button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to touch on the 'New Return' button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
            {
                error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
            {
                error = "Failed to touch on the 'enter retail name' textfield";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
            {
                error = "Failed to enter '" + testData.getData("ReturnRetailer") + "'into the Enter Retailer Name textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
            {
                error = "Failed to wait for the 'carrier' tile to be visible";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
            {
                error = "Failed to touch on the 'carrier' tile";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
            {
                narrator.stepPassedWithScreenShot("Scan parcel screen");
                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
                {
                    error = "Failed to wait for the 'Scan Passed' button";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
                {
                    error = "Failed to touch the  'Scan Passed' button";
                    return false;
                }

            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
            {
                error = "Failed to wait for the  'Customer Acc Number' textfield";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
            {
                error = "Failed to enter '" + testData.getData("AccountNumber") + "'into the 'Customer Name' textfield";
                return false;
            }
            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
            {
                error = "Failed to touch the 'RMA Number' textfield";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
            {
                error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to wait for the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to click the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
            {
                error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
            {
                error = "Failed to touch and expand the 'reason' dropdown";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to wait for the 'Reasons'list to appear.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to touch the 'Reason' for item return";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
            {
                error = "Failed to wait for the doddle email field";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
            {
                error = "Failed to touch the email text field";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
            {
                error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
            {
                error = "Failed to touch the  'Search' Button.";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
            {
                error = "Failed to wait for the customer Email to appear";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the back arrow button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back arrow button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        try
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to press the back button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " .User is able to successfully edit data");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!ReusableFunctionalities.retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retreive item id from couchbase";
            return false;
        }

        validationList = new String[]
        {
            "ADDED_CUSTOMER_DETAILS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newLookThreeDotsButton()))
        {
            error = "Failed to wait fot the edit button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newLookThreeDotsButton()))
        {
            error = "Failed to click the edit button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newLookAccountNumberTextField()))
        {
            error = "Failed to wait for the new look text field";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Current NL account number");

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.newLookAccountNumberTextField(), testData.getData("EditedAccountNumber")))
        {
            error = "Failed to enter the new new look number into text field";
            return false;
        }

        Narrator.stepPassedWithScreenShot("New NL account number");

        try
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to press the back button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newLookEditButton()))
        {
            error = "Failed to wait fot the edit button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newLookEditButton()))
        {
            error = "Failed to click the edit button";
            return false;
        }

        Narrator.stepPassed("Successfully updated return references data");

        AppiumDriverInstance.Driver.swipe(300, 450, 240, 200, 2000);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemsBeingReturnedThreeDotsButton()))
        {
            error = "Failed to wait for the return references screen";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemsBeingReturnedThreeDotsButton()))
        {
            error = "Failed to click the items being returned three dots button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsAddNewItemBtn()))
        {
            Narrator.stepPassedWithScreenShot("Successfully able to edit return reference");
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        itemCode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnItemCode());
        Narrator.stepPassedWithScreenShot("Current Item Code - " + itemCode);

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("EditedItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        itemCode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnItemCode());
        Narrator.stepPassedWithScreenShot("new Item Code - " + itemCode);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason1"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason1"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        validationList = new String[]
        {
            "ADDED_RETAILER_REFS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsItemDecrease(), 2))
        {
            AppiumDriverInstance.Driver.swipe(240, 500, 240, 300, 2000);
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemIncrease()))
        {
            error = "Failed to increase the item quantity";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Increased Item Quantity Successfully");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsItemDecrease()))
        {
            error = "Failed to decrease the item quantity";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Decreased Item Quantity Successfully");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.editButton()))
        {
            error = "Failed to click the edit button";
            return false;
        }

        AppiumDriverInstance.Driver.swipe(240, 500, 240, 300, 2000);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemsBeingReturnedThreeDotsButton()))
        {
            error = "Failed to wait for the return references screen";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemsBeingReturnedThreeDotsButton()))
        {
            error = "Failed to click the items being returned three dots button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsAddNewItemBtn()))
        {
            Narrator.stepPassedWithScreenShot("Successfully able to edit return reference");
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsAddNewItemBtn()))
        {
            error = "Failed to click on the add new item button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode2")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason2"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason2"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        validationList = new String[]
        {
            "ADDED_RETURN_ITEMISATION"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        Narrator.stepPassedWithScreenShot("Successfully added a new item");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.editButton()))
        {
            error = "Failed to click the edit button";
            return false;
        }

        AppiumDriverInstance.Driver.swipe(240, 500, 240, 300, 2000);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerThreeDotsButton()))
        {
            error = "Failed to wait for the return references screen";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerThreeDotsButton()))
        {
            error = "Failed to click the customer three dots button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }

        doddleEmail = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.doddleEmailTextField());
        Narrator.stepPassedWithScreenShot("Current email" + doddleEmail);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("EditedEmail")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        doddleEmail = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.doddleEmailTextField());
        Narrator.stepPassedWithScreenShot("Edited email" + doddleEmail);

        try
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
            AppiumDriverInstance.pause(1000);
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to press the back button";
            return false;
        }

        validationList = new String[]
        {
            "ADDED_CUSTOMER_DETAILS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to click on the cancel this return button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to wait for the go back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to click the go back button";
            return false;
        }

        Narrator.stepPassed("Back on Summary Screen");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to click on the cancel this return button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to wait for the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to click the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
        {
            error = "Failed to click the cancel booking button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the no button on the any more returns popup";
            return false;
        }

        validationList = new String[]
        {
            "CANCELLED_HANDOVER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode2")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to click on the cancel this return button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to wait for the customer changed mind option";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully touched cancel return button");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to click the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
        {
            error = "Failed to click the cancel booking button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the no button on the any more returns popup";
            return false;
        }

        if (!ReusableFunctionalities.returnsBackToDashboard())
        {
            error = "Falied to navigate to dashboard";
            return false;
        }

        return true;
    }

    public boolean tempLabelMockBuild()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to wait for the return tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the  'Confirm and Print Label' Button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 15))
            {
                error = "Failed to wait for the progress circe to no longer be present";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected DVT Printer");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }
        } else
        {
            error = "Scan a temporary screen appeared";
            return false;
        }

        try
        {
            if (!ReusableFunctionalities.retrieveItemIDFromCouchBase())
            {
                ReusableFunctionalities.isSeleniumFailure = true;
                error = "Failed to retreive item id from couchbase";
                return false;
            }

            Shortcuts.newBearer();

            Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

            String bearer = SettersAndGetters.getNewBearer();
            String apiResponse = SettersAndGetters.getAPIResponse();

            //Getting LabelValue
            apiResponse = SettersAndGetters.getAPIResponse();
            regex = "\"labelValue\":\"";
            lines = apiResponse.split(regex);
            liness = lines[1].split("\",\"url\":");
            apiResponse = liness[0];
            LabelValue = apiResponse;
            SettersAndGetters.setReturnsLabelValue(LabelValue);
            System.out.println(LabelValue);

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve the label value from couchbase";
            return false;
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerErrorMessage(), 2))
        {
            error = "failed to connect to printer";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the scan the printed label text";
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!Shortcuts.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed(3)";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the any more returns popup message";
            return false;
        }

        if (!ReusableFunctionalities.returnsBackToDashboard())
        {
            error = "Failed to navigate back to dashboard";
            return false;
        }

        validationList = new String[]
        {
            "CHECKOUT_COMPLETED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        return true;
    }

    public boolean labelDidntPrint()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the customer email to appear";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 15))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected DVT Printer");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPrintedLabelText()))
        {
            error = "Failed to wait for the scan the printed label text";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnLabelDidntPrintButton()))
        {
            error = "Failed to click the label didnt print/wont scan button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText()))
        {
            Narrator.stepPassedWithScreenShot("Scan a temporary label screen appeared");
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retrieve item ID from couchbase";
            return false;
        }

        validationList = new String[]
        {
            "VOIDED_RETURN_LABEL"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to wait for the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to click the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
        {
            error = "Failed to click the cancel booking button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the no button on the any more returns popup";
            return false;
        }

        return true;
    }

    public boolean tempLabel()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!ReusableFunctionalities.wifiOff())
        {
            error = "Faield to turn off the wifi";
            return false;
        }

        wifiStatus = false;

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 8))
        {
            Narrator.stepPassedWithScreenShot("Successfully validated scan a temp label screen");

        } else
        {
            error = "Failed to validate temp label screen";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 1))
        {
            Narrator.stepPassed("Back button not present");
        }

        if (!save.saveShelfLabel(temp))
        {
            error = "Failed to save shelfLabel";
            return false;
        }

        if (!ReusableFunctionalities.scanShelfBarcode(temp))
        {
            error = "Failed to scan temp label barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 8))
        {
            error = "App Crash!";
            return false;
        }

        if (!ReusableFunctionalities.wifiOn())
        {
            error = "Failed to turn the wifi on";
            return false;
        }

        if (!retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retrieve item ID from couchbase";
            return false;
        }

        validationList = new String[]
        {
            "RETURN_ROUTING_DETERMINED", "ADDED_RETURN_LABEL"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 5))
        {
            error = "Failed because of app crash";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton(), 2))
        {
            Narrator.stepPassed("Back button not present");
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to wait for the any more returns popup message";
            return false;
        }

        if (!ReusableFunctionalities.returnsBackToDashboard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        validationList = new String[]
        {
            "CHECKOUT_COMPLETED", "AT_COLLECTION_POINT"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        AppiumDriverInstance.pause(1500);

        if (!ReusableFunctionalities.checkWifiIsOn())
        {
            error = "Failed to switch wifi on";
            return false;
        }

        wifiStatus = true;

        return true;
    }

    public boolean cancelReturnUsingCurrent()
    {

        if (!ReusableFunctionalities.ReturnFlow())
        {
            error = "Failed to do the return flow";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
            {
                error = "Failed to touch 'Returns' button.";
                return false;
            }
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to wait for the 'New Rerurn' button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to touch on the 'New Return' button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to touch Menu";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the main menu button and selected dashboard button");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsCurrentButton()))
        {
            error = "Failed to click the current button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully touched the current option");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Customer changed mind option");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        AppiumDriverInstance.pause(2000);

        validationList = new String[]
        {
            "VOID", "CANCELLED_HANDOVER", "reason"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!ReusableFunctionalities.ReturnFlow())
        {
            error = "Failed to do the return flow";
            return false;
        }

        if (!ReusableFunctionalities.ReturnFlow())
        {
            error = "Failed to do the return flow";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to touch Menu";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the main menu button and selected dashboard button");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsCurrentButton()))
        {
            error = "Failed to click the current button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully touched the current option");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Customer changed mind option");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Available returns screen");

        AppiumDriverInstance.pause(2000);

        validationList = new String[]
        {
            "VOID", "CANCELLED_HANDOVER", "reason"
        };

        return true;
    }

    public boolean cancelReturn()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton(), 1))
        {
            if (!ReusableFunctionalities.returnsBackToDashboard())
            {
                error = "Failed to navigate back to dashboard";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
            {
                error = "Failed to touch 'Returns' button.";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to wait for the 'New Rerurn' button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to touch on the 'New Return' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), 2))
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
            {
                error = "Failed to wait for the  'Customer Acc Number' textfield";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
            {
                error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
                return false;
            }
            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
            {
                error = "Failed to touch the 'RMA Number' textfield";
                return false;
            }
            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
            {
                error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to wait for the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to click the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
            {
                error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
            {
                error = "Failed to touch and expand the 'reason' dropdown";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to wait for the 'Reasons'list to appear.";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to touch the 'Reason' for item return";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField(), 2))
            {

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
                {
                    error = "Failed to wait for the  Enter Carrier order id textfield";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
                {
                    error = "Failed to touch the  Enter Carrier order id textfield";
                    return false;
                }
                if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
                {
                    error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
                    return false;
                }

                Driver.pressKeyCode(AndroidKeyCode.BACK);

                narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
                {
                    error = "Failed to wait for the confirm and print label button";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
                {
                    error = "Failed to touch the  'Search' Button.";
                    return false;
                }
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        String xpath = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.printerOption());

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 15))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {

                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            int count = 0;

            while (count < 3)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 4))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
                    {
                        error = "Failed to click away the error message";
                        return false;
                    }

                    if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
                    {
                        error = "Failed to click on the retry button";
                        return true;
                    }

                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
                    {
                        error = "Failed to click the retry button";
                        return false;
                    }
                } else
                {
                    count = 4;
                }

                count++;
            }

            try
            {
                if (!ReusableFunctionalities.retrieveItemIDFromCouchBase())
                {
                    ReusableFunctionalities.isSeleniumFailure = true;
                    error = "Failed to retrieve itemID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
            {
                error = "Failed to wait for the  'Customer Acc Number' textfield";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
            {
                error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
                return false;
            }
            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
            {
                error = "Failed to touch the 'RMA Number' textfield";
                return false;
            }
            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
            {
                error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to wait for the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to click the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
            {
                error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
            {
                error = "Failed to touch and expand the 'reason' dropdown";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to wait for the 'Reasons'list to appear.";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to touch the 'Reason' for item return";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            AppiumDriverInstance.pause(1000);

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to touch Menu";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Returns Popup");

        return true;
    }

    public boolean cancelReturnUsingAll()
    {

        if (!cancelReturn())
        {
            error = "Failed to do the returns process";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to wait for the do you want to cancel CURRENT return or ALL returns popup";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Cancel return for CURRENT/ALL parcels");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to click the cancel button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the cancel returns all button");

        Narrator.stepPassed("Successfully selected ALL, cancel returns booking screen appeared");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        Narrator.stepPassed("Selected more returns yes");

        AppiumDriverInstance.pause(3000);

        if (!cancelReturn())
        {
            error = "Failed to do the returns process";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to wait for the do you want to cancel CURRENT return or ALL returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to click the cancel button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the cancel returns all button");

        Narrator.stepPassed("Successfully selected ALL, cancel returns booking screen appeared");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected more returns no");

        validationList = new String[]
        {
            "VOID", "CANCELLED_HANDOVER", "reason"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean specificCaseCON1589()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.missguidedRMANumber()))
        {
            error = "Failed to wait for 'RMA Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.missguidedRMANumber()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.missguidedRMANumber(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.missguidedOrderID()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.missguidedOrderID(), MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to wait for the order id field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to click on confirm and print label button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 15))
        {
            Narrator.stepPassed("Successfully generate label with parcelItemisation disabled");
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {

                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            int count = 0;

            while (count < 3)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 4))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
                    {
                        error = "Failed to click away the error message";
                        return false;
                    }

                    if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
                    {
                        error = "Failed to click on the retry button";
                        return true;
                    }

                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
                    {
                        error = "Failed to click the retry button";
                        return false;
                    }
                } else
                {
                    count = 4;
                }

                count++;
            }

            try
            {
                if (!retrieveItemIDFromCouchBase())
                {
                    ReusableFunctionalities.isSeleniumFailure = true;
                    error = "Failed to retrieve item ID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton(), 1))
        {
            if (!ReusableFunctionalities.returnsBackToDashboard())
            {
                error = "Failed to navigate back to dashboard";
                return false;
            }
        } else
        {

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
            {
                error = "Failed to wait for the returns back to dashboard buton";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
            {
                error = "Failed to click the returns back to dashboard buton";
                return false;
            }
        }

        validationList = new String[]
        {
            "CHECKOUT_COMPLETED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean dashboardScan()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the checkin button";
            return false;
        }

        AppiumDriverInstance.pause(3000);

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            if (SettersAndGetters.getReturnsLabelValue() == null)
            {
                error = "Could not print label value";
                return false;
            } else
            {
                error = "Failed to scan label value";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Scanned on dashboard");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moveToCarrierContainerButton(), 10))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moveToCarrierContainerButton()))
            {
                error = "Failed to click on the move to carrier container button";
                return false;
            }
            Narrator.stepPassedWithScreenShot("Select move contatiner to carrier");

            if (!allocatingContainer())
            {
                error = "Failed to put return in container";
                return false;
            }
        } else
        {
            error = "App took too long to display layout";
            return false;
        }

        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }

        validationList = new String[]
        {
            "SCANNED_TO_DESPATCH_CONTAINER", "AT_GOODS_OUT"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabaseUsingLabelValue(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean despatchSelectedContainers()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch the checkin button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Other") + ".";
            return false;
        }

        narrator.stepPassedWithScreenShot("Carrier selection");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to click Other tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        narrator.stepPassedWithScreenShot("dispatch options selection");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!ReusableFunctionalities.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan newly allocated container";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchOfCage()))
        {
            error = "Failed to wait for the 'Confirm dispatch of x cages' button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected container- " + SettersAndGetters.getGeneratedShelfCode() + " " + "Ready for despatch");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchOfCage()))
        {
            error = "Failed to touch on the 'Confirm dispatch of x cages' button ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Dispatch parcel to carrier completed");

        validationList = new String[]
        {
            "DESPATCHED", "SENT_TO_RETAILER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabaseUsingLabelValue(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public boolean specificCase()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (SettersAndGetters.getReturnsLabelValue() == null)
        {
            error = "Failed to scan the returns label value";
            return false;
        }

        if (!ReusableFunctionalities.scan(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan barcode in the storage process";
            return false;
        }

        try
        {
            specificCaseText = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnsStoragePopupAlert());
            Narrator.stepPassedWithScreenShot("Successfully scanned the barcode");
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve popup text";
            return false;
        }

        if (specificCaseText.contains("This is a return!"))
        {
            Narrator.stepPassedWithScreenShot("Successfully validated returns barcode in storage process");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton());
        } else
        {
            error = "Empty string";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the back arrow button after popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back arrow button after popup";
            return false;
        }

        return true;
    }

    public boolean specificCaseTempLabel()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to find the print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.jacekPrinterOption(), 15))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.jacekPrinterOption()))
            {
                error = "Failed to click on the jacek printer option";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
        {
            error = "Failed to wait for select printer button";
            return false;
        }

        for (int i = 0; i < 4; i++)
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            AppiumDriverInstance.pause(15000);

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerNotConnectedText(), 4))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn()))
                {
                    error = "Failed to click the ok button";
                    return false;
                }
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText()))
        {
            error = "Failed to wait for the scan a temp label screen";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Tapped retry button 3 times, Scan a temp label screen appeared.");

        if (!SaveBarcodesFromURL.saveShelfLabel(shelfLabelName))
        {
            error = "Faiiled to generate and save shelflabel";
            return false;
        }

        if (!ReusableFunctionalities.scanShelfBarcode(shelfLabelName))
        {
            error = "Failed to scan shelflabel";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenuCollectionTile(), 5))
        {
            Narrator.stepFailedWithScreenShot("App crash");
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for tnhe returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (!securityScanFailed())
        {
            error = "Failed to do security scan failed " + ReusableFunctionalities.error;
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanFailedDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns message";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dialogueRightOption()))
        {
            error = "Failed toclick the any more returns message";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!retrieveItemIDFromCouchBase())
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "Failed to retrieve item ID from couchbase";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        validationList = new String[]
        {
            "SECURITY_SCAN_PASSED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            ReusableFunctionalities.isSeleniumFailure = true;
            error = "failed to validate couchbase";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
            {
                error = "Failed to touch the  Enter Carrier order id textfield";
                return false;
            }
            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
            {
                error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
            {
                error = "Failed to wait for the confirm and print label button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
            {
                error = "Failed to touch the  'Search' Button.";
                return false;
            }
        }

        AppiumDriverInstance.pause(1000);

        validationList = new String[]
        {
            "ADDED_CUSTOMER_DETAILS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back arrow button";
            return false;
        }

        Narrator.stepPassed("Successfully navigated back");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the list out each of the items being returned below screen";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.editButton()))
        {
            error = "Failed to click the next button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemsBeingReturnedThreeDotsButton()))
        {
            error = "Failed to wait for the return references screen";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemsBeingReturnedThreeDotsButton()))
        {
            error = "Failed to click the items being returned three dots button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsAddNewItemBtn()))
        {
            Narrator.stepPassedWithScreenShot("Successfully able to edit return reference");
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("EditedItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        validationList = new String[]
        {
            "ADDED_RETAILER_REFS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        Narrator.stepPassed("Updated returns items successfully");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.editButton()))
        {
            error = "Failed to click the edit button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerThreeDotsButton()))
        {
            error = "Failed to wait for the return references screen";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerThreeDotsButton()))
        {
            error = "Failed to click the customer three dots button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("EditedEmail")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        try
        {
            WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
            int xLocation = element.getLocation().x + 70;
            int yLocation = element.getLocation().y + 40;

            if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
            {
                error = "Failed to tap at desired location";
                return false;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "EXCEPTION - " + e.getMessage();
            return false;
        }

        validationList = new String[]
        {
            "ADDED_CUSTOMER_DETAILS"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to click on the cancel this return button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to wait for the go back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to click the go back button";
            return false;
        }

        Narrator.stepPassed("Back on Summary Screen");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to click on the cancel this return button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to wait for the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to click the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
        {
            error = "Failed to click the cancel booking button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the no button on the any more returns popup";
            return false;
        }

        validationList = new String[]
        {
            "CANCELLED_HANDOVER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do the validtions";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton(), 1))
        {
            if (!ReusableFunctionalities.returnsBackToDashboard())
            {
                error = "Failed to navigate back to dashboard";
                return false;
            }
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode2")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to click on the cancel this return button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to wait for the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to click the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
        {
            error = "Failed to click the cancel booking button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the no button on the any more returns popup";
            return false;
        }

        if (!ReusableFunctionalities.returnsBackToDashboard())
        {
            error = "Failed to navigate back to dashboard";
            return false;
        }

        if (!ReusableFunctionalities.singelContainer())
        {
            error = "Failed to do the single container flow";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton(), 1))
        {
            if (!ReusableFunctionalities.returnsBackToDashboard())
            {
                error = "Failed to navigate back to dashboard";
                return false;
            }
        } else
        {

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }
        }

        return true;
    }

    public boolean dashboardScanPrecondition()
    {
        if (!ReusableFunctionalities.singelContainer())
        {
            error = "Failed to do the single container process";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }
        } else
        {
            ReusableFunctionalities.returnsBackToDashboard();
        }

        return true;
    }

    public boolean CON1321()
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginButton(), 5))
        {
            if (!ReusableFunctionalities.Login())
            {
                error = "Failed to do the login proceess";
                return false;
            }
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        try
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to press the back button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("go back button present");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to click the go back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.goBackButton()))
        {
            error = "Failed to click the go back button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Succesfull tapped on go back button");

        if (!ReusableFunctionalities.returnsBackToDashboard())
        {
            error = "Failed to navigate back to dashboard";
            return false;
        }

        return true;
    }

    public boolean allocatingContainer()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allocateBtn()))
            {
                error = "Failed to click Allocate new container for Carrier button. " + "Config did not change";
                return false;
            }

            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            if (!scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
            {
                error = "Failed to scan";
                return false;
            }
        } else
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
            {
                SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

                SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

                Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

                Narrator.stepPassedWithScreenShot("Creating new despatch sack - " + SettersAndGetters.getGeneratedShelfCode());

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
                {
                    error = "Failed to put the parcel in the despatch sack";
                    return false;
                }

                Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
                {
                    error = "Failed to put the parcel in the despatch sack";
                    return false;
                }

                Narrator.stepPassedWithScreenShot("Return in depsatch sack" + SettersAndGetters.getGeneratedShelfCode());

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
                {
                    error = "Failed to click the returns done button";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
                {
                    error = "Failed to click the returns done button";
                    return false;
                }

            } else if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.sackBarcode()))
            {
                error = "Failed to wait for the 'put return in container screen'";
                return false;
            }

            String sackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.sackBarcode());

            int a = sackBarcode.indexOf("DPS");
            int b = sackBarcode.lastIndexOf("");

            String str = sackBarcode.substring(a, b);

            SettersAndGetters.setGeneratedShelfCode(str);

            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Return in depsatch sack" + SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }

        return true;
    }
}

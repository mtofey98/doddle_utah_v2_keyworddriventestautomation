/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author hduplessis
 */
public class TC_NextGen_03_Carrier extends BaseClass
{

    String error = "";
    Narrator narrator;
    StringEntity input;

//==========================Variables for custom methods=====================
    String randomNumber = MobileDoddlePageObjects.randomNo();
    //String shelfBarcode = MobileDoddlePageObjects.rand();
    String bCode = MobileDoddlePageObjects.barcode();
    boolean testPassed = true;
    public static TestEntity currentData;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();

//==========================================================================
    public TC_NextGen_03_Carrier(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!ReusableFunctionalities.carrierProcessOtherExecution())
        {
            return narrator.testFailedScreenshot("Failed to do the Carrier process - " + Shortcuts.error);
        }
        
        return narrator.finalizeTest("Successfully completed Carrier Process test");
    }

    public boolean executeBarCode(String barcode, String shelfBarcode)
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            error = "Failed to retrieve the new bearer token - ";
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            error = "Failed to generate and save the barcodes - ";
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            error = "Failed to do the rest call for the payload - ";
        }

        narrator.stepPassed("Successfully completed the Barcode set up");

        return true;
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();

            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, SettersAndGetters.getGeneratedBarCode(), storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public void writeToExcel(String barcode, String fName, String fName2)
    {
        try
        {
            //Get the excel file.
            FileInputStream file = new FileInputStream(new File(System.getProperty("user.dir") + "//DatabaseRecordsLabels//" + fName2));

            //Get workbook for XLS file.
            XSSFWorkbook yourworkbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook.
            //If there have >1 sheet in your workbook, you can change it here IF you want to edit other sheets.
            XSSFSheet sheet1 = yourworkbook.getSheetAt(0);

            // Get the row of your desired cell.
            // Let's say that your desired cell is at row 2.
            Row row = sheet1.getRow(6);
            // Get the column of your desired cell in your selected row.
            // Let's say that your desired cell is at column 2.
            Cell column = row.getCell(1);
            // If the cell is String type.If double or else you can change it.
            String updatename = column.getStringCellValue();
            //New content for desired cell.
            updatename = " "+barcode;
            //Print out the updated content.
            //System.out.println(updatename);
            //Set the new content to your desired cell(column).
            column.setCellValue(updatename);
            //Close the excel file.
            file.close();
            //Where you want to save the updated sheet.
            FileOutputStream out
                    = new FileOutputStream(new File(System.getProperty("user.dir") + "//DatabaseRecordsLabels//" + fName));
            yourworkbook.write(out);
            out.close();

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mlopes
 */
public class TC_NextGen_17_Collection_ConfirmingCustomerIdentity extends BaseClass
{

    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    //barcodes
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    //***************

    //API call body
    StringEntity input;

    //***************
    public TC_NextGen_17_Collection_ConfirmingCustomerIdentity(TestEntity testData)
    {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
            }
            
            if (!initiateCollectionByEmailAddress_DisconfirmID())
            {
                return narrator.testFailedScreenshot("Failed to Disconfirm the Customer ID - " + error);
            }
            
             return narrator.finalizeTest("Successfully confirmed customer identity");
        }
        
        else
        {                  
            if (!initiateCollectionByEmailAddress_DisconfirmID())
            {
                return narrator.testFailedScreenshot("Failed to Disconfirm the Customer ID - " + error);
            }
        }
        
        return narrator.finalizeTest("Successfully confirmed customer identity");
    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            SettersAndGetters.getPreadvicePost().addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, barcode, storeID));
            input.setContentType("application/json");
            SettersAndGetters.getPreadvicePost().setEntity(input);
            HttpResponse preadviceResponse = SettersAndGetters.getClient().execute(SettersAndGetters.getPreadvicePost());
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean initiateCollectionByEmailAddress_DisconfirmID() //added by Manuel Lopes
    {

        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode("DPS" + MobileDoddlePageObjects.barcode());

        Shortcuts.preAadviceCollectionParcel(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), newBearer);

        if (!SaveBarcodesFromURL.saveBarcode(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Click on the collection tile");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to touch the 'Email' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the email option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("IncorrectEmail")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered incorrect email address");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered email address");

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionDisconfirmCustomerID()))
        {
            error = "Failed to wait for the no button";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionDisconfirmCustomerID()))
        {
            error = "Failed to click on the no button";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to touch the 'Email' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the email option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered email address");

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the correct button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }
        
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate to dashboard" + ReusableFunctionalities.error;
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked the ok button");
        

        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.NewDoddleLogin;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.conErorr;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.multiplePopUpMessages;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.popUpMessage;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scrollToSubElement;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author hduplessis
 */
public class TC_NextGen_02_Login extends BaseClass
{

    String error = "";
    Narrator narrator;
    StringEntity input;

//==========================Variables for custom methods=====================
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    //   String cardNo = testData.getData("cardNo");
    boolean testPassed = true;
    public static TestEntity currentData;
    public String newBearer = "";
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    String connectionName = "";

//==========================================================================
    public TC_NextGen_02_Login(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        //Added by Hyran DuPlessis
        executeBarCode();
        //Wifi Log in
        if (!dynamicWifi())
        {
            return narrator.testFailedScreenshot("Failed to attempt Login without wifi" + Shortcuts.error);
        }
        //Wrong store login
        if (!thisLogin())
        {
            return narrator.testFailedScreenshot("Failed to attempt Login with wrong Store " + Shortcuts.error + error);
        }
        //normal login
        if (!(LoginReuseCopy()))
        {
            return narrator.testFailedScreenshot("Failed to Login " + Shortcuts.error);
        }
        //logout
        if (!(ReusableFunctionalities.Logout()))
        {
            return narrator.testFailedScreenshot("Failed to Logout " + Shortcuts.error);
        }
        //Wrong email and password log in
        if (!loginEmail())
        {
            return narrator.testFailedScreenshot("Failed to attempt Login with wrong email " + Shortcuts.error);
        }
        if (!loginPassword())
        {
            return narrator.testFailedScreenshot("Failed to attempt Login with wrong password " + Shortcuts.error);
        }
        //still needs testing/debugging
        return narrator.finalizeTest("Successfully completed Logging In Process test");
    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();

            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, SettersAndGetters.getGeneratedBarCode(), storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        } catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean thisLogin()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText()))
        {
            error = "Failed to wait for the version text to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.versionText()))
        {
            error = "Failed to click on the version text";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.releaseNotesText()))
        {
            error = "Failed to wait for the release notes text to be visible";
            return false;
        }

        narrator.stepPassedWithScreenShot("Release Notes");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.whatsNewCloseBtn()))
        {
            error = "Failed to click on the close button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            return false;
        }

        if (!scrollToSubElement2(testData.getData("IncorrectStore")))
        {
            error = "Could not find " + testData.getData("IncorrectStore");
            return false;
        }
        if (!multiplePopUpMessages2())
        {
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginPassword()))
        {
            error = "Failed to touch the password field.";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }
        String permissionsError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.messagePopup());

        narrator.stepPassedWithScreenShot("Error - " + permissionsError);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to wait for the permissions popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to click on the ok popup";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to click back Arrow";
            return false;
        }

        return true;
    }

    public boolean loginEmail()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText()))
        {
            error = "Failed to wait for the version text to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.versionText()))
        {
            error = "Failed to click on the version text";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.releaseNotesText()))
        {
            error = "Failed to wait for the release notes text to be visible";
            return false;
        }

        narrator.stepPassedWithScreenShot("Release Notes");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.whatsNewCloseBtn()))
        {
            error = "Failed to click on the close button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            return false;
        }

        if (!scrollToSubElement2(testData.getData("StoreSelection")))
        {
            error = "Could not find " + testData.getData("StoreSelection");
            return false;
        }

        if (!popUpMessage2())
        {
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }
        //Enters Wrong email
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("email2")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage2())
        {
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginPassword()))
        {
            error = "Failed to touch the password field.";
            return false;
        }

        if (!popUpMessage2())
        {
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }
        String permissionsError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.messagePopup());

        narrator.stepPassedWithScreenShot("Error - " + permissionsError);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to wait for the permissions popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to click on the ok popup";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to click back Arrow";
            return false;
        }

        return true;
    }

    private boolean loginPassword()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText()))
        {
            error = "Failed to wait for the version text to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.versionText()))
        {
            error = "Failed to click on the version text";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.releaseNotesText()))
        {
            error = "Failed to wait for the release notes text to be visible";
            return false;
        }

        narrator.stepPassedWithScreenShot("Release Notes");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.whatsNewCloseBtn()))
        {
            error = "Failed to click on the close button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            return false;
        }

        if (!scrollToSubElement2(testData.getData("StoreSelection")))
        {
            error = "Could not find " + testData.getData("StoreSelection");
            return false;
        }

        if (!popUpMessage2())
        {
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }
//
//        if (!popUpMessage())
//        {
//            return false;
//        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage2())
        {
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginPassword()))
        {
            error = "Failed to touch the password field.";
            return false;
        }

        if (!popUpMessage2())
        {
            return false;
        }
        //Will enter the wrong password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("wrongPassword")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }
        String permissionsError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.messagePopup());

        narrator.stepPassedWithScreenShot("Error - " + permissionsError);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to wait for the permissions popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to click on the ok popup";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to click back Arrow";
            return false;
        }

        return true;
    }

    public boolean wifiSwitch()
    {
        //String testcase = narrator.getData("Testcase");
        MobileElement doddleText = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.doddleMainText()));

        if (doddleText.isDisplayed())
        {
            Narrator.stepPassed("Successfully re-entered the doddle app");
        } else
        {
            error = "Failed to re-enter the doddle app";
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            return false;
        }

        if (!scrollToSubElement2(narrator.getData("IncorrectStore")))
        {
            error = "Failed to find " + narrator.getData("StoreSelection");
            return false;
        }

        if (!popUpMessage2())
        {
            return false;
        }

        if (!Login2(testData.getData("username"), testData.getData("password"), ""))
        {
            return false;
        }
        Narrator.stepPassedWithScreenShot("Connection error");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn()))
        {
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to wait for the toolbar back text to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to click on the toolbar back text";
            return false;
        }

        return true;
    }

    public boolean wifiSwitchOff()
    {
        String result = MobileDoddlePageObjects.wifiBtnWithIndex();
        try
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                error = "Failed to click the wifi button with index";
                return false;

            }

//            }
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiDisconnected()))
        {
            error = "Failed to wait for the wifi to be disconnected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned off Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }
        public boolean wifiSwitchOff2()
    {
        //String result = MobileDoddlePageObjects.wifiBtnWithIndex3();


        try
        {

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex3(connectionName)))
            {
                error = "Failed to click the wifi button with index";
                return false;

            }

//            }
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiDisconnected()))
        {
            error = "Failed to wait for the wifi to be disconnected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned off Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }
    public boolean Login2(String username, String password, String testcase)
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleMainText(), 2))
        {

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }

            if (!multiplePopUpMessages2())
            {
                error = "Failed to handle popup alerts";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }

        if (!popUpMessage2())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), username))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage2())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!popUpMessage2())
        {
            error = "Failed to handle alert popup message";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), password))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (testcase.equals("Wifi"))
        {
            String connectionError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.messagePopup());
            Narrator.stepPassed("Error - " + connectionError);

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okMessageButton()))
            {
                error = "Failed to wait for the ok pop up message";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okMessageButton()))
            {
                error = "Failed to click on the ok pop up message";
                return false;
            }
        } else
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.blockingScreenClose(), 2))
            {
                if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.blockingScreenClose(), 5))
                {
                    error = "Failed to wait for the semi-blocking screen to not be visible";
                    return false;
                }
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
            {
                error = "Failed to wait for the check in button.";
                return false;
            }

            Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");
        }

        return true;

    }

    public boolean wifiSwitchOn()
    {
//        try {
//            Driver.openNotifications();
//        } catch (Exception e) {
//            error = "Failed to open notifications - " + e;
//            return false;
//        }
//
//        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn())) {
//            error = "Failed to wait for the quick settings button";
//            return false;
//        }
//
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn())) {
//            error = "Failed to click the quick settings button";
//            return false;
//        }

        try
        {
//            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex2())) {
//                error = "Failed to wait for the wifi button with index to be visible";
//                return false;
//            }

//            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
//            String wifiText = element.getAttribute("name");
//
//            System.out.println("");
//
//            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi")) {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex2()))
            {
                error = "Failed to click the wifi button with index";
                return false;

            }

            //}
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
        {
            error = "Failed to wait for the wifi to be connected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    private boolean popUpMessage2()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean multiplePopUpMessages2()
    {

        for (int i = 0; i < 3; i++)
        {
            if (!popUpMessage2())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean LoginReuseCopy()
    {
        if (!conErorr)
        {
            
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages2())
            {
                error = "Failed to handle multiple popups";
                return false;
            }

            String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (!store.equals(testData.getData("StoreSelection")))
            {
                if (!scrollToSubElement2(narrator.getData("StoreSelection")))
                {
                    error = "Could not find " + narrator.getData("StoreSelection");
                    return false;
                }
            }

            if (!multiplePopUpMessages2())
            {
                error = "Failed to handle multiple popups";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages2())
            {
                error = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            error = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                LoginReuseCopy();

            }
        }
//
//        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton())) {
//            error = "Failed to wait for the check in button.";
//            return false;
//        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

        return true;
    }

    public boolean scrollToSubElement2(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages2())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            } else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        } catch (Exception e)
        {
            return false;
        }

    }

    public boolean dynamicWifi()
    {
        if (System.getProperty("user.name").contains("Device Farm"))
        {
            connectionName = "Handhelds";
        }
        
        else
        {
            connectionName = "GTC";
        }
        
        try
        {
            Driver.openNotifications();
        } catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex(), 5))
        {          
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex3(connectionName)))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }
            
            
            if (AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.wifiBtnWithIndex3(connectionName)).equals(connectionName))
            {

                wifiSwitchOff2();
                wifiSwitch();

                // ===========After Login Got a Connection error,it has to open notifications again=====================
                try
                {
                    Driver.openNotifications();
                } catch (Exception e)
                {
                    error = "Failed to open notifications - " + e;
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
                {
                    error = "Failed to wait for the quick settings button";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
                {
                    error = "Failed to click the quick settings button";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex2()))
                {
                    error = "Failed to wait for the wifi button with index to be visible";
                    return false;
                }
                
                wifiSwitchOn();
            }
            
            else
            {
                wifiSwitchOn();
            }
        }
       else if (AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()).equals("DVT-Guest"))
        {
            wifiSwitchOff();
            wifiSwitch();

            // ===========After Login Got a Connection error,it has to open notifications again=====================
            try
            {
                Driver.openNotifications();
            } catch (Exception e)
            {
                error = "Failed to open notifications - " + e;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
            {
                error = "Failed to wait for the quick settings button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
            {
                error = "Failed to click the quick settings button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex2()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }
            wifiSwitchOn();
        } else
        {
            wifiSwitchOn();
        }

        return true;
    }

    public boolean testWifi1()
    {

        if (AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()).equals("DVT-GUEST"))
        {

            wifiSwitchOff();
            wifiSwitch();

            // ===========After Login Got a Connection error,it has to open notifications again=====================
            try
            {
                Driver.openNotifications();
            } catch (Exception e)
            {
                error = "Failed to open notifications - " + e;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
            {
                error = "Failed to wait for the quick settings button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
            {
                error = "Failed to click the quick settings button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex2()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }
            wifiSwitchOn();
        } else
        {
            wifiSwitchOn();
        }
        return true;
    }

}

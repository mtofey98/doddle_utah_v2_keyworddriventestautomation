/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.CollectionProcess;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.backToDashBoard;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.checkCarriersRecordsInDatabase;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.collectionEndProcess;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.collectionOptionPassport;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.moreCollectionCodesNo;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.searchWithCollectionCodes;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.line;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.popUpMessage;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.rescanButton;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanBarcode;
import static KeywordDrivenTestFramework.Testing.TestMarshall.stopAppiumServerIfExists;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.http.client.methods.HttpGet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_64 extends BaseClass
{

    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String amazonBarcode = "";
    String amazonShelf = "";
    String amazonBarcode2 = "";
    String amazonShelf2 = "";
    String amazonBarcode3 = "";
    String amazonShelf3 = "";

    //API call body
    StringEntity input;

    public TC_NextGen_64(TestEntity testData)
    {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
            {
                TC_NextGen_03_Carrier cr = new TC_NextGen_03_Carrier(testData);
                if (!ReusableFunctionalities.Login())
                {
                    return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
                }

                //===================01============
                String bb1 = MobileDoddlePageObjects.amazonBarcode();
                String shelf = MobileDoddlePageObjects.rand();
                cr.executeBarCode(bb1, shelf);

                writeToExcel2(bb1, "TC_NextGen_64bCopy.xlsx", "TC_NextGen_64aCopy.xlsx");

                if (!ReusableFunctionalities.carrierProcessExecution())
                {
                    return narrator.testFailedScreenshot("Failed to do the Carrier process - " +error);
                }

                Shortcuts.newBearer();
                ReusableFunctionalities.createCollectionBarcodeWithOutCustomerBlock2(SettersAndGetters.getGeneratedBarCode(), bb1, shelf);

                Shortcuts.doCollectionsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                if (!Shortcuts.line.contains("\"retailerId\""))
                {
                    error = "\"searchType\": \"retailerOrderId\" was not found in the ITEM_COLLECTION record";
                    return narrator.testFailed(error);
                }

                if (!Shortcuts.changeAPIResponseColor("\"retailerId\""))
                {
                    error = "\"searchType\": \"retailerOrderId\" was not found in the ITEM_COLLECTION record";
                    return narrator.testFailed(error);
                }
                Narrator.stepPassed(Shortcuts.line);

                if (!StorageProccess())
                {
                    return narrator.testFailedScreenshot("Failed to do the Storage process - "+  error);
                }

                if (!checkCarriersRecordsInDatabase("TC_NextGen_64bCopy.xlsx"))
                {
                    return narrator.testFailedScreenshot("Failed to Validate Carrier Parcel- " +error);
                }

                ReusableFunctionalities.backToDashBoard();
                return narrator.finalizeTest("Successfully completed TC_nextGen_64");
            }
            return narrator.testFailedScreenshot("Failed to wait for 'Version Text'");
        }
    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();

            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, SettersAndGetters.getGeneratedBarCode(), storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        } catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean getStoreSystemUIConfig(String newBearer)
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet storeConfigPut = new HttpGet("https://stage-apigw.doddle.it/v2/stores/456DVT");

        try
        {

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input.setContentType("application/json");
            HttpResponse response2 = client.execute(storeConfigPut);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

//            if (!line.contains("SINGLE_CONTAINER"))
//            {
//                narrator.testFailed(line);
//                return false;
//            }
            Narrator.stepPassed(line);
        } catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean validateStore(String fileName)
    {
        String currentValue;
        String textToBeChecked = "";
        String keyValue = "";
        Shortcuts.databaseRecordsLabels(fileName);
        Map<String, String> recordInDatabase = SettersAndGetters.getDatabaseRecords();//getting the Map with different values
        Set<String> keyData = SettersAndGetters.getDatabaseRecords().keySet();//getting only the keys for the Map
        List<String> keyOfRecordInDatabase = new ArrayList<>(keyData);//putting the map keys in a list to be used on the Map

        getStoreSystemUIConfig(SettersAndGetters.getNewBearer());
        String bearerValue = SettersAndGetters.getAPIResponse();

        for (int i = 0; i < keyOfRecordInDatabase.size(); i++)
        {
            switch (recordInDatabase.get(keyOfRecordInDatabase.get(i)))
            {
                case "placeholder":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                case "hiddenValue":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                default:
                {
                    currentValue = recordInDatabase.get(keyOfRecordInDatabase.get(i));
                    if ((keyOfRecordInDatabase.get(i).contains(" ")))
                    {
                        String[] tempRecord = keyOfRecordInDatabase.get(i).split(" ");
                        keyValue = tempRecord[0];
                    } else
                    {
                        keyValue = keyOfRecordInDatabase.get(i);
                    }

                    textToBeChecked = "\"" + keyValue + "\"" + ":" + "\"" + currentValue + "\"";
                    break;
                }
            }

            if (!bearerValue.contains(textToBeChecked))
            {
                error = "Failed to add \"" + textToBeChecked + "\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(textToBeChecked))
            {
                error = "Failed to update text: \"'" + textToBeChecked + "'\" to green color";
                return false;
            }

        }

        Narrator.stepPassed("Carrier added successfully - " + Shortcuts.line);

        return true;
    }

    public boolean carrierProcessLaterBtn()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }

        AppiumDriverInstance.pause(1000);
//        TC_NextGen_03_Carrier cr = new TC_NextGen_03_Carrier(testData);
//        String nbc = MobileDoddlePageObjects.randomNo();
//        cr.executeBarCode(nbc, MobileDoddlePageObjects.rand());

        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Could not Scan";
            return false;
        }

        //Above == 03
        //Below == 04
        Narrator.stepPassedWithScreenShot("Scanning barcode successful");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Store Parcel Later Button Clicked.");
//        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton())) {
//            error = "Failed to wait for the 'back' button.";
//            return false;
//        }
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton())) {
//            error = "Failed to touch on the 'back' button.";
//            return false;
//        }
//
//        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }

    public boolean StorageProccess()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (!scanAndRescanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan barcode";
            return false;
        }

//        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
//        {
//            error = "Failed to wait for the tick heavy parcel button";
//            return false;
//        }
//
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
//        {
//            error = "Failed to touch and check 'Heavy Parcel'.";
//            return false;
//        }
//
//        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
//        {
//            error = "Failed to wait for the take photo button";
//            return false;
//        }
//
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
//        {
//            error = "Failed to touch 'Take photo'.";
//            return false;
//        }
//
//        Narrator.stepPassedWithScreenShot("Photo of parcel");
//
//        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
//        {
//            error = "Failed to wait for 'Use' button.";
//            return false;
//        }
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
//        {
//            error = "Failed to touch 'use' button.";
//            return false;
//        }
        if (!Shortcuts.scanAndRescanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan shelf barcode. "+"Config did not change";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
            {
                error = "Failed to click on the pause storage button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to click the back to dashboard button";
                return false;
            }
        } else if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.finishStorageButton()))
        {
            error = "Failed to touch the 'Pause storage' button.";
            return false;
        }

        Narrator.stepPassed("Successfully retrieved parcel from carrier, scanned and stored parcel.");
        return true;
    }

    public void writeToExcel2(String barcode, String fName, String fName2)
    {
        try
        {
            //Get the excel file.
            FileInputStream file = new FileInputStream(new File(System.getProperty("user.dir") + "//DatabaseRecordsLabels//" + fName2));

            //Get workbook for XLS file.
            XSSFWorkbook yourworkbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook.
            //If there have >1 sheet in your workbook, you can change it here IF you want to edit other sheets.
            XSSFSheet sheet1 = yourworkbook.getSheetAt(0);

            // Get the row of your desired cell.
            // Let's say that your desired cell is at row 2.
            Row row = sheet1.getRow(4);
            // Get the column of your desired cell in your selected row.
            // Let's say that your desired cell is at column 2.
            Cell column = row.getCell(1);
            // If the cell is String type.If double or else you can change it.
            String updatename = column.getStringCellValue();
            //New content for desired cell.
            updatename = barcode;
            //Print out the updated content.
            //System.out.println(updatename);
            //Set the new content to your desired cell(column).
            column.setCellValue(updatename);
            //Close the excel file.
            file.close();
            //Where you want to save the updated sheet.
            FileOutputStream out
                    = new FileOutputStream(new File(System.getProperty("user.dir") + "//DatabaseRecordsLabels//" + fName));
            yourworkbook.write(out);
            out.close();

        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public boolean scanAndRescanBarcode(String barcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!rescanButton())
            {
                if (count == 10)
                {
                    error = "Failed to wait for the pop up message";
                    return false;
                }

                count++;
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            }
            count = 0;

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!popUpMessage())
            {
                if (count == 10)
                {
                    error = "Failed to click pop up message";
                    return false;
                }
                count++;
                AppiumDriverInstance.pressHardWareButtonbyKeyCode(); //Scan again
            }

            snakeFrame.repaint();
            snakeFrame.dispose();

        } catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }
}

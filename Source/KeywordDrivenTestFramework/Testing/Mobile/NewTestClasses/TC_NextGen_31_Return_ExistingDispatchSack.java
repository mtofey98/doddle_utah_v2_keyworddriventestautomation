/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_30_Return_SingleContainer.validationList;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TC_NextGen_31_Return_ExistingDispatchSack extends BaseClass
{

    Narrator narrator;
    static String onscreenDPSBarcode = "";
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String[] lines;
    String[] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    String value;
    String itemReturn;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();
    Shortcuts shortcut = new Shortcuts();
    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String DCBarcode = "";
    public String barcodeName = "";
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public boolean restart;
    public boolean conErorr;
    //==========================Veribles for custom methods=====================
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String text;
    String correctBarcode;
    String newBearer, line;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL = "";
    String destinationFile = "";
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    String temp2 = "";
    public static String[] validationList;
    WebElement barcodeVal;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();
    TC_NextGen_30_Return_SingleContainer nextGen30 = new TC_NextGen_30_Return_SingleContainer();

    //==========================================================================
    public TC_NextGen_31_Return_ExistingDispatchSack(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
            {
                if (!ReusableFunctionalities.Login())
                {
                    return narrator.testFailedScreenshot("Failed to do the login process" + error + Shortcuts.error);
                }

                if (!ReusableFunctionalities.singleContainerExclusivePoshTotty() && ReusableFunctionalities.isSeleniumFailure == false)
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                } else if (ReusableFunctionalities.isSeleniumFailure)
                {
                    return narrator.seleniumTestFailedScreenshot("Failed to retrieve item IDs" + ReusableFunctionalities.error);
                }

                nextGen30.tapNowFlow();

                if (!existingDispatchSackCancel())
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with existing dispatch sack - " + error);
                }

                validationList = new String[]
                {
                    "CREATED", "ACTIVE"
                };

                if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
                {
                    return narrator.testFailedScreenshot("Failed to do the validation process - " + ReusableFunctionalities.error);
                }

                validationList = new String[]
                {
                    ""
                };

                validationList = new String[]
                {
                    "ASSIGN_TO_CONTAINER_DEFERRED"
                };

                if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error);
                }

                validationList = new String[]
                {
                    ""
                };

                if (!ReusableFunctionalities.singleContainerExclusivePoshTotty() && ReusableFunctionalities.isSeleniumFailure == false)
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                } else if (ReusableFunctionalities.isSeleniumFailure)
                {
                    return narrator.seleniumTestFailedScreenshot("Failed to retrieve item IDs" + ReusableFunctionalities.error);
                }

                if (!existingDispatchScanBarcode())
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + error);
                }

                validationList = new String[]
                {
                    "SCANNED_TO_DISPATCH_CONTAINER", "AT_GOODS_OUT"
                };

                if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error);
                }

                validationList = new String[]
                {
                    ""
                };
            } else
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 2))
                {
                    if (!ReusableFunctionalities.singleContainerExclusivePoshTotty() && ReusableFunctionalities.isSeleniumFailure == false)
                    {
                        return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                    } else if (ReusableFunctionalities.isSeleniumFailure)
                    {
                        return narrator.seleniumTestFailedScreenshot("Failed to retrieve item IDs" + ReusableFunctionalities.error);
                    }

                    TC_NextGen_30_Return_SingleContainer.tapNowFlow();
                }

                if (!existingDispatchSackCancel())
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with existing dispatch sack - " + error);
                }

                validationList = new String[]
                {
                    "CREATED", "ACTIVE"
                };

                if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
                {
                    return narrator.testFailedScreenshot("Failed to do the validation process - " + ReusableFunctionalities.error);
                }

                validationList = new String[]
                {
                    ""
                };

                validationList = new String[]
                {
                    "ASSIGN_TO_CONTAINER_DEFERRED"
                };

                if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error);
                }

                validationList = new String[]
                {
                    ""
                };

                if (!ReusableFunctionalities.singleContainerExclusivePoshTotty() && ReusableFunctionalities.isSeleniumFailure == false)
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + ReusableFunctionalities.error);
                } else if (ReusableFunctionalities.isSeleniumFailure)
                {
                    return narrator.seleniumTestFailedScreenshot("Failed to retrieve item IDs" + ReusableFunctionalities.error);
                }

                if (!existingDispatchScanBarcode())
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process with single container - " + error);
                }

                validationList = new String[]
                {
                    "SCANNED_TO_DESPATCH_CONTAINER", "AT_GOODS_OUT"
                };

                AppiumDriverInstance.pause(10000);

                if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
                {
                    return narrator.testFailedScreenshot("Failed to do the Returns process - " + ReusableFunctionalities.error);
                }

                validationList = new String[]
                {
                    ""
                };
            }
            return narrator.finalizeTest("Successfully Completed the existing despatch sack test case");
        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    public boolean existingDispatchSackCancel()
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());
            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to wait for the done button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to click the done button";
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo(), 2))
        {
            Narrator.stepPassedWithScreenShot("Before clicking cancel");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dispatchParcelCancelButtonTwo()))
            {
                error = "Failed to click the parcel cancel button";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Successfully cancelled the parcel dispatch");

        return true;
    }

    public boolean existingDispatchScanBarcode()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed toclick the now button";
            return false;
        }

        //Scanning wrong barcode first
        if (!save.saveReturnsBarcode(temp))
        {
            error = "Failed to save barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(temp))
        {
            error = "Failed to scan incorrect barcode";
            return false;
        }

        try
        {
            String errorMessage = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dispatchErrorSack());

            if (!errorMessage.contains("Please scan dispatch sack"))
            {
                error = "Failed to validate error message";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
            {
                error = "Failed to click the ok button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnSingleContainerBarcode()))
            {
                error = "Failed to load the 'Put return in Container screen'";
                return false;
            }

            onscreenDPSBarcode = "";
            onscreenDPSBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnSingleContainerBarcode());
            int index = onscreenDPSBarcode.indexOf(".");
            onscreenDPSBarcode = onscreenDPSBarcode.substring(index + 1).trim();

            if (!save.saveReturnsBarcode(onscreenDPSBarcode))
            {
                error = "Failed to generate onscreen barcode";
                return false;
            }

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(onscreenDPSBarcode))
        {
            error = "Failed to scan barocode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the back button";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!ReusableFunctionalities.scanBarcode(onscreenDPSBarcode))
        {
            error = "Failed to scan barocode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
        {
            error = "Failed to wait for the dispatch returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
        {
            error = "Failed to click the dispatch returns done button";
            return false;
        }

        return true;
    }
}

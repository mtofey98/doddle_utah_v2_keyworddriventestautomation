/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_31_Return_ExistingDispatchSack.onscreenDPSBarcode;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_31_Return_ExistingDispatchSack.validationList;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.value;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import org.apache.http.entity.StringEntity;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_31_1_MoveReturnItemToContainerWorkflow extends BaseClass
{

    String error = "";
    public static TestEntity currentData;
    TC_NextGen_30_Return_SingleContainer nextGen30 = new TC_NextGen_30_Return_SingleContainer();

    StringEntity input;

    public TC_NextGen_31_1_MoveReturnItemToContainerWorkflow(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!(BaseClass.currentDevice == Enums.Device.ZebraTC70_Doddle))
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
            {
                if (!ReusableFunctionalities.Login())
                {
                    return narrator.testFailedScreenshot("Failed to do the login process" + Shortcuts.error);
                }

                if (!retrieveItemIDFromCouchBase())
                {
                    return narrator.testFailedScreenshot("Failed to retrieve item ID from couch base");
                }

                if (!ReusableFunctionalities.MultipleReturnSingleContainer() && ReusableFunctionalities.isSeleniumFailure == false)
                {
                    return narrator.testFailedScreenshot("Failed to do the single container flow" + ReusableFunctionalities.error);
                } else if (ReusableFunctionalities.isSeleniumFailure)
                {
                    return narrator.seleniumTestFailedScreenshot("Failed to retrieve item ID" + Shortcuts.error);
                }

                if (!cancelReturns())
                {
                    return narrator.testFailedScreenshot("Failed to cancel returns");
                }

                validationList = new String[]
                {
                    "VOID", "CANCELLED_HANDOVER"
                };

                if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
                {
                    return narrator.testFailedScreenshot("Failed to do the validation process - " + ReusableFunctionalities.error);
                }
                return narrator.finalizeTest("Successfully cancelled the multiple returns flow");
            } else
            {

                if (!retrieveItemIDFromCouchBase())
                {
                    return narrator.testFailedScreenshot("Failed to retrieve item ID from couch base");
                }

                if (!ReusableFunctionalities.MultipleReturnSingleContainer() && ReusableFunctionalities.isSeleniumFailure == false)
                {
                    return narrator.testFailedScreenshot("Failed to do the single container flow - " + Shortcuts.error);
                } else if (ReusableFunctionalities.isSeleniumFailure)
                {
                    return narrator.seleniumTestFailedScreenshot("Failed to retrieve item ID - " + error);
                }

                if (!cancelReturns())
                {
                    return narrator.testFailedScreenshot("Failed to cancel returns - " + error);
                }

                validationList = new String[]
                {
                    "VOID", "CANCELLED_HANDOVER"
                };

                if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
                {
                    return narrator.testFailedScreenshot("Failed to do the validation process - " + ReusableFunctionalities.error);
                }
            }
            return narrator.finalizeTest("Successfully cancelled the multiple returns flow");
        } else
        {
            return narrator.finalizeTest("Printer Unavailable For Device");
        }
    }

    public boolean cancelReturns()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to touch Menu";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the dashboard button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the main menu button");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsCancelButton()))
        {
            error = "Failed to wait for the cancel returns popup";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Cancel returns options screen");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsCancelButton()))
        {
            error = "Failed to click the ALL button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the cancel button");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to touch Menu";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the main menu button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to wait for the ALL returns popup";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Cancel returns options screen");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
        {
            error = "Failed to click the ALL button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        AppiumDriverInstance.pause(3000);

        Narrator.stepPassedWithScreenShot("Clicked the ALL button");

        return true;
    }

    public boolean retrieveItemIDFromCouchBase()
    {
        try
        {
            SeleniumDriverInstance.startDriver();

            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURL()))
            {
                error = "Failed to navigate to couchbase URL";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.itemReturnList(), 180))
            {
                String itemReturn = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.itemReturnList());
                value = itemReturn.substring(itemReturn.lastIndexOf(":") + 1).trim();
                System.out.println(value);
                SettersAndGetters.setItemID(value);
            }

            if (value.isEmpty())
            {
                error = "Failed to retrieve the itemID from couchbase";
                return false;
            }

            SeleniumDriverInstance.shutDown();
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve itemID - " + e.getMessage();
            System.out.println(error);
            return false;
        }

        return true;
    }
}

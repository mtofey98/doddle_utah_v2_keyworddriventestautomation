/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.barcode;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.newBearer;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanBarcode;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanShelfBarcode;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mlopes
 */
public class TC_NextGen_11_ReturningParcelToStorage_MissingParcel extends BaseClass
{

    String error = "";
    Narrator narrator;
    StringEntity input;

//==========================Variables for custom methods=====================
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();

    boolean testPassed = true;
    public static TestEntity currentData;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();

//==========================================================================
    public TC_NextGen_11_ReturningParcelToStorage_MissingParcel(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        this.currentData = testData;
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if(!ReusableFunctionalities.Login())
            {
                return narrator.testFailedScreenshot("Failed to login");
            }
            
                    //Added by Manuel Lopes
            if (!executeBarCode())
            {
                return narrator.testFailedScreenshot("Failed to set up the barcode" + error);
            }
            
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to seet the Barcode settings - " + error);
            }
        
            Shortcuts.cleanCollections();

            if (!carriersProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + Shortcuts.error);
            }

            if (!setMissingParcel())
            {
                return narrator.testFailedScreenshot("Failed trying to set parcel to missing status" + error + Shortcuts.error);
            }

            if (!StorageProccess())
            {
                return narrator.testFailedScreenshot("Failed to do the Storage process - " + Shortcuts.error);
            }

            if (!ReusableFunctionalities.checkRecordsInDatabase("TC_NextGen_11.xlsx"))
            {
                return narrator.testFailedScreenshot("Failed check the records on the Database - " + Shortcuts.error);
            }
            return narrator.finalizeTest("Successfully completed Storage Repeating Process test");
        }
        
        else
        {
            
            //Added by Manuel Lopes
            if (!executeBarCode())
            {
                return narrator.testFailedScreenshot("Failed to set up the barcode" + error);
            }
            
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to seet the Barcode settings - " + error);
            }
        
            Shortcuts.cleanCollections();
            if (!carriersProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + Shortcuts.error);
            }

            if (!setMissingParcel())
            {
                return narrator.testFailedScreenshot("Failed trying to set parcel to missing status" + error + Shortcuts.error);
            }

            if (!StorageProccess())
            {
                return narrator.testFailedScreenshot("Failed to do the Storage process - " + Shortcuts.error);
            }

            if (!ReusableFunctionalities.checkRecordsInDatabase("TC_NextGen_11.xlsx"))
            {
                return narrator.testFailedScreenshot("Failed check the records on the Database - " + Shortcuts.error);
            }
            return narrator.finalizeTest("Successfully completed Storage Repeating Process test");
        }
    }
    

    public boolean executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            error = "Failed to retrieve the new bearer token";
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            error = "Failed to generate and save the barcodes";
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            error = "Failed to do the rest call for the payload";
        }

        return true;
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();
            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, barcode, storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean StorageProccess()
    {
        AppiumDriverInstance.pause(10000);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan barcode";
            return false;
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.alertPopup(), 2))
        {
            Narrator.stepFailedWithScreenShot("barcode - " + SettersAndGetters.getGeneratedBarCode() + " not recognized");
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Starting storage process, barcode status is missing");
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.parcelStatus()))
        {
            error = "Failed to wait for the parcel status";
            return false;
        }
        
        if (!"MISSING".equals(AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.parcelStatus())))
        {
            error = "Failed to change parcels status to missing";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Successfully changed parcels status to MISSING");
        
        if (!scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan shelf barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
            {
                error = "Failed to click on the pause storage button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to click the back to dashboard button";
                return false;
            }
        }
        else if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.finishStorageButton()))
        {
            error = "Failed to touch the 'Pause storage' button.";
            return false;
        }

        Shortcuts.doStorageLabelValueAPICall(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getNewBearer());

        Narrator.stepPassed("Successfully retrieved parcel from carrier, scanned and stored parcel.");
        
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard" + ReusableFunctionalities.error;
            return false;
        }
        
        return true;
    }
    
    public boolean setMissingParcel()
    {      
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Successfully clicked the collection option");
        
                
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Failed to wait for the 'other search options' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Successfully clicked the 'other search options' button";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to wait for the collection UPI button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to touch the 'collection UPI' option.";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Successfully clicked the UPI option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter ID field";
            return false;
        }

        //TODO : Get doddle ID from couch base
        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton(), 2))
        {
        
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
            {
                error = "Failed to click the correct button";
                return false;
            }
            
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }
        
        int count = 0;

        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            if (count == 10)
            {
                error = "Failed to wait for the ok button";
                return false;
            }

            count++;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to touch pick parcels from storage button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Picking parcels from storage");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.lostParcelButton()))
        {
            error = "Failed to wait for the lost parcel button to be present";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Parcel to be lost - " +  " barcode");
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.lostParcelButton()))
        {
            error = "failed to click on the lost parcel button";
            return false;
        }
        
        Narrator.stepPassedWithScreenShot("Clicked on lost parcel");
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to wait for the pop up";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to click the yes button in the pop up";
            return false;
        }

        AppiumDriverInstance.pause(2000);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.finishPickingParcelsBtn()))
        {
            error = "Failed to wait for the 'back to dashboard button'";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.finishPickingParcelsBtn()))
        {
            error = "Failed to click the back to dashboard button";
            return false;
        }
        
        return true;
    }
    
    public boolean carriersProcess()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }
        
        barcode = SettersAndGetters.getGeneratedBarCode();

        Narrator.stepPassedWithScreenShot("Scanning barcode successful");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the 'back' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to touch on the 'back' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }
}

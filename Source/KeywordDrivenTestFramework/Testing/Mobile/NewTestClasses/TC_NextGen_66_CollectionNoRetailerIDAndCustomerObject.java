/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.moreCollectionCodesNo;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.putStoreSystemConfigNoCollectionChargeTrue;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author rpeck
 */
public class TC_NextGen_66_CollectionNoRetailerIDAndCustomerObject extends BaseClass {
    
    
    public static TestEntity currentData;

    String error = "";

    //changed when saving barcodes
    boolean testPassed = true;
    //***************

    //barcodes
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    //***************

    //API call body
    StringEntity input;
    
        public TC_NextGen_66_CollectionNoRetailerIDAndCustomerObject(TestEntity testData) {
        this.testData = testData;
        this.narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();     
        }
        
     public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            putStoreSystemConfigNoCollectionChargeTrue();
            
           if (!ReusableFunctionalities.Login())
           {
               return narrator.testFailedScreenshot("Failed to login - " + Shortcuts.error);
           }

            executeBarCode();
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to set the Barcode settings - " + error);
            }
            
            collectionNoRetailerAndCustomerObject();
                   
            return narrator.finalizeTest("Successfully did collection with no retailer ID and customer object.");
        }
        else
        {
            putStoreSystemConfigNoCollectionChargeTrue();
            
            executeBarCode();
            if (testPassed == false)
            {
                return narrator.testFailedScreenshot("Failed to set the Barcode settings - " + error);
            }

            collectionNoRetailerAndCustomerObject();
            
            return narrator.finalizeTest("Successfully did collection with no retailer ID and customer object.");
        }                   
    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode("DC" + MobileDoddlePageObjects.barcode(), MobileDoddlePageObjects.rand()))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();

            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, SettersAndGetters.getGeneratedBarCode(), storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }
    
    public boolean collectionNoRetailerAndCustomerObject()
    {       
        if (!ReusableFunctionalities.preConditionForNoCustomerObjectandRetailerID())
        {   
            error = "Unable to initiate collection with UPI";
            return false;
        }
        
        if (!ReusableFunctionalities.backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }
        
        startFlow();
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.forgetToday()))
        {
            error = "Failed to wait for the forget today button";
            return false;
        }
                
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.forgetToday()))
        {
            error = "Failed to click the forget today button";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Clicked the forget today button");
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.reinstateParcel()))
        {
            error = "Failed to wait for the reinstate button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.reinstateParcel()))
        {
            error = "Failed to reinstate the parcel";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Parcel successfully reinstated");
        
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.photoOfParcel()))
        {
            error = "Failed to find the photo of parcel button";
            return true;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.photoOfParcel()))
        {
            error = "Failed to find the photo of parcel button";
            return true;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.photoParcelBackButton()))
        {
            error = "Failed to find the back button";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.photoParcelBackButton()))
        {
            error = "Failed to click the back button";
            return false;
        }
        
        
        if(!ReusableFunctionalities.CollectionProcessNoCheckout())
        {
            error = "Failed to do the collection proccess";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerCollectionBackToDashboard()))
        {
            error = "Failed to wait for the back to dashboard button";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerCollectionBackToDashboard()))
        {
            error = "Failed to click the back to dashboard button";
            return false;
        }
        
        Shortcuts.doCollectionsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());
        
       if(!Shortcuts.line.contains("\"AT_COLLECTION_POINT\""))
        {
            error = "\"AT_COLLECTION_POINT\" was not found in the ITEM_COLLECTION record";
            return false;
        }
       
        if(!Shortcuts.changeAPIResponseColor("\"AT_COLLECTION_POINT\""))
        {
            error = "AT_COLLECTION_POINT was not found in the ITEM_COLLECTION record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Item at collection " + Shortcuts.line);
        
        startFlow();
        
        if(!ReusableFunctionalities.CollectionProcessNoCheckout())
        {
            error = "Failed to do the collection proccess";
            return false;
        }
        
        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan barcode";
            return false;
        }
                
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkoutXParcels()))
        {
            error = "Failed to wait for the check out button";
            return false;
        }
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkoutXParcels()))
        {
            error = "Failed to click the checkout button";
            return false;
        }      
        return true;
    }
    
    public boolean startFlow()
    {
        String parcel = SettersAndGetters.getGeneratedBarCode();
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked on the collection tile");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionCodeButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionCodeButton()))
        {
            error = "Failed to touch the 'Collection Code' option.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected the collection code option");
        
         if (!SaveBarcodesFromURL.saveBarcode(SettersAndGetters.getBarCodeRefID(), MobileDoddlePageObjects.rand()))
        {
            error = "Unable to save barcode";
            return false;
        }

        
        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to scan barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("Scan collection code");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesNoButton()))
        {
            error = "Failed to select the no button";
            return false;
        }
        
         if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }
        
        SettersAndGetters.setGeneratedBarCode(parcel);
        
        return true;
    }
}
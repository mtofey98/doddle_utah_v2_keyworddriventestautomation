/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.checkCarriersRecordsInDatabase;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities.returnsDispatchPrecondition;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.changeFailedAPIResponseColor;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanBarcode;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.scanShelfBarcode;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author hduplessis
 */
public class TC_NextGen_04_CarriersDispatch extends BaseClass
{

    String error = "";
    Narrator narrator;
    StringEntity input;

//==========================Variables for custom methods=====================
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();

    boolean testPassed = true;
    public static TestEntity currentData;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();

//==========================================================================
    public TC_NextGen_04_CarriersDispatch(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        this.currentData = testData;
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
            {
                TC_NextGen_02_Login carrier = new TC_NextGen_02_Login(testData);
                if (!carrier.LoginReuseCopy())
                {
                    return narrator.testFailedScreenshot("Failed to Login TC_NextGen04");
                }

                if (!validateCarrier("TC_NextGen_04_CarriersDispatch.xlsx"))
                {
                    return narrator.testFailedScreenshot("Failed check the Carrier 'DHL' records in the Database - " + error);
                }

                Narrator.stepPassed(Shortcuts.line);
                AppiumDriverInstance.pause(300000);
                if (!dashboardFlow())
                {
                    //  error = "Failed to execute containers";
                    return narrator.testFailedScreenshot("Failed to do the Containers Dashboard Flow process - " + error);
                }
                if (!manageContainers())
                {
                    //error = "Failed to execute containers";
                    return narrator.testFailedScreenshot("Failed to do Manage Containers Process - " + error);
                }
                if (!removeContainer())
                {
                    // error = "Failed to execute containers";
                    return narrator.testFailedScreenshot("Failed to remove Containers- " + error);
                }
//                if (!validateContainer())
//                {
//                    return narrator.testFailed("Failed to check couch_base for item_load " + error);
//                }
                return narrator.finalizeTest("Successfully completed Carrier Dispatch test");
                
            } else if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername(), 5))
            {

                if (!ReusableFunctionalities.Login())
                {
                    return narrator.testFailedScreenshot("Failed to login - ");
                }
            }
            if (!ReusableFunctionalities.validateCarrier("TC_NextGen_04_CarriersDispatch.xlsx"))
            {
                return narrator.testFailedScreenshot("Failed check the records on the Database - " + error);
            }

            Narrator.stepPassed(Shortcuts.line);
            AppiumDriverInstance.pause(300000);
            if (!dashboardFlow())
            {
                // error = "Failed to execute containers";
                return narrator.testFailedScreenshot("Failed to do the Containers Dashboard Flow process - " + error);
            }
            if (!manageContainers())
            {
                // error = "Failed to execute containers";
                return narrator.testFailedScreenshot("Failed to do the Containers process - " + error);
            }
            if (!removeContainer())
            {
                //  error = "Failed to execute containers";
                return narrator.testFailedScreenshot("Failed to remove Containers- " + error);
            } else
            {
                if (!validateCarrier("TC_NextGen_04_CarriersDispatch.xlsx"))
                {
                    return narrator.testFailedScreenshot("Failed check the Carrier 'DHL' records in the Database - " + error);
                }
                AppiumDriverInstance.pause(300000);
                if (!dashboardFlow())
                {
                    error = "Failed to execute containers";
                    return narrator.testFailedScreenshot("Failed to do the Containers Dashboard Flow process - " + Shortcuts.error);
                }
                if (!manageContainers())
                {
                    error = "Failed to execute containers";
                    return narrator.testFailedScreenshot("Failed to do Manage Containers Process - " + error);
                }
                if (!removeContainer())
                {
                    error = "Failed to execute containers";
                    return narrator.testFailedScreenshot("Failed to remove Containers- " + error);
                }

                return narrator.finalizeTest("Successfully completed Carrier Dispatch test");
            }

        }

    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + Shortcuts.error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), testData.getData("Email"), randomNumber, testData.getData("StoreID"), testData.getData("postal")))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String email, String randomNumber, String storeID, String postalCode)
    {
        try
        {
            HttpPost preadvicePost = SettersAndGetters.getPreadvicePost();
            HttpClient client = HttpClientBuilder.create().build();

            preadvicePost.addHeader(header, "Bearer " + SettersAndGetters.getNewBearer());
            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, SettersAndGetters.getGeneratedBarCode(), storeID));
            input.setContentType("application/json");

            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            String line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        } catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean validateCarrier(String fileName)
    {
        String currentValue;
        String textToBeChecked = "";
        String keyValue = "";
        Shortcuts.databaseRecordsLabels(fileName);
        Map<String, String> recordInDatabase = SettersAndGetters.getDatabaseRecords();//getting the Map with different values
        Set<String> keyData = SettersAndGetters.getDatabaseRecords().keySet();//getting only the keys for the Map
        List<String> keyOfRecordInDatabase = new ArrayList<>(keyData);//putting the map keys in a list to be used on the Map

        Shortcuts.doCarrierDHLAPICall(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getNewBearer());
        String bearerValue = SettersAndGetters.getAPIResponse();

        for (int i = 0; i < keyOfRecordInDatabase.size(); i++)
        {
            switch (recordInDatabase.get(keyOfRecordInDatabase.get(i)))
            {
                case "placeholder":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                case "hiddenValue":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                default:
                {
                    currentValue = recordInDatabase.get(keyOfRecordInDatabase.get(i));
                    if ((keyOfRecordInDatabase.get(i).contains(" ")))
                    {
                        String[] tempRecord = keyOfRecordInDatabase.get(i).split(" ");
                        keyValue = tempRecord[0];
                    } else
                    {
                        keyValue = keyOfRecordInDatabase.get(i);
                    }

                    textToBeChecked = "\"" + keyValue + "\"" + ":" + "\"" + currentValue + "\"";
                    break;
                }
            }

            if (!bearerValue.contains(textToBeChecked))
            {
                error = "Failed to add \"" +changeFailedAPIResponseColor(textToBeChecked) + "\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(textToBeChecked))
            {
                error = "Failed to update text: \"'" + textToBeChecked + "'\" to green color";
                return false;
            }

        }
        // Narrator.stepPassed("Carrier added successfully - "+Shortcuts.line);
        return true;
    }

    public boolean manageContainers()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.manageContainersBtn()))
        {
            error = "Failed to click Manage Containers";
            return false;
        }
        //=============================================================================================
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allocateBtn()))
        {
            error = "Failed to click Allocate new container for Carrier button. "+"Config did not change";
            return false;
        }

        SettersAndGetters.setGeneratedShelfCode(shelfBarcode);

        if (!SaveBarcodesFromURL.saveBarcode(MobileDoddlePageObjects.barcode(), SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "could not save bcode";
            return false;
        }

        if (!scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan";
            return false;
        }
        try
        {
            TC_NextGen_03_Carrier cr = new TC_NextGen_03_Carrier(testData);
            cr.writeToExcel(SettersAndGetters.getGeneratedShelfCode(), "TC_NextGen_04_Carriers_Bar.xlsx", "TC_NextGen_4_Carriers.xlsx");
        } catch (Exception e)
        {
            error = "Failed to write to excel file. ";
        }
        Narrator.stepPassedWithScreenShot("Scanning  successful");

        return true;
    }

    public boolean removeContainer()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.removeContainers()))
        {
            error = "Failed to touch 'Remove Containers' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.selectContainersDelete()))
        {
            error = "Failed to touch 'Select' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmDelete()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.removeContainers()))
        {
            error = "Failed to touch 'Remove Containers' button.";
            return false;
        }
                if (!validateContainer())
                {
                   // error="Failed to check couch_base for item_load ";
                    return false;
                }
        Narrator.stepPassedWithScreenShot("Container Removed");
        for (int i = 0; i < 4; i++)
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
            {
                error = "Failed to click back Arrow";
                return false;
            }
        }
        return true;
    }

    public boolean dashboardFlow()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'carriers' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Other")))
        {
            error = "Failed to search for " + testData.getData("Other") + ".";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the other to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to click Other tile.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterFreeText(), testData.getData("Something")))
        {
            error = "Failed to search for " + testData.getData("Other") + ".";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Successfully entered free text");

        for (int i = 0; i < 2; i++)
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
            {
                error = "Failed to click back Arrow";
                return false;
            }
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }
        //Waiting for dispatch button, if its not there does the code inside.
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchParcel(), 5))
        {
            error = "Failed to wait for dipatch button";
            for (int i = 0; i < 2; i++)
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
                {
                    error = "Failed to click back Arrow";
                    return false;
                }
//                Shortcuts.putStoreSystemUIConfigSingleContainer();
//                AppiumDriverInstance.pause(400000);
            }
            if (!returnsDispatchPrecondition(shelfBarcode))
            {
                error = "Failed to execute returns";
                narrator.testFailedScreenshot("Failed to do the Containers process - ");
            }
        }
        
        //If Parcels Out is > 3
        if (AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.numberOfItems()).equals("6"))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
            {
                error = "Failed to click despatch button to decrease 'Parcels Out'";
                return false;
            }
            try
            {
                String dpsCode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.containerID());
                SettersAndGetters.setGeneratedShelfCode(dpsCode);
            } catch (Exception e)
            {
                error = e.getMessage();
            }
            if (!scanBarcode(SettersAndGetters.getGeneratedShelfCode()))
            {
                error = "Failed to scan barcode";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmDispatcBtn()))
            {
                error = "Failed to click confirm despatch";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
            {
                error = "Failed to touch 'Check In' button.";
                return false;
            }
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
            {
                error = "Failed to wait for the 'Search for Carrier' textfield.";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
            {
                error = "Failed to search for " + testData.getData("Carrier") + ".";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
            {
                error = "Failed to wait for the dhl carrier to be touchable";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
            {
                error = "Failed to touch on the DHL tile.";
                return false;
            }

        }
        Narrator.stepPassed("Dispatch Button is present.");
        return true;
    }

    public boolean validateContainer()
    {
        String event = "";
        String status = "";
        String everything = "";
        String replace = "";
        String r2 = "";
        try
        {
            SeleniumDriverInstance.startDriver();//Starts Driver
        } catch (Exception e)
        {
            error = e.getMessage();
        }

        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURL()))//Goes to website
        {
            error = "Failed to navigate to couchbase URL";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.firstItemLoad(), 160))
        {
            error = "Failed to wait for item_load in couchbase";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.firstItemLoad()))
        {
            error = "Failed to click the first 'item_load' in couchbase";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Successfully Navigated to Couch-Base");
        try
        {
            event = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.eventContainer());
            status = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.containerStatus());
            everything = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.allText());

            if (everything.contains(status))
            {
                replace = everything.replace(status, "<span style=\"color:#3eb24d; \">" + status + "</span>");

            }
            if (everything.contains(event))
            {
                r2 = replace.replace(event, "<span style=\"color:#3eb24d; \">" + event + "</span>");
            }

            SeleniumDriverInstance.shutDown();
        } catch (Exception e)
        {
            error = e.getMessage();
        }
        Narrator.stepPassed(r2);

        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.MSTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.MobileMSPageObjects.MobileMSPageObjects;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class VanillaFlow extends BaseClass
{

    static String orderID = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    String error = "";
    String temp = "";
    Narrator narrator;

    public VanillaFlow(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!Login())
        {
            return narrator.testFailed("Failed to login  - " + error);
        }

        if (!Returns())
        {
            return narrator.testFailed("Failed to do the returns flow - " + error);
        }

        return narrator.finalizeTest("Successfully logged into the M&S app.");
    }

    public boolean Login()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileMSPageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        String version = AppiumDriverInstance.retrieveTextByXpath(MobileMSPageObjects.versionText());

        Narrator.stepPassedWithScreenShot("M&S Version : " + version);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileMSPageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            return false;
        }

        String store = AppiumDriverInstance.retrieveTextByXpath(MobileMSPageObjects.storeSpinner());

        if (!store.equals(narrator.getData("StoreSelection")))
        {
            if (!scrollToSubElement(narrator.getData("StoreSelection")))
            {
                error = "Could not find " + narrator.getData("StoreSelection");
                return false;
            }

        }

        if (!multiplePopUpMessages())
        {
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileMSPageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileMSPageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username into username field.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileMSPageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password into password field.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileMSPageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileMSPageObjects.blockingScreenClose(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileMSPageObjects.blockingScreenClose(), 5))
            {
                error = "Failed to wait for the semi-blocking screen to not be visible";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileMSPageObjects.returnsTile()))
        {
            error = "Failed to wait for the return tile to be displayed.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

        return true;
    }

    public boolean Returns()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileMSPageObjects.returnsTile()))
        {
            error = "Failed to click on the returns tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileMSPageObjects.enterOrderNo()))
        {
            error = "Failed to wait for the enter order no field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileMSPageObjects.enterOrderNo(), testData.getData("orderNo")))
        {
            error = "Failed to enter order no into field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileMSPageObjects.nextButton()))
        {
            error = "Failed to click the next button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileMSPageObjects.referencesConfirmButton()))
        {
            error = "Failed to wait for the return references confirm button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileMSPageObjects.referencesConfirmButton()))
        {
            error = "Failed to click the return references confirm button";
            return false;
        }

        AppiumDriverInstance.pause(3000);
        //tempLabel
        int rand = (int) Math.round(Math.random() * 999999);
        NumberFormat format = new DecimalFormat("000000");
        String tempLabel = format.format(rand);
        temp = "DPS" + tempLabel;

        SeleniumDriverInstance.startDriver();

        generateBarcode(temp, MobileMSPageObjects.randomNo());

        SeleniumDriverInstance.shutDown();

        if (!scanBarcode(temp))
        {
            error = "Failed to scan temp dps barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileMSPageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileMSPageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        return true;
    }

    //================================================================== Custom Methods ============================================================================
    public boolean scanBarcode(String barcode)
    {
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            if (!popUpMessage())
            {
                error = "Failed to click pop up message";
                return true;
            }

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean generateBarcode(String barcodeName, String randomNumber)
    {
        File file = new File("orderID.txt");
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(file)))
        {
            printWriter.println(randomNumber);

        }
        catch (Exception e)
        {
            error = e.getMessage();
        }

        this.orderID = randomNumber;

        narrator.stepPassed("Order ID : " + randomNumber);
        narrator.stepPassed("Barcode : " + barcodeName);

        TestMarshall.CheckBrowserExists();

        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.barcodeGeneratorURL(barcodeName)))
        {
            error = "Failed to generate barcode.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Barcode Generator page");
//==========================================================Saving barcode and converting=================================================

        if (!saveBarcodeASImage((barcodeName)))
        {
            error = "Failed to save barcode as an image";
            return false;
        }
        SeleniumDriverInstance.shutDown();

        return true;
    }

    public boolean saveBarcodeASImage(String barcodeName)
    {
        String imageURL = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.GeneratedBarcode())).getAttribute("src");
        String destionationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcodeName + ".png";
        saveImageUsingURL(imageURL, destionationFile);
        return true;
    }

    public boolean saveImageUsingURL(String imageURL, String destionationFile)
    {
        try
        {
            DataColumn barcodeColumn = new DataColumn("", "", Enums.ResultStatus.UNCERTAIN);

            URL url = new URL(imageURL);
            System.setProperty("jsse.enableSNIExtension", "false");
            InputStream fis = url.openStream();
            OutputStream fos = new FileOutputStream(destionationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = fis.read(b)) != -1)
            {
                fos.write(b, 0, length);
            }

            fis.close();
            fos.close();

            barcodeColumn.columnValue = convertPNGToBase64(destionationFile);
            barcodeColumn.resultStatus = Enums.ResultStatus.UNCERTAIN;
            DataRow currentRow = new DataRow();
            currentRow.DataColumns.add(barcodeColumn);
            listOfBarcodes.add(currentRow);

        }
        catch (Exception e)
        {
            error = "Failed to save image - " + e.getMessage();
            return false;
        }
        return true;
    }

    public String convertPNGToBase64(String imageFilePath)
    {
        String base64ReturnString = "";

        try
        {
            out.println("[INFO] Converting screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            out.println("[INFO] Converting completed, image ready for embedding.");
        }
        catch (Exception ex)
        {
            out.println("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    //====================================================Custom Methods================================================
    public boolean SwipeWithTouchActions(int startX, int startY, int endX, int endY)
    {
        String temp = "";
        int counter = 0;
        TouchAction action = new TouchAction(Driver);

        while (counter < 10)
        {

            List<MobileElement> list = AppiumDriverInstance.Driver.findElements(By.id(MobileDoddlePageObjects.storeList()));

            for (int i = 0; i < list.size(); i++)
            {
                temp = list.get(i).getText();
                if (temp.equals(testData.getData("StoreSelection")))
                {
                    break;
                }

            }
            if (temp.equals(testData.getData("StoreSelection")))
            {

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(testData.getData("StoreSelection"))))
                {
                    error = "Failed to select " + testData.getData("StoreSelection") + " from list.";
                    return false;
                }
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 5))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
                    {
                        error = "Failed to touch the ok button.";
                        return false;
                    }

                }
                break;
            }
            else
            {

                action.longPress(startX, startY).moveTo(endX, endY).release().perform();

            }

            if (!popUpMessage())
            {
                return false;
            }

            counter++;
        }

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }

}

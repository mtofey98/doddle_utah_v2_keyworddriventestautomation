/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.android.AndroidKeyCode;
import org.apache.http.entity.StringEntity;

/**
 *
 * @author jmacauley
 */
public class CustomerRefundFlow extends BaseClass
{
    
    String error = "";
    Narrator narrator;
    StringEntity input;
    
     public CustomerRefundFlow(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();

    }
    
     public TestResult executeTest()
    {
        if (!Refund())
        {
            return narrator.testFailed("Failed to refund a parcel" + error);
        }
//        if (!ReturnsToCarreirContainer())
//        {
//            return narrator.testFailed("Failed to return container to carrier" + error);
//        }
//        if (!DispatchReturn())
//        {
//            return narrator.testFailed("Failed to dispatcj parcel to carrier" + error);
//        }

        return narrator.finalizeTest("Successfully completed the refund parcel test ");
    }
     
     public boolean Refund()
     {
         if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.refundsTile()))
         {
             error = "Failed to click on the refunds tile";
             return false;
         }
         
         if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.refundsEnterInfoField()))
         {
             error = "Failed to wait for the refunds enter info field to be visible";
             return false;
         }
         
         if(!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.refundsEnterInfoField()))
         {
             error = "Failed to click the refunds enter info field";
             return false;
         }
         
         if(!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.refundsEnterInfoField(), testData.getData("Email")))
         {
             error = "Failed to enter " + testData.getData("Email") + " into info field";
             return false;
         }
         
        narrator.stepPassed("Successully entered "+testData.getData("Email")+" into email refunds info field");
        
         switch (currentDevice)

        {
            case ZebraMC40:
                if (!AppiumDriverInstance.tapAtLocation("444.1", "750"))
                {
                    if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailLoadingScreen("Searching for " + testData.getData("Email") + "")))
                    {
                        error = "Failed to wait for the loading screen to appear";
                        return false;
                    }

                    if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.emailLoadingScreen("Searching for " + testData.getData("Email") + ""), 100))
                    {
                        error = "Failed to wait for the 'Searching for " + testData.getData("Email") + " to go away. Time exceeded.'";
                        return false;
                    }
                }
                break;
            case ZebraTC70_Doddle:

                if (!AppiumDriverInstance.tapAtLocation("650", "1235"))
                {

                    if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailLoadingScreen("Searching for " + testData.getData("Email") + "")))
                    {
                        error = "Failed to wait for the loading screen to appear";
                        return false;
                    }

                    if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.emailLoadingScreen("Searching for " + testData.getData("Email") + ""), 100))
                    {
                        error = "Failed to wait for the 'Searching for " + testData.getData("Email") + " to go away. Time exceeded.'";
                        return false;
                    }
                }

        }
         
         
         return true;
     }
     
     
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.System.out;
import java.net.URL;
import java.util.Date;
import java.util.LinkedList;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;

/**
 *
 * @author tknoetzen
 * @author jmacauley
 */
public class DoddleGenerateBarCode extends BaseClass
{

    String error = "";
    String name = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();

    public DoddleGenerateBarCode(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;
    }

    public TestResult executeTest()
    {
        if (!GenerateBarcode())
        {
            return narrator.testFailed("Failed to generate barcodes to use.: " + error);
        }
        if (!Deletebarcodes())
        {
            return narrator.testFailed("deletes all the files within a given folder " + error);
        }
        return narrator.finalizeTest("Scanned Successfully.");
    }

    public   boolean GenerateBarcode()
    {
        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.BarCodeGenURL()))
        {
            error = "Failed to Navigate to Barcode Generator website.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.AirmoneyRadioButton()))
        {
            error = "Failed to load Barcode Generation page.";
        }

        int counter = 0;
        
        int BarcodeSize = Integer.parseInt(testData.getData("NumberOfBarcodes"));
        try
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.InputWithValueXpath("023FPK")))
            {
                error = "Failed to click on the radio button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.InputWithValueXpath("amazon.json")))
            {
                error = "Failed to click on the 'Amazon' readio button";
                return false;
            }

            for (counter = 0; counter < BarcodeSize; counter++)
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.SubmitButton()))
                {
                    error = "Failed to click on submit button.";
                    return false;
                }

                //gets image URL
                String imageURL = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.GeneratedBarcode())).getAttribute("src");
                Date date = new Date();
                name = counter + "";
//              name = narrat  or.formatMessage(name);
                String destionationFile = "C:\\Doddle_UTAH_V2\\Barcodes\\Amazon\\" + name + ".png";
                saveImageUsingURL(imageURL, destionationFile);

                SeleniumDriverInstance.Driver.navigate().back();

            }

        } catch (Exception e)
        {
            error = "Failed to get Barcode Image, error - " + e.getMessage();
            return false;
        }

        testData.gridValidationDataRows = listOfBarcodes;

        int counter2 = 0;
        int BarcodeSize2 = 50;
        try
        {

            if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.InputWithValueXpath("dhl.json")))
            {
                error = "Failed to click on the 'Amazon' readio button";
                return false;
            }

            for (counter2 = 0; counter2 < BarcodeSize2; counter2++)
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.SubmitButton()))
                {
                    error = "Failed to click on submit button.";
                    return false;
                }

                //gets image URL
                String imageURL = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.GeneratedBarcode())).getAttribute("src");
                Date date = new Date();
                name = counter2 + "";
//              name = narrat  or.formatMessage(name);
                String destionationFile = "C:\\Doddle_UTAH_V2\\Barcodes\\dhl\\" + name + ".png";
                saveImageUsingURL(imageURL, destionationFile);

                SeleniumDriverInstance.Driver.navigate().back();

            }

        } catch (Exception e)
        {
            error = "Failed to get Barcode Image, error - " + e.getMessage();
            return false;
        }

        testData.gridValidationDataRows = listOfBarcodes;
        return true;
    }

    public boolean Deletebarcodes()
    {
        File file = new File("C:\\Doddle_UTAH_V2\\Barcodes\\Amazon");
        String[] myFiles;
        if (file.isDirectory())
        {
            myFiles = file.list();
            for (int i = 0; i < myFiles.length; i++)
            {
                File myFile = new File(file, myFiles[i]);
                myFile.delete();
            }
        }
        File file2 = new File("C:\\Doddle_UTAH_V2\\Barcodes\\dhl");
        String[] myFiles2;
        if (file2.isDirectory())
        {
            myFiles2 = file2.list();
            for (int i = 0; i < myFiles2.length; i++)
            {
                File myFile2 = new File(file2, myFiles2[i]);
                myFile2.delete();
            }
        }
        return true;
    }
//======================================================Custom methods================================================================
    public boolean saveImageUsingURL(String imageURL, String destionationFile)
    {
        try
        {
            DataColumn barcodeColumn = new DataColumn("", "", Enums.ResultStatus.UNCERTAIN);
            barcodeColumn.columnHeader = name;

            URL url = new URL(imageURL);
            System.setProperty("jsse.enableSNIExtension", "false");
            InputStream fis = url.openStream();
            OutputStream fos = new FileOutputStream(destionationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = fis.read(b)) != -1)
            {
                fos.write(b, 0, length);
            }

            fis.close();
            fos.close();

            barcodeColumn.columnValue = convertPNGToBase64(destionationFile);
            barcodeColumn.resultStatus = Enums.ResultStatus.UNCERTAIN;
            DataRow currentRow = new DataRow();
            currentRow.DataColumns.add(barcodeColumn);
            listOfBarcodes.add(currentRow);

        } catch (Exception e)
        {
            error = "Failed - " + e.getMessage();
            return false;
        }
        return true;
    }

    public String convertPNGToBase64(String imageFilePath)
    {
        String base64ReturnString = "";

        try
        {
            out.println("[INFO] Converting error screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            out.println("[INFO] Converting completed, image ready for embedding.");
        } catch (Exception ex)
        {
            out.println("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }
}

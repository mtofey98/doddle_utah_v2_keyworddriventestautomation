/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class NewDoddleLogin extends BaseClass
{

    String error = "";
    Narrator narrator;

    public NewDoddleLogin(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        String testcase = narrator.getData("Testcase");

        switch (testcase)
        {
            case "IncorrectStoreLogin":
                if (!IncorrectStoreLogin())
                {
                    return narrator.testFailed("Failed to select incorrect store  - " + error);
                }
                break;
            case "CorrectStoreLogin":
                if (!CorrectStoreLogin())
                {
                    return narrator.testFailed("Failed to login and validate syncing text - " + error);
                }
                break;
            case "IncorrectEmailLogin":
                if (!CorrectStoreLogin())
                {
                    return narrator.testFailed("Failed to login and validate syncing text - " + error);
                }
                break;
            case "IncorrectPasswordLogin":
                if (!CorrectStoreLogin())
                {
                    return narrator.testFailed("Failed to login and validate syncing text - " + error);
                }
                break;
            case "Wifi":
                if (!wifiSwitch())
                {
                    return narrator.testFailed("Failed to login into the doddle app - " + error);
                }
                break;
            case "ScreenOff":
                if (!DeviceScreenOff())
                {
                    return narrator.testFailed("Failed to login into the doddle app - " + error);
                }
                break;
            default:
                break;
        }

        return narrator.finalizeTest("Successfully logged into doddle app.");
    }

    public boolean IncorrectStoreLogin()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText()))
        {
            error = "Failed to wait for the version text to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.versionText()))
        {
            error = "Failed to click on the version text";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.releaseNotesText()))
        {
            error = "Failed to wait for the release notes text to be visible";
            return false;
        }

        narrator.stepPassedWithScreenShot("Release Notes");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.whatsNewCloseBtn()))
        {
            error = "Failed to click on the close button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            return false;
        }

        if (!scrollToSubElement(testData.getData("IncorrectStore")))
        {
            error = "Could not find " + testData.getData("IncorrectStore");
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        //Driver.pressKeyCode(AndroidKeyCode.BACK);
        String storeBefore = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

        if (!scrollToSubElement(testData.getData("StoreSelection")))
        {
            error = "Could not find " + testData.getData("StoreSelection");
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertCancelPopup()))
        {
            error = "Failed to click on the cancel popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertCancelPopup()))
        {
            error = "Failed to click on the cancel popup";
            return false;
        }

        String storeAfter = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

        if (storeBefore.equals(storeAfter))
        {
            narrator.stepPassedWithScreenShot("Validated " + storeAfter + " has been extracted from spinner");
        }
        else
        {
            error = "Failed to compare store before to store after";
            return false;
        }

        if (!scrollToSubElement(testData.getData("StoreSelection")))
        {
            error = "Could not find " + testData.getData("StoreSelection");
            return false;
        }

        if (!multiplePopUpMessages())
        {
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed wait for the username field.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginPassword()))
        {
            error = "Failed to touch the password field.";
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(),testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }

        String permissionsError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.messagePopup());

        narrator.stepPassedWithScreenShot("Error - " + permissionsError);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to wait for the permissions popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to click on the ok popup";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to wait for the toolbar back text to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to click on the toolbar back text";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validate incorrect store login");

        return true;
    }

    public boolean CorrectStoreLogin()
    {

        String testcase = narrator.getData("Testcase");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }
        if (!multiplePopUpMessages())
        {
            return false;
        }

        String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

        if (!store.equals(narrator.getData("StoreSelection")))
        {
            if (!scrollToSubElement(narrator.getData("StoreSelection")))
            {
                error = "Failed to find " + narrator.getData("StoreSelection");
                return false;
            }
        }

        if (!multiplePopUpMessages())
        {
            return false;
        }

        if (!Login(narrator.getData("username"), narrator.getData("password"), testcase))
        {
            return false;
        }

        if (testcase.equals("IncorrectEmailLogin") || testcase.equals("IncorrectPasswordLogin"))
        {
            String invalidEmailError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.messagePopup());

            narrator.stepPassedWithScreenShot("Error - " + invalidEmailError);

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okMessageButton()))
            {
                error = "Failed to wait for the ok message on error message";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okMessageButton()))
            {
                error = "Failed to click the ok message on error message";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.toolbarBackText()))
            {
                error = "Failed to wait for the toolbar back text to be visible";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
            {
                error = "Failed to click on the toolbar back text";
                return false;
            }

        }
        else
        {
            if (AppiumDriverInstance.waitForElementByXpathWithTimeout(MobileDoddlePageObjects.syncingTextView(), 5))
            {
                narrator.stepPassedWithScreenShot("Syncing text is displayed");
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
            {
                error = "Failed to wait for the 'Check In' button.";
                return false;
            }

            if (testcase.equals("CorrectStoreLogin"))
            {
                Narrator.stepPassedWithScreenShot("Successfully logged into Doddle app with Correct store");
            }

            if (!Logout())
            {
                return false;
            }
        }

        return true;
    }

    public boolean wifiSwitch()
    {
        String testcase = narrator.getData("Testcase");

        if (!wifiSwitchOff())
        {
            return false;
        }

        MobileElement doddleText = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.doddleMainText()));

        if (doddleText.isDisplayed())
        {
            Narrator.stepPassed("Successfully re-entered the doddle app");
        }
        else
        {
            error = "Failed to re-enter the doddle app";
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            return false;
        }

        if (!scrollToSubElement(narrator.getData("IncorrectStore")))
        {
            error = "Failed to find " + narrator.getData("StoreSelection");
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        if (!Login(narrator.getData("username"), narrator.getData("password"), testcase))
        {
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to wait for the toolbar back text to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to click on the toolbar back text";
            return false;
        }

        if (!wifiSwitchOn())
        {
            return false;
        }

        return true;
    }

    public boolean DeviceScreenOff()
    {
        String testcase = narrator.getData("Testcase");

        if (!Login(narrator.getData("username"), narrator.getData("password"), testcase))
        {
            return false;
        }

        String carrierBefore = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.carriersTile());

        //Switch Device Screen Off      
        Driver.lockDevice();

        AppiumDriverInstance.pause(1000);

        if (Driver.isLocked())
        {
            narrator.stepPassedWithScreenShot("Device screen is off");
        }
        else
        {
            error = "Failed to take screenshot of device while off";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        Driver.pressKeyCode(AndroidKeyCode.KEYCODE_POWER);

        AppiumDriverInstance.pause(1000);

        String carrierAfter = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.carriersTile());

        AppiumDriverInstance.pause(1000);

        if (carrierBefore.equals(carrierAfter))
        {
            narrator.stepPassedWithScreenShot("Successfully validated Carriers");
        }
        else
        {
            error = "Failed to unlock screen";
            return false;
        }

        return true;
    }

    // ================================================ Custom Methods =====================================================
    public boolean IncorrectStoreSwipe(String store, int startX, int startY, int endX, int endY)
    {
        String temp = "";
        int counter = 0;
        TouchAction action = new TouchAction(Driver);

        while (counter < 10)
        {

            List<MobileElement> list = AppiumDriverInstance.Driver.findElements(By.id(MobileDoddlePageObjects.storeList()));

            // ============================================== SWIPE DOWN ================================================
            for (int i = 0; i < list.size(); i++)
            {
                temp = list.get(i).getText();
                if (temp.equals(store))
                {
                    break;
                }

            }
            if (temp.equals(store))
            {

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(store)))
                {
                    error = "Failed to select " + narrator.getData("IncorrectStore") + " from list.";
                    return false;
                }

                break;
            }

            if (counter <= 5)
            {
                action.longPress(startX, startY).moveTo(endX, endY).release().perform();
            }
            else
            {
                // ============================================== SWIPE UP ================================================

                for (int i = 0; i < 5; i++)
                {
                    for (int t = 0; t < list.size(); t++)
                    {
                        temp = list.get(t).getText();
                        if (temp.equals(narrator.getData("IncorrectStore")))
                        {
                            break;
                        }

                    }
                    action.longPress(endX, endY).moveTo(startX, startY).release().perform();
                    if (temp.equals(narrator.getData("IncorrectStore")))
                    {

                        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(store)))
                        {
                            error = "Failed to select " + narrator.getData("IncorrectStore") + " from list.";
                            return false;
                        }

                        break;
                    }

                }
                break;
            }

            counter++;
        }
        return true;
    }

    public boolean CorrectStoreSwipe(int startX, int startY, int endX, int endY)
    {
        String temp = "";
        int counter = 0;
        TouchAction action = new TouchAction(Driver);

        while (counter < 10)
        {

            List<MobileElement> list = AppiumDriverInstance.Driver.findElements(By.id(MobileDoddlePageObjects.storeList()));

            // ============================================== SWIPE DOWN ================================================
            for (int i = 0; i < list.size(); i++)
            {
                temp = list.get(i).getText();
                if (temp.equals(narrator.getData("StoreSelection")))
                {
                    break;
                }

            }
            if (temp.equals(narrator.getData("StoreSelection")))
            {

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(narrator.getData("StoreSelection"))))
                {
                    error = "Failed to select " + narrator.getData("StoreSelection") + " from list.";
                    return false;
                }
                if (!popUpMessage())
                {
                    error = "Failed to handle popup message";
                    return false;
                }
                break;
            }

            if (counter <= 5)
            {
                action.longPress(startX, startY).moveTo(endX, endY).release().perform();
            }
            else
            {
                // ============================================== SWIPE UP ================================================

                for (int i = 0; i < 5; i++)
                {
                    for (int t = 0; t < list.size(); t++)
                    {
                        temp = list.get(t).getText();
                        if (temp.equals(narrator.getData("StoreSelection")))
                        {
                            break;
                        }

                    }
                    action.longPress(endX, endY).moveTo(startX, startY).release().perform();
                    if (temp.equals(narrator.getData("StoreSelection")))
                    {

                        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(narrator.getData("StoreSelection"))))
                        {
                            error = "Failed to select " + narrator.getData("StoreSelection") + " from list.";
                            return false;
                        }
                        if (!popUpMessage())
                        {
                            error = "Failed to handle popup message";
                            return false;
                        }
                        break;
                    }

                }
                break;
            }

            if (!popUpMessage())
            {
                error = "Failed to handle popup message";
                return false;
            }
            counter++;
        }
        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean Login(String username, String password, String testcase)
    {

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleMainText(), 2))
        {

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = "Failed to handle popup alerts";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), username))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), password))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (testcase.equals("Wifi"))
        {
            String connectionError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.messagePopup());
            Narrator.stepPassed("Error - " + connectionError);

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okMessageButton()))
            {
                error = "Failed to wait for the ok pop up message";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okMessageButton()))
            {
                error = "Failed to click on the ok pop up message";
                return false;
            }
        }
        else
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.blockingScreenClose(), 2))
            {
                if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.blockingScreenClose(), 5))
                {
                    error = "Failed to wait for the semi-blocking screen to not be visible";
                    return false;
                }
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
            {
                error = "Failed to wait for the check in button.";
                return false;
            }

            Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");
        }

        return true;

    }

    public boolean Logout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

//        narrator.stepPassedWithScreenShot("Successfully clicked on the Menu button");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }
        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

//        narrator.stepPassedWithScreenShot("Successfully logged out of the Doddle app");
        return true;
    }

    public boolean wifiSwitchOff()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }

            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
            String wifiText = element.getAttribute("name");

            System.out.println("");

            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                {
                    error = "Failed to click the wifi button with index";
                    return false;

                }

            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiDisconnected()))
        {
            error = "Failed to wait for the wifi to be disconnected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned off Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public boolean wifiSwitchOn()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }

            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
            String wifiText = element.getAttribute("name");

            System.out.println("");

            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                {
                    error = "Failed to click the wifi button with index";
                    return false;

                }

            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
        {
            error = "Failed to wait for the wifi to be connected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }

}

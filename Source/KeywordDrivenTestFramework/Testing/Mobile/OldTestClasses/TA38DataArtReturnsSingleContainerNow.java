/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

/**
 *
 * @author jmacauley
 */
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses.TA38DataArtTestReturnFlowWifiOn.source;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.isCMDRunning;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.*;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.System.out;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.amazonaws.services.lambda.AWSLambdaClient;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class TA38DataArtReturnsSingleContainerNow extends BaseClass
{

    Narrator narrator;
    StringEntity input;
    String randomNo;
    String barcode;
    String name = "";
    String error = "";
    String regex;
    String [] lines;
    String [] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    String value;
    String itemReturn;
    List<S3ObjectSummary> keyListBefore;
    List<S3ObjectSummary> keyListAfter;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    List<S3ObjectSummary> finalList = new LinkedList<>();
    TreeMap<S3ObjectSummary, S3ObjectSummary> preMap = new TreeMap<>();
    Shortcuts shortcut = new Shortcuts();
    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    public String TA38Barcode = "";
    public String DCBarcode = "";
    public String barcodeName = "";
    public static String source = "";
    public static String beforeTime = "";
    public static String afterTime = "";
    public boolean restart;
    public boolean conErorr;
    //==========================Veribles for custom methods=====================
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String shelfLabelName = "DPS" + MobileDoddlePageObjects.randomNo();
    String tempBarcode;
    String tempName;
    String text;
    String correctBarcode;
    String newBearer, line;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String imageURL = "";
    String destinationFile = "";
    String temp = "DPS" + MobileDoddlePageObjects.randomNo();
    String containerShelfLabel = "DPS" + MobileDoddlePageObjects.randomNo();
    String temp2 = "";
    WebElement barcodeVal;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();

    //==========================================================================
    public TA38DataArtReturnsSingleContainerNow(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        Shortcuts.putStoreSystemUIConfigMultipleContainers();
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!Shortcuts.newBearer())
        {
            return narrator.testFailedScreenshot("Failed to generate new Bearer " + error);
        }
        
        if(!save.saveShelfLabels(shelfLabelName, containerShelfLabel))
        {
            return narrator.testFailedScreenshot("Failed to generate the barcode " + error);
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            Login();

            if (!Return())
            {
                return narrator.testFailedScreenshot("Return - " + error);
            }
        }
        else
        {
            if (!Return())
            {
                return narrator.testFailedScreenshot("Failed - " + error);
            }
        }

        return narrator.finalizeTest("Successfully completed the return parcel test with wifi on");
    }

    public boolean Return()
    {

        int count = 0;
        boolean LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.syncingText(), 5);

        while (LoadingCircle || count < 5)
        {
            try
            {
                String syncing = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.syncingText());
                LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.syncingText(), 5);

                if (syncing.isEmpty())
                {
                    break;
                }

                count++;
            }
            catch (Exception e)
            {
                narrator.stepPassed("Successfully waited for syncing to be finished - " + e.getMessage());
                break;

            }

        
        }
       
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

//        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 20))
//            {
//                error = "Failed to wait for the progress circle to be no longer present";
//                return false;
//            }
//        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 8))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }
        
         try
        {
            SeleniumDriverInstance.startDriver();

            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURL()))
            {
                error = "Failed to navigate to couchbase URL";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.itemReturnList(), 60))
            {
                itemReturn = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.itemReturnList());
                value = itemReturn.substring(itemReturn.lastIndexOf(":") + 1).trim();
                System.out.println(value);
            }
            
            if(value.isEmpty())
            {
                error = "Failed to retrieve the itemID from couchbase";
                return false;
            }

            SeleniumDriverInstance.shutDown();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return false;
        }
        
        AppiumDriverInstance.pause(2000);

        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //RETAILER_SELECTED
        if (!Shortcuts.line.contains("RETAILER_SELECTED"))
        {
            error = "Failed to add \"RETAILER_SELECTED\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("RETAILER_SELECTED"))
        {
            error = "Failed to update text: \"RETAILER_SELECTED\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //HANDOVER_FROM_CUSTOMER
        if (!Shortcuts.line.contains("\"status\":\"HANDOVER_FROM_CUSTOMER\""))
        {
            error = "Failed to add \"status\":\"HANDOVER_FROM_CUSTOMER\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("HANDOVER_FROM_CUSTOMER"))
        {
            error = "Failed to update text: \"HANDOVER_FROM_CUSTOMER\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }
        
        AppiumDriverInstance.pause(2000);
        
        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //SECURITY_SCAN_PASSED
        if (!Shortcuts.line.contains("\"SECURITY_SCAN_PASSED\""))
        {
            error = "Failed to add \"eventType\":\"SECURITY_SCAN_PASSED\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("SECURITY_SCAN_PASSED"))
        {
            error = "Failed to update text: \"SECURITY_SCAN_PASSED\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }
        
        AppiumDriverInstance.pause(2000);

        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //ADDED_RETAILER_REFS
        if (!Shortcuts.line.contains("ADDED_RETAILER_REFS"))
        {
            error = "Failed to add \"ADDED_RETAILER_REFS\" event in CouchBase";
            return false;
        }

        if(!Shortcuts.changeAPIResponseColor("ADDED_RETAILER_REFS"))
        {
            error = "Failed to update text: \"ADDED_RETAILER_REFS\" to green color";
            return false;
        }
        
        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        AppiumDriverInstance.pause(2000);
        
        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //ADDED_CUSTOMER_DETAILS
        if (!Shortcuts.line.contains("ADDED_CUSTOMER_DETAILS"))
        {
            error = "Failed to add \"ADDED_CUSTOMER_DETAILS\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("ADDED_CUSTOMER_DETAILS"))
        {
            error = "Failed to update text: \"ADDED_CUSTOMER_DETAILS\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerValidation(testData.getData("Email"))))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 10))
            {
                error = "Failed to wait for the progress circe to no longer be present";
                return false;
            }
        }
        
        AppiumDriverInstance.pause(2000);
        
        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //RETURN_ROUTING_DETERMINED
        //Is done when selecting a printer
        if (!Shortcuts.line.contains("RETURN_ROUTING_DETERMINED"))
        {
            error = "Failed to add \"RETURN_ROUTING_DETERMINED\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("RETURN_ROUTING_DETERMINED"))
        {
            error = "Failed to update text: \"RETURN_ROUTING_DETERMINED\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 2))
        {
            error = "Failed because temp label screen appeared";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dvtPrinterOption(), 4))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.dvtPrinterOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }
            
            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }
            
            Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
            
            try
            {
                
                //Getting BarcodeURL
                apiResponse = Shortcuts.line;
                regex = "\"url\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\"},\"eventType\":");
                apiResponse = liness[0];
                PDFURL = apiResponse;
                System.out.println(PDFURL);
                
                //Getting LabelValue
                apiResponse = Shortcuts.line;
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                System.out.println(LabelValue);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                System.out.println("Error - " + e.getMessage());
                return false;
            }
            
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }
        
        if (!navigateAndTakeScreenshot(PDFURL, LabelValue))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }
        
        if (!scanTA38Barcode(LabelValue))
        {
            error = "Failed to scan generated barcode";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the returns now button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to click the returns now button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpathWithTimeout(MobileDoddlePageObjects.progressCircle(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 10))
            {
                error = "Failed to wait for the progress cicrle to no longer be present";
                return false;
            }
        }

        try
        {
            SeleniumDriverInstance.startDriver();

            DCBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnSingleContainerBarcode());
            int index = DCBarcode.indexOf(".");
            DCBarcode = DCBarcode.substring(index + 1).trim();
//          DCBarcode = "DPS" +randomNo;
            imageURL = WebDoddlePageObjects.barcodeGeneratorURL(DCBarcode);
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + DCBarcode + ".png";

        }
        catch (Exception e)
        {
            System.out.println("Error - " + e.getMessage());
            return false;
        }

        if (!SeleniumDriverInstance.navigateTo(imageURL))
        {
            error = "Failed to generate barcode.";
            return false;
        }

        narrator.stepPassedWithScreenShot(testData.TestCaseId + " - DC barcode Generator page");

        if (!AppiumDriverInstance.takeScreenShotWithCoordinates(destinationFile))
        {
            error = "Failed to take screenshot using coordinates";
            return false;
        }

        SeleniumDriverInstance.shutDown();

        AppiumDriverInstance.pause(1000);

        if (!shortcuts.scanBarcode(DCBarcode))
        {
            error = "Failed to scan the " + DCBarcode + " Barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
        {
            error = "Failed to wait for the dispatch returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
        {
            error = "Failed to click the dispatch returns done button";
            return false;
        }
        
        AppiumDriverInstance.pause(2000);
//       
        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //CHECKOUT_COMPLETE
        if (!Shortcuts.line.contains("CHECKOUT_COMPLETED"))
        {
            error = "Failed to add \"CHECKOUT_COMPLETE\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("CHECKOUT_COMPLETED"))
        {
            error = "Failed to update text: \"CHECKOUT_COMPLETE\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        return true;
    }

    public boolean ReturnsToCarrierContainer()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!scanTA38Barcode(LabelValue))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.barcodeVal()))
        {
            error = "Failed to wait for barcode";
            return false;
        }

        try
        {
            barcodeVal = AppiumDriverInstance.Driver.findElement(By.id("com.doddle.concession:id/collection_item_upi"));
        }
        catch (Exception e)
        {
            System.out.println("Error - " + e.getMessage());
            return false;
        }

        temp = barcodeVal.getText().trim();

        if (!temp.equals(LabelValue))
        {
            error = "Failed to validate that the barcodes are the same. Barcode generated and parcel being returned is does not have the same barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("retailer and Customer Details");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moveToCarrierContainerButton()))
        {
            error = "Failed to touch the 'Move to Carrier Container' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to wait for the 'Allocate new Container for carrier' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to touch the 'Allocate new Container for carrier' button.";
            return false;
        }

        if (!ScanContainerShelfLabel(containerShelfLabel))
        {
            error = "Failed to scan container shelf label";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.containersForDispatchText()))
        {
            error = "Failed to wait for the 'scan Container For Dispatch' button.";
            return false;
        }
        if (!ScanContainerShelfLabel(containerShelfLabel))
        {
            error = "Failed to scan container shelf label";
            return false;
        }

        return true;

    }

    public boolean DispatchReturn()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("DispatchCarrier")))
        {
            error = "Failed to search for " + testData.getData("DispatchCarrier") + ".";
            return false;
        }
        try
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        }
        catch (Exception e)
        {
            error = "Failed to click the harware back button " + e.getMessage();
            return false;

        }

        narrator.stepPassedWithScreenShot("Carrier selection");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.selectCarrier(testData.getData("DispatchCarrier"))))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }
        narrator.stepPassedWithScreenShot("dipatch options selection");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchParcel()))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        if (!shortcuts.scanShelfBarcode(containerShelfLabel))
        {
            error = "Failed to scan the container shelf label";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchOfCage()))
        {
            error = "Failed to wait for the 'Confirm dispatch of x cages' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDespatchOfCage()))
        {
            error = "Failed to touch on the 'Confirm dispatch of x cages' button ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Dispatch parcel to carrier completed");

        File index = new File(System.getProperty("user.dir") + "\\Barcodes");

        String[] entries = index.list();
        for (String s : entries)
        {
            File currentFile = new File(index.getPath(), s);
            currentFile.delete();
        }

        AppiumDriverInstance.pause(5000);

        return true;
    }

    public boolean wifiSwitchOn()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }

            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
            String wifiText = element.getAttribute("name");

            System.out.println("");

            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                {
                    error = "Failed to click the wifi button with index";
                    return false;

                }

            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
        {
            error = "Failed to wait for the wifi to be connected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    //===================================================Custom Methods==============================================
    public boolean LoopBarcodeScan() throws InterruptedException
    {

        try
        {
            for (String barcode : FinalBarcodes)
            {
                //Create Frame
                JFrame snakeFrame = new JFrame();
                ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
                JLabel jLabel = new JLabel(imageIcon);

                //Can set the default size here
                //snakeFrame.setBounds(100, 200, 800, 800);
                java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int centerX = screenSize.width / 28;
                int centerY = (screenSize.height / 2) + 200;

                snakeFrame.setLocation(centerX, centerY);

                //Must set visible - !
                snakeFrame.setVisible(true);

                //Load image - will be autoSized.
                snakeFrame.add(jLabel);
                snakeFrame.pack();
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
                if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
                {
                    error = "Failed to click on the 'OK' Popup button";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
                {
                    error = "Failed to click on the 'OK' Popup button";
                    return false;
                }
                AppiumDriverInstance.pause(2000);
                snakeFrame.repaint();
                snakeFrame.dispose();
            }

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public String convertPNGToBase64(String imageFilePath)
    {
        String base64ReturnString = "";

        try
        {
            out.println("[INFO] Converting error screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            out.println("[INFO] Converting completed, image ready for embedding.");
        }
        catch (Exception ex)
        {
            out.println("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }

    public boolean waitForElementNotVisible(String xpath, int maxWaitCount)
    {
        int timeCounter = 0;
        boolean itemPresent = AppiumDriverInstance.waitForElementByXpath(xpath);
        while (itemPresent)
        {
            if (timeCounter > maxWaitCount)
            {
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetWithTextView("Loading, please wait...")))
            {
                error = "Failed to wait for the progress bar to dissapear";
                return false;
            }
            //pause
            AppiumDriverInstance.pause(10000);

            //click the empty space
            //increment max counter
            itemPresent = AppiumDriverInstance.waitForElementByXpath(xpath);
            timeCounter++;
        }

        return true;
    }

    public boolean ScanBarcode(String barcode)
    {
        try
        {

            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            AppiumDriverInstance.pause(1000);
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean ScanContainerShelfLabel(String shelfLabel)
    {
        try
        {

            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfLabel + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            AppiumDriverInstance.pause(1000);
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean Logout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
            {
                error = "Failed to touch the 'yes' button on the popup message.";
                return false;
            }
        }

        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        return true;
    }

    public boolean menuDashboardBack()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the 'Dashboard' button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to touch the 'Dashboard' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to wait for the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to click the customer changed mind option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
        {
            error = "Failed to click the cancel booking button";
            return false;
        }

        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to wait for the carriers tile button.";
            return false;
        }

        return true;
    }

    public boolean Login()
    {

        if (!conErorr)
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (!store.equals(narrator.getData("StoreSelection")))
            {
                if (!scrollToSubElement(narrator.getData("StoreSelection")))
                {
                    error = "Could not find " + narrator.getData("StoreSelection");
                    return false;
                }

            }

            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                Login();

            }
        }

//        if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//           if(!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 4))
//           {
//               error = "Failed to wait for the progress circe to no longer be present";
//               return false;
//           }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");
        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean scanTA38Barcode(String barcode)
    {
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = 50;
            int centerY = 600;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            if (!popUpMessage())
            {
                error = "Failed to click pop up message";
                return true;
            }

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean keyListBefore()
    {
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat();
            Calendar calendar = Calendar.getInstance();
            sdf = new SimpleDateFormat("HH:mm:ss");
            beforeTime = sdf.format(calendar.getTime());

            System.out.println("BEFORE TIME : " + beforeTime);

            BasicAWSCredentials credentials = new BasicAWSCredentials("AKIAJRAPJHVMP4GUN6LQ", "G+OxEsMfLwHOqb9pstMygjotGRi0pBC2wuCSwXq2");
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_1).withCredentials(new AWSStaticCredentialsProvider(credentials)).build();

            keyListBefore = new ArrayList<S3ObjectSummary>();

            ObjectListing objects = s3Client.listObjects("stage-doddle", "labels/RETURN_SHIPPING/");

            keyListBefore.addAll(objects.getObjectSummaries());

        }
        catch (Exception | Error e)
        {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public boolean keyListAfter()
    {
        try
        {

            SimpleDateFormat sdf = new SimpleDateFormat();
            Calendar calendar = Calendar.getInstance();
            sdf = new SimpleDateFormat("HH:mm:ss");
            afterTime = sdf.format(calendar.getTime());

            System.out.println("AFTER TIME : " + afterTime);

            BasicAWSCredentials credentials = new BasicAWSCredentials("AKIAJRAPJHVMP4GUN6LQ", "G+OxEsMfLwHOqb9pstMygjotGRi0pBC2wuCSwXq2");
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_1).withCredentials(new AWSStaticCredentialsProvider(credentials)).build();

            keyListAfter = new ArrayList<S3ObjectSummary>();

            ObjectListing objects = s3Client.listObjects("stage-doddle", "labels/RETURN_SHIPPING/");

            keyListAfter.addAll(objects.getObjectSummaries());

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public boolean getFinalKey()
    {

        try
        {
            for (int i = 0; i < keyListAfter.size(); i++)
            {
                if (!keyListBefore.get(i).getKey().equals(keyListAfter.get(i).getKey()))
                {
                    finalList.add(keyListAfter.get(i));
                    break;
                }
                Thread.sleep(200);
            }

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public boolean navigateAndTakeScreenshot(String url, String barcodeName)
    {

        SeleniumDriverInstance.startDriver();

        if (!SeleniumDriverInstance.navigateToTA38Url(url))
        {
            error = "Failed to navigate to generated url";
            return false;
        }

        AppiumDriverInstance.pause(5000);

        SeleniumDriverInstance.takeScreenShotNoId(System.getProperty("user.dir") + "\\Barcodes\\" + barcodeName + ".png");

        SeleniumDriverInstance.shutDown();

        return true;
    }

    public boolean putStoreConfigSingleContainer()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.singleContainerStoreConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);
            narrator.stepPassed(line);

            if (!line.contains("SINGLE_CONTAINER"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean generateBarcode(String temp, String containerShelfLabel)
    {

        //temp label ==================================================================
        TestMarshall.CheckBrowserExists();

        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(temp);
        destinationFile = System.getProperty("user.dir") + "\\ShelfLabels\\" + temp + ".png";

        if (!SeleniumDriverInstance.navigateTo(imageURL))
        {
            error = "Failed to generate shelfLabel barcode.";
            return false;
        }

        narrator.stepPassedWithScreenShot("ShelfLabel Barcode Generator page");

        if (!AppiumDriverInstance.takeScreenShotWithCoordinates(destinationFile))
        {
            error = "Failed to take screenshot and save barcode to file";
            return false;
        }

        //containerShelfLabel ===========================================================
        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(containerShelfLabel);

        if (!SeleniumDriverInstance.navigateTo(imageURL))
        {
            error = "Failed to generate shelfLabel barcode.";
            return false;
        }

        narrator.stepPassedWithScreenShot("ShelfLabel Barcode Generator page");

        if (!AppiumDriverInstance.takeScreenShotWithCoordinates(destinationFile))
        {
            error = "Failed to take screenshot and save barcode to file";
            return false;
        }

        SeleniumDriverInstance.shutDown();

        return true;
    }

}

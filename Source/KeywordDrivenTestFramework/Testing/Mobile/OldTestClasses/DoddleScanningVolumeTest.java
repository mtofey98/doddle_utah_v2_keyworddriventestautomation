/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import static java.lang.System.out;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class DoddleScanningVolumeTest extends BaseClass
{

    String name = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    String error = "";
    Narrator narrator;
    StringEntity input;
    int Run = 0;
    int timeout = 5;
    int ListSize = 0;
    int ListSizeBefore = 0;
    String temp = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    int counter = 0;
    int TotalNumOfBarcodes;
    int barcodeCount;

    public DoddleScanningVolumeTest(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);

    }

    public TestResult executeTest()
    {

        if (!GenerateBarcodeAndPopulate())
        {
            return narrator.testFailed("Failed to generate barcode and populate barcode " + error);
        }
        if (!Carrier())
        {
            return narrator.testFailed("Failed to book in parcel to carrier");
        }
        if (!StorageProccess())
        {
            return narrator.testFailed("Failed to scan all barcodes in storage - " + error);
        }
        if (!Collection())
        {
            return narrator.testFailed("Failed to scan all barcodes in process " + error);
        }

        return narrator.finalizeTest("Successfully completed the scan volume test ");
    }

    public boolean GenerateBarcodeAndPopulate()
    {
        int NumOfbarcodes = Integer.parseInt(testData.getData("NumberOfBarcodes"));

        for (TotalNumOfBarcodes = 0; TotalNumOfBarcodes < NumOfbarcodes; TotalNumOfBarcodes++)
        {

            int pass = (int) Math.round(Math.random() * 999999999);
            NumberFormat formatter = new DecimalFormat("000000000");
            String randomNo = formatter.format(pass);
            System.out.println(randomNo);
            narrator.stepPassed("Order ID : " + randomNo);

            UUID uid = UUID.randomUUID();
            // checking the value of random UUID
            System.out.println("Random UUID value: " + uid.toString().replaceAll("-", "").substring(0, 12));
            String barcode = uid.toString().replaceAll("-", "").substring(0, 12).replaceAll("'", "");
            narrator.stepPassed("Barcode ID : " + barcode);

            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.barcodeGeneratorURL(barcode)))
            {
                error = "Failed to generate barcode.";
                return false;
            }

            narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Barcode Generator page");
//==========================================================Saving barcode and converting=================================================

            String imageURL = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.GeneratedBarcode())).getAttribute("src");
            Date date = new Date();

//              name = narrat  or.formatMessage(name);
            String destionationFile = System.getProperty("user.dir") + "\\Barcodes\\" + counter + ".png";
            saveImageUsingURL(imageURL, destionationFile);
            counter++;
//=======================================================================================================================================
//        HttpClient client = new DefaultHttpClient();
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost("https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE");
            post.addHeader("Authorization", "Basic NDBBR1YzNERKVU8xNTNaNURESkFKMjNQRTpsOE1mMmFRMzlCRHhQZHhqNW00RTliZ3oxQkZXMGRFeWV0UmlWdVZsajJV");

            String testcase = narrator.getData("Testcase");

            /* purpose of the switch case?
        * The switch case is in place for the following:
        * a few tickets have the exact same process, thus instead of rescripting/copying a test case and have multiple tickets doing the same proccess
        * we set this switch case in place. How it works is each ticket will have its own testpack but within each testpack we are calling upon the same class (this one we are in now called : Payload_Integrated collection)
        * Now instead of having to have allot of horrible code in out test class we create page objects for each payload so the switch case determines the payload to use based on what we specifie in our testpack i.e TA22, TA19
        * So all we do is create a new page object with specific details and we set our case to use a certain payload
             */
            //Very Important! - The order to witch you enter data into the payload Matters. Get the order correct!
            try
            {

                switch (testcase)
                {
                    case "TA-19":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(testData.getData("Email"), randomNo, barcode, testData.getData("StoreID")));
                        break;
                    case "TA-21":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA21(testData.getData("Email"), randomNo, barcode, testData.getData("StoreID")));
                        break;
                    case "TA-22":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA22(testData.getData("Email"), testData.getData("CellNo"), testData.getData("CardNo"), randomNo, barcode, testData.getData("StoreID")));
                        break;
                    case "TA-23":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(testData.getData("Email"), randomNo, barcode, testData.getData("StoreID")));
                        break;
                    case "TA-24":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA24(testData.getData("Email"), testData.getData("cellNo"), randomNo, barcode, testData.getData("StoreID")));
                        break;
                    case "TA-29":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA29(testData.getData("PostalCode"), testData.getData("Email"), randomNo, barcode, testData.getData("StoreID")));
                        break;
                    case "TA-31":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA31(testData.getData("Email"), testData.getData("cellNo"), randomNo, barcode, testData.getData("StoreID")));
                        break;
                    case "TA-32":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA32(testData.getData("PostalCode"), testData.getData("Email"), testData.getData("cellNo"), testData.getData("CardNo"), randomNo, barcode, testData.getData("StoreID")));
                        break;
                    case "TA-34":
                        input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA34(testData.getData("PostalCode"), testData.getData("Email"), testData.getData("cellNo"), randomNo, barcode, testData.getData("StoreID")));
                        break;

                }

                //StringEntity input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(randomNo, Barcode, testData.getData("storeID")));
                input.setContentType("application/json");
                post.setEntity(input);
                HttpResponse response = client.execute(post);
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                line = rd.readLine();
                System.out.println("");
                barcode = "";
                randomNo = "";

                narrator.stepPassed("Successfully generated barcode with ID: " + uid.toString().replaceAll("-", "").substring(0, 12) + " and populated the barcode with data using API call.");

            }
            catch (UnsupportedEncodingException e)
            {
                error = "exeption: " + e.getMessage();
                return false;
            }
            catch (IOException IOE)
            {
                error = "exeption: " + IOE;
                return false;
            }
        }
        SeleniumDriverInstance.shutDown();
        return true;
    }

    public boolean Carrier()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetTextViewToImageView(testData.getData("Carrier"))))
        {
            error = "Failed to touch on the " + testData.getData("Carrier") + " tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByID(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyID(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.yesButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.yesButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }
        if (!carrierLoopBarcodeScan())
        {
            return false;
        }

        narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Scanning barcode successfull");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByID(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyID(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("");
        narrator.stepPassed("Successfully scanned and received parcel from carrier.");
        return true;
    }

    public boolean StorageProccess()
    {

        if (!AppiumDriverInstance.waitForElementByID(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyID(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }

        for (barcodeCount = 0; barcodeCount < TotalNumOfBarcodes; barcodeCount++)
        {
            if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanFirstParcelText()))
            {
                error = "Failed to wait for the scan the first red-lined parcel text to be visible";
                return false;
            }    
            
            if (!ScanBarcode())
            {
                return false;
            }
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
            {
                error = "Failed to touch and check 'Heavy Parcel'.";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
            {
                error = "Failed to touch and check 'Heavy Parcel'.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
            {
                error = "Failed to touch 'Take photo'.";
                return false;
            }

            narrator.stepPassedWithScreenShot(testData.TestCaseId + "Photo of parcel");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
            {
                error = "Failed to wait for 'Use' button.";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
            {
                error = "Failed to touch 'use' button.";
                return false;
            }
            //pausing to give the device a few seconds to process and store the image
            AppiumDriverInstance.pause(1500);
            if (!ShelfBarcodScan())
            {
                return false;
            }

            if (barcodeCount == TotalNumOfBarcodes - 1)
            {
                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton()))
                {
                    error = "Failed to wait for the 'Pause storage' button.";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
                {
                    error = "Failed to touch the 'Pause storage' button.";
                    return false;
                }
                break;

            }
            else
            {
                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageStoreNextParcel()))
                {
                    error = "Failed to wait for the store next parcel button to be visible";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageStoreNextParcel()))
                {
                    error = "Failed to touch the store next parcel button";
                    return false;
                }

            }

        }

        narrator.stepPassedWithScreenShot(testData.TestCaseId + "Successfully stored parcelon doddle in store shelf");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
        {
            error = "Failed to wait for 'Back to Dashboard' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
        {
            error = "Failed to touch the 'Back to dashboard' button.";
            return false;
        }

        narrator.stepPassed("Successfully retrieved parcel from carrier, scanned and stored parcel.");
        return true;
    }

    public boolean Collection()
    {
        //Will click on the Check in button
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to touch the 'Email' option.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to touch the 'Enter email' text field to make the keyboard appear";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfull entered email address");
        //Pressing the enter key on the android device
        if (!AppiumDriverInstance.tapAtLocation("444.1", "750"))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.widgetWithTextView("Searching for " + testData.getData("Email") + ""), 100))
            {
                error = "Failed to wait for the 'Searching for " + testData.getData("Email") + " to go away. Time exceeded.'";
                return false;
            }
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.whatIsYourNameValidationText()))
        {
            error = "Failed to wait for the Dialogue bubble 'What is your NAME?'.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to touch the button with text 'Correct'";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the 'Got it' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the 'Got it' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to wait for the 'Pick parcels from storage' button.'";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to touch the 'Pick parcels from storage' button.'";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the 'Pick parcels from storage' button.'";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the 'Pick parcels from storage' button.'";
            return false;
        }
        narrator.stepPassedWithScreenShot(testData.TestCaseId + "- Parcel list befor scanning");

        if (!LoopBarcodeCollection())
        {
            error = "You failed to get the data!!! noooooo";
            return false;
        }

        if (!LoopBarcodeScan())
        {
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.parcelFoundtext()))
        {
            error = "Failed to wait for the 'Pick parcels from storage' button.'";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.issueParcelToCustomerButton()))
        {
            error = "Failed to touch the 'Issue parcels to customer' button.'";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.noOneWantsMeYetText()))
        {
            error = "Failed to wait for the 'No one wants me yet.' message";
            return false;
        }

        narrator.stepPassedWithScreenShot(testData.TestCaseId + " - After 1st scan to allocate parcel to customer");

        if (!LoopBarcodeScan())
        {
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.boomAnotherHappyCustomerText()))
        {
            error = "Failed to wait for the 'Boom! Another happy customer' message.'";
            return false;
        }

        narrator.stepPassedWithScreenShot(testData.TestCaseId + "Complete scanning proccess");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkout()))
        {
            error = "Failed to click the 'Check out.' message";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cardOption(testData.getData("CardNo")), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cardOption(testData.getData("CardNo"))))
            {
                error = "Failed to touch the ok button.";
                return false;
            }
            if (AppiumDriverInstance.waitForElementByIDWithTimeout(MobileDoddlePageObjects.retailerCheckYesButton(), 2))
            {
                if (!AppiumDriverInstance.waitForElementByID(MobileDoddlePageObjects.retailerCheckYesButton()))
                {
                    error = "Failed to wait for the 'Yes - Let's go' button to be touchable";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyID(MobileDoddlePageObjects.retailerCheckYesButton()))
                {
                    error = "Failed to wait for the 'Yes - Let's go' button to be touchable";
                    return false;
                }

            }
            else
            {
                if (!AppiumDriverInstance.waitForElementByID(MobileDoddlePageObjects.checkYesButton()))
                {
                    error = "Failed to wait for the 'Yes - Let's go' button to be touchable";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyID(MobileDoddlePageObjects.checkYesButton()))
                {
                    error = "Failed to wait for the 'Yes - Let's go' button to be touchable";
                    return false;
                }

            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.creditCardOptionButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.creditCardOptionButton()))
            {
                error = "Failed to touch the ok button.";
                return false;
            }
            if (!AppiumDriverInstance.waitForElementByID(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to wait for the 'Yes - Let's go' button to ne touchable";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyID(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to wait for the 'Yes - Let's go' button to ne touchable";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to wait for the 'Yes - Let's go' button to ne touchable";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to wait for the 'Yes - Let's go' button to ne touchable";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to wait for the 'Proceed to Payment' button to be touchable";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to touch the 'Proceed to Payment' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByID(MobileDoddlePageObjects.paymentProcessBackToDashboard()))
        {
            error = "Failed to wait for the 'Complete Collection' button to be touchable";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyID(MobileDoddlePageObjects.paymentProcessBackToDashboard()))
        {
            error = "Failed to touch the 'Complete Collection' button.";
            return false;
        }

        File index = new File(System.getProperty("user.dir") + "\\Barcodes");

        String[] entries = index.list();
        for (String s : entries)
        {
            File currentFile = new File(index.getPath(), s);
            currentFile.delete();
        }

        AppiumDriverInstance.pause(5000);

        narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Back at dashboard after parcel was allocated.");
        narrator.stepPassed("Successfully scanned, linked and handed over the parcel to the client. Another happy client!");

        return true;
    }

    //===================================================Custom Methods==============================================
    public boolean LoopBarcodeCollection()
    {

//        while (Run < timeout)
//        {
//            ParcelListSize = AppiumDriverInstance.Driver.findElements(By.id("com.doddle.concession:id/itemBarcode"));
//            for (int i = 0; i < ParcelListSize.size(); i++)
//            {
//                temp = ParcelListSize.get(i).getText().toString();
//                parcelList.add(temp.trim());
//            }
//            for (int i = 0; i < parcelList.size(); i++)
//            {
//                FinalBarcodes.add(parcelList.get(i).toString());
//
//            }
//            ListSizeBefore = ListSize;
//            ListSize = ListSize + FinalBarcodes.size();
//            try
//            {
//                AppiumDriverInstance.Driver.swipe(232, 554, 232, 295, 2000);
//            }
//            catch (Exception e)
//            {
//                error = "Failed to scroll to desired co-ordinates";
//                return false;
//            }
//
//            if (!(ListSizeBefore == FinalBarcodes.size()))
//            {
//                Run++;
//            }
         while (Run < timeout)
        {
            ParcelListSize = AppiumDriverInstance.Driver.findElements(By.id("com.doddle.concession:id/itemBarcode"));
            for (int i = 0; i < ParcelListSize.size(); i++)
            {
                temp = ParcelListSize.get(i).getText().toString();
                parcelList.add(temp.trim());
            }
            for (int i = 0; i < parcelList.size(); i++)
            {
                FinalBarcodes.add(parcelList.get(i).toString());

            }
            ListSizeBefore = ListSize;
            ListSize =  FinalBarcodes.size();
            try
            {
                AppiumDriverInstance.Driver.swipe(237, 509, 237, 262, 2000);
            }
            catch (Exception e)
            {
                error = "Failed to scroll to desired co-ordinates";
                return false;
            }

            if ((ListSizeBefore == ListSize))
            {
                Run++;
            }
        }
        try
        {
            SeleniumDriverInstance.startDriver();
        }
        catch (Exception e)
        {
            error = "Failed to Start Selenium WebDriver";
            return false;
        }

        for (String barcode : FinalBarcodes)
        {
            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.barcodeGeneratorURL(barcode)))
            {
                error = "Failed to generate barcode.";
                return false;
            }
            narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Barcode Generator page");
//==========================================================Saving barcode and converting=================================================
            int counter = 0;
            String imageURL = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.GeneratedBarcode())).getAttribute("src");
            Date date = new Date();
            name = barcode;
//              name = narrat  or.formatMessage(name);
            String destionationFile = System.getProperty("user.dir") + "\\Barcodes\\" + name + ".png";
            saveImageUsingURL(imageURL, destionationFile);

        }
        SeleniumDriverInstance.shutDown();
        return true;
    }

    public boolean LoopBarcodeScan()
    {

        try
        {
            for (String barcode : FinalBarcodes)
            {
                //Create Frame
                JFrame snakeFrame = new JFrame();
                ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
                JLabel jLabel = new JLabel(imageIcon);

                //Can set the default size here
                //snakeFrame.setBounds(100, 200, 800, 800);
                java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int centerX = screenSize.width / 28;
                int centerY = (screenSize.height / 2) + 200;

                snakeFrame.setLocation(centerX, centerY);

                //Must set visible - !
                snakeFrame.setVisible(true);

                //Load image - will be autoSized.
                snakeFrame.add(jLabel);
                snakeFrame.pack();
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
                if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
                {
                    error = "Failed to click on the 'OK' Popup button";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
                {
                    error = "Failed to click on the 'OK' Popup button";
                    return false;
                }
                AppiumDriverInstance.pause(1000);
                snakeFrame.repaint();
                snakeFrame.dispose();
            }

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean carrierLoopBarcodeScan()
    {

        try
        {

            for (barcodeCount = 0; barcodeCount < TotalNumOfBarcodes; barcodeCount++)
            {

                //Create Frame
                JFrame snakeFrame = new JFrame();
                ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcodeCount + ".png");
                JLabel jLabel = new JLabel(imageIcon);

                //Can set the default size here
                //snakeFrame.setBounds(100, 200, 800, 800);
                java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int centerX = screenSize.width / 28;
                int centerY = (screenSize.height / 2) + 200;

                snakeFrame.setLocation(centerX, centerY);

                //Must set visible - !
                snakeFrame.setVisible(true);

                //Load image - will be autoSized.
                snakeFrame.add(jLabel);
                snakeFrame.pack();
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
                if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
                {
                    error = "Failed to click on the 'OK' Popup button";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
                {
                    error = "Failed to click on the 'OK' Popup button";
                    return false;
                }
                AppiumDriverInstance.pause(1000);
                snakeFrame.repaint();
                snakeFrame.dispose();
            }

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean saveImageUsingURL(String imageURL, String destionationFile)
    {
        try
        {
            DataColumn barcodeColumn = new DataColumn("", "", Enums.ResultStatus.UNCERTAIN);
            barcodeColumn.columnHeader = name;

            URL url = new URL(imageURL);
            System.setProperty("jsse.enableSNIExtension", "false");
            InputStream fis = url.openStream();
            OutputStream fos = new FileOutputStream(destionationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = fis.read(b)) != -1)
            {
                fos.write(b, 0, length);
            }

            fis.close();
            fos.close();

            barcodeColumn.columnValue = convertPNGToBase64(destionationFile);
            barcodeColumn.resultStatus = Enums.ResultStatus.UNCERTAIN;
            DataRow currentRow = new DataRow();
            currentRow.DataColumns.add(barcodeColumn);
            listOfBarcodes.add(currentRow);

        }
        catch (Exception e)
        {
            error = "Failed - " + e.getMessage();
            return false;
        }
        return true;
    }

    public String convertPNGToBase64(String imageFilePath)
    {
        String base64ReturnString = "";

        try
        {
            out.println("[INFO] Converting error screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            out.println("[INFO] Converting completed, image ready for embedding.");
        }
        catch (Exception ex)
        {
            out.println("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }

    public boolean waitForElementNotVisible(String xpath, int maxWaitCount)
    {
        int timeCounter = 0;
        boolean itemPresent = AppiumDriverInstance.waitForElementByXpath(xpath);
        while (itemPresent)
        {
            if (timeCounter > maxWaitCount)
            {
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetWithTextView("Loading, please wait...")))
            {
                error = "Failed to wait for the progress bar to dissapear";
                return false;
            }
            //pause
            AppiumDriverInstance.pause(10000);

            //click the empty space
            itemPresent = AppiumDriverInstance.waitForElementByXpath(xpath);
            timeCounter++;
        }

        return true;
    }

    public boolean ScanBarcode()
    {
        try
        {

            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcodeCount + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            snakeFrame.repaint();
            snakeFrame.dispose();
            counter++;

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean ShelfBarcodScan()
    {
        try
        {

            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + 0 + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

}

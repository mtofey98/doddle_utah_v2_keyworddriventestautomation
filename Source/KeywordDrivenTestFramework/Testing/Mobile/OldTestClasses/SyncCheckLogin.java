/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import java.util.List;
import org.openqa.selenium.By;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class SyncCheckLogin extends BaseClass
{

    String error = "";
    Narrator narrator;

    public SyncCheckLogin(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!DoddleLogin())
        {
            return narrator.testFailed("Failed to login  - " + error);
        }
        return narrator.finalizeTest("Successfully logged into the Doddle app.");
    }

    public boolean DoddleLogin()
    {
        int counter = 0;
//        if (!login.DoddleLogin())
//        {
//            return false;
//        }
        boolean LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 10);

        while (LoadingCircle || counter < 5)
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
            {
                error = "Failed to wait for the the 'Menu' button to be visible";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
            {
                error = "Failed to click on the 'Menu' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked on the Menu button");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
            {
                error = "Failed to wait for the 'Logout/ switch User' button to be visible";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
            {
                error = "Failed to touch the 'Logout/ switch User' button.";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
            {
                error = "Failed to wait for the 'Are you Sure' popup to be visible";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
            {
                error = "Failed to touch the 'yes' button on the popup message.";
                return false;
            }

            //validation for logout proccess completed
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            LoadingCircle = AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 15);
            counter++;
        }

        return true;
    }

    public boolean Login()
    {
        String temp;
        // madatory pause requested by client to cater for popup message.

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        try
        {
            MobileElement version = (MobileElement) AppiumDriverInstance.Driver.findElement(By.id(MobileDoddlePageObjects.appVersionName()));

            temp = version.getText();

            //narrator.stepPassedWithScreenShot("APP version number : " + temp);
            narrator.reportStepPassed(temp);

        }
        catch (Exception e)

        {
            error = "Failed toe extract thew Version number for the app - " + e.getMessage();
            return false;
        }
        narrator.stepPassedWithScreenShot("Doddle Version : " + temp);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }
        if (!popUpMessage())
        {
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerButton()))
        {
            error = "Failed to click the store drop down";
            return false;
        }

        if (!SwipeWithTouchActions(235, 688, 235, 107))
        {
            error = "Fails to swipe to desired location";
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }

        if (!popUpMessage())
        {
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }
        if (AppiumDriverInstance.waitForElementByXpathWithTimeout(MobileDoddlePageObjects.blockingScreenClose(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.blockingScreenClose()))
            {
                error = "slksdlkfjhasldknmf ";
                return false;
            }

        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to click the 'Check In' button.";
            return false;
        }

        return true;

    }

    public boolean SwipeWithTouchActions(int startX, int startY, int endX, int endY)
    {
        String temp = "";
        int counter = 0;
        TouchAction action = new TouchAction(Driver);

        while (counter < 10)
        {

            List<MobileElement> list = AppiumDriverInstance.Driver.findElements(By.id(MobileDoddlePageObjects.storeList()));

            for (int i = 0; i < list.size(); i++)
            {
                temp = list.get(i).getText();
                if (temp.equals(testData.getData("StoreSelection")))
                {
                    break;
                }

            }
            if (temp.equals(testData.getData("StoreSelection")))
            {

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(testData.getData("StoreSelection"))))
                {
                    error = "Failed to select " + testData.getData("StoreSelection") + " from list.";
                    return false;
                }
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 5))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
                    {
                        error = "Failed to touch the ok button.";
                        return false;
                    }

                }
                break;
            }
            else
            {

                action.longPress(startX, startY).moveTo(endX, endY).release().perform();

            }

            if (!popUpMessage())
            {
                return false;
            }

            counter++;
        }

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }
}

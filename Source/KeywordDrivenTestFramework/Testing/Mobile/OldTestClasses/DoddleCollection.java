/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class DoddleCollection extends BaseClass
{

    String error = "";
    Narrator narrator;

    public DoddleCollection(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();

    }

    public TestResult executeTest()
    {
        if (!Login())
        {
            return narrator.testFailed("Failed to Login - " + error);
        }

        return narrator.finalizeTest("Process: Clear Chat Ran Successfully ");
    }

    public boolean Login()
    {

        //Will click on the Check in button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierTile()))
        {
            error = "Failed to click Check in button.";
            return false;
        }

        return true;
    }
}

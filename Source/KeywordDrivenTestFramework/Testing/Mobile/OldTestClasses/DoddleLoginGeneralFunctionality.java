/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import KeywordDrivenTestFramework.Utilities.CryptoUtility;


/**
 *
 * @author gdean
 * @author jmacauley
 */
public class DoddleLoginGeneralFunctionality extends BaseClass
{

    String error = "";
    Narrator narrator;

    public DoddleLoginGeneralFunctionality(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();
    }
    
    public TestResult executeTest()
    {
        if (!NegativeLoginTest())
        {
            return narrator.testFailed("Failed to test the login page with incorrect details - " + error);
        }
        if (!PositiveLoginTest())
        {
            return narrator.testFailed("Failed to login with the correct details and valiate the login was successfull " + error);
        }

        return narrator.finalizeTest("Successfully logged into the Doddle app.");
    }

    public boolean NegativeLoginTest()
    {
       if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetWithTextView("LOGIN")))
        {
            error = "Failed to wait for the login button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetWithTextView("LOGIN")))
        {
            error = "Failed to touch the login button.";
            return false;
        }
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetButtonWithText("Ok")))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("Ok")))
            {
                error = "Failed to touch the ok button.";
                return false;
            }

        }
        //the store selection
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetButtonWithText("Ok"), 10))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("Ok")))
            {
                error = "Failed to touch the ok button.";
                return false;
            }

        }
        
         if (!ScrollToElement())
        {
            return false;
        }
         
       if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("IncorrectUsername")))
        {
            error = "Failed to enter  username into the username field.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginPassword()))
        {
            error = "Failed to touch on the password field.";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("IncorrectPassword")))
        {
            error = "Failed to enter the  Incorrect Password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }
        //validate that the incorrect details where entered

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetWithTextView("Invalid \"Email address\" or \"Password\"")))
        {
            error = "Failed to wait for the login button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("ok")))
        {
            error = "Failed to click Login button.";
            return false;
        }

        
        return true;
    }

    public boolean PositiveLoginTest()
    {

    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter  username into the username field.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginPassword()))
        {
            error = "Failed to touch on the password field.";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the  Password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("LOGIN")))
        {
            error = "Failed to click Login button.";
            return false;
        }
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to click Check in button.";
            return false;
        }

        //Will click on the login button
        //Validate login
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Login validate failed.";
            return false;
        }

        return true;
    }
    //====================================================Custom Methods================================================
    public boolean ScrollToElement()
    {

          if (!AppiumDriverInstance.clickElementbyXpath("//android.widget.Spinner"))
        {
            error = "Failed to click on the store dropdown list";
            return false;
        }
              if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetButtonWithText("Ok"),10))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("Ok")))
            {
                error = "Failed to enter username.";
                return false;
            }
        }
        if (!AppiumDriverInstance.scrollToElement(testData.getData("StoreSelection")))
        {
            error = "Failed to scroll to element";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetTextViewWithText(testData.getData("StoreSelection"))))
        {
            error = "Failed to click on the store from scroll list";
            return false;
        }
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetButtonWithText("Ok"),10))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("Ok")))
            {
                error = "Failed to enter username.";
                return false;
            }
        }
        return true;
    }

}

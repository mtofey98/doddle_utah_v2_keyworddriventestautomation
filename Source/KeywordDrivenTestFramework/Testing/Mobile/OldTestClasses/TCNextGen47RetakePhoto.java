/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import KeywordDrivenTestFramework.Utilities.CryptoUtility;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TCNextGen47RetakePhoto extends BaseClass
{

    String name = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    String error = "";
    Narrator narrator;
    StringEntity input;
    String[] lines;
    String[] liness;
    String regex;

//==========================Veribles for custom methods=====================
    int Run = 0;
    int timeout = 8;
    int ListSize = 0;
    int ListSizeBefore = 0;
    String temp = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String imageURL = "";
    String destinationFile = "";
    List<WebElement> parcelListSize;
    Set<String> finalBarcodes = new HashSet<>();
    public String newBearer = "";
    String line = "";
    String apiLine;
    int run = 0;
    int listSizeBefore = 0;
    int listSize = 0;
    public boolean conErorr;
    String value;
    String itemReturn;
    int i = 0;
    public static URL url;
    String apiResponse;
    String itemID;
    int notificationBellBefore, notificationBellAfter;
    CryptoUtility crypt = new CryptoUtility();

    public static TestEntity currentData;
    Shortcuts shortcuts = new Shortcuts();
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();

//==========================================================================
    public TCNextGen47RetakePhoto(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        this.currentData = testData;
    }

    public TestResult executeTest()
    {

        String email = testData.getData("Email");
        String cellNumber = testData.getData("CellNo");
        String storeID = testData.getData("StoreID");
        String cardNo = testData.getData("cardNo");
        String postalCode = testData.getData("postal");

        if (!Shortcuts.newBearer())
        {
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + error);
        }

        if (!save.saveBarcode(barcode, shelfBarcode))
        {
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

//        if(!retrieveItemID(barcode))
//        {
//             return narrator.testFailedScreenshot("Failed to retrieve the item ID token - " + error);
//        }
        if (!restCall(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue(), email, cellNumber, barcode, randomNumber, storeID, cardNo, postalCode))
        {
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            Login();

            if (!carrierProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
            }
        }
        else
        {
            if (!carrierProcess())
            {
                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
            }
        }

        if (!storageProccess())
        {
            return narrator.testFailedScreenshot("Failed to do the Storage process - " + error);
        }

        if (!retrieveItemID(barcode))
        {
            return narrator.testFailedScreenshot("Failed to retrieve the item ID token - " + error);
        }

        if (!retakePhoto())
        {
            return narrator.testFailedScreenshot("Failed to retake photo on the website - " + error);
        }

        if (!retakePhotoTask())
        {
            return narrator.testFailedScreenshot("Failed to retake photo on the website - " + error);
        }

        return narrator.finalizeTest("Successfully completed Payload integration collection test");
    }

    public boolean retrieveItemID(String labelValue)
    {
        try
        {
            Shortcuts.doLabelValueAPICall(labelValue, Shortcuts.newBearer);
            System.out.println(Shortcuts.line);
            //regex
            apiResponse = Shortcuts.line;
            regex = ",\"itemId\":\"";
            lines = apiResponse.split(regex);
            liness = lines[1].split("\",\"labelValue\":");
            apiResponse = liness[0];
            itemID = apiResponse;
            System.out.println(itemID);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "Error - " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean carrierProcess()
    {

        try
        {
            notificationBellBefore = Integer.parseInt(AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.notificationCount()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "Error - " + e.getMessage();
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!shortcuts.scanBarcode(barcode))
        {
            error = "Failed to scan carrier barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Scanning barcode successful");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the 'back' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to touch on the 'back' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }

    public boolean storageProccess()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (!shortcuts.scanBarcode(barcode))
        {
            error = "Failed to scan storage barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }

        if (!shortcuts.scanShelfBarcode(shelfBarcode))
        {
            error = "Failed to scan shelf barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
            {
                error = "Failed to click on the pause storage button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to click the back to dashboard button";
                return false;
            }
        }
        else if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.finishStorageButton()))
        {
            error = "Failed to touch the 'Pause storage' button.";
            return false;
        }

        Narrator.stepPassed("Successfully retrieved parcel from carrier, scanned and stored parcel.");

        return true;
    }

    public boolean restCall(String header, String headerValue, String email, String cellNumber, String barcodeName, String randomNumber, String storeID, String cardNo, String postalCode)
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost preadvicePost = new HttpPost("https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        tokenPost.addHeader(header, headerValue);
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");

            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            preadvicePost.addHeader(header, "Bearer " + newBearer);

            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA19(email, randomNumber, barcode, storeID));
            input.setContentType("application/json");
            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean Login()
    {

        if (!conErorr)
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (!store.equals(narrator.getData("StoreSelection")))
            {
                if (!scrollToSubElement(narrator.getData("StoreSelection")))
                {
                    error = "Could not find " + narrator.getData("StoreSelection");
                    return false;
                }

            }

            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                Login();

            }
        }

//        if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//           if(!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 4))
//           {
//               error = "Failed to wait for the progress circe to no longer be present";
//               return false;
//           }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean retakePhoto()
    {
        try
        {
            SeleniumDriverInstance.startDriver();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "Error - " + e.getMessage();
            return false;
        }

        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.doddleSupportURL()))
        {
            error = "Failed to navigate to doddle support url";
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.switchToFrameByXpath(WebDoddlePageObjects.iframe()))
        {
            error = "Failed to switch to iframe";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.doddleSupportUsername(), testData.getData("username")))
        {
            error = "Failed to enter " + testData.getData("username") + " username into username field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WebDoddlePageObjects.doddleSupportPassword(), crypt.decrypt(testData.getData("password"))))
        {
            error = "Failed to enter " + testData.getData("password") + " username into username field";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.doddleSupportSignInBtn()))
        {
            error = "Failed to click the doddle support sign in button";
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.whatIsThisText()))
        {
            error = "Failed to wait for the what is this? text";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.sortByID()))
        {
            error = "Failed to sort ticket subjects by ID";
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.sortByID()))
        {
            error = "Failed to sort ticket subjects by ID";
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.latestSubjectTicket()))
        {
            error = "Failed to click the latest ticket subject";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.newPhotoReqCheckbox()))
        {
            error = "Failed to wait for the new photo required checkbox option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.newPhotoReqCheckbox()))
        {
            error = "Failed to click the new photo required checkbox option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.submitAsDropDown()))
        {
            error = "Failed to click the submitAsDropDown Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.submitAsSolvedOption()))
        {
            error = "Failed to click the submit as solved option";
            return false;
        }

        try
        {
            SeleniumDriverInstance.pause(1500);
            SeleniumDriverInstance.shutDown();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to shutdown selenium - " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean retakePhotoTask()
    {
        int count = 0;

        while (count < 6)
        {
            notificationBellAfter = Integer.parseInt(AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.notificationCount()));

            if (notificationBellBefore != notificationBellAfter)
            {
                break;
            }
            else
            {
                count++;
            }

        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tasksNotificationBell()))
        {
            error = "Failed to click the task notification bell";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Task's Layout");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retakeCollectionPhotoLayout()))
        {
            error = "Failed to wait for the retake collection task layout";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.completeTaskButton()))
        {
            error = "Failed to click the retake collection task button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loadingView(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.loadingView(), 15))
            {
                error = "Failed to wait for the loading view to be no longer visible";
                return false;
            }
        }

        try
        {
            String BarcodeText = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.retakePhotoItemBarcode());

            if (BarcodeText.equals(barcode))
            {
                Narrator.stepPassedWithScreenShot("Successfully validated correct parcel");
            }
            else
            {
                error = "Failed to validate parcel";
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "Error - " + e.getMessage();
            return false;
        }

        if (!shortcuts.scanBarcode(barcode))
        {
            error = "Failed to scan carrier barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Scanning barcode successful");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.startColletionButton()))
        {
            error = "Failed to wait for the retake photo button";
            return false;
        }

        try
        {
            String collectionStatus = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.collectionStatus());

            if (collectionStatus.equalsIgnoreCase("ON SHELF"))
            {
                Narrator.stepPassedWithScreenShot("Successfully validated status of parcel");
            }
            else
            {
                error = "Failed to validate status of parcel";
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "Error - " + e.getMessage();
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.startColletionButton()))
        {
            error = "Failed to click the retake photo button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to click the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for the use this photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to click the use this photo button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully retook photo task");

        AppiumDriverInstance.pause(500);

        Shortcuts.doLabelValueAPICall(barcode, Shortcuts.newBearer);

        if (!Shortcuts.line.contains("PHOTO_TAKEN"))
        {
            error = "Failed to add \"PHOTO_TAKEN\" event in CouchBase";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("PHOTO_TAKEN"))
        {
            error = "Failed to update text: \"PHOTO_TAKEN\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.closeTaskButton()))
        {
            error = "Failed to click the task close button";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        Shortcuts.doLabelValueAPICall(barcode, Shortcuts.newBearer);

        if (!Shortcuts.line.contains("ITEM_COLLECTION"))
        {
            error = "Failed to add \"ITEM_COLLECTION\" event in CouchBase";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("ITEM_COLLECTION"))
        {
            error = "Failed to update text: \"ITEM_COLLECTION\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        return true;
    }
    

}

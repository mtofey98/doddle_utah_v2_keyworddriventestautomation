/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.android.AndroidKeyCode;

/**
 *
 * @author jmacauley
 */
public class TA39DeviceScreenOffLogin extends BaseClass
{

    String error = "";
    public boolean conErorr;
    Narrator narrator;
    //Shortcuts shortcuts = new Shortcuts();

    public TA39DeviceScreenOffLogin(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            if (!DeviceScreenOff())
            {
                return narrator.testFailedScreenshot("Failed to do the login process with device screen off - " + error);
            }
        }
        else
        {
            if (!DeviceScreenOff())
            {
                return narrator.testFailedScreenshot("Failed to do the login process with device screen off - " + error);
            }

        }

        return narrator.finalizeTest("Successfully did the login process with device screen off.");
    }

    public boolean DeviceScreenOff()
    {
        if (!conErorr)
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to click the main login button";
                return false;
            }

            if (!popUpMessage())
            {
                error = "Failed to handle alert popup message";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!popUpMessage())
            {
                error = "Failed to handle alert popup message";
                return false;
            }

        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }

        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                DeviceScreenOff();

            }
        }

//        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.blockingScreenClose(), 2))
//        {
//            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.blockingScreenClose(), 4))
//            {
//                error = "Failed to wait for the semi-blocking screen to not be visible";
//                return false;
//            }
//        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        String carrierBefore = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.carriersTile());

        //Switch Device Screen Off      
        Driver.lockDevice();

        AppiumDriverInstance.pause(1000);

        if (Driver.isLocked())
        {
            narrator.stepPassedWithScreenShot("Device screen is off");
        }
        else
        {
            error = "Failed to take screenshot of device while off";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        Driver.pressKeyCode(AndroidKeyCode.KEYCODE_POWER);

        AppiumDriverInstance.pause(1000);

        String carrierAfter = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.carriersTile());

        AppiumDriverInstance.pause(1000);

        if (carrierBefore.equals(carrierAfter))
        {
            narrator.stepPassedWithScreenShot("Successfully validated Carriers");
        }
        else
        {
            error = "Failed to unlock screen";
            return false;
        }

        return true;
    }

    public boolean Logout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Menu button");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully logged out of the Doddle app");

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }
}

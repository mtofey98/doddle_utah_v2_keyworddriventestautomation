/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class TA21Payload_IntergratedCollection extends BaseClass
{

//==========================Global Variables=====================
    String name = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    String error = "";
    Narrator narrator;
    StringEntity input;
    String email;
    String cellNumber;
    String storeID;
    String cardNo;
    String postalCode;
    boolean testPassed = true;

//==========================Veribles for custom methods=====================
    int Run = 0;
    int timeout = 8;
    int ListSize = 0;
    int ListSizeBefore = 0;
    String temp = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String imageURL = "";
    String destinationFile = "";
    List<WebElement> parcelListSize;
    Set<String> finalBarcodes = new HashSet<>();
    public String newBearer = "";
    String line = "";
    int run = 0;
    int listSizeBefore = 0;
    int listSize = 0;
    public boolean conErorr;
    String value;
    String itemReturn;
    SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    Shortcuts shortcuts = new Shortcuts();

    public static TestEntity currentData;

//==========================================================================
    public TA21Payload_IntergratedCollection(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        this.currentData = testData;
    }

    public TestResult executeTest()
    {

        email = narrator.getData("Email");
        cellNumber = narrator.getData("CellNo");
        storeID = narrator.getData("StoreID");
        cardNo = narrator.getData("cardNo");
        postalCode = narrator.getData("postal");

        //Added by Manuel Lopes
        switch (this.testData.TestCaseId)
        {
            case "Carrier Process":
            {
                executeBarCode();
                if (testPassed == false)
                {
                    return narrator.testFailedScreenshot("Failed to seet the Barcode settings - " + error);
                }

                if (!ReusableFunctionalities.carrierProcessExecution())
                {
                    return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
                }
                return narrator.finalizeTest("Successfully completed Carrier Process test");

            }
            case "Storage Process":
            {
                if (!ReusableFunctionalities.StorageProccess())
                {
                    return narrator.testFailedScreenshot("Failed to do the Storage process - " + error);
                }
                return narrator.finalizeTest("Successfully completed Storage Process test");
            }

            case "Collection":
            {
                if (!ReusableFunctionalities.Collection())
                {
                    return narrator.testFailedScreenshot("Failed to do the Collection process - " + error);
                }
                Narrator.stepPassedWithScreenShot("Successfully completed the Collection process test");
            }

        }
        return narrator.finalizeTest("Successfully completed Payload integration collection test");
    }

    public TestResult executeBarCode()
    {
        if (!Shortcuts.newBearer())
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + error);
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, shelfBarcode))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }

        if (!restCall(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue(), email, cellNumber, barcode, randomNumber, storeID, cardNo, postalCode))
        {
            testPassed = false;
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        return narrator.finalizeTest("Successfully completed the Barcode set up");
    }

    public boolean restCall(String header, String headerValue, String email, String cellNumber, String barcodeName, String randomNumber, String storeID, String cardNo, String postalCode)
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost preadvicePost = new HttpPost("https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        tokenPost.addHeader(header, headerValue);
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {

            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");

            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            preadvicePost.addHeader(header, "Bearer " + newBearer);

            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA21(email, randomNumber, barcode, storeID));
            input.setContentType("application/json");
            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }

        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TA39CorrectStoreLogin extends BaseClass
{

    StringEntity input;
    String line, newBearer;
    String error = "";
    public boolean conErorr;
    Narrator narrator;
    //Shortcuts shortcuts = new Shortcuts();

    public TA39CorrectStoreLogin(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {  
        if (!CorrectStoreLogin())
        {
            return narrator.testFailedScreenshot("Failed to login to next gen app with correct store - " + error);
        }

        return narrator.finalizeTest("Successfully logged in with the correct store.");
    }

    public boolean CorrectStoreLogin()
    {

        if (!conErorr)
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinnerText());

            if (!store.equals(narrator.getData("StoreSelection")))
            {
                if (!scrollToSubElement(narrator.getData("StoreSelection")))
                {
                    error = "Could not find " + narrator.getData("StoreSelection");
                    return false;
                }

            }

            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the correct store");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                CorrectStoreLogin();

            }
        }

//        if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//           if(!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 4))
//           {
//               error = "Failed to wait for the progress circe to no longer be present";
//               return false;
//           }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        if (narrator.getData("StoreSelection").equals("CorrectStoreLogin"))
        {
            Narrator.stepPassedWithScreenShot("Successfully logged into Doddle app with Correct store");
        }

        return true;
    }

    public boolean Logout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Menu button");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully logged out of the Doddle app");

        AppiumDriverInstance.pause(30000);

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            error = e.getMessage();
            return false;
        }

    }

    public boolean putStoreConfigMultipleContainers()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.multipleContainerStoreConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);
            narrator.stepPassed(line);

            if (!line.contains("MULTIPLE_CONTAINERS"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            System.out.println("Exception - " + e.getMessage());
            return false;
        }

        return true;
    }

}

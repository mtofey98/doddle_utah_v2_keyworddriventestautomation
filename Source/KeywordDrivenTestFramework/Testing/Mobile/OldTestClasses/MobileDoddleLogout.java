/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileBrowserType;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class MobileDoddleLogout extends BaseClass
{

    static String error = "";
    Narrator narrator;

    public MobileDoddleLogout(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }

    public MobileDoddleLogout()
    {
        
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            return narrator.finalizeTest("Already Logged out of App");
        }
        else
        {
            if (!Logout())
            {
                return narrator.testFailed("Failed to logout of the Doddle App - " + error);
            }
        }

        return narrator.finalizeTest("Successfully logged out of the doddle app.");
    }

    public static boolean Logout()
    {    
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to click the Ok message button";
                return false;
            }
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to click the ok message popup button";
                return false;
            }
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.securityScanFailedBackToDashbaord(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.securityScanFailedBackToDashbaord()))
            {
                error = "Failed ot click the security scan failed button";
                return false;
            }
        }
              
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tryAgain(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tryAgain()))
            {
                error = "Failed to click the try again button";
                return false;
            }
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch(), 2))
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
            {
                error = "Failed to click the ok button";
                return false;
            }
        }
        
        //Closes the device keypad blocking the menu button
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.deviceKeypad(), 2))
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
            {
                error = "Failed to click on the 'Menu' button.";
                return false;
            }
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
            {
                error = "Failed to touch the 'Logout/switch User' button.";
                return false;
            }
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
            {
                error = "Failed to cancel click the all button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelBookingButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
            {
                error = "Failed to touch the 'yes' button on the popup message.";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
            {
                error = "Failed to click the more returns no button";
                return false;
            }
        }
        
        //Restarts the App in the case of the app having crashed
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.appMenu(), 2)
            || AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.googleSearchBar(), 2)
            || AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiTitle(), 2))
        {
            Narrator.stepFailedWithScreenShot("The app has crashed");
            AppiumDriverInstance.stopAndroidServer();
           _isRemote = false;
            AppiumDriverUtility.isCMDRunning = false;
            
            try
            {
                AppiumDriverInstance = new AppiumDriverUtility();
                Thread.sleep(2000);
            }
            catch (InterruptedException e)
            {
                Narrator.logError("[ERROR] - " + e.getMessage());
            }       
        }
        
        else
        {                 
            //validation for logout proccess completed
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }
        }
 
        
        Shortcuts.error = "";
              
        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TA39IncorrectStoreLogin extends BaseClass
{

    String error = "";
    public boolean conErorr;
    Narrator narrator;
    String storeBefore;
    //Shortcuts shortcuts = new Shortcuts();

    public TA39IncorrectStoreLogin(TestEntity testData)
    {
        this.testData = testData;
       narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            if (!IncorrectStoreLogin())
            {
                return narrator.testFailedScreenshot("Failed to do the login process with incorrect store - " + error);
            }
        }
        else
        {
            Logout();

            if (!IncorrectStoreLogin())
            {
                return narrator.testFailedScreenshot("Failed to do the login process with incorrect store - " + error);
            }
        }

        return narrator.finalizeTest("Successfully the login process with incorrect store.");
    }

    public boolean IncorrectStoreLogin()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle popup messages";
            return false;
        }

        if (!scrollToSubElement(narrator.getData("IncorrectStore")))
        {
            error = "Could not find " + narrator.getData("IncorrectStore");
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle popup messages";
            return false;
        }

        //Driver.pressKeyCode(AndroidKeyCode.BACK);
        try
        {
            storeBefore = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (!scrollToSubElement(narrator.getData("StoreSelection")))
            {
                error = "Could not find " + narrator.getData("StoreSelection");
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Error - " + e.getMessage());
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertCancelPopup()))
        {
            error = "Failed to click on the cancel popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.alertCancelPopup()))
        {
            error = "Failed to click on the cancel popup";
            return false;
        }

        try
        {
            String storeAfter = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (storeBefore.equals(storeAfter))
            {
                narrator.stepPassedWithScreenShot("Validated " + storeAfter + " has been extracted from spinner");
            }
            else
            {
                error = "Failed to compare store before to store after";
                return false;
            }

            if (!scrollToSubElement(narrator.getData("StoreSelection")))
            {
                error = "Could not find " + narrator.getData("StoreSelection");
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Error - " + e.getMessage());
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), narrator.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), narrator.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginBtn()))
        {
            error = "Failed to click Login button.";
            return false;
        }
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to wait for the permissions popup";
            return false;
        }
        
        
        String permissionsError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.messagePopup());
            
        Narrator.stepPassedWithScreenShot(permissionsError);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
        {
            error = "Failed to click on the ok popup";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to wait for the toolbar back text to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to click on the toolbar back text";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validate incorrect store login");

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean Logout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Menu button");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully logged out of the Doddle app");

        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }

}

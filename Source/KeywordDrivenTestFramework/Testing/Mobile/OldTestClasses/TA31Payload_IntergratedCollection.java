/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SaveBarcodesFromURL;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class TA31Payload_IntergratedCollection extends BaseClass
{

    String name = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    String error = "";
    Narrator narrator;
    StringEntity input;

//==========================Veribles for custom methods=====================
    int Run = 0;
    int timeout = 8;
    int ListSize = 0;
    int ListSizeBefore = 0;
    String temp = "";
    Set<String> FinalBarcodes = new HashSet<>();
    List<WebElement> ParcelListSize;
    List<String> parcelList = new ArrayList();
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String imageURL = "";
    String destinationFile = "";
    List<WebElement> parcelListSize;
    Set<String> finalBarcodes = new HashSet<>();
    public String newBearer = "";
    String line = "";
    int run = 0;
    int listSizeBefore = 0;
    int listSize = 0;
    public boolean conErorr;
    String value;
    String itemReturn;
    Shortcuts shortcuts = new Shortcuts();

    public static TestEntity currentData;

//==========================================================================
    public TA31Payload_IntergratedCollection(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        this.currentData = testData;
    }

    public TestResult executeTest()
    {

        String email = narrator.getData("Email");
        String cellNumber = narrator.getData("CellNo");
        String storeID = narrator.getData("StoreID");
        String cardNo = narrator.getData("cardNo");
        String postalCode = narrator.getData("postal");

        if (!Shortcuts.newBearer())
        {
            return narrator.testFailedScreenshot("Failed to retrieve the new bearer token - " + error);
        }
        
//        if (!generateBarcode(barcode, shelfBarcode, randomNumber))
//        {
//            return narrator.testFailedScreenshot("Failed to generate the barcodes - " + error);
//        }

        SaveBarcodesFromURL save = new SaveBarcodesFromURL();

        if (!save.saveBarcode(barcode, shelfBarcode))
        {
            return narrator.testFailedScreenshot("Failed to generate and save the barcodes - " + error);
        }
        
        if (!restCall(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue(), email, cellNumber, barcode, randomNumber, storeID, cardNo, postalCode))
        {
            return narrator.testFailedScreenshot("Failed to do the rest call for the payload - " + error);
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            Login();

            if (!carrierProcess())
            {

                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
            }
        }
        else
        {
            if (!carrierProcess())
            {

                return narrator.testFailedScreenshot("Failed to do the Carrier process - " + error);
            }
        }

        if (!StorageProccess())
        {

            return narrator.testFailedScreenshot("Failed to do the Storage process - " + error);
        }

        if (!Collection())
        {

            return narrator.testFailedScreenshot("Failed to do the Collection process - " + error);
        }

        return narrator.finalizeTest("Successfully completed Payload integration collection test");
    }

    public boolean carrierProcess()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

//        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 3))
//            {
//                error = "Failed to wait for the 'Loading, Please wait...' to be no longer visible";
//                return false;
//            }
//        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!shortcuts.scanBarcode(barcode))
        {
            return false;
        }

        Narrator.stepPassedWithScreenShot("Scanning barcode successful");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the 'back' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to touch on the 'back' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }

    public boolean StorageProccess()
    {
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }
        
        if(!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (!shortcuts.scanBarcode(barcode))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }
        if (!shortcuts.scanShelfBarcode(shelfBarcode))
        {
            error = "Failed to scan shelfLabel barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
            {
                error = "Failed to click on the pause storage button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to click the back to dashboard button";
                return false;
            }
        }
        else if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.finishStorageButton()))
        {
            error = "Failed to touch the 'Pause storage' button.";
            return false;
        }

        Narrator.stepPassed("Successfully retrieved parcel from carrier, scanned and stored parcel.");
        return true;
    }

    public boolean Collection()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to touch the 'Email' option.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfull entered email address");

        if (!AppiumDriverInstance.tapCoordinates(650, 1235))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                error = "Failed to wait for the loading screen to be no longer visible";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.whatIsYourNameValidationText()))
        {
            error = "Failed to wait for the Dialogue bubble 'What is your NAME?'.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to touch the button with text 'Correct'";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the 'ok' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the 'ok' button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionRetrievingParcels(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.collectionRetrievingParcels(), 60))
            {
                error = "Failed to wait for the 'loading screen' to disappear";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("");
        
        //======================================================================================================================
        AppiumDriverInstance.pause(2000);
        Shortcuts.doLabelValueAPICall(barcode, Shortcuts.newBearer);

        if (!Shortcuts.line.contains("EXPECTED"))
        {
            error = "Failed to add \"EXPECTED\" event in CouchBase";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("EXPECTED"))
        {
            error = "Failed to update text: \"EXPECTED\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        //======================================================================================================================
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.noParcelsText(), 2))
        {
            error = "There are no parcels to scan";
            Narrator.stepFailedWithScreenShot("No Parcels");
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to wait for the pick parcels from storage button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to touch the 'Pick parcels from storage' button.'";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the 'Pick parcels from storage' button(2).'";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the 'Pick parcels from storage' button.'";
            return false;
        }

//        String expected1 = AppiumDriverInstance.retrieveTextByName(MobileDoddlePageObjects.expectedParcels());
//        String expected2 = AppiumDriverInstance.retrieveTextByName(MobileDoddlePageObjects.expectedParcels());
//
//        Narrator.stepFailedWithScreenShot("Expected Parcels");
//        
//        if (expected1.equals("EXPECTED") & expected2.endsWith("EXPECTED"))
//        {
//            error = "Expected Parcels";
//            return false;
//        }
//
//        if (AppiumDriverInstance.waitForElementByXpathWithTimeout(MobileDoddlePageObjects.useScannerButtonOK(), 10))
//        {
//
//            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
//            {
//                error = "Failed to touch the 'Pick parcels from storage' button.'";
//                return false;
//            }
//            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
//            {
//                error = "Failed to wait for the 'Pick parcels from storage' button(2).'";
//                return false;
//            }
//            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
//            {
//                error = "Failed to touch the 'Pick parcels from storage' button.'";
//                return false;
//            }
//        }
//        else
//        {
//
//            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
//            {
//                error = "Failed to touch the 'Pick parcels from storage' button.'";
//                return false;
//            }
//
//            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
//            {
//                error = "Failed to wait for the 'Pick parcels from storage' button().'";
//                return false;
//            }
//            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
//            {
//                error = "Failed to touch the 'Pick parcels from storage' button.'";
//                return false;
//            }
//
//        }
        Narrator.stepPassedWithScreenShot(testData.TestCaseId + "- Parcel list before scanning");

        AppiumDriverInstance.pause(2000);

        if (!shortcuts.loopBarcodeCollection())
        {
            error = Shortcuts.error;
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!shortcuts.loopBarcodeScan())
        {
            error = Shortcuts.error;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to wait for the 'Return to customer' message.'";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Complete scanning proccess");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to click the 'Return to customer' button";
            return false;
        }

        //======================================================================================================================
        AppiumDriverInstance.pause(2000);
        Shortcuts.doLabelValueAPICall(barcode, Shortcuts.newBearer);

        if (!Shortcuts.line.contains("ON_SHELF"))
        {
            error = "Failed to add \"ON_SHELF\" event in CouchBase";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("ON_SHELF"))
        {
            error = "Failed to update text: \"ON_SHELF\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        //======================================================================================================================
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.yesCheckoutButton()))
        {
            error = "Failed to wait for the yes checkout button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.yesCheckoutButton()))
        {
            error = "Failed to click the yes checkout button";
            return false;
        }
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cardOption(testData.getData("cardNo")), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cardOption(testData.getData("cardNo"))))
            {
                error = "Failed to touch the ok button.";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Card No");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerCheckYesButton()))
            {
                error = "Failed to wait for the 'Yes - Let's go' button to be touchable(1)";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerCheckYesButton()))
            {
                error = "Failed to touch the 'Yes - Let's go' button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.creditCardOptionButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.creditCardOptionButton()))
            {
                error = "Failed to touch the ok button.";
                return false;
            }
            Narrator.stepPassedWithScreenShot("Credit card option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to wait for the 'Yes - Let's go' button to be touchable(2)";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to touch the 'Yes - Let's go' button";
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.passportOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.passportOption()))
            {
                error = "Failed to touch the ok button.";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Passport option");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to wait for the 'Yes - Let's go' button to be touchable(2)";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to touch the 'Yes - Let's go' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to wait for the proceed to payment button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to click the proceed to payment button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to wait for the 'Proceed to Payment' button to be touchable";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to touch the 'Proceed to Payment' button.";
            return false;
        }
       
        //======================================================================================================================
        AppiumDriverInstance.pause(2000);
        Shortcuts.doLabelValueAPICall(barcode, Shortcuts.newBearer);

        if (!Shortcuts.line.contains("COLLECTED_BY_CUSTOMER"))
        {
            error = "Failed to add \"COLLECTED_BY_CUSTOMER\" event in CouchBase";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("COLLECTED_BY_CUSTOMER"))
        {
            error = "Failed to update text: \"COLLECTED_BY_CUSTOMER\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        //======================================================================================================================
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.paymentProcessBackToDashboard()))
        {
            error = "Failed to wait for the 'Complete Collection' button to be touchable";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.paymentProcessBackToDashboard()))
        {
            error = "Failed to touch the 'Complete Collection' button.";
            return false;
        }

        if (!shortcuts.clearBarcodeFolder())
        {
            error = "Failed to clear the barcode folder";
            return false;
        }

        AppiumDriverInstance.pause(2000);

        //======================================================================================================================
        Shortcuts.doLabelValueAPICall(barcode, Shortcuts.newBearer);

        if (!Shortcuts.line.contains("WITH_CUSTOMER"))
        {
            error = "Failed to add \"AT_COLLECTION_POINT\" event in CouchBase";
            return false;
        }

        if (!Shortcuts.changeAPIResponseColor("WITH_CUSTOMER"))
        {
            error = "Failed to update text: \"WITH_CUSTOMER\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        //======================================================================================================================
        
        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Back at dashboard after parcel was allocated.");
        Narrator.stepPassed("Successfully scanned, linked and handed over the parcel to the client. Another happy client!");

        return true;
    }

    public boolean generateBarcode(String barcodeName, String shelfLabel, String randomNumber)
    {
        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(barcodeName);
        destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcodeName + ".png";

        SeleniumDriverInstance.startDriver();

        if (!SeleniumDriverInstance.navigateTo(imageURL))
        {
            error = "Failed to generate barcode.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Barcode Generator page");

        if (!AppiumDriverInstance.takeScreenShotWithCoordinates(destinationFile))
        {
            error = "Failed to take screenshot and save barcode to file";
            return false;
        }

        //======================= ShelfLabel Barcode ====================================
        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(shelfLabel);
        destinationFile = System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfLabel + ".png";

        if (!SeleniumDriverInstance.navigateTo(imageURL))
        {
            error = "Failed to generate shelfLabel barcode.";
            return false;
        }

        narrator.stepPassedWithScreenShot("ShelfLabel Barcode Generator page");

        if (!AppiumDriverInstance.takeScreenShotWithCoordinates(destinationFile))
        {
            error = "Failed to take screenshot and save barcode to file";
            return false;
        }

        SeleniumDriverInstance.shutDown();

        return true;
    }

    public boolean restCall(String header, String headerValue, String email, String cellNumber, String barcodeName, String randomNumber, String storeID, String cardNo, String postalCode)
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost preadvicePost = new HttpPost("https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        tokenPost.addHeader(header, headerValue);
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {

            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");

            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            preadvicePost.addHeader(header, "Bearer " + newBearer);

            input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA31(email, cellNumber, randomNumber, barcodeName, storeID));
            input.setContentType("application/json");
            preadvicePost.setEntity(input);
            HttpResponse preadviceResponse = client.execute(preadvicePost);
            rd = new BufferedReader(new InputStreamReader(preadviceResponse.getEntity().getContent()));

            line = rd.readLine();

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean loopBarcodeCollection()
    {
        while (run < timeout)
        {
            parcelListSize = AppiumDriverInstance.Driver.findElements(By.xpath(MobileDoddlePageObjects.retakePhotoItemBarcode()));
            for (int i = 0; i < parcelListSize.size(); i++)
            {
                temp = parcelListSize.get(i).getText();
                parcelList.add(temp.trim());
            }
            for (int i = 0; i < parcelList.size(); i++)
            {
                finalBarcodes.add(parcelList.get(i));

            }
            listSizeBefore = listSize;
            listSize = finalBarcodes.size();
            try
            {
                AppiumDriverInstance.Driver.swipe(237, 509, 237, 262, 2000);
            }
            catch (Exception e)
            {
                error = "Failed to scroll to the desired co-ordinates - " + e.getMessage();
                return false;
            }

            if ((listSizeBefore == listSize))
            {
                run++;
            }
        }
        try
        {
            SeleniumDriverInstance.startDriver();
        }
        catch (Exception e)
        {
            error = "Failed to Start Selenium WebDriver - " + e.getMessage();
            return false;
        }

        for (String barcode : finalBarcodes)
        {
            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.barcodeGeneratorURL(barcode)))
            {
                error = "Failed to generate barcode.";
                return false;
            }
//==========================================================Saving barcode and converting=================================================
            String imageURL = WebDoddlePageObjects.barcodeGeneratorURL(barcode);
            String destionationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png";
            if (!AppiumDriverInstance.takeScreenShotWithCoordinates(destionationFile))
            {
                error = "Failed to take multiple screenshots in the loopbarcode scan";
                return false;
            }

        }
        SeleniumDriverInstance.shutDown();
        return true;
    }

    //Scans multiple barcodes
    public boolean loopBarcodeScan()
    {

        try
        {
            for (String barcode : finalBarcodes)
            {
                //Create Frame
                JFrame snakeFrame = new JFrame();
                ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
                JLabel jLabel = new JLabel(imageIcon);

                //Can set the default size here
                //snakeFrame.setBounds(100, 200, 800, 800);
                java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int centerX = screenSize.width / 28;
                int centerY = (screenSize.height / 2) + 200;

                snakeFrame.setLocation(centerX, centerY);

                //Must set visible - !
                snakeFrame.setVisible(true);

                //Load image - will be autoSized.
                snakeFrame.add(jLabel);
                snakeFrame.pack();
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();

                if (!popUpMessage())
                {
                    error = "Failed to click pop up message";
                    return true;
                }

                AppiumDriverInstance.pause(2000);
                snakeFrame.repaint();
                snakeFrame.dispose();
            }
        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }
        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean Login()
    {

        if (!conErorr)
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

            if (!store.equals(narrator.getData("StoreSelection")))
            {
                if (!scrollToSubElement(narrator.getData("StoreSelection")))
                {
                    error = "Could not find " + narrator.getData("StoreSelection");
                    return false;
                }

            }

            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                Login();

            }
        }

//        if(AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
//        {
//           if(!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 4))
//           {
//               error = "Failed to wait for the progress circe to no longer be present";
//               return false;
//           }
//        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");
        return true;
    }

    public boolean Logout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked on the Menu button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully logged out of the Doddle app");

        return true;
    }


    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean scanShelfBarcode(String barcode)
    {
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\ShelfLabels\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            if (!popUpMessage())
            {
                error = "Failed to click pop up message";
                return true;
            }

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }
}

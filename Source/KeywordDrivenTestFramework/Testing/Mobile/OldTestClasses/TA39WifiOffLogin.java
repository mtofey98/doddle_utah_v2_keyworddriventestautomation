/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts.error;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class TA39WifiOffLogin extends BaseClass
{

    String error = "";
    String connectionError = "";
    Narrator narrator;
    static boolean wifi = true;
    //Shortcuts shortcuts = new Shortcuts();

    public TA39WifiOffLogin(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData, " - Contains: " + testData.getData("testIDs"));
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            if (!wifiSwitch())
            {
                return narrator.testFailedScreenshot("Failed to do the login process with wifi off - " + error);
            }
        }
        else
        {
            Logout();

            if (!wifiSwitch())
            {
                return narrator.testFailedScreenshot("Failed to do the login process with wifi off - " + error);
            }
        }

        if (!wifi)
        {
            wifiSwitchOn();
        }

        return narrator.finalizeTest("Successfully did the login process with wifi off.");
    }

    public boolean wifiSwitch()
    {
        if (wifi)
        {
            if (!wifiSwitchOff())
            {
                error = "Failed to handle the popup messages";
                return false;
            }

            wifi = false;
            AppiumDriverInstance.pause(5000);
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!scrollToSubElement(narrator.getData("StoreSelection")))
        {
            error = "Failed to find " + narrator.getData("StoreSelection");
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        try
        {
            connectionError = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.connectionErrorPopup());

            if (connectionError.contains("Connection"))
            {
                narrator.stepPassedWithScreenShot("Connection Error");
            }

            AppiumDriverInstance.pause(2000);
            AppiumDriverInstance.Driver.pressKeyCode(AndroidKeyCode.BACK);
        }
        catch (Exception e)
        {
            error = "Failed to click the " + connectionError + " Popup";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to wait for the toolbar back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.toolbarBackText()))
        {
            error = "Failed to click the toolbar back button";
            return false;
        }

        if (!wifiSwitchOn())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        wifi = true;

        return true;
    }

    public boolean Logout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Menu button");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully logged out of the Doddle app");

        return true;
    }

    public boolean wifiSwitchOn()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }

            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
            String wifiText = element.getAttribute("name");

            System.out.println("");

            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                {
                    error = "Failed to click the wifi button with index";
                    return false;

                }

            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
        {
            error = "Failed to wait for the wifi to be connected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public boolean wifiSwitchOff()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }

            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
            String wifiText = element.getAttribute("name");

            System.out.println("");

            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                {
                    error = "Failed to click the wifi button with index";
                    return false;

                }

            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiDisconnected()))
        {
            error = "Failed to wait for the wifi to be disconnected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned off Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }
}

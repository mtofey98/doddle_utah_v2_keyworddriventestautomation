/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.ReusableFunctionalities;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class DoddleLogin extends BaseClass
{

    String error = "";
    StringEntity input;
    public String line;
    public String newBearer;
    Narrator narrator;
    public boolean conError;
    public boolean configUpdated;

    public DoddleLogin(TestEntity testData)
    {
        BaseClass.testData = testData;
        narrator = new Narrator(testData); 
        AppiumDriverInstance = new AppiumDriverUtility();
        
    }

    public TestResult executeTest()
    {
        
        if (!ReusableFunctionalities.checkWifiIsOn())
        {
            return narrator.testFailed("Failed to validate Wifi  - " + error);
        }
      
        if (!Shortcuts.newBearer())
        {
            return narrator.testFailed("Failed to generate bearer token - " + error);
        }

        if (!DoddleLogin())
        {
            return narrator.testFailed("Failed to login  - " + error);
        }

        return narrator.finalizeTest("Successfully logged into the Doddle app.");
    }

    public boolean wifiSwitch()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {

            if (currentDevice == Enums.Device.ZebraMC40)
            {
                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                {
                    error = "Failed to wait for the wifi button with index to be visible";
                    return false;
                }

                MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
                String wifiText = element.getAttribute("name");

                System.out.println(wifiText);

                if (wifiText.contains("off"))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                    {
                        error = "Failed to click the wifi button with index";
                        return false;
                    }
                }
                else
                {
                    Driver.pressKeyCode(AndroidKeyCode.BACK);
                    return true;
                }
            }
            else if (currentDevice == Enums.Device.ZebraTC70_Doddle)
            {
                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleWiFiBtnWithIndex()))
                {
                    error = "Failed to wait for the wifi button with index to be visible";
                    return false;
                }

                MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.doddleWiFiBtnWithIndex()));
                String wifiText = element.getAttribute("name");

                System.out.println(wifiText);

                if (wifiText.contains("off"))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleWiFiBtnWithIndex()))
                    {
                        error = "Failed to click the wifi button with index";
                        return false;
                    }
                }
                else
                {
                    Driver.pressKeyCode(AndroidKeyCode.BACK);
                    return true;
                }
            }
        }

        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
        {
            error = "Failed to wait for the wifi to be connected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public boolean DoddleLogin()
    {

        String temp;

        if (!conError)
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to wait for the login button.";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }
            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            try
            {
                String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinnerText());

                if (!store.equals(testData.getData("StoreSelection")))
                {
                    if (!scrollToSubElement(testData.getData("StoreSelection")))
                    {
                        error = "Could not find " + testData.getData("StoreSelection");
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                error = "Error - " + e.getMessage();
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = Shortcuts.error;
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages())
            {
                error = "Failed to handle the popup messages";
                return false;
            }
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }

        if (!multiplePopUpMessages())
        {
            error = "Failed to handle the popup messages";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully entered the login details: <br> Username: " + testData.getData("username") + " <br> Password: " + testData.getData("password"));

        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 8))
        {
            AppiumDriverInstance.Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!conError)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conError = true;
                DoddleLogin();

            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

        return true;

    }

    public boolean SwipeWithTouchActions(int startX, int startY, int endX, int endY)
    {
        String temp = "";
        int counter = 0;
        TouchAction action = new TouchAction(Driver);

        while (counter < 10)
        {

            List<MobileElement> list = AppiumDriverInstance.Driver.findElements(By.id(MobileDoddlePageObjects.storeList()));

            for (int i = 0; i < list.size(); i++)
            {
                temp = list.get(i).getText();
                if (temp.equals(testData.getData("StoreSelection")))
                {
                    break;
                }

            }
            if (temp.equals(testData.getData("StoreSelection")))
            {

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(testData.getData("StoreSelection"))))
                {
                    error = "Failed to select " + testData.getData("StoreSelection") + " from list.";
                    return false;
                }
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 5))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
                    {
                        error = "Failed to touch the ok button.";
                        return false;
                    }

                }
                break;
            }
            else
            {

                action.longPress(startX, startY).moveTo(endX, endY).release().perform();

            }

            if (!popUpMessage())
            {
                return false;
            }

            counter++;
        }

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 6; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean scrollToStore(String storeSelection)
    {

        try
        {
            AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.spinnerButton())).click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            List<WebElement> list = AppiumDriverInstance.Driver.findElements(By.xpath("android.widget.ListView"));

            String store = storeSelection;

            for (int i = 0; i < list.size(); i++)
            {
                if (list.get(i).toString().equals(store))
                {
                    list.get(i).click();
                    break;
                }
                else
                {
                    TouchAction action = new TouchAction(AppiumDriverInstance.Driver);
                    action.press(i, i);
                }
            }

        }
        catch (Exception e)
        {
            System.out.println("Error - " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean putStoreConfigMultipleContainers()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.multipleContainerStoreConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);
            narrator.stepPassed(line);

            if (!line.contains("MULTIPLE_CONTAINERS"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public boolean putStoreConfigSingleContainer()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.singleContainerStoreConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);
            narrator.stepPassed(line);

            if (!line.contains("SINGLE_CONTAINER"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author mtofey
 */
public class TC_NextGen_03_TestClass extends BaseClass 
{
    String error = "";
    boolean testRequiresAppium;
    boolean testRequiresStoreConfigUpdate;
    String randomNumber = MobileDoddlePageObjects.randomNo();
    String barcode = MobileDoddlePageObjects.barcode();
    String shelfBarcode = MobileDoddlePageObjects.rand();
    String imageURL = "";
    String destinationFile = "";
    
    public TC_NextGen_03_TestClass(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }
    
    public TestResult executeTest()
    {
        
        if(!defineEnvironment())
        {
            return narrator.testFailed("Failed to define the environment for the test to run");
        }
        
        //Pre-condition: App is logged in
        
        if (!generateBarcode(barcode, shelfBarcode))
        {
            return narrator.testFailedScreenshot("Failed to generate the barcode " + error);
        }
        
        
        if(!carrierProcess())
        {
            return narrator.testFailed("Failed to add \"At Goods in\" status on parcel");
        }
        
        return narrator.finalizeTest("Successfully added \"At Goods in\" status on parcel");
    }
    
    public boolean defineEnvironment()
    {
        try
        {                 
            if(testData.getData("testRequiresStoreConfigUpdate").equalsIgnoreCase("true"))
                this.testRequiresStoreConfigUpdate = true;
            if(testData.getData("testRequiresAppium").equalsIgnoreCase("true"))
                this.testRequiresAppium = true;
                
            if(this.testRequiresStoreConfigUpdate)
            {
                //Not needed at this stage - Added for future updates
                if(!doAPICall())
                {
                    throw new Exception("Failed to update the store config");
                }
            }
            
            if(this.testRequiresAppium)            {
                AppiumDriverInstance = new AppiumDriverUtility();
            }
        }
        catch(Exception e)
        {
            Narrator.logError("[ERROR] - Failed to deffine the test environment. Reason; "+e.getMessage());
            return false;
        }
        
        return true;
    }
    
    public boolean doAPICall()
    {
        return true;
    }
    
    public boolean carrierProcess()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!scanBarcode(barcode))
        {
            error = "Failed to scan carrier barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Scanning barcode successful");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the 'back' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to touch on the 'back' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }
    
    public boolean generateBarcode(String barcodeName, String shelfLabel)
    {
        //========================= Normal Barcode =====================================

        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(barcodeName);
        destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcodeName + ".png";

        SeleniumDriverInstance.startDriver();

        if (!SeleniumDriverInstance.navigateTo(imageURL))
        {
            error = "Failed to generate barcode.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Barcode Generator page");

        if (!AppiumDriverInstance.takeScreenShotWithCoordinates(destinationFile))
        {
            error = "Failed to take screenshot and save barcode to file";
            return false;
        }

        //======================= ShelfLabel Barcode ====================================
        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(shelfLabel);
        destinationFile = System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfLabel + ".png";

        if (!SeleniumDriverInstance.navigateTo(imageURL))
        {
            error = "Failed to generate shelfLabel barcode.";
            return false;
        }

        narrator.stepPassedWithScreenShot("ShelfLabel Barcode Generator page");

        if (!AppiumDriverInstance.takeScreenShotWithCoordinates(destinationFile))
        {
            error = "Failed to take screenshot and save barcode to file";
            return false;
        }

        SeleniumDriverInstance.shutDown();

        return true;
    }
    
    public boolean scanBarcode(String barcode)
    {
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            if (!popUpMessage())
            {
                error = "Failed to click pop up message";
                return true;
            }

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }
    
    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

}

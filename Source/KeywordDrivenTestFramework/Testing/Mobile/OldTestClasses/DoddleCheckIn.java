/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.android.AndroidKeyCode;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class DoddleCheckIn extends BaseClass
{

    String error = "";
    Narrator narrator;

    public DoddleCheckIn(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);

    }

    public TestResult executeTest() 
    {
        if (!Checkin())
        {
            return narrator.testFailed("Failed to Checkin - " + error);
        }
         if (!Storage())
        {
            return narrator.testFailed("Failed to Checkin - " + error);
        }

        return narrator.finalizeTest("Process: Checkin completed Successfully. ");
    }

    public boolean Checkin()
    {
        //Will click on the Check in button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to touch Check in button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetWithEditText("Enter Carrier name")))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield..";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetWithEditText("Enter Carrier name")))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield..";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.widgetWithEditText("Enter Carrier name"), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetTextViewToImageView(testData.getData("Carrier"))))
        {
            error = "Failed to touch on the " + testData.getData("Carrier") + " Tile.";
            return false;
        }

        if (!waitForElementNotVisible(MobileDoddlePageObjects.widgetWithTextView("Loading, please wait..."), 25))
        {
            error = "Failed to wait for the loading sign to go away.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetLinearLayoutWithIndex("3")))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield..";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetLinearLayoutWithIndex("3")))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield..";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetButtonWithText("Yes")))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("Yes")))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }

    
        
        if (!AppiumDriverInstance.BarcodeScan())
        {
            return false;
        }



        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetButtonWithText("All parcels have been scanned")))
        {
            error = "Failed to wait on the 'All parcels have been scanned' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("All parcels have been scanned")))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button";
            return false;
        }

        //Will click on the Store parcel now button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("Store parcels now")))
        {
            error = "Failed to touch the 'sore parcel now' button.";
            return false;
        }
      //   if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.linearLayoutWithTextView("Scan the first Red-lined parcel")))
     //   {
     //       error = "Failed to wait on the 'All parcels have been scanned' button";
    //        return false;
    //    }
        Driver.pressKeyCode(AndroidKeyCode.BACK);
        narrator.stepPassed("Successfully scanned a parcel and checked the parcel in.");
        return true;
    }
    public boolean Storage() 
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetLinearLayoutWithBounds("[270,252][432,380]")))
        {
            error = "Failed to wait for the storage tile.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetLinearLayoutWithBounds("[270,252][432,380]")))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }
      if (!AppiumDriverInstance.BarcodeScan())
        {
            return false;
        }
      if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.linearLayoutWithLinearLayoutWithTextView("Heavy Parcel")))
        {
            error = "Failed to touch 'Heavy Parcel'.";
            return false;
        }
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.linearLayoutWithTextView("Next step")))
//        {
//            error = "Failed to touch 'Next Step' .";
//            return false;
//        }
//
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("No - next step")))
//        {
//            error = "Failed to touch 'No - next step'.";
//            return false;
//        }
//            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.linearLayoutWithTextView("Scan the shelf location barcode")))
//        {
//            error = "Failed to touch 'menu'.";
//            return false;
//        }
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu("Menu")))
//        {
//            error = "Failed to touch 'menu'.";
//            return false;
//        }
//          if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.linearLayoutWithTextView("Logout / Switch User")))
//        {
//            error = "Failed to wait for the  'logout/switch user' button .";
//            return false;
//        }
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.linearLayoutWithTextView("Logout / Switch User")))
//        {
//            error = "Failed to touch the 'logout/switch user' button.";
//            return false;
//        }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetButtonWithText("Yes")))
        {
            error = "Failed to touch the 'yes' button.";
            return false;
        }
            //validate user is logged out
           if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.widgetWithTextView("LOGIN")))
        {
            error = "Failed to wait for the login button.";
            return false;
        }
        

        return true;
    }
    //===================================================Custom Methods==============================================

    public boolean waitForElementNotVisible(String xpath, int maxWaitCount)
    {
        int timeCounter = 0;
        boolean itemPresent = AppiumDriverInstance.waitForElementByXpath(xpath);
        while (itemPresent)
        {
            if (timeCounter > maxWaitCount)
            {
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.widgetWithTextView("Loading, please wait...")))
            {
                error = "Failed to wait for the progress bar to dissapear";
                return false;
            }
            //pause
            AppiumDriverInstance.pause(10000);

            //click the empty space
            //increment max counter
            itemPresent = AppiumDriverInstance.waitForElementByXpath(xpath);
            timeCounter++;
        }

        return true;
    }
    public boolean test()
    {
  
    return true;
    }

}

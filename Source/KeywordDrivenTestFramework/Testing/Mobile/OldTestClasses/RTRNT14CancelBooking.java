/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.OldTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.System.out;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jmacauley
 */
public class RTRNT14CancelBooking extends BaseClass
{

    String name = "";
    Narrator narrator;
    String error = "";
    String temp = "";
    String regex;
    String [] lines;
    String [] liness;
    String apiResponse;
    String PDFURL;
    String LabelValue;
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    String shelfLabelName;
    String value;
    String itemReturn;

    public RTRNT14CancelBooking(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        if (!Shortcuts.newBearer())
        {
            return narrator.testFailedScreenshot("Failed to generate the barcode " + error);
        }
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 5))
        {
            Login();

            if (!Returns())
            {
                return narrator.testFailed("Failed to cancel the returns process - " + error);
            }
        }
        else
        {
            if (!Returns())
            {
                return narrator.testFailed("Failed to cancel the returns process - " + error);
            }
        }

        return narrator.finalizeTest("Successfully completed canceling the returns process");
    }

    public boolean Login()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        String version = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.appVersionName());

        Narrator.stepPassedWithScreenShot("Doddle Version : " + version);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
        {
            error = "Failed to touch the login button.";
            return false;
        }

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        String store = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());

        if (!store.equals(narrator.getData("StoreSelection")))
        {
            if (!scrollToSubElement(narrator.getData("StoreSelection")))
            {
                error = "Could not find " + narrator.getData("StoreSelection");
                return false;
            }

        }

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleMainText(), 2))
        {

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
            {
                error = "Failed to touch the login button.";
                return false;
            }

            for (int i = 0; i < 5; i++)
            {
                if (!popUpMessage())
                {
                    error = "Failed to handle alert popups";
                    return false;
                }
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
        {
            error = "Failed to touch the username field.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }

        if (!popUpMessage())
        {
            error = "Failed to handle alert popup message";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click the Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.blockingScreenClose(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.blockingScreenClose(), 5))
            {
                error = "Failed to wait for the semi-blocking screen to not be visible";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

        return true;
    }

    public boolean Returns()
    {
        //
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("ReturnRetailer") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'carrier' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        
        try
        {
            SeleniumDriverInstance.startDriver();

            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURL()))
            {
                error = "Failed to navigate to couchbase URL";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.itemCollectionList(), 60))
            {
                itemReturn = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.itemCollectionList());
                value = itemReturn.substring(itemReturn.lastIndexOf(":") + 1).trim();
                System.out.println(value);
            }
            
            if(value.isEmpty())
            {
                error = "Failed to retrieve the itemID from couchbase";
                return false;
            }

            SeleniumDriverInstance.shutDown();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return false;
        }

        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //RETAILER_SELECTED
        if (!Shortcuts.line.contains("RETAILER_SELECTED"))
        {
            error = "Failed to add \"RETAILER_SELECTED\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("RETAILER_SELECTED"))
        {
            error = "Failed to update text: \"RETAILER_SELECTED\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //HANDOVER_FROM_CUSTOMER
        if (!Shortcuts.line.contains("\"status\":\"HANDOVER_FROM_CUSTOMER\""))
        {
            error = "Failed to add \"status\":\"HANDOVER_FROM_CUSTOMER\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("HANDOVER_FROM_CUSTOMER"))
        {
            error = "Failed to update text: \"HANDOVER_FROM_CUSTOMER\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        
        AppiumDriverInstance.pause(1000);

        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //SECURITY_SCAN_PASSED
        if (!Shortcuts.line.contains("\"SECURITY_SCAN_PASSED\""))
        {
            error = "Failed to add \"eventType\":\"SECURITY_SCAN_PASSED\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("SECURITY_SCAN_PASSED"))
        {
            error = "Failed to update text: \"SECURITY_SCAN_PASSED\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("AccountNumber") + "'into the 'Customer Name' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

         Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //ADDED_RETAILER_REFS
        if (!Shortcuts.line.contains("ADDED_RETAILER_REFS"))
        {
            error = "Failed to add \"ADDED_RETAILER_REFS\" event in CouchBase";
            return false;
        }

        if(!Shortcuts.changeAPIResponseColor("ADDED_RETAILER_REFS"))
        {
            error = "Failed to update text: \"ADDED_RETAILER_REFS\" to green color";
            return false;
        }
        
        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        
        
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }
        
        AppiumDriverInstance.pause(2000);

        Shortcuts.doReturnsAPICall(value, Shortcuts.newBearer);
        //ADDED_CUSTOMER_DETAILS
        if (!Shortcuts.line.contains("ADDED_CUSTOMER_DETAILS"))
        {
            error = "Failed to add \"ADDED_CUSTOMER_DETAILS\" event in CouchBase";
            return false;
        }
        
        if(!Shortcuts.changeAPIResponseColor("ADDED_CUSTOMER_DETAILS"))
        {
            error = "Failed to update text: \"ADDED_CUSTOMER_DETAILS\" to green color";
            return false;
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);
        

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerValidation(testData.getData("Email"))))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to wait for the cancel this return button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnButton()))
        {
            error = "Failed to click the cancel this return button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsBookingScreen()))
        {
            error = "Failed to wait for the cancel returns booking screen";
            return false;
        }

        narrator.stepPassedWithScreenShot("Cancel Returns Booking Popup");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
        {
            error = "Failed to check the customer changed mind option";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected customer changed mind option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelBookingButton()))
        {
            error = "Failed to wait for the cancel booking button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
        {
            error = "Failed click the cancel booking button";
            return false;
        }

        return true;
    }

    public boolean Logout()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }
        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully logged out of the app");

        return true;
    }

    public boolean popUpMessage()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public boolean wifiSwitchOff()
    {
        AppiumDriverInstance.wifiSettings();

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiDisconnected()))
        {
            error = "Failed to wait for the wifi to be disconnected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned off Wifi");

        Driver.pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);

        if (!AppiumDriverInstance.waitForElementByXpathWithTimeout(MobileDoddlePageObjects.menuNextGen(), 2))
        {
            error = "Failed wait for the nextGen app to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuNextGen()))
        {
            error = "Failed to click the nextGen app";
            return false;
        }

        return true;
    }

    public boolean wifiSwitchOn()
    {
        AppiumDriverInstance.wifiSettings();

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
        {
            error = "Failed to wait for the wifi to be connected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");

        Driver.pressKeyCode(AndroidKeyCode.KEYCODE_APP_SWITCH);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuNextGen()))
        {
            error = "Failed wait for the nextGen app to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuNextGen()))
        {
            error = "Failed to click the nextGen app";
            return false;
        }

        return true;
    }

    public boolean ScanBarcode(String barcode)
    {
        try
        {

            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            AppiumDriverInstance.pause(1000);
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean ScanContainerShelfLabel()
    {
        try
        {

            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\ShelfLabels\\" + name + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            AppiumDriverInstance.pause(1000);
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            if (!AppiumDriverInstance.waitForElementByID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyID("android:id/button1"))
            {
                error = "Failed to click on the 'OK' Popup button";
                return false;
            }
            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {

            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public String convertPNGToBase64(String imageFilePath)
    {
        String base64ReturnString = "";

        try
        {
            out.println("[INFO] Converting error screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            out.println("[INFO] Converting completed, image ready for embedding.");
        }
        catch (Exception ex)
        {
            out.println("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }

    public boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            for (int i = 0; i < 2; i++)
            {
                if (!multiplePopUpMessages())
                {
                    return false;
                }
            }

            WebElement list = Driver.findElement(By.className("android.widget.ListView"));

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }
}

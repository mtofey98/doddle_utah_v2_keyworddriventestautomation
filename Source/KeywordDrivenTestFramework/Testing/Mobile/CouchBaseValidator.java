/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.EmailUtilInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author lmaarman
 */
public class CouchBaseValidator extends BaseClass
{

    int counter = 1;

    public CouchBaseValidator(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }

    String error = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {
        if (!VerifyURLPageHasLoaded())
        {
            return narrator.testFailed("Failed to navigate to URL tab page");
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to URL tab page");
        if (!Test())
        {
            return narrator.testFailed("Failed to Find Entry");
        }

        return narrator.finalizeTest("Successfully clicked on entry.");
    }

    public boolean VerifyURLPageHasLoaded()
    {

        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURL()))
        {
            error = "Failed to navagate to Couch Base URL";
            return false;
        }
        int count = 0;
        boolean notFound = false;
        narrator.stepPassedWithScreenShot("Successfully navigated to URL.");

        while (notFound && count < 5)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorHeader()))
            {

                error = "Failed to wait for Couch Base Header";
                return false;

            }

        }
        count++;
        narrator.stepPassedWithScreenShot("Successfully waited for couch base page to open.");
        return true;

    }

    public boolean Test()
    {
        String Entry = "";
        String Type = "Return";
        boolean found = false;
        int count = 0;

        switch (Type)
        {
            case "Return":
                Entry = "ITEM_RETURN";
                while (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry)))
                {
                    if (count == 5)
                    {

                        error = "Failed to wait for entry to load.";
                        return false;
                    }
                    count++;
                }

                String EntryRetrieve = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry));
                narrator.stepPassedWithScreenShot("Successfully waited for entry to load.");

                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry)))
                {
                    error = "Failed to click entry";
                    return false;
                }
                narrator.stepPassed("Successfully clicked entry : " + "[" + EntryRetrieve + "]");
                break;

            case "Collection":
                Entry = "ITEM_COLLECTION";
                while (found && count < 5)
                {
                    if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry)))
                    {

                        error = "Failed to wait for entry to load.";
                        return false;

                    }
                    count++;
                }

                EntryRetrieve = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry));
                narrator.stepPassedWithScreenShot("Successfully waited for entry to load.");
                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry)))
                {
                    error = "";
                    return false;
                }
                narrator.stepPassed("Successfully clicked entry : " + "[" + EntryRetrieve + "]");
                break;

            case "Storage":
                Entry = "STORAGE";
                while (found && count < 5)
                {
                    if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry)))
                    {

                        error = "Failed to wait for entry to load.";
                        return false;

                    }
                    count++;
                }

                EntryRetrieve = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry));
                narrator.stepPassedWithScreenShot("Successfully waited for entry to load.");
                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry)))
                {
                    error = "";
                    return false;
                }
                narrator.stepPassed("Successfully clicked entry : " + "[" + EntryRetrieve + "]");
                break;

            case "Carriers":
                Entry = "CARRIERS";
                while (found && count < 5)
                {
                    if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry)))
                    {

                        error = "Failed to wait for entry to load.";
                        return false;

                    }
                    count++;
                }

                EntryRetrieve = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry));
                narrator.stepPassedWithScreenShot("Successfully waited for entry to load.");
                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.CouchBaseValidatorClickFirst(Entry)))
                {
                    error = "";
                    return false;
                }
                narrator.stepPassed("Successfully clicked entry : " + "[" + EntryRetrieve + "]");
                break;

        }

        String ID = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnSTID()).replaceAll("\"staffId\":", "").trim();
        ID = ID.replaceAll("\"", "");

        if (!ID.equals(narrator.getData("StaffId")))
        {
            error = "Failed to validate StaffId :" + narrator.getData("StaffId");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated StaffId : " + ID);

        for (int i = 0; i < 4; i++)
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.CouchBaseValidatorClickdown()))
            {
                error = "Failed to scroll to Lable Value.";
                return false;
            }

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnLableValue()))
        {
            error = "Failed to wait for Lable Value.";
            return false;
        }

        String LableValue = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnLableValue());
        narrator.stepPassedWithScreenShot("Successfully srcolled to Lable Value : " + "[" + LableValue + "]");

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnURL()))
        {
            error = "Failed to wait for Lable Value.";
            return false;
        }

        String URL = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnURL());
        narrator.stepPassedWithScreenShot("Successfully srcolled to URL : " + "[" + URL + "]");

        String EntryName = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.CouchBaseValidatorURLRawDocRetrieve());
        if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.CouchBaseValidatorClickRawDoc()))
        {
            error = "Failed to click on Raw Dcument.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorClickRawDocWait()))
        {
            error = "Failed to wait for Raw Document page.";
            return false;
        }
        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURLRawDoc(EntryName)))
        {
            error = "Failed to navigate to Raw Document.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnStatus()))
        {
            error = "Failed to scroll to Lable Value.";
            return false;
        }

        narrator.stepPassed("Successfully found Status.");
        return true;
    }
}

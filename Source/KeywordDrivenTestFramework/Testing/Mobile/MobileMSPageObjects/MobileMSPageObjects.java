/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.MobileMSPageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author jmacauley
 */
public class MobileMSPageObjects extends BaseClass
{
    public static String mainLoginButton()
    {
        return "//*[@resource-id='com.doddle.whitelabel.mock:id/ll_login_button']";
    }
    
    public static String versionText()
    {
        return "//*[@resource-id='com.doddle.whitelabel.mock:id/ll_app_version_name']";
    }
    
    public static String storeSpinner()
    {
        return "//android.widget.Spinner//android.widget.TextView";
    }
    
     public static String loginUsername()
    {
        return "//*[@resource-id='com.doddle.concession:id/email']";
    }

    public static String loginPassword()
    {
        return "//*[@resource-id='com.doddle.concession:id/password']";
    }

    public static String loginButton()
    {
        return "//android.widget.Button[@text='LOGIN']";
    }
    
    public static String blockingScreenClose()
    {
        return "//*[@resource-id='com.doddle.concession:id/blocking_screen_close']";
    }
    
    public static String returnsTile()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Returns']";
    }

    public static String enterOrderNo()
    {
        return "//*[@resource-id='com.doddle.whitelabel.mock:id/return_references_label']";
    }
    
    public static String nextButton()
    {
        return "//*[@resource-id='com.doddle.whitelabel.mock:id/return_references_next_button']";
    }
    
    public static String referencesConfirmButton()
    {
        return "";
    }
    
    public static String returnsDoneButton()
    {
        return "//*[@resource-id='com.doddle.whitelabel.mock:id/ll_return_affixing_done']";
    }
    
    //=============================================================================================
    
    public static String randomNo()
    {
        int pass = (int) Math.round(Math.random() * 99999999);
        NumberFormat formatter = new DecimalFormat("00000000");
        String randomNo = formatter.format(pass);
        return randomNo;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.EmailUtilInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author lmaarman
 */
public class CouchBaseValidator_2 extends BaseClass
{

    int counter = 1;

    public CouchBaseValidator_2(TestEntity testData)
    {
        narrator = new Narrator(testData);
        this.testData = testData;

    }
    String error = "";
    public static String doddleID = "";

    public TestResult executeTest()
    {
        if (!VerifyURLPageHasLoaded())
        {
            return narrator.testFailed("Failed to navigate to URL tab page");
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to URL tab page");
        if (!Test())
        {
            return narrator.testFailed("Failed to Find Entry");
        }

        return narrator.finalizeTest("Successfully Validated Information.");
    }

    public boolean VerifyURLPageHasLoaded()
    {

        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidator_2URL()))
        {
            error = "Failed to navagate to Couch Base URL";
            return false;
        }
        int count = 0;
        boolean notFound = false;
        narrator.stepPassedWithScreenShot("Successfully navigated to URL.");

        while (notFound && count < 5)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorURLRawDocRetrieve()))
            {

                error = "Failed to wait for Couch Base Header";
                return false;

            }

        }
        count++;
        narrator.stepPassedWithScreenShot("Successfully waited for couch base page to open.");
        return true;

    }

    public boolean Test()
    {
        boolean isFound = false;
        String value1 = "RETAILER_SELECTED";
        String value2 = "456DVT";
        String value3 = "HANDOVER_FROM_CUSTOMER";
        String value4 = "dvt_automation";
        String value5 = "SECURITY_SCAN_PASSED";
        String value6 = "ADDED_RETAILER_REFS";
        String value7 = "ADDED_RETURN_ITEMISATION";
        String value8 = "ADDED_CUSTOMER_DETAILS";
        String value9 = "RETURN_ROUTING_DETERMINED";
        String value10 = "ADDED_RETURN_LABEL";
        String value11 = "CHECKOUT_COMPLETED";
        String value12 = "AT_COLLECTION_POINT";

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorURLRawDocRetrieve()))
        {
            error = "Failed to wait for Raw Document page.";
            return false;
        }
        if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidator_2URLRawDoc()))
        {
            error = "Failed to navigate to Raw Document.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnRetailerSelected(value1)))
        {
            error = "Failed to Validate that Retailer Selected is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found RETAILER_SELECTED : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturn456DVT(value2)))
        {
            error = "Failed to Validate that HANDOVER_FROM_CUSTOMER is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found dvt_automation : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnHANDOVER_FROM_CUSTOMER(value3)))
        {
            error = "Failed to Validate that HANDOVER_FROM_CUSTOMER is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found HANDOVER_FROM_CUSTOMER : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturndvt_automation(value4)))
        {
            error = "Failed to Validate that dvt_automation is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found dvt_automation : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnSECURITY_SCAN_PASSED(value5)))
        {
            error = "Failed to Validate that SECURITY_SCAN_PASSED is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found SECURITY_SCAN_PASSED : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnADDED_RETAILER_REFS(value6)))
        {
            error = "Failed to Validate that ADDED_RETAILER_REFS is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found ADDED_RETAILER_REFS : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnADDED_RETURN_ITEMISATION(value7)))
        {
            error = "Failed to Validate that ADDED_RETURN_ITEMISATION is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found ADDED_RETURN_ITEMISATION : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnADDED_CUSTOMER_DETAILS(value8)))
        {
            error = "Failed to Validate that ADDED_CUSTOMER_DETAILS is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found ADDED_CUSTOMER_DETAILS : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnRETURN_ROUTING_DETERMINED(value9)))
        {
            error = "Failed to Validate that RETURN_ROUTING_DETERMINED is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found RETURN_ROUTING_DETERMINED : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnADDED_RETURN_LABEL(value10)))
        {
            error = "Failed to Validate that ADDED_RETURN_LABEL is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found ADDED_RETURN_LABEL : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnCHECKOUT_COMPLETED(value11)))
        {
            error = "Failed to Validate that CHECKOUT_COMPLETED is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found CHECKOUT_COMPLETED : " + isFound);

        if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.CouchBaseValidatorReturnAT_COLLECTION_POINT(value12)))
        {
            error = "Failed to Validate that AT_COLLECTION_POINT is present.";
            return false;
        }
        isFound = true;
        narrator.stepPassed("Successfully found AT_COLLECTION_POINT : " + isFound);
        return true;

    }
}

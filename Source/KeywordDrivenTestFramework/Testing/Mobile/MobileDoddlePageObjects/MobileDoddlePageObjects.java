/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.Shortcuts;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.UUID;
import nl.flotsam.xeger.Xeger;

/**
 *
 * @author gdean
 */
public class MobileDoddlePageObjects extends BaseClass
{

    public static String DoddleURL()
    {
        // Use ENUM
        return currentEnvironment.PageUrl;
    }

    public static String SMSUrl()
    {

        return "http://doddletestphone.appspot.com/checkmessages?since=14762";
    }

    //==============================================HTTP Coonection values===================================================
    public static String endPointURL()
    {
        return "https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE";
    }

    public static String returnsStoragePopupAlert()
    {
        return "//*[@resource-id='android:id/message']";
    }
    
    public static String header()
    {
        return "Authorization";
    }

    public static String makeUpANewDispatchSackLayout()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_create_despatch_box_description_large']";
    }

    public static String noContainersText()
    {
        return "//*[@resource-id='com.doddle.concession:id/no_containers_text']";
    }
    
    public static String menuDashboardButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/dashboardLayout']";
    }

    public static String editButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/returns_items_list_next_btn']";
    }
    public static String pickParcel()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_pick_parcels_from_storage_button']";
    }
    
    public static String itemContainerSelectButton(int index)
    {
        return "//android.widget.RelativeLayout[@index='" + index + "']//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.TextView[@text='Select']";
    }

    public static String containerItemHolder()
    {
        return "//*[@resource-id='com.doddle.concession:id/containerTopItemHolder']";
    }

    public static String startValue()
    {
        return "Basic NlYwSjU5TTFWSU45NEkwN0EzRDhVU0FWNjp6cWw1V3pSOE5CZU0yT1RjZWdrRWJhUE5GQWFUK0F5S3RzWmlCWmplcjVz";
    }

    public static String sealTheDispatchSackText()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_box_is_full_description_screen_title']";
    }

    public static String dispatchSackBarcode()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_box_is_full_description_container_value']";
    }

    public static String badConnectionTitle()
    {
        return "//*[@resource-id='com.doddle.concession:id/bad_connection_title']";
    }

    public static String itemsBeingReturnedThreeDotsButton()
    {
        return "//android.widget.FrameLayout[@index='3']//android.widget.ImageView[@index='1']";
    }

    public static String tryAgainButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/btn_try_again']";
    }

    public static String labelValue()
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[contains(@text, 'Please scan and use label:')]";
    }

    public static String contentType()
    {
        return "Content-Type";
    }
   
    public static String contentValue()
    {
        return "application/x-www-form-urlencoded";
    }

    public static String noParcelsText()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_collection_parcels_of_customer_no_parcels']";
    }

    public static String anyMoreReturnsMessage()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Any more returns?']";
    }

    public static String anyMoreCollectionCodesMessage()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Any more collection codes?']";
    }

    public static String dispatchParcelCancelButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/dispatch_process_cancel']";
    }
    
    public static String dispatchParcelCancelButtonTwo()
    {
        return "//*[@resource-id='com.doddle.concession:id/dispatch_parcel_cancel']";
    }

    public static String customerThreeDotsButton()
    {
        return "//android.widget.RelativeLayout[@index='4']//android.widget.ImageView[@index='2']";
    }

    public static String dialogueLeftOption()
    {
        return "//*[@resource-id='android:id/button2']";
    }

    public static String dialogueRightOption()
    {
        return "//*[@resource-id='android:id/button1']";
    }

    public static String rescanButton()
    {
        return "//*[@text='Rescan']";
    }

    public static String securityScanFailed()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_return_security_scan_failed']";
    }

    public static String noMatchesFoundText()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_no_matches_found']";
    }

    public static String itemFailedScan()
    {
        return "//android.widget.RelativeLayout//android.widget.TextView";
    }

    public static String wifiBtnWithIndex()
    {
        //return "//android.widget.ScrollView//android.widget.FrameLayout[@index='3']";
        return "//android.widget.RelativeLayout[@index='0']//android.widget.TextView[@text='DVT-Guest']";
    }
     public static String wifiBtnWithIndex3(String wifiName)
    {
        //return "//android.widget.ScrollView//android.widget.FrameLayout[@index='3']";
        return "//android.widget.RelativeLayout[@index='0']//android.widget.TextView[@text='" + wifiName + "']";
    }
    public static String wifiBtnWithIndex2()
    {
        return "//android.widget.RelativeLayout[@index='0']//android.widget.TextView[@text='Wi-Fi Off']";
    }

    public static String returnReferencesHelpIcon(int index)
    {
        return "//android.widget.RelativeLayout[@index='" + index + "']//android.widget.TextView[@text='?']";
    }

    public static String doddleWiFiBtnWithIndex()
    {
        return "//*[contains(@content-desc,'Wifi')]";
    }

    public static String connectionErrorOkBtn()
    {
        return "//*[@resource-id='android:id/button1']";
    }

    public static String retailerLogo()
    {
        return "//*[@resource-id='com.doddle.concession:id/return_references_retailer_icon']";
    }

    public static String loginBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/email_sign_in_button']";
    }

    public static String storeCheckBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/btn_stock_check']";
    }

    public static String DataArtNameTextField(String retailer)
    {
        return "//android.widget.LinearLayout//android.widget.EditText[@text='" + retailer + "']";
    }

    public static String expectedText()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_collection_parcels_of_customer_rv_item_top_info_shelfID']";
    }

    public static String wifiStatus()
    {
        return "//*[@resource-id='android:id/summary']";

    }

    public static String toolbarBackText()
    {
        return "//*[@resource-id='com.doddle.concession:id/toolbar_back']";
    }

    public static String syncingText()
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@resource-id='com.doddle.concession:id/tv_sync_indicator']";
    }

    public static String forceStartSyncBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/force_start_sync']";
    }

    public static String returnsNowButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/now_btn']";
    }

    public static String returnsLaterButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/later_btn']";
    }

    public static String failedToConnectText()
    {
        return "//android.widget.ScrollView//android.widget.TextView[@text='Could not connect to device: failed to connect to /10.0.3.118 (port 9100) after 15000ms: isConnected failed: EHOSTUNREACH (No route to host)']";
    }

    public static String dispatchProcessCancelButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/dispatch_process_cancel']";
    }

    public static String boxIsFullCancelButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/btn_box_is_full_cancel']";
    }

    public static String returnsRetryPrintoutButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/btn_retry_printout']";
    }

    public static String returnsDoneButton()
    {
        return "//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.TextView[@text='Done']";
    }

    public static String returnsDoneButton2()
    {
        return "//*[@resource-id='com.doddle.concession:id/dispatch_box_ready_process_done']";
    }

    public static String returnsFinishDispatchDoneButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/finish_dispatch_return_done']";
    }

    public static String returnsBoxIsFull()
    {
        return "//*[@resource-id='com.doddle.concession:id/dispatch_parcel_box_is_full']";
    }

    public static String boxIsFullBarcode()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_box_is_full_description_container_value']";
    }

    public static String dashboardHelpDeskIcon()
    {
        return "//*[@resource-id='com.doddle.concession:id/zendeskLayout']";
    }

    public static String returnsItemDeleteButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/returns_item_delete_button']";
    }

    public static String returnsBarcodeText1()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_finish_despatch_return_description_return_code_value_text']";
    }

    public static String returnsBarcodeText2()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_finish_despatch_return_description_container_code_value_text']";
    }

    public static String returnsItemTooLarge()
    {
        return "//*[@resource-id='com.doddle.concession:id/dispatch_parcel_item_too_large']";
    }

    public static String returnsItemTooLargeDescription1()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_item_too_large_description_large']";
    }

    public static String returnsItemTooLargeDescription2()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_item_too_large_description_small']";
    }

    public static String returnsItemTooLargeCloseButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/btn_item_too_large_close']";
    }

    public static String onScreenBarcode()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_box_is_full_description_container_value']";
    }

    public static String tasksNotificationBell()
    {
        return "//*[@resource-id='com.doddle.concession:id/notification_bell']";
    }

    public static String completeTaskButtonLayout()
    {
        return "//android.support.v7.widget.RecyclerView//android.widget.RelativeLayout[@index='0']";
    }

    public static String alreadyOneOpenContainer()
    {
        return "//*[@resource-id='android:id/message']";
    }

    public static String dispatchErrorSack()
    {
        return "//*[@resource-id='android:id/message']";
    }

    public static String completeTaskButton()
    {
        return "//android.support.v7.widget.RecyclerView//android.widget.RelativeLayout[@index='0']//android.widget.Button[@text='Complete Task']";
    }

    public static String retakeCollectionPhotoLayout()
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@text='Retake Collection Photo']";
    }

    public static String loadingView()
    {
        return "//*[@resource-id='com.doddle.concession:id/retrieve_loading_view']";
    }

    public static String retakePhotoItemBarcode()
    {
        return "//*[@resource-id='com.doddle.concession:id/itemBarcode']";
    }

    public static String returnedParcel()
    {
        return "//android.support.v7.widget.RecyclerView//android.widget.RelativeLayout[@index='0']//android.widget.TextView";
    }

    public static String closeTaskButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/tasks_close_btn']";
    }

    public static String putReturnInContainerText()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_despatch_parcel_description_large']";
    }

    public static String notificationCount()
    {
        return "//*[@resource-id='com.doddle.concession:id/notification_count']";
    }

    public static String headerValue()
    {
        return "Basic NDBBR1YzNERKVU8xNTNaNURESkFKMjNQRTpsOE1mMmFRMzlCRHhQZHhqNW00RTliZ3oxQkZXMGRFeWV0UmlWdVZsajJV";
    }

    public static String appVersionName()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_app_version_name']";
    }

    public static String versionText()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_app_version_name']";
    }

    public static String releaseNotesText()
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@text='Release Notes']";
    }

    public static String whatsNewCloseBtn()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Close']";
    }

    public static String storeList()
    {
        return "android:id/text1";
    }

    public static String alertOkPopup()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='ok']";
    }

    public static String alertCancelPopup()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='Cancel']";
    }

    public static String incorrectStore()
    {
        return "//android.widget.ScrollView//android.widget.TextView[@text=' User \"nextgen.one\" has permission for stores: 008SOU, 020RMF, 023FPK, 028PAD, 029RMD, 030HHE, 033TOG, 043LSB, 906SCU']";
    }

    public static String moreReturnsNoButton()
    {
        return "//*[@resource-id='android:id/button2']";
    }

    public static String blockingScreenClose()
    {
        return "//*[@resource-id='com.doddle.concession:id/blocking_screen_close']";
    }

    public static String newLookEditButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/return_references_next_button']";
    }

    public static String newLookAccountNumberTextField()
    {
        return "//*[@resource-id='com.doddle.concession:id/return_references_label']";
    }

    public static String scanFailedDashboardButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_return_security_scan_failed_next']";
    }

    //=======================================================================================================================
    public static String goBackButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/backButton']";
    }

    public static String moreReturnsYesButton()
    {
        return "//*[@resource-id='android:id/button1']";
    }

    public static String newLookThreeDotsButton()
    {
        return "//android.support.v7.widget.RecyclerView//android.widget.LinearLayout[@index='0']//android.widget.ImageView[@index='2']";
    }

    public static String loginButtonSplash()
    {
        return "//android.widget.TextView[@text='LOGIN']";
    }

    public static String returnLabelDidntPrintButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_return_label_didnt_print']";
    }

    public static String loginUsername()
    {
        return "//*[@resource-id='com.doddle.concession:id/email']";
    }

    public static String loginPassword()
    {
        return "//*[@resource-id='com.doddle.concession:id/password']";
    }

    public static String loginButton()
    {
        return "//android.widget.Button[@text='LOGIN']";
    }

    public static String checkInButton()
    {
        return "//android.widget.LinearLayout[@index='1']//android.widget.TextView[@index='0']";
    }

    public static String storeDropdownList()
    {
        return "//android.widget.Spinner";
    }

    public static String progressCircle()
    {
        return "//android.widget.RelativeLayout//android.widget.RelativeLayout//android.widget.ProgressBar";
    }

    public static String progressBar()
    {
        return "//*[@resource-id='android.widget.ProgressBar']";
    }

    public static String progressCircleXpath()
    {
        return "//*[@resource-id='android:id/progress']";
    }

    public static String confirmThisCustomerScreen()
    {
        return "//android.widget.TextView[contains(@text, 'Confirm')]";
    }

    public static String despatchBoxDescription()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_create_despatch_box_description_large']";
    }

    public static String syncingTextView()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_sync_indicator']";
    }

    public static String scanATempLabelText()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Scan a temporary label']";
    }

    public static String moveToCarrierContainerButton()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Move to Carrier Container']";
    }

    public static String doddleMainText()
    {
        return "//android.widget.RelativeLayout//android.widget.ImageView";
    }

    public static String connectionErrorPopup()
    {
        return "//android.widget.LinearLayout//android.widget.ScrollView//android.widget.TextView[@text='Connection error']";
    }

    public static String messagePopup()
    {
        return "//*[@resource-id='android:id/message']";
    }

    public static String allocateNewContainrForCarrierButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Allocate new container for Carrier']";
    }

    public static String scanContainerForDispatch()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Scan containers for dispatch']";
    }

    public static String returnTile()
    {
        return "//*[@resource-id='com.doddle.concession:id/parcelReturnsLayout']";
    }

    public static String newReturnButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='New Return']";
    }

    public static String wifiConnected()
    {
        return "//android.widget.LinearLayout[@index='0']//android.widget.RelativeLayout[@index='1']//android.widget.TextView[@text='Connected']";
    }

    public static String forceStartSync()
    {
        return "//*[@resource-id='com.doddle.concession:id/force_start_sync']";
    }

    public static String retailNameTextField()
    {
        return "//android.widget.LinearLayout//android.widget.EditText[@text='Enter retailers name']";
    }

    public static String enterCarrierOrderID()
    {
        return "//android.widget.RelativeLayout//android.widget.EditText[@text='Enter House of Fraser Order ID']";
    }

    public static String spinnerOptionSelection(String text)
    {
        return "//android.widget.ListView//android.widget.CheckedTextView[@text='" + text + "']";
    }

    public static String finsburyParkStore()
    {
        return "//android.widget.Spinner//android.widget.TextView[@text='023FPK - Finsbury Park']";
    }

    public static String storeSpinner()
    {
        return "//android.widget.Spinner//android.widget.TextView";
    }

    public static String storeSpinnerText()
    {
        return "//*[@resource-id='android:id/text1']";
    }

    public static String wifiSwitch()
    {
        return "//android.widget.Switch";
    }

    public static String wifiOffSwitch()
    {
        return "//android.widget.FrameLayout//android.widget.RelativeLayout//android.widget.TextView[@index='3']";
    }

    public static String quickSettingsBtn()
    {
        return "//*[@resource-id='com.android.systemui:id/settings_button']";
    }

    public static String wifibtn()
    {
        return "//android.widget.FrameLayout[contains(@content-desc, 'Wi-Fi')]";
    }

    public static String menuNextGen()
    {
        return "//android.widget.FrameLayout[@content-desc='NextGen']";
    }

    public static String wifiDisconnected()
    {
        return "//android.widget.FrameLayout//android.view.View//android.widget.Switch[@text='OFF']";
    }

    public static String spinnerButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/login_store_spinner']";
    }

    public static String storeName()
    {
        return "//android.widget.ListView//android.widget.CheckedTextView[@text='023FPK - Finsbury Park']";
    }

    public static String scanPassedButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Scan passed']";
    }

    public static String doddleEmailTextField()
    {
        return "//*[@resource-id='com.doddle.concession:id/searchText']";
    }

    public static String customerNameTextfield()
    {
        return "//android.widget.RelativeLayout//android.widget.EditText[@text='Enter CUSTOMER NAME']";
    }

    public static String customerNewLookAccNumber()
    {
        return "//*[@resource-id='com.doddle.concession:id/return_references_label']";
    }

    public static String rmaNumberTextfield()
    {
        return "//android.widget.RelativeLayout//android.widget.EditText[@text='Enter RMA NUMBER']";
    }

    public static String missguidedOrderID()
    {
        return "//android.widget.RelativeLayout//android.widget.EditText[@text='Enter Order ID']";
    }

    public static String rmaNumberField()
    {
        return "//android.widget.RelativeLayout[@index='1']//*[@resource-id='com.doddle.concession:id/return_references_label']";
    }

    public static String missguidedRMANumber()
    {
        return "//*[@resource-id='com.doddle.concession:id/return_references_label']";
    }

    public static String validationGeneratingLabelText()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Generating the label']";
    }

    public static String selectPrinterOptions(String text)
    {
        return "//android.widget.RadioGroup//android.widget.RadioButton[@text='" + text + "']";
    }

    public static String selectPrinterButton()
    {
        return "com.doddle.concession:id/ll_return_select_printer";
    }

    public static String useATempLabelButton()
    {
        return "com.doddle.concession:id/tv_use_temporary_label";
    }

    public static String affixLabelToParcelDoneButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Done']";
    }

    public static String returnsBackToDashboard()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Back to Dashboard']";
    }

    public static String returnsDispatchDoneButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/finish_dispatch_return_done']";
    }

    public static String returnSingleContainerBarcode()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_despatch_parcel_description_small']";
    }

    public static String containersForDispatchText()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Scan containers for despatch']";
    }

    public static String parcelReturnedValadation()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Outstanding Work!!']";
    }

    public static String customerValidation(String text)
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@text='" + text + "']";
    }

    public static String contactSearchButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/searchBtn']";
    }

    public static String confirmAndPrintLabelButton()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Confirm and Print Label']";
    }

    public static String orderIDNextButton()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Next']";
    }

    public static String ASOSOrderIDField()
    {
        return "//android.widget.RelativeLayout//android.widget.EditText[@text='Enter ASOS Order ID']";
    }

    public static String HouseOFFraserOrderIDField()
    {
        return "//android.widget.RelativeLayout//android.widget.EditText[@text='Enter House of Fraser Order ID']";
    }

    public static String FarFetchOrderIDField()
    {
        return "//android.widget.RelativeLayout//android.widget.EditText[@text='Enter FarFetch Order ID']";
    }

    public static String itemNameTextfield()
    {
        return "com.doddle.concession:id/returns_items_label_value";
    }

    public static String itemReturnReasonDropdown()
    {
        return "//*[@resource-id='com.doddle.concession:id/returns_items_reason_spinner']";
    }

    public static String barcodeVal()
    {
        return "//*[@resource-id='com.doddle.concession:id/collection_item_upi']";
    }

    public static String itemReturnReasonDropdownList(String text)
    {
        return "//android.widget.ListView//android.widget.TextView[@text='" + text + "'] ";
    }

    public static String returnsAddNewItemBtn()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Add New Item']";
    }

    public static String returnItemCode()
    {
        return "//*[@resource-id='com.doddle.concession:id/returns_items_label_value']";
    }

    public static String returnsItemIncrease()
    {
        return "//*[@resource-id='com.doddle.concession:id/returns_items_increase']";
    }

    public static String returnsItemDecrease()
    {
        return "//*[@resource-id='com.doddle.concession:id/returns_items_decrease']";
    }

    public static String errorAlertTitle()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Error']";
    }

    public static String errorAlertOkButton()
    {
        return "//*[@resource-id='android:id/button1']";
    }

    public static String cancelReturnsBookingScreen()
    {
        return "//*[@resource-id='com.doddle.concession:id/cancelProcessTitle']";
    }

    public static String newWifiButton()
    {
        return "//*[@resource-id='com.android.systemui:id/activity_in']";
    }
    
    public static String wifiOffText()
    {
        return "//android.widget.FrameLayout[@index='0']//android.widget.FrameLayout[@index='3']//android.widget.RelativeLayout//*[@resource-id='com.android.systemui:id/text']";
    }

    public static String cancelBookingButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/cancelProcessButton']";
    }

    public static String settingsButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/settingsLayout']";
    }

    public static String prebookedReturn()
    {
        return "//*[@resource-id='com.doddle.concession:id/type_prebooked_btn']";
    }

    public static String preBookedReturnText()
    {
        return "//*[@resource-id='com.doddle.concession:id/textLookupBooked']";
    }

    public static String lookupBookedReturn()
    {
        return "//*[@resource-id='com.doddle.concession:id/textLookupBooked']";
    }

    public static String customerChangedMindOption()
    {
        return "//android.widget.RadioGroup//android.widget.RadioButton[@text='Customer Changed Mind']";
    }

    public static String cancelProcessTitle()
    {
        return "//*[@resource-id='com.doddle.concession:id/cancelProcessTitle']";
    }

    public static String itemReturnNextButton()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Next'] ";
    }

    public static String itemBarcode()
    {
        return "//*[@resource-id='com.doddle.concession:id/itemBarcode']";
    }

    public static String collectionStatus()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='ON SHELF']";
    }

    public static String startColletionButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/start_collection_button']";
    }

    public static String okPopupMessage()
    {
        return "//*[@resource-id='android:id/button1']";
    }

    public static String retailerSelection(String text)
    {
        return "//android.widget.TextView[@text='" + text + "']/..";
    }

    public static String doddleHomescreen(String index)
    {
        return "//android.widget.LinearLayout[@index='" + index + "']//android.widget.TextView[@index='0']";
    }

//    public static String storeSelection(String text)
//    {
//        return "//android.widget.ListView//android.widget.CheckedTextView[@text='" + text + "']";
//    }
    public static String storeValidation(String text)
    {
        return "//android.widget.Spinner//android.widget.TextView[@text='" + text + "']";
    }

    public static String checkIn()
    {
        return "//android.support.v7.widget.RecyclerView[@index='1']//android.widget.RelativeLayout[@index='1']//android.widget.ImageView[@index='0']";
    }

    public static String checkInYes()
    {
        return "//android.widget.Button[@text='Yes']";
    }

    public static String makeATestPrintout()
    {
        return "//android.widget.LinearLayout//android.widget.RelativeLayout//android.widget.TextView[@text='Make a test printout']";
    }

    public static String SettingsPrinterOption()
    {
        return "//android.widget.RadioButton[@text='DVT Printer']";
    }

    public static String settingsSelectDvtPrinter()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_receipt_select_printer']";
    }

    public static String checkInWait()
    {
        return "//android.widget.TextView[@text='Scan the first parcel']";
    }

    public static String storageWait()
    {
        return "//android.widget.TextView[@text='Scan the first Red-lined parcel']";
    }

    public static String checkInAllParcel()
    {
        return "//android.widget.Button[@text='All parcels have been scanned']";
    }

    public static String cancelRecieptPrintout()
    {
        return "//*[@resource-id='com.doddle.concession:id/btn_cancel_receipt_printout']";
    }

    public static String checkInStore()
    {
        return "//android.widget.Button[@text='']";
    }

    public static String checkInNoStore()
    {
        return "//android.widget.Button[@text='Store parcels later']";
    }

    public static String storeHeavy()
    {
        return "//android.widget.TextView[@text='Heavy Parcel']";
    }

    public static String storeLarge()
    {
        return "//android.widget.TextView[@text='Large Parcel']";
    }

    public static String storeNext()
    {
        return "//android.widget.TextView[@text='Next step']";
    }

    public static String storePhoto()
    {
        return "//android.widget.TextView[@text='Take Photo']";
    }

    public static String storeYesPhoto()
    {
        return "//android.widget.Button[@text='Yes - take a new photo']";
    }

    public static String storeUse()
    {
        return "//android.widget.Button[@text='Use']";
    }

    public static String storeParcel()
    {
        return "//android.widget.TextView[@text='Store the next parcel']";
    }

    public static String storePause()
    {
        return "//android.widget.TextView[@text='Pause storage']";
    }

    public static String storeBack()
    {
        return "//android.widget.TextView[@text='Back to Dashboard']";
    }

    public static String collectionDoddleID()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='Doddle ID']";
    }

    public static String collectionEnterID()
    {
        return "//*[@resource-id='com.doddle.concession:id/fillInfoText']";
    }

    public static String mainLoginButton()
    {
        return "//android.widget.TextView[@text='LOGIN']";
    }

    public static String okPopupMessageButton()
    {
        return "//android.widget.Button[@text='Ok']";
    }

    public static String okMessageButton()
    {
        return "//android.widget.Button[@text='ok']";
    }

    public static String carrierTile()
    {
        return "//android.widget.TextView[@text='Collection']";
    }

    public static String carriersTile()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Carriers']";
    }

    public static String collectionTile()
    {
        return "com.doddle.concession:id/parcelCollectionLayout";
    }

    public static String returnsTile()
    {
        return "com.doddle.concession:id/parcelReturnsLayout";
    }

    public static String enterCarrierTextfield()
    {
        return "//android.widget.EditText[@text='Enter Carrier name']";
    }

    public static String takeParcelFromCarrierButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/takeParcelsButton']";
    }

    public static String yesButton()
    {
        return "//android.widget.Button[@text='Yes']";
    }

    public static String yesCheckoutButton()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Yes - CHECKOUT']";
    }

    public static String allParcelsHaveBeenScannedButton()
    {
        return "//android.widget.Button[@text='All parcels have been scanned']";
    }

    public static String storeParcelNowButton()
    {
        return "//android.widget.Button[@text='Store parcels now']";
    }

    public static String backArrowButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/doddle_back_button']";
    }

    public static String widgetTextViewToImageView(String text)
    {
        return "//android.widget.TextView[@text='" + text + "']//..//android.widget.ImageView";
    }

    public static String carrierDHL()
    {
        return "//*[@resource-id='com.doddle.concession:id/carrier_image']";
    }

    public static String selectCarrier(String text)
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@text='" + text + "']/..";
    }

    public static String dispatchParcelToCarrierButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Dispatch parcels to this Carrier']";
    }

    public static String returnsDespatchParcel()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Despatch parcels to this Carrier']";
    }

    public static String confirmDispatchofCage()
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@text='Confirm dispatch of']";
    }

    public static String returnsDespatchOfCage()
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@text='Confirm despatch of']";
    }

    public static String numberOfParcelsLabel()
    {
        return "//*[@resource-id = 'com.doddle.concession:id/rt_total_count']";
    }

    public static String tickHeavyParcelButton()
    {
        return "//android.widget.LinearLayout//android.widget.LinearLayout//android.widget.TextView[@text='Heavy Parcel']";
    }

    public static String takePhotoButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Take Photo']";
    }

    public static String retakePhotoButton()
    {
        return "//android.widget.Button[@text='Retake']";
    }

    public static String useThisPhotoButton()
    {
        return "//android.widget.Button[@text='Use']";
    }

    public static String appleContactInfoTextfield()
    {
        return "com.doddle.concession:id/searchText";
    }

    public static String retailerTileSelection(String text)
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@text='" + text + "']/..";
    }

    public static String retailerSelection()
    {
        return "//*[@resource-id='com.doddle.concession:id/retailer_image']";
    }

    public static String pauseStorageButton()
    {
        return "//android.widget.Button[@text='Pause storage']";
    }

    public static String dashboardBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/positiveBtn']";
    }
    
    public static String cancelReturnsCancelButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='CANCEL']";
    }
    
    public static String cancelReturnsCurrentButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='CURRENT']";
    }

    public static String cancelReturnsAllButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='ALL']";
    }
    
    public static String securityScanFailedBackToDashbaord()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_return_security_scan_failed_next']";
    }

    public static String finishStorageButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/finishStorage']";
    }

    public static String preBookedbackToDashboard()
    {
        return "//*[@resource-id='com.doddle.concession:id/lookupBookedBackToDashboard']";
    }

    public static String totalCountText()
    {
        return "//*[@resource-id='com.doddle.concession:id/rt_total_count']";
    }

    public static String storageBackToDashboardButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Back to Dashboard']";
    }

    public static String mainMenuCollectionTile()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Collection']";
    }

    public static String emailCollectionButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='Email']";
    }

    public static String collectionCodeButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='Collection Code']";
    }

    public static String collectionEnterEmailAddressField()
    {
        return "//android.widget.EditText[@text='Enter an Email Address']";
    }

    public static String collectionEnterCollectionCodeField()
    {
        return "//*[@resource-id='com.doddle.concession:id/fillInfoText']";
    }

    public static String widgetWithTextView(String text)
    {
        return "//android.widget.TextView[@text='" + text + "']";
    }

    public static String moreCollectionCodesYesButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='Yes']";
    }

    public static String moreCollectionCodesNoButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='No']";
    }

    public static String emailLoadingScreen(String text)
    {
        return "//android.widget.TextView[@text='" + text + "']";
    }

    public static String collectionRetrievingParcels()
    {
        return "//android.widget.TextView[@text='Retrieving parcels data...']";
    }

    public static String mainLoginBtn()
    {
        return "//android.widget.TextView[@text='LOGIN']";
    }

    public static String whatIsYourNameValidationText()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='What is your NAME?']";
    }

    public static String correctButton()
    {
        return "//android.widget.Button[@text='Correct']";
    }

    public static String okButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='ok']";

    }

    public static String finishPickingParcelsBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_collection_parcels_of_customer_btn_finish_picking_parcels']";
    }

    public static String carrierOkButton()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='ok']";
    }

    public static String gotItButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Got it']";
    }

    public static String useScannerButtonOK()
    {
        return "//*[@resource='com.doddle.concession:id/ll_collection_parcels_of_customer_parcel_dialog_hint_got_it_button']";
    }

    public static String photoParcelBackButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_parcel_photo_back_button']";
    }

    public static String okHintPopup()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@class='ok']";
    }

    public static String photoOfParcelLabel()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_rv_item_photo_of_parcel_button']";
   
    }

    public static String forgetTodayButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_rv_item_forget_today_button']";
    }

    public static String reinstateButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_rv_item_reinstate_button']";
    }

    public static String lostParcelButton()
    {
        return "//android.widget.LinearLayout[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_rv_item_lost_parcel_button']";
    }

    public static String parcelPriceLabel()
    {
        return "//android.widget.FrameLayout[@index='0']//android.widget.LinearLayout[@index='1']//android.widget.LinearLayout//android.widget.LinearLayout//android.widget.TextView[@text() = 'Price of Parcel']";
    }

    public static String retailerValue()
    {
        return "//android.widget.TextView[@resource-id = 'com.doddle.concession:id/itemRetailer']";
    }

    public static String BarcodeLabel()
    {
        return "//android.widget.TextView[@text() = 'Retailer']";
    }

    public static String pickParcelFromStorageButton()
    {
        return "//android.widget.LinearLayout//android.widget.LinearLayout//android.widget.TextView[@text='Pick parcels from storage']";
    }

    public static String parcelFoundtext()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Parcel Found']";
    }

    public static String issueParcelToCustomerButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Issue parcels to customer']";
    }

    public static String noOneWantsMeYetText()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='No one wants me yet']";
    }

    public static String boomAnotherHappyCustomerText()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Boom! Another happy customer']";
    }

    public static String finishPickingParcelButton()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Finish Picking Parcels']";
    }

    public static String returnToCustomerBtn()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Return to customer']";
    }

    public static String finishCheckoutBtn()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Finish Check Out']";
    }

    public static String yesCheckoutBtn()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Yes - CHECKOUT']";
    }

    public static String noChooseParcelsToCheckout()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='No - Choose parcels to checkout']";
    }

    public static String checkout()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Check out']";
    }

    public static String creditCardOptionButton()
    {
        return "//android.widget.RelativeLayout//android.widget.TextView[@text='Credit Card']";
    }

    public static String passportOption()
    {
        return "//android.widget.RelativeLayout//android.widget.RelativeLayout//android.widget.TextView[@text='Passport']";
    }

    public static String refundsTile()
    {
        return "//*[@resource-id='com.doddle.concession:id/parcelRefundsBtn']";
    }

    public static String refundsEnterInfoField()
    {
        return "//*[@resource-id='com.doddle.concession:id/searchText']";
    }

    public static String postCodeOption(String text)
    {
        return "//android.widget.RelativeLayout//android.widget.RelativeLayout//android.widget.TextView[@text='" + text + "']";
    }

    public static String cardOption(String text)
    {
        return "//android.widget.RelativeLayout//android.widget.RelativeLayout//android.widget.TextView[@text='" + text + "']";
    }

    public static String mainMenu()
    {
        return "//android.widget.RelativeLayout//android.widget.RelativeLayout//android.widget.TextView[@text='Menu']";
    }

    //Added by Manuel Lopes
    public static String footerMenu()
    {
        return "//android.widget.TextView[@text='Menu']";
    }

    public static String procceedToPaymentButton()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Proceed to Payment']";
    }

    public static String yesLetsGoBuutton()
    {
        return "//*[@resource-id='com.doddle.concession:id/id_check_yes_button']";
    }

    public static String completeCollectionButton()
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='Complete Collection']";
    }

    public static String paymentProcessBackToDashboard()
    {
        return "//*[@resource-id='com.doddle.concession:id/payment_process_back_to_dashboard']";
    }

    public static String expectedParcelsBackToDashboard()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_collection_parcels_of_customer_btn_pick_parcels']";
    }

    //Added By Hyran
    public static String checkBox()
    {
        return "//*[@resource-id='android:id/checkbox']";
    }
     public static String numberOfItems()
    {
        return "//*[@resource-id='com.doddle.concession:id/numberOfContainers']";
    }
     public static String containerID()
     {
         return"//*[@resource-id='com.doddle.concession:id/containerId']";
     }
    public static String enterFreeText()
    {
        return "//*[@resource-id='com.doddle.concession:id/doddle_free_text_enter_value']";
    }
    public static String confirmDispatcBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/confirmDispatchBtn']";
    }       
    public static String manageContainersBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/manageContainersButton']";
    }

    public static String allocateBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/allocateButton']";
    }

    public static String removeContainers()
    {
        return "//*[@resource-id='com.doddle.concession:id/removeContainersButton']";
    }

    public static String confirmDeletionOfContainers()
    {
        return "//*[@resource-id='com.doddle.concession:id/confirmDeleteButton']";
    }

    public static String selectContainersDelete()
    {
        return "//*[@resource-id='com.doddle.concession:id/status']";
    }

    public static String confirmDelete()
    {
        return "//*[@resource-id='com.doddle.concession:id/confirmDeleteButton']";
    }

    public static String collectionUPI()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='UPI']";
    }

    public static String collectionEnterUPIField()
    {
        return "//android.widget.EditText[@text='Enter a UPI']";
    }

    public static String otherSearchOptions()
    {
        return "//*[@resource-id='com.doddle.concession:id/otherSearchOptionsBtn']";
    }

    public static String collectionOrderID()
    {
        return "//*[@resource-id='com.doddle.concession:id/otherOptionOrderId']";
    }

    public static String collectionEnterOrderIDField()
    {
        return "//android.widget.EditText[@text='Enter an Order ID']";
    }

//==========================================================Simplefied Xpaths for Android==============================================
    public static String widgetButtonWithText(String text)
    {
        return "//android.widget.Button[@text='" + text + "']";
    }

    public static String widgetButtonWithIndex(String text)
    {
        return "//android.widget.Button[@index='" + text + "']";
    }

    public static String widgetWithEditText(String text)
    {
        return "//android.widget.EditText[@text='" + text + "']";
    }

    public static String widgetEditTextWithIndex(String text)
    {
        return "//android.widget.EditText[@index='" + text + "']";
    }

    public static String widgetTextViewWithIndex(String text)
    {
        return "//android.widget.CheckedTextView[@index='" + text + "']";
    }

    public static String widgetTextViewWithText(String text)
    {
        return "//android.widget.CheckedTextView[@text='" + text + "']";
    }

    public static String widgetImageViewWithIndex(String text)
    {
        return "//android.widget.ImageView[@index='" + text + "']";
    }

    public static String linearLayoutWithLinearLayoutWithTextView(String text)
    {
        return "//android.widget.LinearLayout//android.widget.LinearLayout//android.widget.TextView[@text='" + text + "']";
    }

    public static String signatureNextButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/confirmBtn']";
    }

    public static String expectedParcels()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_collection_parcels_of_customer_rv_item_top_info_shelfID']";
    }

    public static String linearLayoutWithLinearLayoutWithIndex(String text)
    {
        return "//android.widget.LinearLayout//android.widget.LinearLayout[@index='" + text + "']";
    }

    public static String dvtPrinterOption()
    {
        return "//android.widget.RadioGroup//android.widget.RadioButton[@text='DVT Printer']";
    }
    
    public static String jacekPrinterOption()
    {
        return "//android.widget.RadioGroup//android.widget.RadioButton[@text='Jacek Printer']";
    }
    
    public static String printerOption()
    {
        return "//android.widget.RadioGroup//android.widget.RadioButton[@text='" + Shortcuts.getPrinter() + "']";
    }
    
    public static String doddlePrinterOption()
    {
        return "//android.widget.RadioGroup//android.widget.RadioButton[@text='Printer']";
    }

    public static String dvtPrinterOptionBoxIsFull()
    {
        return "//android.widget.RadioGroup//android.widget.RadioButton[@text='DVT printer']";
    }

    public static String confirmPrinterButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_return_select_printer']";
    }

    public static String connectionProgressCircle()
    {
        return "//*[@resource-id='android:id/progress']";
    }

    public static String printerNotConnectedText()
    {
        return "//*[@resource-id='android:id/message']";
    }

    public static String scanPrintedLabelText()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Scan the Printed Label']";
    }

    public static String linearLayoutWithTextviewWithLinearLayout(String text)
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Take parcels from carrier']/..//android.widget.LinearLayout";
    }

    public static String logoutSwitchUserBtn()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Logout / Switch User']";
    }

    public static String widgetTextViewContainsText(String text)
    {
        return "//android.widget.TextView[contains(@text,'" + text + "')]";
    }

    public static String widgetLinearLayoutWithBounds(String text)
    {
        return "//android.widget.LinearLayout[@bounds='" + text + "']";
    }

    public static String storageTile()
    {
        return "//*[@resource-id='com.doddle.concession:id/parcelStorageLayout']";
    }

    public static String widgetLinearLayoutWithResourceID(String text)
    {
        return "//android.widget.Button[contains(@resource-id,'" + text + "')]";
    }

    public static String widgetLinearLayoutWithIndex(String text)
    {
        return "//android.widget.LinearLayout[@index='" + text + "']";
    }

    public static String relativeLayoutWithButton(String text)
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@text='" + text + "']";
    }

    public static String relativeLayoutWithCompleteTaskButton()
    {
        return "//android.widget.RelativeLayout[@index='0']//android.widget.Button[@text='Complete Task']";
    }

    public static String relativeLayoutWithButtonWithIndex(String text)
    {
        return "//android.widget.RelativeLayout//android.widget.Button[@index='" + text + "']";
    }

    public static String frameLayoutWithButton(String text)
    {
        return "//android.widget.FrameLayout//android.widget.Button[@text='" + text + "']";
    }

    public static String relativeLayoutWithRelativeLayoutWithIndex(String text)
    {
        return "android.widget.RelativeLayout//android.widget.RelativeLayout//android.widget.TextView[@index='" + text + "']";
    }

    public static String linearLayoutWithImageView(String text)
    {
        return "//android.widget.LinearLayout//android.widget.ImageView[@index='" + text + "']";
    }

    public static String relativeLayoutWithRelativeLayoutWithTextView(String text)
    {
        return "android.widget.RelativeLayout//android.widget.RelativeLayout//android.widget.TextView[@text='" + text + "']";
    }

    public static String linearlayoutWithView(String text)
    {
        return "//android.widget.LinearLayout//android.view.View[@index='" + text + "']";
    }

    public static String linearlayoutWithButton(String text)
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='" + text + "']";
    }

    public static String logoutPopUp()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='Yes']";
    }

    //=============================================================== Resource ID Main ====================================================================
    public static String checkYesButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/id_check_yes_button']";
    }

    public static String cancelReturnButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_return_summary_cancel_btn']";
    }

    public static String retailerCheckYesButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/id_retailer_check_yes_button']";
    }

    public static String takeParcelsButton()
    {
        return "com.doddle.concession:id/takeParcelsButton";
    }

    public static String whatIsYourNameConformationButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/confirmBtn']";
    }

    public static String refundsSearchScreen()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Refunds search']";
    }

    public static String hintButtonClose()
    {
        return "//*[@resource-id='com.doddle.concession:id/doddle_hint_btn_close']";
    }

    public static String storeNextParcelButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/storeNextParcel']";
    }

    public static String alertOkButton()
    {
        return "//*[@resource-id='android:id/button1']";
    }

    public static String endStoreCheckButton()
    {
        return "//*[@resource-id='com.doddle.concession:id/btn_end_stock_check']";
    }

    public static String storeParcelLater()
    {
        return "//android.widget.Button[@text='Store parcels later']";
    }

    public static String warningNoButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='No']";
    }

    public static String warningYesButton()
    {
        return "//android.widget.LinearLayout//android.widget.Button[@text='Yes']";
    }

    public static String searchCollection()
    {
        return "//*[@resource-id='com.doddle.concession:id/searchButton']";
    }

    public static String deleteCollectionCode()
    {
        return "//*[@resource-id='com.doddle.concession:id/item_delete_btn']";
    }
    
    public static String forgetToday()
    {
         return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_rv_item_forget_today_button']";     
    }
    
    public static String photoOfParcel()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_rv_item_photo_of_parcel_button']";
    }
    
    public static String reinstateParcel()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_rv_item_reinstate_button']";
    }
    
    public static String checkoutXParcels()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_checkout_parcels_indicatior']";
    }
    
    public static String chooseParcels()
    {
        return "//*[@resource-id='com.doddle.concession:id/collection_btn_choose_parcels']";
    }
    
    public static String customerCollectionBackToDashboard()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_finish_checkout_parcels_button']";
    }
    
    public static String returnParcelToStorage()
    {
        return "//*[@resource-id='com.doddle.concession:id/payment_return_to_storage']";
    }
    
    public static String itemStatus()
    {
        return "//*[@resource-id='com.doddle.concession:id/itemStatus']";
    }
    
    public static String cancelMultipleReturns()
    {
        return "//*[@resource-id='android:id/button3']";
    }
    
    public static String deviceKeypad()
    {
        return "//*[@resource-id='com.doddle.concession:id/toolbar']";
    }
    
    public static String tryAgain()
    {
        return "//*[@resource-id='com.doddle.concession:id/btn_try_again']";
    }
    
    public static String printerErrorMessage()
    {
        return "//*[@resource-id='android:id/message']";
    }
    
    public static String sackBarcode()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_despatch_parcel_description_small']";
    }

    public static String appMenu()
    {
        return "//*[@resource-id='android:id/tabs']";
    }
    
    public static String googleSearchBar()
    {
        return "//*[@resource-id='com.android.launcher:id/search_button_container']";
    }
    
    public static String CollectionsBackToDashboardBtn()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_collection_parcels_of_customer_pick_parcels_from_storage_button']";
    }
    
    public static String returnsLabel()
    {
        return "//*[@resource-id='com.doddle.concession:id/label_value']";
    }
    
    public static String malformedPrinterError()
    {
        return "//*[@resource-id='android:id/message']";
    }
    
    public static String retry()
    {
        return "//*[@resource-id='com.doddle.concession:id/ll_return_select_printer']";
    }
    
    public static String yesTakeNewPhoto()
    {
        return "//*[@resource-id='com.doddle.concession:id/ask_photo_btn_yes']";
    }
    
    public static String preBookedBooking()
    {
        return "//*[@resource-id='com.doddle.concession:id/retailer_item']";
    }
    
    public static String bookingRefCode(String bookingRef)
    {
        return "android.widget.TextView[@text = '"+ bookingRef +"']/..";
    }
    
    public static String editReferences()
    {
        return "//*[@resource-id='com.doddle.concession:id/tv_three_dots_btn']";
    }
    
    public static String orderID()
    {
        return "//*[@resource-id='com.doddle.concession:id/return_references_label']";
    }
    
    public static String customerBlockEdit()
    {
        return "//*[@resource-id='com.doddle.concession:id/customer_three_dots_btn']";
    }
    
    public static String skipEdit()
    {
        return "//*[@resource-id='com.doddle.concession:id/skipBtn']";
    }
    
    public static String crashWait()
    {
        return "//android.widget.Button[@text = 'wait']";
    }
    
    public static String printerError()
    {
        return "//android.widget.TextView[@text = 'Error']";
    }
    
    public static String checkedCheckBox()
    {
        return "//android.widget.CheckBox[@checked = 'true']";
    }
    
    public static String wifiTitle()
    {
        return "//android.widget.TextView[@text = 'Wi-Fi']";
    }
    
    public static String alertPopup()
    {
        return "//android.widget.TextView[@text = 'Alert']";
    }
    
    public static String parcelStatus()
    {
        return "//*[@resource-id='com.doddle.concession:id/item_status_help_info']";
    }
    
    


//====================================================Number Generator=============================================================================
    public static String randomNo()
    {
        int pass = (int) Math.round(Math.random() * 9999999);
        NumberFormat formatter = new DecimalFormat("0000000");
        String randomNo = formatter.format(pass);
        return randomNo;
    }

    public static String randomThreeDigitNo()
    {
        int pass = (int) Math.round(Math.random() * 999);
        NumberFormat formatter = new DecimalFormat("000");
        String randomNo = formatter.format(pass);
        return randomNo;
    }

    public static String rand()
    {
        int rand = (int) Math.round(Math.random() * 999999999);
        NumberFormat formatter = new DecimalFormat("000000");
        String randomNo = formatter.format(rand);
        return "DPS" + randomNo;
    }

    public static String randomString(int len)
    {
        String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
        {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

    public static String barcode()
    {
        UUID uid = UUID.randomUUID();
        // checking the startValue of random UUID
        String barcode = uid.toString().replaceAll("-", "").substring(0, 13).replaceAll("'", "");
        return barcode;
    }

    public static String storedAmazonBarcode = "";

    public static String amazonBarcode()
    {
        String regex = "[QH]{1}[0-9]{11}";
        Xeger generator = new Xeger(regex);
        String amazonBarcode = generator.generate();

        return amazonBarcode;
    }

    public static String collectionDisconfirmCustomerID()
    {
        return "//android.widget.Button[@text = 'No']";
    }

    public static String doddleBackButton()
    {
        return "//android.widget.TextView[@text = 'Back']";
    }

//================================================JSON Payloads=====================================================================================  
    public static String jsonQRCodeTA38Start()
    {
        //return "grant_type=staff_password&username=nextgen.one&password=Password123&scope=cbsync_doddle_stores_sync%20customer";
        return "grant_type=staff_password&username=nextgen.one&password=Password123&scope=cbsync_doddle_stores_sync%20customer_all_access%20labels_read%20cms_book%20cms_options%20Geo";
    }

    public static String jsonQRCodeTA38End()
    {
        return "{\""
                + "carrierId\":"
                + "\"DHL\","
                + "\"itemId\":"
                + "\"ebba7e5d-b87d-4d48-984c-3236bb2867a8\","
                + "\"labelContentType\":"
                + "\"image/png\","
                + "\"retailerId\":"
                + "\"DataArtTest\","
                + "\"returnAddress\":"
                + "{\"address\":{"
                + "\"line1\":"
                + "\"New Post Ltd\","
                + "\"line2\":"
                + "\"Gorky park\","
                + "\"postcode\":"
                + "\"NE11 0TU\","
                + "\"town\":"
                + "\"Kharkov\"}"
                + "},"
                + "\"storeId\":"
                + "\"028PAD\""
                + "}";
    }

    public static String jsonPayloadTA19(String email, String randomNo, String Barcode, String storeCode)
    {

        return "  {\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"countryCode\": \"ES\",\n"
                + "      \"postCode\": \"11111\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"31 Donde Esta\"\n"
                + "      ],\n"
                + "      \"town\": \"La Zapateria\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"first\": \"Moo\",\n"
                + "      \"last\": \"Test\"\n"
                + "    },\n"
                + "    \"sendSMS\": false\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"id\":\"" + Barcode + "\",\n"
                + "      \"storeID\": \"" + storeCode + "\",\n"
                + "      \"weight\": 280\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"Moo\"\n"
                + "}";
    }

    public static String jsonPayloadTA23(String email, String randomNo, String Barcode, String storeCode)
    {

        return "  {\n"
                + "  \"customer\": {\n"
                + "    \"sendSMS\": false, \n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\"\n"
                + "    }, \n"
                + "    \"name\": {\n"
                + "      \"last\": \"Test\", \n"
                + "      \"first\": \"Moo\"\n"
                + "    }, \n"
                + "    \"address\": {\n"
                + "      \"town\": \"La Zapateria\", \n"
                + "      \"streetAddress\": [\n"
                + "        \"31 Donde Esta\"\n"
                + "      ], \n"
                + "      \"postCode\": \"11111\", \n"
                + "      \"countryCode\": \"ES\"\n"
                + "    }\n"
                + "  }, \n"
                + "  \"orderID\": \"" + randomNo + "\", \n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"storeID\": \"" + storeCode + "\", \n"
                + "      \"id\": \"" + Barcode + "\", \n"
                + "      \"weight\": 280\n"
                + "    }\n"
                + "  ], \n"
                + "  \"retailerID\": \"Moo\"\n"
                + "}";
    }

    public static String jsonPayloadTA22(String email, String cellNo, String cardNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "\"customer\": {\n"
                + "\"address\":\n"
                + "{ \"countryCode\": \"GB\", \"postCode\": \"NW1 0BV\", \"streetAddress\": [ \"Flat 3\", \"163A Shoe Aholics Street\" ], \"town\": \"London\" }\n"
                + "\n"
                + ",\n"
                + "\"contact\":\n"
                + "{ \"emailAddress\": \"" + email + "\", \"phoneNumber\": \"" + cellNo + "\" }\n"
                + "\n"
                + ",\n"
                + "\"name\":\n"
                + "{ \"first\": \"Shoeaholics\", \"last\": \"Test\" }\n"
                + "\n"
                + ",\n"
                + "\"sendSMS\": true,\n"
                + "\"verification\":\n"
                + "{ \"type\": \"cardNumber\", \"value\": \"" + cardNo + "\" }\n"
                + "\n"
                + "},\n"
                + "\"orderID\": \"" + randomNo + "\",\n"
                + "\"parcels\": [\n"
                + "{ \"id\": \"" + Barcode + "\", \"storeID\": \"" + storeCode + "\" }\n"
                + "\n"
                + "],\n"
                + "\"retailerID\": \"Shoeaholics\"\n"
                + "}";
    }

    public static String jsonPayloadTA24(String email, String cellNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"first\": \"Wiggle\",\n"
                + "      \"last\": \"Test\"\n"
                + "    },\n"
                + "    \"sendSMS\": true\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"deliveryEstimate\": {\n"
                + "        \"date\": \"2016-06-21\"\n"
                + "      },\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"largeItem\": false,\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"Wiggle\"\n"
                + "}";
    }

    public static String jsonPayloadTA25(String PostalCode, String email, String cellNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"countryCode\": \"GB\",\n"
                + "      \"postCode\": \"" + PostalCode + "\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"DODDLE - BIRMINGHAM NEW\"\n"
                + "      ],\n"
                + "      \"town\": \"BIRMINGHAM\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"full\": \"Newlook Test\"\n"
                + "    },\n"
                + "    \"sendSMS\": true,\n"
                + "    \"verification\": {\n"
                + "      \"type\": \"postCode\",\n"
                + "      \"value\": \"B2 4BY\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"NewLook\"\n"
                + "}";
    }

    public static String jsonPayloadTA21(String email, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"countryCode\": \"GB\",\n"
                + "      \"postCode\": \"M41 7LG\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"B&Q Trafford Park\"\n"
                + "      ],\n"
                + "      \"town\": \"Greater Manchester\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"full\": \"Missguided   Test\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"carrierName\": \"DoddleLogisticsOUTER\",\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"serviceName\": \"DoddleLate\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"Missguided\"\n"
                + "}";
    }

    public static String jsonPayloadTA26(String email, String cellNo, String CardNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"first\": \"City Forex\"\n"
                + "    },\n"
                + "    \"sendSMS\": true,\n"
                + "    \"verification\": {\n"
                + "      \"type\": \"cardNumber\",\n"
                + "      \"value\": \"" + CardNo + "\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"storageType\": \"secure\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"CityForex\"\n"
                + "}";
    }

    public static String jsonPayloadTA29(String PostCode, String email, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"countryCode\": \"GB\",\n"
                + "      \"postCode\": \"" + PostCode + "\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"023FPK|Doddle\"\n"
                + "      ],\n"
                + "      \"town\": \"London\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"full\": \"ASOS test\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"carrierName\": \"DoddleLogisticsCORE\",\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"serviceName\": \"DoddleLate\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"ASOS\"\n"
                + "}";
    }

    public static String jsonPayloadTA30(String email, String cellNo, String cardNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"first\": \"Airmoney Test\"\n"
                + "    },\n"
                + "    \"sendSMS\": true,\n"
                + "    \"verification\": {\n"
                + "      \"type\": \"cardNumber\",\n"
                + "      \"value\": \"" + cardNo + "\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"storageType\": \"secure\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"Airmoney\"\n"
                + "}";
    }

    public static String jsonPayloadTA31(String email, String cellNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"first\": \"Farfetch\",\n"
                + "      \"last\": \"Test\"\n"
                + "    },\n"
                + "    \"sendSMS\": true\n"
                + "  },\n"
                + "  \"orderID\": \"" + cellNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"storageType\": \"standard\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"Farfetch\"\n"
                + "}";
    }

    public static String jsonPayloadTA32(String PostalCode, String email, String cellNo, String cardNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"countryCode\": \"GB\",\n"
                + "      \"postCode\": \"" + PostalCode + "\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"127 Hawes Close\",\n"
                + "        \"New Curtis\"\n"
                + "      ],\n"
                + "      \"town\": \"Croydon\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"first\": \"Hawes\",\n"
                + "      \"last\": \"Test\",\n"
                + "      \"prefix\": \"Mrs\"\n"
                + "    },\n"
                + "    \"verification\": {\n"
                + "      \"type\": \"cardNumber\",\n"
                + "      \"value\": \"" + cardNo + "\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"containerID\": \"60845755\",\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"storeID\": \"" + storeCode + "\",\n"
                + "      \"weight\": 1600\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"HawesCurtis\"\n"
                + "}";
    }

    public static String jsonPayloadTA33(String email, String cellNo, String PostalCode, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"countryCode\": \"GB\",\n"
                + "      \"postCode\": \"SW18 1AA\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"FLAT 108, HOBBS HOUSE\"\n"
                + "      ],\n"
                + "      \"town\": \"LONDON\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"first\": \"HOBBS\",\n"
                + "      \"last\": \"TEST\",\n"
                + "      \"prefix\": \"MISS\"\n"
                + "    },\n"
                + "    \"sendSMS\": false,\n"
                + "    \"verification\": {\n"
                + "      \"type\": \"postCode\",\n"
                + "      \"value\": \"" + PostalCode + "\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"storageType\": \"standard\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"Hobbs\"\n"
                + "}";
    }

    public static String jsonPayloadTA34(String PostalCode, String email, String cellNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"countryCode\": \"GB\",\n"
                + "      \"postCode\": \"" + PostalCode + "\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"Doddle - Throgmorton Street\",\n"
                + "        \"Warnford Court, 29 Throgmorton Street\",\n"
                + "        \"et\"\n"
                + "      ],\n"
                + "      \"town\": \"London\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"full\": \"Nespresso test\"\n"
                + "    },\n"
                + "    \"sendSMS\": false\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"carrierName\": \"YODEL - Home Delivery Network (HDNL)\",\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"serviceName\": \"YODEL_@Home24_B2CHome_POD\",\n"
                + "      \"storeID\": \"" + storeCode + "\",\n"
                + "      \"weight\": 1500\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"Nespresso\"\n"
                + "}";
    }

    public static String jsonPayloadTA35(String PostalCode, String email, String cellNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"address\": {\n"
                + "      \"postCode\": \"" + PostalCode + "\",\n"
                + "      \"streetAddress\": [\n"
                + "        \"9 TMLEWIN AVENUE\"\n"
                + "      ],\n"
                + "      \"town\": \"LONDON\"\n"
                + "    },\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"full\": \"MR TMLEWIN TEST\"\n"
                + "    },\n"
                + "    \"sendSMS\": false,\n"
                + "    \"verification\": {\n"
                + "      \"type\": \"postCode\",\n"
                + "      \"value\": \"" + PostalCode + "\"\n"
                + "    }\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"TMLewin\"\n"
                + "}";
    }

    public static String jsonPayloadTA36(String email, String cellNo, String randomNo, String Barcode, String storeCode)
    {

        return "{\n"
                + "  \"customer\": {\n"
                + "    \"contact\": {\n"
                + "      \"emailAddress\": \"" + email + "\",\n"
                + "      \"phoneNumber\": \"" + cellNo + "\"\n"
                + "    },\n"
                + "    \"name\": {\n"
                + "      \"first\": \"Farfetch\",\n"
                + "      \"last\": \"Test\"\n"
                + "    },\n"
                + "    \"sendSMS\": true\n"
                + "  },\n"
                + "  \"orderID\": \"" + randomNo + "\",\n"
                + "  \"parcels\": [\n"
                + "    {\n"
                + "      \"id\": \"" + Barcode + "\",\n"
                + "      \"storageType\": \"standard\",\n"
                + "      \"storeID\": \"" + storeCode + "\"\n"
                + "    }\n"
                + "  ],\n"
                + "  \"retailerID\": \"Farfetch\"\n"
                + "}";

    }

    public static String singleContainerStoreConfig()
    {
        return "{\n"
                + "    \"storeSystemUIConfig\": {\n"
                + "        \"moveToContainerFlow\": \"SINGLE_CONTAINER\"\n"
                + "    }\n"
                + "}";
    }

    public static String multipleContainerStoreConfig()
    {
        return "{\n"
                + "  \"storeSystemUIConfig\": {\n"
                + "        \"moveToContainerFlow\": \"MULTIPLE_CONTAINERS\"\n"
                + "    }\n"
                + "}";
    }

    public static String storageStoreNextParcel()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Store the next parcel']";
    }

    public static String scanFirstParcelText()
    {
        return "//android.widget.LinearLayout//android.widget.TextView[@text='Scan the first Red-lined parcel']";
    }

    public static String storeSelection(String text)
    {
        return "//android.widget.CheckedTextView[@text='" + text + "']";
    }

    public static String preadvisedSkipPhotoConfigFalse()
    {
        return "{\n"
                + "	\"storeSystemUIConfig\": {\n"
                + "		\"ifPreadvisedSkipPhoto\": false\n"
                + " 	}\n"
                + "}";
    }

    public static String preadvisedSkipPhotoConfigTrue()
    {
        return "{\n"
                + "	\"storeSystemUIConfig\": {\n"
                + "		\"ifPreadvisedSkipPhoto\": true\n"
                + " 	}\n"
                + "}";
    }
    
    public static String preadvicedDisableParcelItemisation()
    {
        return "{\n"
                + "     \"parcelItemisation\":  {\n"
                + "             \"input\": \"DISABLED\" \n"
                + "     }\n"
                + "}";

    }
    
    public static String preadvisedNoIDChargeConfig()
    {
        return "{\n"
                + "	\"storeSystemUIConfig\": {\n"
                + "		\"noIdCollectionCharge\": true\n"
                + " 	}\n"
                + "}";
    }
    
    public static String collectionsPreAdviceBody(String labelValue, String shelfID)
    {
        //DHL is hardcoded, please change if more retailers are used
        return "{\n"
                + "    \"organisationId\" : \"DODDLE\",\n"
                + "    \"labelValue\" : \"" + labelValue + "\",\n"
                + "    \"preadvisedLate\": true,\n"
                + "    \"location\" : {\n"
                + "        \"storeId\" : \"456DVT\",\n"
                + "        \"shelfId\" : \"" + shelfID + "\"\n"
                + "    },\n"
                + "    \"customer\" : {\n"
                + "        \"email\" : \"mootest@doddle.test\",\n"
                + "        \"customerId\" : \"XXX11111\"\n"
                + "    },\n"
                + "    \"retailerId\" : \"DHL\",\n"
                + "    \"retailerOrderId\" : \"DHL-1508840916-0001\",\n"
                + "    \"expectedDate\" : \"2017-10-01\",\n"
                + "    \"dimensions\" : [ 500, 200, 100 ],\n"
                + "    \"weight\" : 1000,\n"
                + "    \"status\" : \"AT_GOODS_IN\",\n"
                + "    \"eventHistory\" : [\n"
                + "      { \"eventType\" : \"PREADVICE_RECEIVED\", \"dateTime\": \"" + Calendar.getInstance().toInstant() + "\" }\n"
                + "    ]\n"
                + "}";
    }
    //2
     public static String collectionsPreAdviceBody2(String labelValue, String shelfID)
    {
        //DHL is hardcoded, please change if more retailers are used
        return "{\n"
                + "    \"organisationId\" : \"DODDLE\",\n"
                + "    \"labelValue\" : \"" + labelValue + "\",\n"
                + "    \"preadvisedLate\": true,\n"
                + "    \"location\" : {\n"
                + "        \"storeId\" : \"456DVT\",\n"
                + "        \"shelfId\" : \"" + shelfID + "\"\n"
                + "    },\n"
                + "    \"customer\" : {\n"
                + "        \"email\" : \"mootest@doddle.test\""
                + "    },\n"
                + "    \"retailerId\" : \"DHL\",\n"
                + "    \"retailerOrderId\" : \"DHL-1508840916-0001\",\n"
                + "    \"expectedDate\" : \"2017-10-01\",\n"
                + "    \"dimensions\" : [ 500, 200, 100 ],\n"
                + "    \"weight\" : 1000,\n"
                + "    \"status\" : \"AT_GOODS_IN\",\n"
                + "    \"eventHistory\" : [\n"
                + "      { \"eventType\" : \"PREADVICE_RECEIVED\", \"dateTime\": \"" + Calendar.getInstance().toInstant() + "\" }\n"
                + "    ]\n"
                + "}";
    }
     //3
          public static String collectionsPreAdviceBody3(String labelValue, String shelfID)
    {
        //DHL is hardcoded, please change if more retailers are used
        return "{\n"
                + "    \"organisationId\" : \"DODDLE\",\n"
                + "    \"labelValue\" : \"" + labelValue + "\",\n"
                + "    \"preadvisedLate\": true,\n"
                + "    \"location\" : {\n"
                + "        \"storeId\" : \"456DVT\",\n"
                + "        \"shelfId\" : \"" + shelfID + "\"\n"
                + "    },\n"
                + "    \"customer\" : {\n"
                + "        \"customerId\" : \"XXX11111\"\n"
                + "    },\n"
                + "    \"retailerId\" : \"DHL\",\n"
                + "    \"retailerOrderId\" : \"DHL-1508840916-0001\",\n"
                + "    \"expectedDate\" : \"2017-10-01\",\n"
                + "    \"dimensions\" : [ 500, 200, 100 ],\n"
                + "    \"weight\" : 1000,\n"
                + "    \"status\" : \"AT_GOODS_IN\",\n"
                + "    \"eventHistory\" : [\n"
                + "      { \"eventType\" : \"PREADVICE_RECEIVED\", \"dateTime\": \"" + Calendar.getInstance().toInstant() + "\" }\n"
                + "    ]\n"
                + "}";
    }
     
    public static String collectionsCleanUpBody()
    {
        return "{\n" +
        "    \"resource\" : {\n" +
        "        \"completed\" : true\n" +
        "    },\n" +
        "    \"events\" : [\n" +
        "      {\n" +
        "        \"dateTime\": \"2017-06-09T12:05:57.717Z\",\n" +
        "        \"eventType\": \"MARKED_COMPLETED\"\n" +
        "      }\n" +
        "    ]\n" +
        "}";
    }

    public static String collectionsPreAdviceBodyNoCustomerBlock(String labelValue, String shelfID)
    {
        //DHL is hardcoded, please change if more retailers are used
        return "{\n"
                + "    \"organisationId\" : \"DODDLE\",\n"
                + "    \"labelValue\" : \"" + labelValue + "\",\n"
                + "    \"preadvisedLate\": true,\n"
                + "    \"location\" : {\n"
                + "        \"storeId\" : \"456DVT\",\n"
                + "        \"shelfId\" : \"" + shelfID + "\"\n"
                + "    },\n"
                + "    \"retailerId\" : \"DHL\",\n"
                + "    \"retailerOrderId\" : \"DHL-1508840916-0001\",\n"
                + "    \"expectedDate\" : \"2017-10-01\",\n"
                + "    \"dimensions\" : [ 500, 200, 100 ],\n"
                + "    \"weight\" : 1000,\n"
                + "    \"status\" : \"AT_GOODS_IN\",\n"
                + "    \"eventHistory\" : [\n"
                + "      { \"eventType\" : \"PREADVICE_RECEIVED\", \"dateTime\": \"" + Calendar.getInstance().toInstant() + "\" }\n"
                + "    ]\n"
                + "}";
    }
     public static String collectionsPreAdviceBodyNoCustomerBlock2(String labelValue, String shelfID)
    {
        //DHL is hardcoded, please change if more retailers are used
        return "{\n"
                + "    \"organisationId\" : \"DODDLE\",\n"
                + "    \"labelValue\" : \"" + labelValue + "\",\n"
                + "    \"preadvisedLate\": true,\n"
                + "    \"location\" : {\n"
                + "        \"storeId\" : \"456DVT\",\n"
                + "        \"shelfId\" : \"" + shelfID + "\"\n"
                + "    },\n"
                + "    \"retailerId\" : \"Amazon\",\n"
                + "    \"retailerOrderId\" : \"Amazon-1508840916-0001\",\n"
                + "    \"expectedDate\" : \"2017-10-01\",\n"
                + "    \"dimensions\" : [ 500, 200, 100 ],\n"
                + "    \"weight\" : 1000,\n"
                + "    \"status\" : \"AT_GOODS_IN\",\n"
                + "    \"eventHistory\" : [\n"
                + "      { \"eventType\" : \"PREADVICE_RECEIVED\", \"dateTime\": \"" + Calendar.getInstance().toInstant() + "\" }\n"
                + "    ]\n"
                + "}";
    }
    public static String returnsPreAdviceBody(String clientVersion, String deviceIdentifier, String email, String storeID, String retailerID, String value)
    {
        //DHL is hardcoded, please change if more retailers are used
        return "{\n"
                + "    \"meta\" : {\n"
                + "         \"clientVersion\" : \"" + clientVersion + "\",\n"
                + "         \"deviceIdentifier\": \"" + deviceIdentifier + "\"\n"
                + "},"
                + "    \"resource\" : {\n"
                + "        \"customer\" : {\n"
                + "        \"email\" : \"" + email + "\",\n"
                + "    \"name\" : {\n"
                + "         \"firstName\" : \"Barry\",\n"
                + "         \"lastName\" : \"Scott\"\n"
                + "}"
                +"},"
                + "    \"location\" : {\n"
                + "         \"storeId\" : \"" + storeID + "\"\n"
                + "},"
                + "    \"retailerId\" : \"" + retailerID + "\", \n"
                + "    \"retailerItemData\": {\n"
                + "     \"returnReferences\": {\n"
                + "          \"ref1\": {\n"
                + "             \"title\": \"ASOS Order Number\", \n"
                + "             \"value\": \"" + value + "\"\n"
                + "    }\n"
                + "   }\n"
                + "  }\n"
                + " }\n"
                + "}";
                
        
        
    }

    public static String collectionsPreAdviceBodyNoCustomerWithCustomOrderID(String labelValue, String shelfID, String retailerOrderID)
    {
        //DHL is hardcoded, please change if more retailers are used
        return "{\n"
                + "    \"organisationId\" : \"DODDLE\",\n"
                + "    \"labelValue\" : \"" + labelValue + "\",\n"
                + "    \"preadvisedLate\": true,\n"
                + "    \"location\" : {\n"
                + "        \"storeId\" : \"456DVT\",\n"
                + "        \"shelfId\" : \"" + shelfID + "\"\n"
                + "    },\n"
                + "    \"retailerId\" : \"DHL,\n"
                + "    \"retailerOrderId\" : \""+retailerOrderID+"\",\n"
                + "    \"expectedDate\" : \"2017-10-01\",\n"
                + "    \"dimensions\" : [ 500, 200, 100 ],\n"
                + "    \"weight\" : 1000,\n"
                + "    \"status\" : \"AT_GOODS_IN\",\n"
                + "    \"eventHistory\" : [\n"
                + "      { \"eventType\" : \"PREADVICE_RECEIVED\", \"dateTime\": \"" + Calendar.getInstance().toInstant() + "\" }\n"
                + "    ]\n"
                + "}";
    }
    
    public static String collectionPreAdviceBodyNoRetialerID(String labelValue, String shelfID)
    {
                return "{\n"
                + "    \"meta\" : \"DODDLE\",\n"
                + "    \"labelValue\" : \"" + labelValue + "\",\n"
                + "    \"preadvisedLate\": true,\n"
                + "    \"location\" : {\n"
                + "        \"storeId\" : \"456DVT\",\n"
                + "        \"shelfId\" : \"" + shelfID + "\"\n"
                + "    },\n"
                + "    \"retailerOrderId\" : \"DHL-1508840916-0001\",\n"
                + "    \"expectedDate\" : \"2017-10-01\",\n"
                + "    \"dimensions\" : [ 500, 200, 100 ],\n"
                + "    \"weight\" : 1000,\n"
                + "    \"status\" : \"AT_GOODS_IN\",\n"
                + "    \"eventHistory\" : [\n"
                + "      { \"eventType\" : \"PREADVICE_RECEIVED\", \"dateTime\": \"" + Calendar.getInstance().toInstant() + "\" }\n"
                + "    ]\n"
                + "}";
    }
    
        public static String collectionsPreAdviceBodyNotPassedGoodsIn(String labelValue, String shelfID)
    {
        //DHL is hardcoded, please change if more retailers are used
        return "{\n"
                + "    \"organisationId\" : \"DODDLE\",\n"
                + "    \"labelValue\" : \"" + labelValue + "\",\n"
                + "    \"preadvisedLate\": true,\n"
                + "    \"location\" : {\n"
                + "        \"storeId\" : \"456DVT\",\n"
                + "        \"shelfId\" : \"" + shelfID + "\"\n"
                + "    },\n"
                + "    \"customer\" : {\n"
                + "        \"email\" : \"mootest@doddle.test\",\n"
                + "        \"customerId\" : \"XXX11111\"\n"
                + "    },\n"
                + "    \"retailerId\" : \"DHL\",\n"
                + "    \"retailerOrderId\" : \"DHL-1508840916-0001\",\n"
                + "    \"expectedDate\" : \"2017-10-01\",\n"
                + "    \"dimensions\" : [ 500, 200, 100 ],\n"
                + "    \"weight\" : 1000,\n"
                + "    \"status\" : \"EXPECTED\",\n"
                + "    \"eventHistory\" : [\n"
                + "      { \"eventType\" : \"PREADVICE_RECEIVED\", \"dateTime\": \"" + Calendar.getInstance().toInstant() + "\" }\n"
                + "    ]\n"
                + "}";
    }

}

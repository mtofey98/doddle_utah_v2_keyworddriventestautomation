/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 *
 * @author jmacauley
 */
public class SaveBarcodesFromURL extends BaseClass
{
//    public static String barcode = MobileDoddlePageObjects.barcode();
//    public static String shelfLabel = MobileDoddlePageObjects.randomNo();

    public static String imageURL;
    public static String destinationFile;
    public static InputStream inputStream = null;
    public static OutputStream outputStream = null;
    public static URL url;

    public static boolean saveBarcode(String barcode, String shelfLabel)
    {

//        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(barcode);
        try
        {
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png";
            save();
            SettersAndGetters.setGeneratedBarCode(barcode);

            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(shelfLabel));
            destinationFile = System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfLabel + ".png";
            save();
            SettersAndGetters.setGeneratedShelfCode(shelfLabel);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean saveShelfLabels(String shelfLabel1, String shelfLabel2)
    {

//        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(barcode);
        try
        {
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(shelfLabel1));
            destinationFile = System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfLabel1 + ".png";
            save();

            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(shelfLabel2));
            destinationFile = System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfLabel2 + ".png";
            save();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean saveShelfLabel(String shelfLabel)
    {

//        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(barcode);
        try
        {
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(shelfLabel));
            destinationFile = System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfLabel + ".png";
            save();
            SettersAndGetters.setGeneratedShelfCode(shelfLabel);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }
    
    public static boolean saveLabel(String labelValue)
    {

//        imageURL = WebDoddlePageObjects.barcodeGeneratorURL(barcode);
        try
        {
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(labelValue));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + labelValue + ".png";
            save();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }
    
    public static boolean saveMultipleBarcodes(String barcode, String barcode2, String barcode3, String barcode4, String shelfLabel)
    {
        try
        {
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png";
            save();
            SettersAndGetters.setGeneratedBarCode(barcode);
            
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode2));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode2 + ".png";
            save();
            SettersAndGetters.setGeneratedBarCode(barcode2);
            
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode3));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode3 + ".png";
            save();
            SettersAndGetters.setGeneratedBarCode(barcode3);
            
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode4));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode4 + ".png";
            save();
            SettersAndGetters.setGeneratedBarCode(barcode4);

            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(shelfLabel));
            destinationFile = System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfLabel + ".png";
            save();
            SettersAndGetters.setGeneratedShelfCode(shelfLabel);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        
        return true;
    }
    
    public boolean saveBarcodes(String barcode, String barcode2)
    {
        try
        {
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png";
            save();

            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode2));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode2 + ".png";
            save();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    public boolean saveReturnsBarcode(String barcode)
    {
        try
        {
            url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode));
            destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png";
            save();


        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }

    public static boolean save()
    {
        try
        {

//          URL url = new URL("https://barcode.tec-it.com/barcode.ashx?translate-esc=off&data="+barcodeName+"&code=Code128&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=000000&bgcolor=FFFFFF&qunit=Mm&quiet=0");
            inputStream = url.openStream();
            outputStream = new FileOutputStream(destinationFile);

            byte[] buffer = new byte[2048];
            int length;

            while ((length = inputStream.read(buffer)) != -1)
            {
                outputStream.write(buffer, 0, length);
            }
        }
        catch (Exception e)
        {
            System.out.println("Exception :- " + e.getMessage());
            return false;
        }
        finally
        {
            try
            {
                inputStream.close();
                outputStream.close();
            }
            catch (IOException e)
            {
                System.out.println("IOException  Exception :- " + e.getMessage());
                return false;
            }
        }

        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes;

import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_02_Login;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_03_Carrier;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_16_Collection_OrderID.orderID;
import KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return;
import static KeywordDrivenTestFramework.Testing.Mobile.NewTestClasses.TC_NextGen_22_Return.LabelValue;
import static KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes.SettersAndGetters.getGeneratedBarCode;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

/**
 *
 * @author mlopes
 */
public class ReusableFunctionalities extends Shortcuts
{

    public static boolean isSeleniumFailure = false;

    public static boolean Login()
    {
        String selectedStore = "";

        if (!conErorr)
        {
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 2))
            {
//                if (!(ReusableFunctionalities.checkWifiIsOn()))
//                {
//                    error = "Failed to switch wifi-on " + Shortcuts.error;
//                    return false;
//                }
                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginButton(), 2))
                {
                    error = "Failed to find the version text";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainLoginButton()))
                {
                    error = "Failed to touch the login button.";
                    return false;
                }

                if (!multiplePopUpMessages2())
                {
                    error = "Failed to handle multiple popups";
                    return false;
                }
            }

            if (!multiplePopUpMessages2())
            {
                error = "Failed to handle multiple popups";
                return false;
            }

            try
            {
                selectedStore = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.storeSpinner());
            } catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to retrieve current store";
                return false;
            }

            if (!selectedStore.contains(testData.getData("StoreSelection")))
            {
                if (!scrollToSubElement(testData.getData("StoreSelection")))
                {
                    error = "Could not find " + testData.getData("StoreSelection");
                    return false;
                }
            }

            if (!multiplePopUpMessages2())
            {
                error = "Failed to handle multiple popups";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.loginUsername()))
            {
                error = "Failed to touch the username field.";
                return false;
            }

            if (!multiplePopUpMessages2())
            {
                error = "Failed to handle multiple popups";
                return false;
            }
        }

        conErorr = false;
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginUsername(), testData.getData("username")))
        {
            error = "Failed to enter username.";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            error = "Failed to handle multiple popups";
            return false;
        }

        if (!multiplePopUpMessages2())
        {
            error = "Failed to handle multiple popups";
            return false;
        }
        //Will enter the password
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.loginPassword(), testData.getData("password")))
        {
            error = "Failed to enter the password.";
            return false;
        }
        //Will click on the login button
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.loginButton()))
        {
            error = "Failed to click Login button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.connectionErrorPopup(), 10))
        {
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());

            if (!conErorr)
            {
                Narrator.stepPassedWithScreenShot("False Connection Error!");
                conErorr = true;
                ReusableFunctionalities.Login();

            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to wait for the check in button.";
            return false;
        }

        Narrator.stepPassed("Successfully waited for the semi-bclocking screen to close");

        return true;
    }

    public static boolean checkWifiIsOn()
    {

        try
        {
            Driver.openNotifications();
        } catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            String wifiOffText = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.wifiOffText());

            if (wifiOffText.contains("Off") || wifiOffText.contains("OFF"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiOffText()))
                {
                    error = "Failed to navigate to wifi switch";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
                {
                    error = "Failed to wait for the wifi switch to be visible";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
                {
                    error = "Failed to click wifi switch";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
                {
                    error = "Failed to wait for the wifi to be connected";
                    return false;
                }

                Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");
            }

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to check wifi status - " + e.getMessage();
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public static boolean wifiOff()
    {

        try
        {
            Driver.openNotifications();
        } catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            String wifiOffText = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.wifiOffText());

            if (!wifiOffText.contains("Off") || wifiOffText.contains("OFF"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiOffText()))
                {
                    error = "Failed to navigate to wifi switch";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
                {
                    error = "Failed to wait for the wifi switch to be visible";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
                {
                    error = "Failed to click wifi switch";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiDisconnected()))
                {
                    error = "Failed to wait for the wifi to be connected";
                    return false;
                }

                Narrator.stepPassedWithScreenShot("Successfully turned off Wifi");
            }

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to check wifi status - " + e.getMessage();
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public static boolean wifiOn()
    {

        try
        {
            Driver.openNotifications();
        } catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            String wifiOffText = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.wifiOffText());

            if (wifiOffText.contains("Off") || wifiOffText.contains("OFF"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiOffText()))
                {
                    error = "Failed to navigate to wifi switch";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
                {
                    error = "Failed to wait for the wifi switch to be visible";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
                {
                    error = "Failed to click wifi switch";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
                {
                    error = "Failed to wait for the wifi to be connected";
                    return false;
                }

                Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");
            }

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to check wifi status - " + e.getMessage();
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public static boolean carrierProcessExecution()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!Login())
            {
                return false;
            }

            if (!carrierProcess())
            {
                return false;
            }
        } else
        {
            if (!carrierProcess())
            {
                return false;
            }
        }

        return true;
    }

    public static boolean checkCarrierRecordsInDatabase()
    {
        try
        {
            String[] recordInDatabase
                    =
                    {
                        "deliveryCarrierId", "clientVersion", "deviceIdentifier", "eventType", "storeId", "newStatus", "staffId", "labelValue", "status"
                    };

            Shortcuts.doStorageLabelValueAPICall(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getNewBearer());

            for (String recordInDatabase1 : recordInDatabase)
            {

                if (!SettersAndGetters.newBearer.contains(recordInDatabase1))
                {
                    error = "Failed to add \"'" + recordInDatabase1 + "'\" event in CouchBase";
                    // return false;
                }

                if (!Shortcuts.changeAPIResponseColor(recordInDatabase1))
                {
                    error = "Failed to update text: \"'" + recordInDatabase1 + "'\" to green color";
                    return false;
                }
            }
        } catch (Exception e)
        {
            error = e.getMessage();
            return false;
        }
        return true;
    }

    public static boolean checkCarriersRecordsInDatabase(String fileName)
    {
        String currentValue;
        String textToBeChecked = "";
        String keyValue = "";
        Shortcuts.databaseRecordsLabels(fileName);
        Map<String, String> recordInDatabase = SettersAndGetters.getDatabaseRecords();//getting the Map with different values
        Set<String> keyData = SettersAndGetters.getDatabaseRecords().keySet();//getting only the keys for the Map
        List<String> keyOfRecordInDatabase = new ArrayList<>(keyData);//putting the map keys in a list to be used on the Map
        // keyOfRecordInDatabase.add("deviceIdentifier");
        Shortcuts.doStorageLabelValueAPICall(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getNewBearer());
        String bearerValue = SettersAndGetters.getAPIResponse();
        String dev = "deviceIdentifier";
        for (int i = 0; i < keyOfRecordInDatabase.size(); i++)
        {
            switch (recordInDatabase.get(keyOfRecordInDatabase.get(i)))
            {
                case "placeholder":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                case "hiddenValue":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                default:
                {
                    currentValue = recordInDatabase.get(keyOfRecordInDatabase.get(i));
                    if ((keyOfRecordInDatabase.get(i).contains(" ")))
                    {
                        String[] tempRecord = keyOfRecordInDatabase.get(i).split(" ");
                        keyValue = tempRecord[0];
                    } else
                    {
                        keyValue = keyOfRecordInDatabase.get(i);
                    }

                    textToBeChecked = "\"" + keyValue + "\"" + ":" + "\"" + currentValue + "\"";
                    break;
                }
            }

            if (!bearerValue.contains(textToBeChecked))
            {
                error = "Failed to add \"" + changeFailedAPIResponseColor(textToBeChecked) + "\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(textToBeChecked))
            {
                error = "Failed to update text: \"'" + changeFailedAPIResponseColor(textToBeChecked) + "'\" to green color";
                return false;
            }
            if (!Shortcuts.changeAPIResponseColor(dev))
            {
                error = dev + " was not found in the ITEM_COLLECTION record";
                return false;
            }

        }
//         if (!bearerValue.contains(dev))
//            {
//                error = "Failed to add \"" + dev+ "\" event in CouchBase";
//                return false;
//            }

//            if (!Shortcuts.changeAPIResponseColor(dev))
//            {
//                error = "Failed to update text: \"'" + dev + "'\" to green color";
//                return false;
//            }
        Narrator.stepPassed("Carrier added successfully - " + Shortcuts.line);
        return true;

    }

    public static boolean validateCarrier(String fileName)
    {
        String currentValue;
        String textToBeChecked = "";
        String keyValue = "";
        Shortcuts.databaseRecordsLabels(fileName);
        Map<String, String> recordInDatabase = SettersAndGetters.getDatabaseRecords();//getting the Map with different values
        Set<String> keyData = SettersAndGetters.getDatabaseRecords().keySet();//getting only the keys for the Map
        List<String> keyOfRecordInDatabase = new ArrayList<>(keyData);//putting the map keys in a list to be used on the Map

        Shortcuts.doCarrierDHLAPICall(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getNewBearer());
        String bearerValue = SettersAndGetters.getAPIResponse();

        for (int i = 0; i < keyOfRecordInDatabase.size(); i++)
        {
            switch (recordInDatabase.get(keyOfRecordInDatabase.get(i)))
            {
                case "placeholder":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                case "hiddenValue":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                default:
                {
                    currentValue = recordInDatabase.get(keyOfRecordInDatabase.get(i));
                    if ((keyOfRecordInDatabase.get(i).contains(" ")))
                    {
                        String[] tempRecord = keyOfRecordInDatabase.get(i).split(" ");
                        keyValue = tempRecord[0];
                    } else
                    {
                        keyValue = keyOfRecordInDatabase.get(i);
                    }

                    textToBeChecked = "\"" + keyValue + "\"" + ":" + "\"" + currentValue + "\"";
                    break;
                }
            }

            if (!bearerValue.contains(textToBeChecked))
            {
                error = "Failed to add \"" + changeFailedAPIResponseColor(textToBeChecked) + "\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(textToBeChecked))
            {
                error = "Failed to update text: \"'" + changeFailedAPIResponseColor(textToBeChecked) + "'\" to green color";
                return false;
            }

        }
        Narrator.stepPassed("Carrier added successfully - " + SettersAndGetters.getAPIResponse());
        return true;
    }

    public static boolean carrierProcessOtherExecution()
    {
        TC_NextGen_02_Login carrier = new TC_NextGen_02_Login(testData);
        if (!carrier.LoginReuseCopy())
        {
            return false;
        }

        if (!ReusableFunctionalities.checkHintsScreenIsOn())
        {
            error = "Failed to ensure hints screen is activated";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Other")))
        {
            error = "Failed to search for " + testData.getData("Other") + ".";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the other to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to click Other tile.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterFreeText(), testData.getData("Something")))
        {
            error = "Failed to search for " + testData.getData("Other") + ".";
            return false;
        }
        Narrator.stepPassed("Successfully entered free text");
        for (int i = 0; i < 2; i++)
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
            {
                error = "Failed to click back Arrow";
                return false;
            }
        }
        if (!carrierProcessLaterBtn())
        {
            return false;
        }
        if (!carrierProcessContinue())
        {
            //error = "Failed to do the carrier process";

            return false;
        }
//        else if (!carrierProcess())
//        {
//            return false;
//        }

        return true;
    }

    public static boolean checkReturnsRecordsInDatabase(String[] itemsToValidate)
    {
        if (SettersAndGetters.getNewBearer() == null)
        {
            Shortcuts.newBearer();
        }

        Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

        String bearer = SettersAndGetters.getNewBearer();
        String apiResponse = SettersAndGetters.getAPIResponse();

        for (String recordInDatabase1 : itemsToValidate)
        {
            if (!SettersAndGetters.getAPIResponse().contains(recordInDatabase1))
            {
                error = "Failed to add \"'" + changeFailedAPIResponseColor(recordInDatabase1) + "'\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(recordInDatabase1))
            {
                error = "Failed to update text: \"'" + recordInDatabase1 + "'\" to green color.";
                return false;
            }

            Narrator.stepPassed(SettersAndGetters.getAPIResponse());
        }

        return true;
    }

    public static boolean checkReturnsRecordsInDatabaseUsingLabelValue(String[] itemToValidate)
    {
        if (SettersAndGetters.getNewBearer().equals(""))
        {
            Shortcuts.newBearer();
        }

        Shortcuts.doStorageLabelValueAPICall(SettersAndGetters.getReturnsLabelValue(), SettersAndGetters.getNewBearer());

        String bearer = SettersAndGetters.getNewBearer();
        String apiResponse = SettersAndGetters.getAPIResponse();

        for (String recordInDatabase1 : itemToValidate)
        {
            if (!SettersAndGetters.getAPIResponse().contains(recordInDatabase1))
            {
                error = "Failed to add \"'" + recordInDatabase1 + "'\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(recordInDatabase1))
            {
                error = "Failed to update text: \"'" + recordInDatabase1 + "'\" to green color.";
                return false;
            }

            Narrator.stepPassed(SettersAndGetters.getAPIResponse());
        }

        return true;
    }

    public static boolean checkStoreTaskRecordsInDatabase(String[] itemsToValidate)
    {
        retrieveStoreTaskJsonResponse();

        String bearer = SettersAndGetters.getNewBearer();
        String apiResponse = SettersAndGetters.getAPIResponse();

        for (String recordInDatabase1 : itemsToValidate)
        {
            if (!SettersAndGetters.getAPIResponse().contains(recordInDatabase1))
            {
                error = "Failed to add \"'" + recordInDatabase1 + "'\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(recordInDatabase1))
            {
                error = "Failed to update text: \"'" + recordInDatabase1 + "'\" to green color.";
                return false;
            }

            Narrator.stepPassed(SettersAndGetters.getAPIResponse());
        }

        return true;
    }

    public static boolean retrieveStoreTaskJsonResponse()
    {
        try
        {
            SeleniumDriverInstance.startDriver();

            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURL()))
            {
                error = "Failed to navigate to couchbase URL";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.storeTaskList(), 180))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.storeTaskList()))
                {
                    error = "Failed to click the 1st store task link";
                    return false;
                }

                SeleniumDriverInstance.pause(2000);

                if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.rawDocumentURL()))
                {
                    error = "Failed to wait for the raw document URL link";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.rawDocumentURL()))
                {
                    error = "Failed to wait for the raw document URL link";
                    return false;
                }

                SeleniumDriverInstance.pause(5000);
                SeleniumDriverInstance.Driver.navigate().back();
                SeleniumDriverInstance.pause(2000);

                line = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.rawDocumentData());
                System.out.println(line);
                SettersAndGetters.setAPIResponse(line);

            }

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve json response - " + e.getMessage();
            return false;
        } finally
        {
            SeleniumDriverInstance.shutDown();
        }

        return true;
    }

    public static boolean checkLoadRecordsInDatabase(String[] itemsToValidate)
    {
        retrieveLoadJsonResponse();

        String bearer = SettersAndGetters.getNewBearer();
        String apiResponse = SettersAndGetters.getAPIResponse();

        for (String recordInDatabase1 : itemsToValidate)
        {
            if (!SettersAndGetters.getAPIResponse().contains(recordInDatabase1))
            {
                error = "Failed to add \"'" + recordInDatabase1 + "'\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(recordInDatabase1))
            {
                error = "Failed to update text: \"'" + recordInDatabase1 + "'\" to green color.";
                return false;
            }

            Narrator.stepPassed(SettersAndGetters.getAPIResponse());
        }

        return true;
    }

    public static boolean retrieveLoadJsonResponse()
    {
        try
        {
            SeleniumDriverInstance.startDriver();

            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURL()))
            {
                error = "Failed to navigate to couchbase URL";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.load(), 180))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.load()))
                {
                    error = "Failed to click the 1st store task link";
                    return false;
                }

                SeleniumDriverInstance.pause(2000);

                if (!SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.rawDocumentURL()))
                {
                    error = "Failed to wait for the raw document URL link";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(WebDoddlePageObjects.rawDocumentURL()))
                {
                    error = "Failed to wait for the raw document URL link";
                    return false;
                }

                SeleniumDriverInstance.pause(2000);
                SeleniumDriverInstance.Driver.navigate().back();
                SeleniumDriverInstance.pause(1000);

                line = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.rawDocumentData());
                System.out.println(line);
                SettersAndGetters.setAPIResponse(line);

            }

        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve json response - " + e.getMessage();
            return false;
        } finally
        {
            SeleniumDriverInstance.shutDown();
        }

        return true;
    }

    public static boolean checkCollectionsRecordsInDatabase(String[] itemsToValidate)
    {

        Shortcuts.doLabelValueAPICall(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getNewBearer());

        System.out.println(SettersAndGetters.getGeneratedBarCode());
        String bearer = SettersAndGetters.getNewBearer();
        String apiResponse = SettersAndGetters.getAPIResponse();

        for (String recordInDatabase1 : itemsToValidate)
        {
            if (!SettersAndGetters.getAPIResponse().contains(recordInDatabase1))
            {
                error = "Failed to add \"'" + changeFailedAPIResponseColor(recordInDatabase1) + "'\" event in CouchBase";
                return false;
            }

            if (!Shortcuts.changeAPIResponseColor(recordInDatabase1))
            {
                error = "Failed to update text: \"'" + changeFailedAPIResponseColor(recordInDatabase1) + "'\" to green color.";
                return false;
            }

            Narrator.stepPassed(SettersAndGetters.getAPIResponse());
        }

        return true;
    }

    public static boolean StorageProccess()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to wait for the storage tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageTile()))
        {
            error = "Failed to touch the storage tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeCheckBtn()))
        {
            error = "Failed to wait for the store check button";
            return false;
        }

        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to wait for the tick heavy parcel button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tickHeavyParcelButton()))
        {
            error = "Failed to touch and check 'Heavy Parcel'.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to wait for the take photo button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takePhotoButton()))
        {
            error = "Failed to touch 'Take photo'.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Photo of parcel");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to wait for 'Use' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.useThisPhotoButton()))
        {
            error = "Failed to touch 'use' button.";
            return false;
        }
        if (!scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to scan shelf barcode";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pauseStorageButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pauseStorageButton()))
            {
                error = "Failed to click on the pause storage button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to wait for the back to dashboard button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storageBackToDashboardButton()))
            {
                error = "Failed to click the back to dashboard button";
                return false;
            }
        } else
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.finishStorageButton()))
            {
                error = "Failed to touch the 'Pause storage' button.";
                return false;
            }
        }

        Narrator.stepPassed("Successfully retrieved parcel from carrier, scanned and stored parcel.");
        return true;
    }

    public static boolean checkRecordsInDatabase(String fileName)
    {
        String currentValue;
        String textToBeChecked = "";
        String keyValue = "";
        Shortcuts.databaseRecordsLabels(fileName);
        Map<String, String> recordInDatabase = SettersAndGetters.getDatabaseRecords();//getting the Map with different values
        Set<String> keyData = SettersAndGetters.getDatabaseRecords().keySet();//getting only the keys for the Map
        List<String> keyOfRecordInDatabase = new ArrayList<>(keyData);//putting the map keys in a list to be used on the Map
        Shortcuts.newBearer();
        SeleniumDriverInstance.pause(2000);
        doLabelValueAPICall(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getNewBearer());
        String bearerValue = SettersAndGetters.getAPIResponse();

        for (int i = 0; i < keyOfRecordInDatabase.size(); i++)
        {
            switch (recordInDatabase.get(keyOfRecordInDatabase.get(i)))
            {
                case "placeholder":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                case "hiddenValue":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                default:
                {
                    currentValue = recordInDatabase.get(keyOfRecordInDatabase.get(i));
                    if ((keyOfRecordInDatabase.get(i).contains(" ")))
                    {
                        String[] tempRecord = keyOfRecordInDatabase.get(i).split(" ");
                        keyValue = tempRecord[0];
                    } else
                    {
                        keyValue = keyOfRecordInDatabase.get(i);
                    }

                    textToBeChecked = "\"" + keyValue + "\"" + ":" + "\"" + currentValue + "\"";
                    break;
                }
            }

            if (!bearerValue.contains(textToBeChecked))
            {
                error = "Failed to add \"" + Shortcuts.changeFailedAPIResponseColor(textToBeChecked) + "\" event in CouchBase";
                return false;
            }
        }

        Narrator.stepPassed("Retailer event added successfully - " + Shortcuts.line);

        return true;
    }

    public static boolean initiateCollection_EmailAddress() //added by Manuel Lopes
    {

        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode("DPS" + MobileDoddlePageObjects.barcode());

        Shortcuts.preAadviceCollectionParcel(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), newBearer);

        if (!SaveBarcodesFromURL.saveBarcode(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Click on the collection tile");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to touch the 'Email' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the email option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("IncorrectEmail")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered incorrect email address");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered email address");

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked the correct button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked the ok button");
        return true;
    }

    public static boolean CollectionProcess()
    {
        int count = 0;

        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            if (count == 10)
            {
                error = "Failed to wait for the ok button";
                return false;
            }

            count++;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to touch pick parcels from storage button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Picking parcels from storage");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        AppiumDriverInstance.pause(2000);

        if (!Shortcuts.loopBarcodeCollection())
        {
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!Shortcuts.loopBarcodeScan())
        {
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to wait for the return to customer button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Complete scanning proccess");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to click the 'Return to customer' button";
            return false;
        }

        //===========================
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.yesCheckoutBtn()))
        {
            error = "Failed to wait for the yes - checkout btn to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.yesCheckoutBtn()))
        {
            error = "Failed to touch the yes - checkout btn";
            return false;
        }
        narrator.stepPassedWithScreenShot("Complete scanning proccess");
        return true;
    }

    public static boolean CollectionProcessNoCheckout()
    {
        int count = 0;

        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            if (count == 10)
            {
                error = "Failed to wait for the ok button";
                return false;
            }

            count++;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to touch pick parcels from storage button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Picking parcels from storage");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        AppiumDriverInstance.pause(2000);

        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan the barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("Complete scanning proccess");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to wait for the return to customer button'";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to click the 'Return to customer' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.yesCheckoutBtn()))
        {
            error = "Failed to wait for the checkout button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.noChooseParcelsToCheckout()))
        {
            error = "Failed to click the choose parcels to checkout button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected the 'no choose parcels to checkout' option");

        return true;
    }

    public static boolean collectionOptionCard()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cardOption(testData.getData("cardNo"))))
        {
            error = "Failed to touch the ok button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Card No");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerCheckYesButton()))
        {
            error = "Failed to wait for the 'Yes - Let's go' button to be touchable(1)";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerCheckYesButton()))
        {
            error = "Failed to touch the 'Yes - Let's go' button";
            return false;
        }

        return true;
    }

    public static boolean collectionOptionCreditCard()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.creditCardOptionButton()))
        {
            error = "Failed to touch the ok button.";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Credit card option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkYesButton()))
        {
            error = "Failed to wait for the 'Yes - Let's go' button to be touchable(2)";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkYesButton()))
        {
            error = "Failed to touch the 'Yes - Let's go' button";
            return false;
        }

        return true;
    }

    public static boolean collectionOptionPassport()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.passportOption()))
        {
            error = "Failed to touch the ok button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Passport option");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkYesButton()))
        {
            error = "Failed to wait for the 'Yes - Let's go' button to be touchable(2)";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkYesButton()))
        {
            error = "Failed to touch the 'Yes - Let's go' button";
            return false;
        }

        return true;
    }

    public static boolean collectionEndProcess()
    {
        try
        {
            AppiumDriverInstance.Driver.swipe(70, 290, 70, 410, 10);

            AppiumDriverInstance.Driver.swipe(70, 290, 115, 290, 10);

            AppiumDriverInstance.Driver.swipe(115, 290, 140, 320, 10);

            AppiumDriverInstance.Driver.swipe(140, 320, 140, 380, 10);

            AppiumDriverInstance.Driver.swipe(140, 380, 115, 410, 10);

            AppiumDriverInstance.Driver.swipe(115, 410, 70, 410, 10);

            //V for vendetta
            AppiumDriverInstance.Driver.swipe(170, 290, 205, 410, 10);

            AppiumDriverInstance.Driver.swipe(205, 410, 240, 290, 10);

            //T for Thomas
            AppiumDriverInstance.Driver.swipe(270, 290, 340, 290, 10);

            AppiumDriverInstance.Driver.swipe(305, 290, 305, 410, 10);
        } catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return false;
        }

        Narrator.stepPassedWithScreenShot("Customer Signature");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.signatureNextButton()))
        {
            error = "Failed to wait for the next step";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.signatureNextButton()))
        {
            error = "Failed to wait for the next step";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to wait for the proceed to payment button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to click the proceed to payment button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Clicked proceed to payment button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to wait for the 'Proceed to Payment' button to be touchable";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to touch the 'Proceed to Payment' button.";
            return false;
        }

        //======================================================================================================================
        AppiumDriverInstance.pause(2000);

        //======================================================================================================================
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.paymentProcessBackToDashboard()))
        {
            error = "Failed to wait for the 'Complete Collection' button to be touchable";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Back to dashboard");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.paymentProcessBackToDashboard()))
        {
            error = "Failed to touch the 'Complete Collection' button.";
            return false;
        }

        if (!AppiumDriverInstance.clearBarcodeFolder())
        {
            error = "Failed to clear the barcode folder";
            return false;

        }

        return true;
    }

    public static boolean inititateCollection_IncorrectEmail()
    {
        Narrator.stepPassedWithScreenShot("Initiating collection with incorrect email");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to touch the 'Email' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the email option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("IncorrectEmail")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered incorrect email address");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        return true;
    }

    public static boolean initiateCollection_DoddleID()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionDoddleID()))
        {
            error = "Failed to wait for the collection ID button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionDoddleID()))
        {
            error = "Failed to touch the 'collection ID' option.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterID()))
        {
            error = "Failed to wait for the enter ID field";
            return false;
        }

        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode("DPS" + MobileDoddlePageObjects.barcode());

        Shortcuts.preAadviceCollectionParcel(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), newBearer);

        if (!SaveBarcodesFromURL.saveBarcode(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterID()))
        {
            error = "Failed to click on the enter enter ID field";
            return false;
        }

        //TODO : Get doddle ID from couch base
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterID(), testData.getData("doddleID")))
        {
            error = "Failed to enter the Doddle ID : " + testData.getData("doddleID") + " into the enter Doddle ID field.";
            return false;
        }
        Narrator.stepPassedWithScreenShot(testData.TestCaseId + "Successfully entered doddle ID ");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked the correct button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        return true;
    }

    public static boolean initiateCollectionIncorrectDoddleID()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionDoddleID()))
        {
            error = "Failed to wait for the collection ID button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionDoddleID()))
        {
            error = "Failed to touch the 'collection ID' option.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterID()))
        {
            error = "Failed to wait for the enter ID field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterID()))
        {
            error = "Failed to click on the enter enter ID field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("IncorrectDoddleID")))
        {
            error = "Failed to enter Doddle ID : " + testData.getData("IncorrectDoddleID") + " into the Enter Doddle ID textbox.";
            return false;
        }
        Narrator.stepPassedWithScreenShot(testData.TestCaseId + "Successfully entered incorrect doddle ID ");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        return true;
    }

    public static boolean initiateCollection_UPI()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Failed to wait for the 'other search options' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Successfully clicked the 'other search options' button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Other search options window");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to wait for the 'UPI' option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to touch the 'UPI' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the UPI option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter UPI field";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter UPI field";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to click on the enter the UPI in the UPI Field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterUPIField(), testData.getData("IncorrectUPI")))
        {
            error = "Failed to enter UPI in the 'UPI' field";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Entered incorrect UPI");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter UPI field";
            return false;
        }

        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode("DPS" + MobileDoddlePageObjects.barcode());

        Shortcuts.preAadviceCollectionParcel(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), newBearer);

        if (!SaveBarcodesFromURL.saveBarcode(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        if (!scanAndRescanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan and rescan barcode";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Scanned and rescanned barcode");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to navigate back";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to wait for the 'UPI' option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to touch the 'UPI' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the UPI option");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to click on the enter the UPI in the UPI Field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterUPIField(), SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to enter UPI in the 'UPI' field";
            return false;
        }

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered UPI ");

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        return true;
    }

    public static boolean initiateCollectionIncorrect_UPI()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Failed to wait for the 'other search options' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Successfully clicked the 'other search options' button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Other search options window");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to wait for the 'UPI' option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to touch the 'UPI' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the UPI option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to wait for the enter UPI field";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to click on the enter the UPI in the UPI Field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterUPIField(), testData.getData("IncorrectUPI")))
        {
            error = "Failed to enter UPI in the 'UPI' field";
            return false;
        }

        //clicking on the done keyword
        Narrator.stepPassedWithScreenShot("Entered incorrect UPI");
        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        return true;
    }

    public static boolean initiateCollection_OrderID()
    {
        String order = "";
        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode("DPS" + MobileDoddlePageObjects.barcode());

        Shortcuts.preAadviceCollectionParcelSettingOrderID(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode(), newBearer);
        order = SettersAndGetters.getGeneratedBarCode();

        if (!SaveBarcodesFromURL.saveBarcode(SettersAndGetters.getGeneratedBarCode(), SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked the collection tile");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Failed to wait for the 'other search options' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Successfully clicked the 'other search options' button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected other search options");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionOrderID()))
        {
            error = "Failed to wait for the order ID button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionOrderID()))
        {
            error = "Failed to touch the 'order ID' option.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterOrderIDField()))
        {
            error = "Failed to wait for the enter order ID field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterOrderIDField()))
        {
            error = "Failed to click on the enter order ID field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterOrderIDField(), testData.getData("IncorrectOrderID")))
        {
            error = "Failed to enter order ID in the enter order ID field";
            return false;
        }

        narrator.stepPassedWithScreenShot("entered incorrect order ID ");
        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterOrderIDField()))
        {
            error = "Failed to wait for the enter order ID field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterOrderIDField()))
        {
            error = "Failed to click on the enter order ID field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterOrderIDField(), order))
        {
            error = "Failed to enter order ID in the enter order ID field";
            return false;
        }

        narrator.stepPassedWithScreenShot("Entered correct order ID ");

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        return true;

    }

    public static boolean initiateCollectionIncorrectOrderID()
    {
        //incorrect order ID
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the collecion tile");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Failed to wait for the 'other search options' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Successfully clicked the 'other search options' button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Other options screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionOrderID()))
        {
            error = "Failed to wait for the order ID button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionOrderID()))
        {
            error = "Failed to touch the 'order ID' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Selected collection order ID");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterOrderIDField()))
        {
            error = "Failed to wait for the enter order ID field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterOrderIDField()))
        {
            error = "Failed to click on the enter order ID field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterOrderIDField(), testData.getData("IncorrectOrderID")))
        {
            error = "Failed to enter order ID in the enter order ID field";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully entered incorrect order ID ");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }

        return true;
    }

    public static boolean initiateCollection_Code()
    {

        //String labelValue = SettersAndGetters.getGeneratedBarCode();
        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

        String parcel = SettersAndGetters.getGeneratedBarCode();
        Shortcuts.preAadviceCollectionParcelSettingReferenceID(barcode, MobileDoddlePageObjects.rand(), newBearer);
        Shortcuts.preAadviceCollectionParcelSettingReferenceID(parcel, SettersAndGetters.getGeneratedShelfCode(), newBearer);

        String collectionCode = SettersAndGetters.getBarCodeRefID();

        if (!SaveBarcodesFromURL.saveBarcode(parcel, SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(barcode, SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(collectionCode, SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        SettersAndGetters.setGeneratedBarCode(parcel);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked on the collection tile");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionCodeButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionCodeButton()))
        {
            error = "Failed to touch the 'Collection Code' option.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected the collection code option");

        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to scan barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("Scan collection code");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }

        narrator.stepPassedWithScreenShot("Select no for more collections");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesNoButton()))
        {
            error = "Failed to select the no button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully Loaded the \"Confirm this is the customer\" Screen");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Click the correct button");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {

            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        return true;

    }

    public static boolean initiateCollection_CodeAmazonBarcode()
    {

        String amazonBarcode;

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to click the collection tile";
            return false;
        }

        narrator.stepPassedWithScreenShot("Clicked the collection tile");

        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.amazonBarcode());
        SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

        amazonBarcode = SettersAndGetters.getGeneratedBarCode();

        //Creates a new parcel without the customer block being present in the couchbase
        Shortcuts.preAadviceCollectionParcelSettingReferenceIDNoCustomerBlock(amazonBarcode, SettersAndGetters.getGeneratedShelfCode(), newBearer);

        String collectionCode = SettersAndGetters.getBarCodeRefID();

        if (!SaveBarcodesFromURL.saveBarcode(amazonBarcode, SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!SaveBarcodesFromURL.saveBarcode(collectionCode, SettersAndGetters.getGeneratedShelfCode()))
        {
            error = "Failed to set up the amazon bar code";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionCodeButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionCodeButton()))
        {
            error = "Failed to touch the 'Collection Code' option.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Selected the collection code option option");

        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("Scan collection code");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }

        narrator.stepPassedWithScreenShot("Select no for more collection codes");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesNoButton()))
        {
            error = "Failed to select the no button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        return true;
    }

    public static boolean moreCollectionCodesNo()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreCollectionCodesNoButton()))
        {
            error = "Failed to select the no button";
            return false;
        }

        if (!SettersAndGetters.getGeneratedBarCode().startsWith("H") && !SettersAndGetters.getGeneratedBarCode().startsWith("Q"))
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
            {
                error = "Failed to wait for the correct button to appear";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
            {
                error = "Failed to clicked the correct button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to wait for the ok button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to touch the ok button";
                return false;
            }
        } else
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to wait for the ok button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to touch the ok button";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Successfully navigated to the customer parcels screen");
        return true;
    }

    public static boolean backToDashBoard()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'dashboard' option to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'dashboard' option";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
            {
                error = "Failed to touch the 'yes' button on the popup message.";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Navigate back to dashboard");

        return true;
    }

    public static boolean returnsBackToDashboard()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.menuDashboardButton()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cancelReturnsAllButton(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelReturnsAllButton()))
            {
                error = "Failed to click the cancel all returns button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerChangedMindOption(), 1))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.customerChangedMindOption()))
            {
                error = "Failed to click the customer changed mind option";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cancelBookingButton()))
            {
                error = "Failed to click the cancel booking button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
            {
                error = "Failed to touch the 'yes' button on the popup message.";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
            {
                error = "Failed to click the more returns no button";
                return false;
            }
        }

        return true;
    }

    public static boolean moreCollectionCodesYes()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to wait for the barcode alert to close and display new alert";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to select the Yes button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to wait for the correct button to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to clicked the correct button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        backToDashBoard();
        return true;
    }

    public static boolean preBookedReturnsExecution()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            Login();

            if (!preBookedReturn())
            {
                return false;
            }

        } else
        {
            if (!preBookedReturn())
            {
                return false;
            }
        }

        return true;
    }

    public static boolean preBookedReturn()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to wait for the prebooked return screen";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed click the prebooked return screen";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.preBookedReturnText()))
        {
            error = "Failed to wait for the no customer with booked returns screen";
            return false;
        }

        Narrator.stepFailedWithScreenShot("No customers with booked returns found");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.preBookedbackToDashboard()))
        {
            error = "Failed to click the dashboard button";
            return false;
        }

        return true;
    }

    public static boolean returnsExecution()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.versionText(), 1))
        {
            if (!Login())
            {
                return false;
            }

            if (!Return())
            {
                return false;
            }

        } else
        {
            if (!Return())
            {
                return false;
            }
        }

        return true;
    }

    public static boolean Return()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the returns back button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click the returns back button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        try
        {
            textField = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.DataArtNameTextField(testData.getData("ReturnRetailer"))));
            textField.click();
            textField.sendKeys(Keys.CONTROL + "a");
            textField.sendKeys(Keys.DELETE);
            Narrator.stepPassedWithScreenShot("Successfully cleared retailer selection text");
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve text";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("DVTRetailer")))
        {
            error = "Failed to enter '" + testData.getData("DVTRetailer") + "'into the Enter Retailer Name textfield";
            return false;
        }

        try
        {
            String noMatchFound = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.noMatchesFoundText());
            Narrator.stepPassedWithScreenShot("Successfully validated no matches found if retailer doesnt not exist");
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve text";
            return false;
        }

        try
        {
            textField = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.DataArtNameTextField(testData.getData("DVTRetailer"))));
            textField.click();
            textField.sendKeys(Keys.CONTROL + "a");
            textField.sendKeys(Keys.DELETE);
            Narrator.stepPassedWithScreenShot("Successfully cleared incorrect retailer selection text");
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve text";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (!retrieveItemIDFromCouchBase())
        {
            isSeleniumFailure = true;
            error = "Failed to retrieve item ID from couchbase";
            return false;
        }

        validationList = new String[]
        {
            "RETAILER_SELECTED", "HANDOVER_FROM_CUSTOMER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public static boolean Collection()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
        {
            error = "Failed to touch the button with text 'Correct'";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the 'ok' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the 'ok' button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionRetrievingParcels(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.collectionRetrievingParcels(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the 'loading screen' to disappear";
                    return false;
                }

                count++;
            }
        }

        Narrator.stepPassedWithScreenShot("");

        //======================================================================================================================
        AppiumDriverInstance.pause(2000);

        validationList = new String[]
        {
            "EXPECTED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };
        //======================================================================================================================

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.noParcelsText(), 2))
        {
            error = "There are no parcels to scan";
            Narrator.stepFailedWithScreenShot("No Parcels");
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to wait for the pick parcels from storage button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.pickParcelFromStorageButton()))
        {
            error = "Failed to touch the 'Pick parcels from storage' button.'";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the 'Pick parcels from storage' button(2).'";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the 'Pick parcels from storage' button.'";
            return false;
        }
        Narrator.stepPassedWithScreenShot(testData.TestCaseId + "- Parcel list before scanning");

        AppiumDriverInstance.pause(2000);

        if (!loopBarcodeCollection())
        {
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!loopBarcodeScan())
        {
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to wait for the 'Return to customer' message.'";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Complete scanning proccess");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnToCustomerBtn()))
        {
            error = "Failed to click the 'Return to customer' button";
            return false;
        }

        //======================================================================================================================
        AppiumDriverInstance.pause(2000);

        validationList = new String[]
        {
            "ON_SHELF"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        //======================================================================================================================
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.yesCheckoutButton()))
        {
            error = "Failed to wait for the yes checkout button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.yesCheckoutButton()))
        {
            error = "Failed to click the yes checkout button";
            return false;
        }
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.cardOption(testData.getData("cardNo")), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.cardOption(testData.getData("cardNo"))))
            {
                error = "Failed to touch the ok button.";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Card No");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerCheckYesButton()))
            {
                error = "Failed to wait for the 'Yes - Let's go' button to be touchable(1)";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerCheckYesButton()))
            {
                error = "Failed to touch the 'Yes - Let's go' button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.creditCardOptionButton(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.creditCardOptionButton()))
            {
                error = "Failed to touch the ok button.";
                return false;
            }
            Narrator.stepPassedWithScreenShot("Credit card option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to wait for the 'Yes - Let's go' button to be touchable(2)";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to touch the 'Yes - Let's go' button";
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.passportOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.passportOption()))
            {
                error = "Failed to touch the ok button.";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Passport option");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to wait for the 'Yes - Let's go' button to be touchable(2)";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkYesButton()))
            {
                error = "Failed to touch the 'Yes - Let's go' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to wait for the proceed to payment button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.procceedToPaymentButton()))
        {
            error = "Failed to click the proceed to payment button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to wait for the 'Proceed to Payment' button to be touchable";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.completeCollectionButton()))
        {
            error = "Failed to touch the 'Proceed to Payment' button.";
            return false;
        }

        //======================================================================================================================
        validationList = new String[]
        {
            "COLLECTED_BY_CUSTOMER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        //======================================================================================================================
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.paymentProcessBackToDashboard()))
        {
            error = "Failed to wait for the 'Complete Collection' button to be touchable";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.paymentProcessBackToDashboard()))
        {
            error = "Failed to touch the 'Complete Collection' button.";
            return false;
        }

        if (!AppiumDriverInstance.clearBarcodeFolder())
        {
            error = "Failed to clear the barcode folder";
            return false;
        }

        AppiumDriverInstance.pause(2000);
        //======================================================================================================================

        validationList = new String[]
        {
            "WITH_CUSTOMER"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        //======================================================================================================================
        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Back at dashboard after parcel was allocated.");
        Narrator.stepPassed("Successfully scanned, linked and handed over the parcel to the client. Another happy client!");

        return true;
    }

    public static boolean carrierProcess()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        barcode = SettersAndGetters.getGeneratedBarCode();

        //Above == 03
        //Below == 04
        Narrator.stepPassedWithScreenShot("Scanning barcode successful");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the 'back' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to touch on the 'back' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }

    public static boolean carrierProcessLaterBtn()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'OK' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'OK' button.";
            return false;
        }

        AppiumDriverInstance.pause(1000);
        TC_NextGen_03_Carrier cr = new TC_NextGen_03_Carrier(testData);
        String nbc = MobileDoddlePageObjects.randomNo();
        cr.executeBarCode(nbc, MobileDoddlePageObjects.rand());

        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            error = "Could not Scan";
            return false;
        }

        //Above == 03
        //Below == 04
        Narrator.stepPassedWithScreenShot("Scanning barcode successful");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelLater()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Store Parcel Later Button Clicked.");
//        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton())) {
//            error = "Failed to wait for the 'back' button.";
//            return false;
//        }
//        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton())) {
//            error = "Failed to touch on the 'back' button.";
//            return false;
//        }
//
//        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }

    public static boolean carrierProcessContinue()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("Carrier")))
        {
            error = "Failed to search for " + testData.getData("Carrier") + ".";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to wait for the dhl carrier to be touchable";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierDHL()))
        {
            error = "Failed to touch on the DHL tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.takeParcelFromCarrierButton()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to wait for the 'Yes' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carrierOkButton()))
        {
            error = "Failed to touch on the 'Yes' button.";
            return false;
        }
        TC_NextGen_03_Carrier cr = new TC_NextGen_03_Carrier(testData);
        AppiumDriverInstance.pause(3000);
        //===================01============
        String bb1 = MobileDoddlePageObjects.barcode();

        cr.executeBarCode(bb1, MobileDoddlePageObjects.rand());

        cr.writeToExcel(bb1, "TC_NextGen_3_Carriers_Bar.xlsx", "TC_NextGen_3_Carriers.xlsx");

        if (!ReusableFunctionalities.scanAndRescanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton(), 60))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }

        if (!checkCarriersRecordsInDatabase("TC_NextGen_3_Carriers_Bar.xlsx"))
        {
            return false;
        }
        Narrator.stepPassedWithScreenShot("1.Barcode Scanned successfully");

        //============02==============
        String bb2 = MobileDoddlePageObjects.barcode();
        cr.executeBarCode(bb2, MobileDoddlePageObjects.rand());
        cr.writeToExcel(bb2, "TC_NextGen_3_Carriers_Bar.xlsx", "TC_NextGen_3_Carriers.xlsx");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            return false;
        }
        AppiumDriverInstance.pause(3000);
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton(), 120))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!checkCarriersRecordsInDatabase("TC_NextGen_3_Carriers_Bar.xlsx"))
        {
            return false;
        }
        Narrator.stepPassedWithScreenShot("2.2nd Barcode Scanned Successfully");

        //===================03============
        String bNumber = MobileDoddlePageObjects.randomNo();
        cr.executeBarCode(bNumber, MobileDoddlePageObjects.rand());
        cr.writeToExcel(bNumber, "TC_NextGen_3_Carriers_Bar.xlsx", "TC_NextGen_3_Carriers.xlsx");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            return false;
        }
        AppiumDriverInstance.pause(3000);
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton(), 90))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!checkCarriersRecordsInDatabase("TC_NextGen_3_Carriers_Bar.xlsx"))
        {
            return false;
        }
        Narrator.stepPassedWithScreenShot("3.Number Barcode Scanned Successfully");
        //=================04=============
        String rndStrin = MobileDoddlePageObjects.randomString(10);
        cr.executeBarCode(rndStrin, MobileDoddlePageObjects.rand());
        cr.writeToExcel(rndStrin, "TC_NextGen_3_Carriers_Bar.xlsx", "TC_NextGen_3_Carriers.xlsx");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            return false;
        }
        AppiumDriverInstance.pause(3000);
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton(), 90))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!checkCarriersRecordsInDatabase("TC_NextGen_3_Carriers_Bar.xlsx"))
        {
            return false;
        }
        Narrator.stepPassedWithScreenShot("4.Text Barcode Scanned Successfully");
        //===================05============
        String txtNum = MobileDoddlePageObjects.barcode();
        cr.executeBarCode(txtNum, MobileDoddlePageObjects.rand());
        cr.writeToExcel(txtNum, "TC_NextGen_3_Carriers_Bar.xlsx", "TC_NextGen_3_Carriers.xlsx");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            return false;
        }
        AppiumDriverInstance.pause(3000);
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton(), 90))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!checkCarriersRecordsInDatabase("TC_NextGen_3_Carriers_Bar.xlsx"))
        {
            return false;
        }
        Narrator.stepPassedWithScreenShot("5.Barcode with Text And Numbers Scanned Successfully");
        //===================06============Should do alert pop-up
        //String bb =MobileDoddlePageObjects.barcode();
        cr.executeBarCode(bb1, MobileDoddlePageObjects.rand());
        cr.writeToExcel(bb1, "TC_NextGen_3_Carriers_Bar.xlsx", "TC_NextGen_3_Carriers.xlsx");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            return false;
        }
        Narrator.stepPassedWithScreenShot("6.Scanned Duplicate Barcode Successfully");
        //==========
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!scanBarcode(SettersAndGetters.getGeneratedBarCode()))
        {
            return false;
        }
        Narrator.stepPassedWithScreenShot("Scanned Duplicate Barcode Successfully");

        //==========
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to wait for the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allParcelsHaveBeenScannedButton()))
        {
            error = "Failed to touch on the 'All parcels have been scanned' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to wait for the 'Store parcels now' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.storeParcelNowButton()))
        {
            error = "Failed to touch on the 'Store parcels now' button.";
            return false;
        }
        Narrator.stepPassedWithScreenShot("Store Parcel Now Button Clicked");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to wait for the 'back' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to touch on the 'back' button.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully scanned and received parcel from carrier.");

        return true;
    }

    public static boolean Logout()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to wait for the the 'Menu' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to click on the 'Menu' button.";
            return false;
        }

//        narrator.stepPassedWithScreenShot("Successfully clicked on the Menu button");
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to wait for the 'Logout/switch User' button to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutSwitchUserBtn()))
        {
            error = "Failed to touch the 'Logout/switch User' button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to wait for the 'Are you Sure' popup to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.logoutPopUp()))
        {
            error = "Failed to touch the 'yes' button on the popup message.";
            return false;
        }
        //validation for logout proccess completed
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.mainLoginBtn()))
        {
            error = "Failed to wait for the login button.";
            return false;
        }

//        narrator.stepPassedWithScreenShot("Successfully logged out of the Doddle app");
        return true;
    }

    public static boolean singelContainer()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to wait for the 'Next' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to find the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 20))
        {

            error = "Failed because temp label screen appeared";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 4))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 4))
            {
                error = "Failed to connect to printer";
                Narrator.stepFailedWithScreenShot("error");
                AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
                return false;
            }

            try
            {
                if (!retrieveItemIDFromCouchBase())
                {
                    isSeleniumFailure = true;
                    error = "Failed to retrieve item ID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        return true;
    }

    public static boolean MultipleReturnSingleContainer()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 20))
        {
            error = "Failed because temp label screen appeared";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {

                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
            {
                Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
                AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
                return false;
            }

            try
            {
                if (!retrieveItemIDFromCouchBase())
                {
                    isSeleniumFailure = true;
                    error = "Failed to retrieve item ID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 20))
        {

            error = "Failed because temp label screen appeared";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {

                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
            {
                Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
                AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
                return false;
            }

            try
            {
                if (!retrieveItemIDFromCouchBase())
                {
                    isSeleniumFailure = true;
                    error = "Failed to retrieve item ID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        return true;
    }

    public static boolean searchWithCollectionCodes()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.searchCollection()))
        {
            error = "Failed to wait for the search button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.searchCollection()))
        {
            error = "Failed to click the search button";
            return false;
        }

        if (!SettersAndGetters.getGeneratedBarCode().startsWith("H") && !SettersAndGetters.getGeneratedBarCode().startsWith("Q"))
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.correctButton()))
            {
                error = "Failed to wait for the correct button to appear";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.correctButton()))
            {
                error = "Failed to clicked the correct button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to wait for the ok button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to touch the ok button";
                return false;
            }
        } else
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to wait for the ok button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
            {
                error = "Failed to touch the ok button";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Clicked on search collection");

        return true;
    }
    //Returns method for dispatch button

    public static boolean returnsDispatchPrecondition(String containerLabel)
    {
        if (!dispatchPrecondition())
        {
            error = "Failed to do the dispatch pre condition";
            return false;
        }

        if (!returnsToCarrierContainer(containerLabel))
        {
            error = "Failed to do the disaptch pre condition";
            return false;
        }

        if (!DispatchReturn(containerLabel))
        {
            error = "Failed to do the dispatch pre condition";
            return false;
        }
        Shortcuts.putStoreSystemUIConfigMultipleContainers();
        AppiumDriverInstance.pause(400000);
        return true;
    }

    public static boolean dispatchPrecondition()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        //Driver.pressKeyCode(AndroidKeyCode.BACK);
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'retailer' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        AppiumDriverInstance.pause(1000);

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        AppiumDriverInstance.pause(2000);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 30))
            {
                error = "Failed to wait for the progress circe to no longer be present";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 2))
        {
            error = "Failed because temp label screen appeared";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 5))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected DVT Printer");

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 5))
            {
                if (AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okMessageButton()))
                {
                    error = "Failed to click the ok pop up";
                    return false;
                }

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
                {
                    error = "Failed to click the retry button";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
                {
                    error = "Failed to click the retry button";
                    return false;
                }

            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
            {
                Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
                AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
                return false;
            }

            try
            {
                if (!retrieveItemIDFromCouchBase())
                {
                    isSeleniumFailure = true;
                    error = "Failed to retrieve item ID from couchbase";
                    return false;
                }

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                System.out.println(e.getMessage());
                return false;
            }

            if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
            {
                error = "Failed to generate and save return barcode";
                return false;
            }

            if (!Shortcuts.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
            {
                error = "Failed to scan return barcode";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
            {
                error = "Failed to wait for the returns done button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
            {
                error = "Failed to click the returns done button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
            {
                error = "Failed to wait for the any more returns popup";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
            {
                error = "Failed t!o click the more returns no button";
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard(), 2))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
                {
                    error = "Failed to touch on the 'Returns Back To Dashboard' button.";
                    return false;
                }

            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLaterButton(), 2))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsLaterButton()))
                {
                    error = "Failed to touch on the 'Store parcels now' button.";
                    return false;
                }

            }

        }

        return true;
    }

    public static boolean returnsToCarrierContainer(String containerLabel)
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.forceStartSync()))
        {
            error = "Failed to click on the syncing indicator";
            return false;
        }

        AppiumDriverInstance.pause(3000);

        if (!scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan barcode in dashboard scan";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircle(), 2))
        {
            if (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircle(), 10))
            {
                error = "Failed to wait for the progress circle to be no longer present";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.barcodeVal()))
        {
            error = "Failed to wait for barcode";
            return false;
        }

        try
        {
            barcodeVal = AppiumDriverInstance.Driver.findElement(By.id("com.doddle.concession:id/collection_item_upi"));
            temp = barcodeVal.getText().trim();
        } catch (Exception e)
        {
            System.out.println("Error - " + e.getMessage());
            return false;
        }

        if (!temp.equals(LabelValue))
        {
            error = "Failed to validate that the barcodes are the same. Barcode generated and parcel being returned is does not have the same barcode";
            return false;
        }

        narrator.stepPassedWithScreenShot("retailer and Customer Details");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moveToCarrierContainerButton()))
        {
            error = "Failed to touch the 'Move to Carrier Container' button.";
            return false;
        }
        //TODO

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to wait for the 'Allocate new Container for carrier' button.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to touch the 'Allocate new Container for carrier' button.";
            return false;
        }

        if (!scanShelfBarcode(containerLabel))
        {
            error = "Failed to scan container shelf label";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.containersForDispatchText()))
        {
            error = "Failed to wait for the 'scan Container For Dispatch' button.";
            return false;
        }
        if (!scanShelfBarcode(containerLabel))
        {
            error = "Failed to scan container shelf label";
            return false;
        }

        return true;
    }

    public static boolean DispatchReturn(String containerLabel)
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkInButton()))
        {
            error = "Failed to touch 'Check In' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to wait for the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.enterCarrierTextfield()))
        {
            error = "Failed to touch the 'Search for Carrier' textfield.";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.enterCarrierTextfield(), testData.getData("DispatchCarrier")))
        {
            error = "Failed to search for " + testData.getData("DispatchCarrier") + ".";
            return false;
        }
        try
        {
            Driver.pressKeyCode(AndroidKeyCode.BACK);
        } catch (Exception e)
        {
            error = "Failed to click the harware back button " + e.getMessage();
            return false;

        }

        narrator.stepPassedWithScreenShot("Carrier selection");
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.selectCarrier(testData.getData("DispatchCarrier"))))
        {
            error = "Failed to touch on the " + testData.getData("DispatchCarrier") + " tile.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDespatchParcel(), 2))
        {
            narrator.stepPassedWithScreenShot("Despatch parcels to this Carrier succesfully displayed");
        } else
        {
            error = "Despatch parcels to this Carrier failed to display";
            return false;
        }

        return true;
    }

    public static boolean preConditionForNoCustomerObjectandRetailerID()
    {

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenuCollectionTile()))
        {
            error = "Failed to touch Collection Tile.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Failed to wait for the 'other search options' button";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.otherSearchOptions()))
        {
            error = "Successfully clicked the 'other search options' button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Other search options window");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to wait for the 'UPI' option";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionUPI()))
        {
            error = "Failed to touch the 'UPI' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the UPI option");

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
        SettersAndGetters.setGeneratedShelfCode("DPS" + MobileDoddlePageObjects.barcode());

        String parcel = SettersAndGetters.getGeneratedBarCode();
        String shelf = SettersAndGetters.getGeneratedShelfCode();

        Shortcuts.preAadviceCollectionParcelSettingReferenceIDNoCustomerBlockNoRetailerID(parcel, shelf, newBearer);

        String collectionCode = SettersAndGetters.getBarCodeRefID();

        if (!SaveBarcodesFromURL.saveBarcode(parcel, shelf))
        {
            error = "Failed to set up the bar code";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterUPIField()))
        {
            error = "Failed to click on the enter the UPI in the UPI Field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterUPIField(), parcel))
        {
            error = "Failed to enter UPI in the 'UPI' field";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered UPI ");

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okButton()))
        {
            error = "Failed to touch the ok button";
            return false;
        }

        return true;
    }

    public static boolean multiplePopUpMessages2()
    {

        for (int i = 0; i < 3; i++)
        {
            if (!popUpMessage2())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public static boolean popUpMessage2()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
            {
                error = "Failed to touch the OK button.";
                return false;
            }
        }
        return true;
    }

    public static boolean checkShelfRecordsInDatabase(String fileName)
    {
        String currentValue;
        String textToBeChecked = "";
        String keyValue = "";
        Shortcuts.databaseRecordsLabels(fileName);
        Map<String, String> recordInDatabase = SettersAndGetters.getDatabaseRecords();//getting the Map with different values
        Set<String> keyData = SettersAndGetters.getDatabaseRecords().keySet();//getting only the keys for the Map
        List<String> keyOfRecordInDatabase = new ArrayList<>(keyData);//putting the map keys in a list to be used on the Map

        Shortcuts.doStorageLabelValueAPICall(SettersAndGetters.generatedShelfCode, SettersAndGetters.getNewBearer());
        String bearerValue = SettersAndGetters.getAPIResponse();

        for (int i = 0; i < keyOfRecordInDatabase.size(); i++)
        {
            switch (recordInDatabase.get(keyOfRecordInDatabase.get(i)))
            {
                case "placeholder":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                case "hiddenValue":
                {
                    textToBeChecked = keyOfRecordInDatabase.get(i);
                    break;
                }
                default:
                {
                    currentValue = recordInDatabase.get(keyOfRecordInDatabase.get(i));
                    if ((keyOfRecordInDatabase.get(i).contains(" ")))
                    {
                        String[] tempRecord = keyOfRecordInDatabase.get(i).split(" ");
                        keyValue = tempRecord[0];
                    } else
                    {
                        keyValue = keyOfRecordInDatabase.get(i);
                    }

                    textToBeChecked = "\"" + keyValue + "\"" + ":" + "\"" + currentValue + "\"";
                    break;
                }
            }

            if (!bearerValue.contains(textToBeChecked))
            {

                if (bearerValue.contains(textToBeChecked))
                {

                    error = "Failed to add \"" + textToBeChecked + "\" event in CouchBase";
                    return false;
                }

            }
            if (!Shortcuts.changeAPIResponseColor(textToBeChecked))
            {
                error = "Failed to update text: \"'" + textToBeChecked + "'\" to green color";
                return false;
            }
            Narrator.stepPassed("Carrier added successfully - " + Shortcuts.line);

        }

        return true;

    }

    public static boolean createCollectionBarcodeWithOutCustomerBlock()
    {
        try
        {
            Shortcuts.newBearer();

            String parcel = "";
            String shelf = "";
            String bearerToken = SettersAndGetters.getNewBearer();

            SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.amazonBarcode());
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            parcel = SettersAndGetters.getGeneratedBarCode();
            shelf = SettersAndGetters.getGeneratedShelfCode();

            Shortcuts.preAadviceCollectionParcelSettingReferenceIDNoCustomerBlock(parcel, shelf, bearerToken);

            return true;
        } catch (Exception e)
        {
            narrator.logFailure("Failed to preadvise barcode with reference ID");
            narrator.logError(e.getMessage());
            System.out.println("Failed to preadvise barcode with reference ID");
            return false;
        }
    }

    public static String customRetailerOrderID()
    {
        return "DHL-" + MobileDoddlePageObjects.randomNo() + "-" + "";
    }

    public static boolean createCollectionBarcodeWithOutCustomerBlock(String retailerOrderID)
    {
        try
        {
            String parcel = "";
            String shelf = "";
            String bearerToken = SettersAndGetters.getNewBearer();

            SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.amazonBarcode());
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            parcel = SettersAndGetters.getGeneratedBarCode();
            shelf = SettersAndGetters.getGeneratedShelfCode();

            Shortcuts.preAadviceCollectionParcelSettingOrderIDWithoutCustomerBlock(parcel, shelf, bearerToken);

            //SettersAndGetters.setGeneratedBarCode(ReusableFunctionalities.customRetailerOrderID());
            return true;
        } catch (Exception e)
        {
            narrator.logFailure("Failed to preadvise barcode with reference ID");

            narrator.logError(e.getMessage());
            System.out.println("Failed to preadvise barcode with reference ID");
            return false;
        }
    }

    public static boolean createCollectionBarcodeWithOutCustomerBlock65(String retailerOrderID)
    {
        try
        {
            String parcel = "";
            String shelf = "";
            String bearerToken = SettersAndGetters.getNewBearer();

//            SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.amazonBarcode());
//            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());
            parcel = SettersAndGetters.getGeneratedBarCode();
            shelf = SettersAndGetters.getGeneratedShelfCode();

            Shortcuts.preAadviceCollectionParcelSettingOrderIDWithoutCustomerBlock(parcel, shelf, bearerToken);

            //SettersAndGetters.setGeneratedBarCode(ReusableFunctionalities.customRetailerOrderID());
            return true;
        } catch (Exception e)
        {
            narrator.logFailure("Failed to preadvise barcode with reference ID");

            narrator.logError(e.getMessage());
            System.out.println("Failed to preadvise barcode with reference ID");
            return false;
        }
    }

    public static boolean createCollectionBarcodeWithOutCustomerBlock2(String retailerOrderID, String bb1, String shelf2)
    {
        try
        {
            String parcel = "";
            String shelf = "";
            String bearerToken = SettersAndGetters.getNewBearer();

            SettersAndGetters.setGeneratedBarCode(bb1);
            SettersAndGetters.setGeneratedShelfCode(shelf2);

            parcel = SettersAndGetters.getGeneratedBarCode();
            shelf = SettersAndGetters.getGeneratedShelfCode();

            Shortcuts.preAadviceCollectionParcelSettingOrderIDWithoutCustomerBlock2(parcel, shelf, bearerToken);

            //SettersAndGetters.setGeneratedBarCode(ReusableFunctionalities.customRetailerOrderID());
            return true;
        } catch (Exception e)
        {
            narrator.logFailure("Failed to preadvise barcode with reference ID");
            narrator.logError(e.getMessage());
            System.out.println("Failed to preadvise barcode with reference ID");
            return false;
        }
    }

    public static boolean createCollectionBarcodeWithCustomerBlock()
    {
        try
        {
            Shortcuts.newBearer();

            String parcel = "";
            String shelf = "";
            String bearerToken = SettersAndGetters.getNewBearer();

            SettersAndGetters.setGeneratedBarCode(MobileDoddlePageObjects.barcode());
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            parcel = SettersAndGetters.getGeneratedBarCode();
            shelf = SettersAndGetters.getGeneratedShelfCode();

            Shortcuts.preAadviceCollectionParcelSettingReferenceID(parcel, shelf, bearerToken);

            SettersAndGetters.setGeneratedBarCode(parcel);

            return true;
        } catch (Exception e)
        {
            narrator.logFailure("Failed to preadvise barcode with reference ID");
            narrator.logError(e.getMessage());
            System.out.println("Failed to preadvise barcode with reference ID");
            return false;
        }
    }

    public static boolean initiateExistingCustomerAccount()
    {

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to wait for the 'What does the customer have' screen to make a selection";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.emailCollectionButton()))
        {
            error = "Failed to touch the 'Email' option.";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully clicked the email option");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to wait for the enter email address field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField()))
        {
            error = "Failed to click on the enter email address field";
            return false;
        }

        //clicking on the done keyword
        WebElement element = AppiumDriverUtility.Driver.findElement(By.xpath(MobileDoddlePageObjects.footerMenu()));
        int xLocation = element.getLocation().x + 70;
        int yLocation = element.getLocation().y + 40;

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.collectionEnterEmailAddressField(), testData.getData("Email")))
        {
            error = "Failed to enter email : " + testData.getData("Email") + " into the Enter email textbox.";
            return false;
        }

        Narrator.stepPassedWithScreenShot(testData.TestCaseId + " - Successfully entered email address");

        if (!AppiumDriverInstance.tapCoordinates(xLocation, yLocation))
        {
            error = "Failed to tap at desired location";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.progressCircleXpath(), 2))
        {
            int count = 0;
            while (!AppiumDriverInstance.waitForElementNotVisible(MobileDoddlePageObjects.progressCircleXpath(), 60))
            {
                if (count == 5)
                {
                    error = "Failed to wait for the loading screen to be no longer visible";
                    return false;
                }
                count++;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmThisCustomerScreen(), 80))
        {
            error = "Failed to load the 'Confirm this is the customer' Screen.";
            return false;
        }

        return true;
    }

    public static boolean retrieveItemIDFromCouchBase()
    {

        try
        {
            AppiumDriverInstance.pause(4000);

            isSeleniumFailure = false;

            SeleniumDriverInstance.startDriver();

            if (!SeleniumDriverInstance.navigateTo(WebDoddlePageObjects.CouchBaseValidatorURL()))
            {
                error = "Failed to navigate to couchbase URL";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(WebDoddlePageObjects.itemReturnList(), 180))
            {
                itemReturn = SeleniumDriverInstance.retrieveTextByXpath(WebDoddlePageObjects.itemReturnList());
                value = itemReturn.substring(itemReturn.lastIndexOf(":") + 1).trim();
                System.out.println(value);
                SettersAndGetters.setItemID(value);
            }

            if (value.isEmpty() || value == "")
            {
                error = "Failed to retrieve the itemID from couchbase";
                return false;
            }

            value = "";

            SeleniumDriverInstance.shutDown();
        } catch (Exception e)
        {
            e.printStackTrace();
            error = "Failed to retrieve itemID - " + e.getMessage();
            System.out.println(error);
            return false;
        }

        return true;
    }

    public static boolean addParceltoCollectionPointFromReturns()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
            {
                error = "Failed to touch 'Returns' button.";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to wait for the 'New Rerurn' button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to touch on the 'New Return' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), 2))
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
            {
                error = "Failed to wait for the  'Customer Acc Number' textfield";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
            {
                error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
                return false;
            }
            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
            {
                error = "Failed to touch the 'RMA Number' textfield";
                return false;
            }
            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
            {
                error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to wait for the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
            {
                error = "Failed to click the return item code field";
                return false;
            }

            if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
            {
                error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
                return false;
            }

            Driver.pressKeyCode(AndroidKeyCode.BACK);

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
            {
                error = "Failed to touch and expand the 'reason' dropdown";
                return false;
            }

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to wait for the 'Reasons'list to appear.";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
            {
                error = "Failed to touch the 'Reason' for item return";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
            {
                error = "Failed to touch the 'Next' Button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField(), 2))
            {

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
                {
                    error = "Failed to wait for the  Enter Carrier order id textfield";
                    return false;
                }
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
                {
                    error = "Failed to touch the  Enter Carrier order id textfield";
                    return false;
                }
                if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
                {
                    error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
                    return false;
                }

                Driver.pressKeyCode(AndroidKeyCode.BACK);

                narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

                if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
                {
                    error = "Failed to wait for the confirm and print label button";
                    return false;
                }

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
                {
                    error = "Failed to touch the  'Search' Button.";
                    return false;
                }
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to wait for the customer Email to appear";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 15))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {

                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            try
            {
                if (!ReusableFunctionalities.retrieveItemIDFromCouchBase())
                {
                    ReusableFunctionalities.isSeleniumFailure = true;
                    error = "Failed to retrieve itemID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsBackToDashboard(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsBackToDashboard()))
            {
                error = "Failed to click the back to dashboard button";
                return false;
            }
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLaterButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsLaterButton()))
            {
                error = "Failed to click on the returns later button";
                return false;
            }
        }

        return true;
    }

    public boolean dispatchingContainers()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to click the carrier tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.carriersTile()))
        {
            error = "Failed to click the carrier tile";
            return false;
        }
        return true;
    }

    public static boolean retrieveReturnsLabel()
    {
        String dummyBarcode = MobileDoddlePageObjects.barcode();

        if (!SaveBarcodesFromURL.saveLabel(dummyBarcode))
        {
            error = "Failed to save a dummy barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(dummyBarcode))
        {
            error = "Failed to scan the barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLabel()))
        {
            error = "Failed to wait for the information pop up to display";
            return false;
        }

        String returnBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.returnsLabel());

        SettersAndGetters.setReturnsLabelValue(returnBarcode.substring(returnBarcode.lastIndexOf("G2")));

        if (!SaveBarcodesFromURL.saveLabel(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to save a dummy barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to click the 'try again' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.tryAgainButton()))
        {
            error = "Failed to click the try again button";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        return true;
    }

    public static boolean tapLaterFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsLaterButton()))
        {
            error = "Failed to wait for the returns later button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsLaterButton()))
        {
            error = "Failed to clicks the returns later button";
            return false;
        }

        validationList = new String[]
        {
            "CREATED", "ACTIVE"
        };

        if (!ReusableFunctionalities.checkStoreTaskRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase" + ReusableFunctionalities.error;
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        validationList = new String[]
        {
            "ASSIGN_TO_CONTAINER_DEFERRED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to validate couchbase";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        return true;
    }

    public static boolean tapNowFlow()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to wait for the returns now button";
            return false;
        }

        Narrator.stepPassedWithScreenShot("");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsNowButton()))
        {
            error = "Failed to clicks the returns now button";
            return false;
        }

        return true;
    }

    public static boolean allocatingContainerForPrebookedReturn()
    {
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to find the allocate new container button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to click the allocate new container button";
            return false;
        }

        SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

        SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

        Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

        Narrator.stepPassedWithScreenShot("Allocating new container - " + SettersAndGetters.getGeneratedShelfCode());

        if (!backToDashBoard())
        {
            error = "Failed to navigate back to the dashboard";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.moveToCarrierContainerButton(), 10))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moveToCarrierContainerButton()))
            {
                error = "Failed to click on the move to carrier container button";
                return false;
            }
            Narrator.stepPassedWithScreenShot("Select move contatiner to carrier");

        } else
        {
            error = "App took too long to display layout";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
        {
            error = "Failed to find the allocate new container button";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            if (SettersAndGetters.getReturnsLabelValue() == null)
            {
                error = "Could not print label value";
                return false;
            } else
            {
                error = "Failed to scan label value";
                return false;
            }
        }

        Narrator.stepPassedWithScreenShot("Moved return - " + SettersAndGetters.getReturnsLabelValue() + "" + "To container - " + SettersAndGetters.getGeneratedShelfCode());

        return true;
    }

    public static boolean allocatingContainer()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.allocateNewContainrForCarrierButton()))
            {
                error = "Failed to click the allocate new container button";
                return false;
            }

            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            Narrator.stepPassedWithScreenShot("Allocating new container - " + SettersAndGetters.getGeneratedShelfCode());

        } else if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.dispatchParcelCancelButton(), 2))
        {
            SettersAndGetters.setGeneratedShelfCode(MobileDoddlePageObjects.rand());

            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            Narrator.stepPassedWithScreenShot("Creating new despatch sack - " + SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton2()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Return in depsatch sack" + SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
            {
                error = "Failed to click the returns done button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
            {
                error = "Failed to click the returns done button";
                return false;
            }

        } else
        {
            String sackBarcode = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.sackBarcode());

            int a = sackBarcode.indexOf("DPS");
            int b = sackBarcode.lastIndexOf("");

            String str = sackBarcode.substring(a, b);

            SettersAndGetters.setGeneratedShelfCode(str);

            SaveBarcodesFromURL.saveShelfLabel(SettersAndGetters.getGeneratedShelfCode());

            Shortcuts.scanShelfBarcode(SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
            {
                error = "Failed to put the parcel in the despatch sack";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Return in depsatch sack" + SettersAndGetters.getGeneratedShelfCode());

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDispatchDoneButton()))
            {
                error = "Failed to click the returns done button";
                return false;
            }
        }

        return true;
    }

    public static boolean initiatePreBookedReturn()
    {
        validationList = new String[]
        {
            "BOOKING_MADE", "EXPECTED"
        };

        if (!ReusableFunctionalities.checkReturnsRecordsInDatabase(validationList))
        {
            error = "Failed to do validations";
            return false;
        }

        validationList = new String[]
        {
            ""
        };

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to wait for the returns tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.prebookedReturn()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        //Enter Email
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to click the return tile";
            return false;
        }

        //Scan reference code   
        Narrator.stepPassed("Scanning barcode - " + SettersAndGetters.getBarCodeRefID());

        if (!SaveBarcodesFromURL.saveLabel(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to save the email barcode";
            return false;
        }

        if (!Shortcuts.scanAndRescanBarcode(SettersAndGetters.getBarCodeRefID()))
        {
            error = "Failed to scan email";
            return false;
        }

        return true;
    }

    public static boolean ReturnFlow()
    {
        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnTile(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
            {
                error = "Failed to touch 'Returns' button.";
                return false;
            }
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to wait for the 'New Rerurn' button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
            {
                error = "Failed to touch on the 'New Return' button";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("Carrier") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.customerNewLookAccNumber()))
        {
            error = "Failed to wait for the  'Customer Acc Number' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.customerNewLookAccNumber(), testData.getData("AccountNumber")))
        {
            error = "Failed to enter '" + testData.getData("CustomerName") + "'into the 'Customer Name' textfield";
            return false;
        }
        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rmaNumberField()))
        {
            error = "Failed to touch the 'RMA Number' textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.rmaNumberField(), "RMA" + MobileDoddlePageObjects.randomNo()))
        {
            error = "Failed to enter 'RMA" + MobileDoddlePageObjects.randomNo() + "'into the 'RMA Number' textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to wait for the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnItemCode()))
        {
            error = "Failed to click the return item code field";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.returnItemCode(), testData.getData("ItemCode")))
        {
            error = "Failed to enter " + testData.getData("ItemCode") + " into item code field";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdown()))
        {
            error = "Failed to touch and expand the 'reason' dropdown";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to wait for the 'Reasons'list to appear.";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnReasonDropdownList(testData.getData("ReturnReason"))))
        {
            error = "Failed to touch the 'Reason' for item return";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to wait for the 'Next' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.itemReturnNextButton()))
        {
            error = "Failed to touch the 'Next' Button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to find the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 20))
        {

            error = "Failed because temp label screen appeared";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 4))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 4))
            {
                error = "Failed to connect to printer";
                Narrator.stepFailedWithScreenShot("error");
                AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
                return false;
            }

            try
            {
                if (!retrieveItemIDFromCouchBase())
                {
                    isSeleniumFailure = true;
                    error = "Failed to retrieve item ID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsYesButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        return true;
    }

    public static boolean singleContainerExclusivePoshTotty()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnTile()))
        {
            error = "Failed to touch 'Returns' button.";
            return false;
        }
        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to wait for the 'New Rerurn' button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.newReturnButton()))
        {
            error = "Failed to touch on the 'New Return' button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to wait for the 'Enter Retail Name' textfield to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailNameTextField()))
        {
            error = "Failed to touch on the 'enter retail name' textfield";
            return false;
        }

        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.retailNameTextField(), testData.getData("ReturnRetailer")))
        {
            error = "Failed to enter '" + testData.getData("ReturnRetailer") + "'into the Enter Retailer Name textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retailerSelection(), 10))
        {
            error = "Failed to wait for the 'retailer' tile to be visible";
            return false;
        }
        narrator.stepPassedWithScreenShot("Retailer name : " + testData.getData("ReturnRetailer") + ".");

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retailerSelection()))
        {
            error = "Failed to touch on the 'carrier' tile";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton(), 2))
        {
            narrator.stepPassedWithScreenShot("Scan parcel screen");
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to wait for the 'Scan Passed' button";
                return false;
            }
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.scanPassedButton()))
            {
                error = "Failed to touch the  'Scan Passed' button";
                return false;
            }

        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to wait for the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.doddleEmailTextField()))
        {
            error = "Failed to touch the  Enter Carrier order id textfield";
            return false;
        }
        if (!AppiumDriverInstance.enterTextByXpath(MobileDoddlePageObjects.doddleEmailTextField(), testData.getData("Email")))
        {
            error = "Failed to enter '" + testData.getData("Email") + "'into the Enter doddle ID/Email textfield";
            return false;
        }

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        narrator.stepPassedWithScreenShot("Customer email : " + testData.getData("Email") + " . ");

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to wait for the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.contactSearchButton()))
        {
            error = "Failed to touch the  'Search' Button.";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to find the confirm and print label button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmAndPrintLabelButton()))
        {
            error = "Failed to touch the  'Confirm and Print Label' Button.";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.scanATempLabelText(), 20))
        {

            error = "Failed because temp label screen appeared";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerOption(), 4))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.printerOption()))
            {
                error = "Failed to click the dvt printer";
                return false;
            }

            Narrator.stepPassedWithScreenShot("Successfully selected the DVT Printer option");

            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to wait for select printer button";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.confirmPrinterButton()))
            {
                error = "Failed to click the select printer button";
                return false;
            }

            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 4))
            {
                error = "Failed to connect to printer";
                Narrator.stepFailedWithScreenShot("error");
                AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
                return false;
            }

            int count = 0;

            while (count < 3)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.printerError(), 4))
                {
                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.errorAlertOkButton()))
                    {
                        error = "Failed to click away the error message";
                        return false;
                    }

                    if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.retry()))
                    {
                        error = "Failed to click on the retry button";
                        return true;
                    }

                    if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.retry()))
                    {
                        error = "Failed to click the retry button";
                        return false;
                    }
                } else
                {
                    count = 4;
                }

                count++;
            }

            try
            {
                if (!retrieveItemIDFromCouchBase())
                {
                    isSeleniumFailure = true;
                    error = "Failed to retrieve item ID from couchbase";
                    return false;
                }

                Shortcuts.newBearer();

                Shortcuts.doReturnsAPICall(SettersAndGetters.getGeneratedItemID(), SettersAndGetters.getNewBearer());

                String bearer = SettersAndGetters.getNewBearer();
                String apiResponse = SettersAndGetters.getAPIResponse();

                //Getting LabelValue
                apiResponse = SettersAndGetters.getAPIResponse();
                regex = "\"labelValue\":\"";
                lines = apiResponse.split(regex);
                liness = lines[1].split("\",\"url\":");
                apiResponse = liness[0];
                LabelValue = apiResponse;
                SettersAndGetters.setReturnsLabelValue(LabelValue);
                System.out.println(LabelValue);

            } catch (Exception e)
            {
                e.printStackTrace();
                error = "EXCEPTION - " + e.getMessage();
                return false;
            }

        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.failedToConnectText(), 2))
        {
            Narrator.stepFailedWithScreenShot("Failed to connect to printer!");
            AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.connectionErrorOkBtn());
            return false;
        }

        if (!save.saveReturnsBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to navigate and generate barcode";
            return false;
        }

        if (!ReusableFunctionalities.scanBarcode(SettersAndGetters.getReturnsLabelValue()))
        {
            error = "Failed to scan generated barcode";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to wait for the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.returnsDoneButton()))
        {
            error = "Failed to click the returns done button";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.anyMoreReturnsMessage()))
        {
            error = "Failed to wait for the any more returns popup";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.moreReturnsNoButton()))
        {
            error = "Failed to click the more returns no button";
            return false;
        }

        return true;
    }

    public static boolean checkHintsScreenIsOn()
    {
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.mainMenu()))
        {
            error = "Failed to touch Menu";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.settingsButton()))
        {
            error = "Failed to wait for the settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.settingsButton()))
        {
            error = "Failed click settings";
            return false;
        }

        if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessage(), 2))
        {
            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessage()))
            {
                error = "Failed to click pop-up";
                return false;
            }
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkedCheckBox(), 2))
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.checkBox()))
            {
                error = "Failed to find the hints screen checkbox";
                return false;
            }

            if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.checkBox()))
            {
                error = "Failed to activate the hints checkbox";
                return false;
            }

        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.backArrowButton()))
        {
            error = "Failed to click Back Arrow";
            return false;
        }

        return true;
    }
}

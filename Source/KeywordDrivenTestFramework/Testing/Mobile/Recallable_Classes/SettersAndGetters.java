/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes;

import java.util.HashMap;
import java.util.Set;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;

/**
 *
 * @author mlopes
 */
public class SettersAndGetters
{

    //List of barcodes for Collection
    public static Set<String> parcelList;

    //Other Variables
    public static HashMap<String, String> databaseRecords;
    public static String itemID;
    public static String validationText;

    //Scanning variables
    public static String generatedShelfCode;
    public static String generatedBarCode;
    public static String barCodeRefID;
    public static String labelValue;

    //New Bearer variables
    public static HttpClient client;
    public static HttpPost preadvicePost;
    public static String newBearer;
    public static String bearerValue;

    //*************************************************Setters******************************************************************
    public static void setNewBearer(String newBearer)
    {
        SettersAndGetters.newBearer = newBearer;
    }

    public static void setGeneratedBarCode(String generatedBarCode)
    {
        SettersAndGetters.generatedBarCode = generatedBarCode;
    }

    public static void setBarCodeRefID(String barcodeReferenceID)
    {
        SettersAndGetters.barCodeRefID = barcodeReferenceID;
    }

    public static void setGeneratedShelfCode(String generatedShelfCode)
    {
        SettersAndGetters.generatedShelfCode = generatedShelfCode;
    }

    public static void setPreadvicePost(HttpPost preadvicePost)
    {
        SettersAndGetters.preadvicePost = preadvicePost;
    }

    public static void setClient(HttpClient client)
    {
        SettersAndGetters.client = client;
    }

    public static void setDatabaseRecords(HashMap<String, String> databaseRecords)
    {
        SettersAndGetters.databaseRecords = databaseRecords;
    }

    public static void setAPIResponse(String bearerLine)
    {
        SettersAndGetters.bearerValue = bearerLine;
    }

    public static void setParcelList(Set<String> parcelList)
    {
        SettersAndGetters.parcelList = parcelList;
    }
    
    public static void setItemID(String itemID)
    {
        SettersAndGetters.itemID = itemID;
    }
    
    public static void setReturnsLabelValue(String labelValue)
    {
        SettersAndGetters.labelValue = labelValue;
    }
    
    public static void setValidationErrorText(String validationText)
    {
        SettersAndGetters.validationText = validationText;
    }

    //*************************************************Getters******************************************************************
    public static String getGeneratedBarCode()
    {
        return SettersAndGetters.generatedBarCode;
    }
    
    public static String getBarCodeRefID()
    {
        return SettersAndGetters.barCodeRefID;
    }

    public static String getGeneratedShelfCode()
    {
        return SettersAndGetters.generatedShelfCode;
    }

    public static String getNewBearer()
    {
        return newBearer;
    }

    public static HttpPost getPreadvicePost()
    {
        return preadvicePost;
    }

    public static HttpClient getClient()
    {
        return client;
    }

    public static HashMap<String, String> getDatabaseRecords()
    {
        return databaseRecords;
    }

    public static String getAPIResponse()
    {
        return bearerValue;
    }

    public static Set<String> getParcelList()
    {
        return parcelList;
    }
    
    public static String getGeneratedItemID()
    {
        return SettersAndGetters.itemID;
    }    
    
    public static String getReturnsLabelValue()
    {
        return SettersAndGetters.labelValue;
    }
    
    public static String getValidationErrorText()
    {
        return SettersAndGetters.validationText;
    }
    
}

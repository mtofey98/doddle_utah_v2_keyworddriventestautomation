/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author jmacauley
 */
public class OpenExtents extends BaseClass
{

    static String error = "";
//    static String year = "2017";
    static String [] paths = {};
    static String [] months = {"June","July","August"};


    public static void main(String[] args) throws MalformedURLException
    {
//        URL url = new URL("file://gtc-fs01/GTC%20Repository/Projects/Doddle/Nightly%20Runs/2017/June/1%20June%202017/Mobile/Stable/Doddle%20Mobile%20Regression_2017-06-01_07-41-34/extentReport.html");
//        openExtent(url.toString());
        
        for (int month = 0; month < months.length; month++)
        {
            try
            {
                for (int i = 1; i < 32; i++)
                {
                    getPaths("file://gtc-fs01\\GTC%20Repository\\Projects\\Doddle\\Nightly%20Runs\\2017\\", months[month], "2017", i, "Mobile","Stable"+System.getProperty("user.dir")+"\\ReportList.txt");

                }
            }
            catch(ArrayIndexOutOfBoundsException e)
            {
                System.out.println("Days of month exceeded");
                continue;
            }
            
        }
    }

    public static void openExtent(String filePath)
    {

        try
        {
            SeleniumDriverInstance = new SeleniumDriverUtility(Enums.BrowserType.Chrome);
            SeleniumDriverInstance.startDriver();
            SeleniumDriverInstance.navigateTo(filePath);
        }
        catch (Exception e)
        {
            error = e.getMessage();
            System.out.println(error);
        }
    }
    
    public static String [] getPaths(String networkLocation,String month, String date, int day,String platform, String condition)
    {
        return OpenExtents.paths;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.Mobile.Recallable_Classes;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.AppiumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.Mobile.MobileDoddlePageObjects.MobileDoddlePageObjects;
import KeywordDrivenTestFramework.Testing.Web.WebDoddlePageObjects.WebDoddlePageObjects;
import static KeywordDrivenTestFramework.Utilities.AppiumDriverUtility.Driver;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import static java.lang.System.out;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import nl.flotsam.xeger.Xeger;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author gdean
 * @author jmacauley
 */
public class Shortcuts extends BaseClass
{

    String name = "";
    LinkedList<DataRow> listOfBarcodes = new LinkedList<>();
    public static String error = "";
    static StringEntity input;
    static String temp = "";
    static Set<String> finalBarcodes = new HashSet<>();
    static int run = 0;
    static int timeout = 15;
    static int listSize = 0;
    static int listSizeBefore = 0;
    static List<WebElement> parcelListSize;
    static List<String> parcelList = new ArrayList();
    public static String line = "";
    public static String errorMessage;
    public static String newBearer = "";
    //  String randomNumber = MobileDoddlePageObjects.randomNo();
    public static String random = "";
    public static String TA38Barcode = "";
    static Narrator narrator;

    //Generating barcodes
    public static String randomNumber = MobileDoddlePageObjects.randomNo();
    public static String barcode = MobileDoddlePageObjects.barcode();
    public static String shelfBarcode = MobileDoddlePageObjects.rand();
    public static String imageURL = "";
    public static String destinationFile = "";
    public static boolean conErorr;
    static WebElement barcodeVal;
    public static String value;
    public static String store;
    static String itemReturn;
    static String regex;
    static String[] lines;
    static String[] liness;
    static String apiResponse;
    static String PDFURL;
    public static String LabelValue;
    static MobileElement textField;
    public static String[] validationList;
    static SaveBarcodesFromURL save = new SaveBarcodesFromURL();
    static String shelfID;
    public static ArrayList<String> collectionReferenceIDarr = new ArrayList<>();
    static String referenceID;
    static String retailerOrderID;
    public static ArrayList<String> orderIDArray = new ArrayList<>();
    String connectionName;
    public static String printer;

    public Shortcuts()
    {

    }

    //==========================================================This method posts the data=================================================
    public boolean restCall(String header, String headerValue, String testcase, String email, String cellNumber, String barcodeName, String randomNumber, String storeID, String cardNo, String postalCode)
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE");

        post.addHeader(header, headerValue);

        //Recieves parameters from testpack such as email, store ID etc. Inserts the post query to the API
        try
        {
            switch (testcase)
            {

                case "TA-25":
                    input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA25(postalCode, email, cellNumber, randomNumber, barcodeName, storeID));
                    break;
                case "TA-26":
                    input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA26(email, cellNumber, cardNo, randomNumber, barcodeName, storeID));
                    break;
                case "TA-30":
                    input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA30(email, cellNumber, cardNo, randomNumber, barcodeName, storeID));
                    break;
                case "TA-33":
                    input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA33(email, cellNumber, postalCode, randomNumber, barcodeName, storeID));
                    break;
                case "TA-35":
                    input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA35(postalCode, email, cellNumber, randomNumber, barcodeName, storeID));
                    break;
                case "TA-36":
                    input = new StringEntity(MobileDoddlePageObjects.jsonPayloadTA36(email, cellNumber, randomNumber, barcodeName, storeID));
                    break;
            }

            input.setContentType("application/json");
            post.setEntity(input);
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            line = rd.readLine();
            System.out.println("");

        }
        catch (UnsupportedEncodingException e)
        {
            error = "exeption: " + e.getMessage();
            return false;
        }
        catch (IOException IOE)
        {
            error = "exeption: " + IOE.getMessage();
            return false;
        }

        return true;
    }

    public boolean jsonPostTA38Start(String header, String headerValue, String contentType, String contentValue)
    {

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token/?api_key=6V0J59M1VIN94I07A3D8USAV6");

        post.addHeader(header, headerValue);
        post.addHeader(contentType, contentValue);

        try
        {
            String postValue = MobileDoddlePageObjects.jsonQRCodeTA38Start();
            input = new StringEntity(postValue);

            input.setContentType("application/json");
            post.setEntity(input);
            HttpResponse response = client.execute(post);
            Header h = post.getFirstHeader("Authorization");
            Header h1 = post.getFirstHeader("Content-Type");
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            line = rd.readLine();
            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;
            SettersAndGetters.setNewBearer(line);

        }
        catch (UnsupportedEncodingException e)
        {
            error = "exception: " + e.getMessage();
        }
        catch (IOException IOE)
        {
            error = "exception: " + IOE.getMessage();
        }

        return true;
    }

    public boolean jsonPostTA38End(String header)
    {

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://stage-api.doddle.io/v1/labels/labelType/RETURN_SHIPPING/generate/");

        post.addHeader(header, "Bearer " + newBearer);

        try
        {
            input = new StringEntity(MobileDoddlePageObjects.jsonQRCodeTA38End());

            input.setContentType("application/json");
            post.setEntity(input);
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            line = rd.readLine();
            String regex = "\",\"url\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"encodedLabelContentType\":\"");

            line = liness[0];
            System.out.println(line);

            int rand = (int) Math.round(Math.random() * 999999);
            NumberFormat formatter = new DecimalFormat("000000");
            String randomNo = formatter.format(rand);

            TA38Barcode = randomNo;

            SeleniumDriverInstance.startDriver();

            if (!SeleniumDriverInstance.navigateToTA38Url(line))
            {
                error = "Failed to navigate to generated url";
                return false;
            }

            AppiumDriverInstance.pause(5000);

            SeleniumDriverInstance.takeScreenShotNoId(System.getProperty("user.dir") + "\\Barcodes\\" + randomNo + ".png");

            SeleniumDriverInstance.shutDown();

        }
        catch (UnsupportedEncodingException e)
        {
            error = "exception: " + e.getMessage();
        }
        catch (IOException IOE)
        {
            error = "exception: " + IOE.getMessage();
        }

        return true;
    }

    public boolean saveBarcodeASImage(String barcodeName)
    {
        String imageURL = SeleniumDriverInstance.Driver.findElement(By.xpath(WebDoddlePageObjects.GeneratedBarcode())).getAttribute("src");
        String destionationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcodeName + ".png";
        saveImageUsingURL(imageURL, destionationFile);
        return true;
    }

    public boolean saveImageUsingURL(String imageURL, String destionationFile)
    {
        try
        {
            DataColumn barcodeColumn = new DataColumn("", "", Enums.ResultStatus.UNCERTAIN);

            URL url = new URL(imageURL);
            System.setProperty("jsse.enableSNIExtension", "false");
            InputStream fis = url.openStream();
            OutputStream fos = new FileOutputStream(destionationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = fis.read(b)) != -1)
            {
                fos.write(b, 0, length);
            }

            fis.close();
            fos.close();

            barcodeColumn.columnValue = convertPNGToBase64(destionationFile);
            barcodeColumn.resultStatus = Enums.ResultStatus.UNCERTAIN;
            DataRow currentRow = new DataRow();
            currentRow.DataColumns.add(barcodeColumn);
            listOfBarcodes.add(currentRow);

        }
        catch (Exception e)
        {
            error = "Failed to save image - " + e.getMessage();
            return false;
        }
        return true;
    }

    public String convertPNGToBase64(String imageFilePath)
    {
        String base64ReturnString = "";

        try
        {
            out.println("[INFO] Converting screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            out.println("[INFO] Converting completed, image ready for embedding.");
        }
        catch (Exception ex)
        {
            out.println("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }

    public static boolean loopBarcodeCollection()
    {
        while (run < timeout)
        {
            parcelListSize = AppiumDriverInstance.Driver.findElements(By.xpath(MobileDoddlePageObjects.retakePhotoItemBarcode()));
            for (int i = 0; i < parcelListSize.size(); i++)
            {
                temp = parcelListSize.get(i).getText();
                parcelList.add(temp.trim());
            }
            for (int i = 0; i < parcelList.size(); i++)
            {
                finalBarcodes.add(parcelList.get(i));

            }
            listSizeBefore = listSize;
            listSize = finalBarcodes.size();
            try
            {
                AppiumDriverInstance.Driver.swipe(237, 509, 237, 262, 2000);
            }
            catch (Exception e)
            {
                error = "Failed to scroll to the desired co-ordinates - " + e.getMessage();
                return false;
            }

            if ((listSizeBefore == listSize))
            {
                run++;
            }

        }

        run = 0;
        parcelListSize.clear();
        parcelList.clear();

        SettersAndGetters.setParcelList(finalBarcodes);

        for (String barcode : finalBarcodes)
        {

            try
            {
                save.destinationFile = System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png";
                save.url = new URL(WebDoddlePageObjects.newBarcodeGenURL(barcode));
                save.save();

            }
            catch (Exception e)
            {
                e.printStackTrace();
                error = "Failed to do the loopbarcodeCollection - " + e.getMessage();
                return false;
            }
        }

        return true;
    }

    public static boolean loopBarcodeScan()
    {
        int count = 0;
        Set<String> parcelLists = SettersAndGetters.getParcelList();

        try
        {
            for (String barcodes : parcelLists)
            {
                //Create Frame
                JFrame snakeFrame = new JFrame();
                ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcodes + ".png");
                JLabel jLabel = new JLabel(imageIcon);

                //Can set the default size here
                snakeFrame.setBounds(100, 200, 800, 800);
                java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int centerX = screenSize.width / 28;
                int centerY = (screenSize.height / 2) + 200;

                snakeFrame.setLocation(centerX, centerY);

                //Must set visible - !
                snakeFrame.setVisible(true);

                //Load image - will be autoSized.
                snakeFrame.add(jLabel);
                snakeFrame.pack();
                snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
                
                if (System.getProperty("user.name").contains("Device Farm"))
                {
                    AppiumDriverInstance.killChromeOnlyInstance();
                }
                
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();

                while (!popUpMessage())
                {
                    if (count == 0)               
                        snakeFrame.setLocation(centerX, centerY+75);

                    if (count == 1)
                        snakeFrame.setLocation(centerX+55, centerY);


                    if (count == 2)               
                        snakeFrame.setLocation(centerX, centerY+100);


                    if (count == 3)               
                        snakeFrame.setLocation(centerX+20, centerY+80);


                    if (count == 4)               
                        snakeFrame.setLocation(centerX+75, centerY+75);


                    if (count == 5)               
                        snakeFrame.setLocation(centerX, centerY-50);


                    if (count == 6)                
                        snakeFrame.setLocation(centerX-100,centerY+75);


                    if (count == 7)               
                        snakeFrame.setLocation(centerX-50, centerY);


                    if (count == 8)            
                        snakeFrame.setLocation(centerX-30, centerY+50); 


                    if (count == 9)              
                        snakeFrame.setLocation(centerX-30, centerY-70);                  


                    if (count == 10)              
                       snakeFrame.setLocation(centerX-100,centerY+75);


                    if (count == 11)
                    {                                       
                        error = "Failed to scanning barcode";
                        snakeFrame.repaint();
                        snakeFrame.dispose();
                        return false;
                    }

                    count++;

                    AppiumDriverInstance.pressHardWareButtonbyKeyCode(); //Scan again
                }

                snakeFrame.repaint();
                snakeFrame.dispose();

                Narrator.stepPassed("Successfully scanned barcode - " + barcode);
            }

            finalBarcodes.clear();
            parcelLists.clear();
            parcelListSize.clear();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean loopBarcodeScanRescan()
    {
        int count = 0;
        Set<String> parcelLists = SettersAndGetters.getParcelList();

        try
        {
            for (String barcodes : parcelLists)
            {
                //Create Frame
                JFrame snakeFrame = new JFrame();
                ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcodes + ".png");
                JLabel jLabel = new JLabel(imageIcon);

                //Can set the default size here
                snakeFrame.setBounds(100, 200, 800, 800);
                java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int centerX = screenSize.width / 28;
                int centerY = (screenSize.height / 2) + 200;

                snakeFrame.setLocation(centerX, centerY);

                //Must set visible - !
                snakeFrame.setVisible(true);

                //Load image - will be autoSized.
                snakeFrame.add(jLabel);
                snakeFrame.pack();
                snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);

                if (System.getProperty("user.name").contains("Device Farm"))
                {
                    AppiumDriverInstance.killChromeOnlyInstance();
                }
                
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();

                while (!rescanButton())
                {
                    if (count == 10)
                    {
                        error = "Failed to wait for the pop up message";
                        return false;
                    }
                    count++;
                    AppiumDriverInstance.pressHardWareButtonbyKeyCode();
                }

                count = 0;

                AppiumDriverInstance.pressHardWareButtonbyKeyCode();

                while (!popUpMessage())
                {
                    if (count == 10)
                    {
                        error = "Failed to click pop up message";
                        return true;
                    }
                    count++;
                    AppiumDriverInstance.pressHardWareButtonbyKeyCode();
                }

                AppiumDriverInstance.pause(1000);

                snakeFrame.repaint();
                snakeFrame.dispose();
            }
        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean storeCheckScanBarcode(String barcode)
    {
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
            
            if (System.getProperty("user.name").contains("Device Farm"))
            {
                AppiumDriverInstance.killChromeOnlyInstance();
            }

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.alertOkButton());

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean scanShelfBarcode(String shelfBarcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfBarcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
            
            if (System.getProperty("user.name").contains("Device Farm"))
            {
                AppiumDriverInstance.killChromeOnlyInstance();
            }
            
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!popUpMessage())
            {
                if (count == 0)               
                    snakeFrame.setLocation(centerX, centerY+75);
                               
                if (count == 1)
                    snakeFrame.setLocation(centerX+55, centerY);

                
                if (count == 2)               
                    snakeFrame.setLocation(centerX, centerY+100);
                
                
                if (count == 3)               
                    snakeFrame.setLocation(centerX+20, centerY+80);
                
                
                if (count == 4)               
                    snakeFrame.setLocation(centerX+75, centerY+75);
                
                
                if (count == 5)               
                    snakeFrame.setLocation(centerX, centerY-50);
                
                
                if (count == 6)                
                    snakeFrame.setLocation(centerX-100,centerY+75);
                
                
                if (count == 7)               
                    snakeFrame.setLocation(centerX-50, centerY);
                
                
                if (count == 8)            
                    snakeFrame.setLocation(centerX-30, centerY+50); 
                
                
                if (count == 9)              
                    snakeFrame.setLocation(centerX-30, centerY-70);                  
                
                
                if (count == 10)              
                   snakeFrame.setLocation(centerX-100,centerY+75);
                
                
                if (count == 11)
                {                                       
                    error = "Failed to scanning barcode";
                    snakeFrame.repaint();
                    snakeFrame.dispose();
                    return false;
                }

                count++;

                AppiumDriverInstance.pressHardWareButtonbyKeyCode(); //Scan again
            }

            snakeFrame.repaint();
            snakeFrame.dispose();
            
            Narrator.stepPassed("Successfully scanned barcode - " + barcode);

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean scanShelfBarcodeAppCrash(String shelfBarcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfBarcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            if (!popUpMessage())
            {
                error = "Failed to handle the pop up message";
                return false;
            }

            AppiumDriverInstance.pause(1000);

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean rescanShelfBarcodeAndWait(String shelfBarcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfBarcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!rescanButton())
            {
                if (count != 10)
                {
                    error = "Failed to wait for the pop up message";
                    return false;
                }

                count++;
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            }
            count = 0;
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            AppiumDriverInstance.pause(10000);

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean scanAndRescanShelfBarcode(String shelfBarcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\ShelfLabels\\" + shelfBarcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!rescanButton())
            {
                if (count == 10)
                {
                    error = "Failed to wait for the pop up message";
                    return false;
                }

                count++;
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            }
            count = 0;
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!popUpMessage())
            {
                if (count == 10)
                {
                    error = "Failed to click pop up message";
                    return true;
                }
                count++;
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            }

            AppiumDriverInstance.pause(1000);

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }
//=======================================================Scan barcodes====================================================    

    public static boolean scanBarcode(String barcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            AppiumDriverInstance.pause(1500);
            //Must set visible - !
            snakeFrame.setVisible(true);
            snakeFrame.toFront();
            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
                        
            if (System.getProperty("user.name").contains("Device Farm"))
            {
                AppiumDriverInstance.killChromeOnlyInstance();
            }
            
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!popUpMessage())
            {
                //Shift barcode around
                if (count == 0)               
                    snakeFrame.setLocation(centerX, centerY+75);
                               
                if (count == 1)
                    snakeFrame.setLocation(centerX+55, centerY);

                
                if (count == 2)               
                    snakeFrame.setLocation(centerX, centerY+100);
                
                
                if (count == 3)               
                    snakeFrame.setLocation(centerX+20, centerY+80);
                
                
                if (count == 4)               
                    snakeFrame.setLocation(centerX+75, centerY+75);
                
                
                if (count == 5)               
                    snakeFrame.setLocation(centerX, centerY-50);
                
                
                if (count == 6)                
                    snakeFrame.setLocation(centerX-100,centerY+75);
                
                
                if (count == 7)               
                    snakeFrame.setLocation(centerX-50, centerY);
                
                
                if (count == 8)            
                    snakeFrame.setLocation(centerX-30, centerY+50); 
                
                
                if (count == 9)              
                    snakeFrame.setLocation(centerX-30, centerY-70);                  
                
                
                if (count == 10)              
                   snakeFrame.setLocation(centerX-100,centerY+75);
                
                
                if (count == 11)
                {                                       
                    error = "Failed to scanning barcode";
                    snakeFrame.repaint();
                    snakeFrame.dispose();
                    return false;
                }

                count++;

                AppiumDriverInstance.pressHardWareButtonbyKeyCode(); //Scan again
            }

            snakeFrame.repaint();
            snakeFrame.dispose();
            
            Narrator.stepPassed("Successfully scanned barcode - " + barcode);

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean scan(String barcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
            
            if (System.getProperty("user.name").contains("Device Farm"))
            {
                AppiumDriverInstance.killChromeOnlyInstance();
            }
            
            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!popupMessageHalt())
            {
                if (count == 0)               
                    snakeFrame.setLocation(centerX, centerY+75);
                               
                if (count == 1)
                    snakeFrame.setLocation(centerX+55, centerY);

                
                if (count == 2)               
                    snakeFrame.setLocation(centerX, centerY+100);
                
                
                if (count == 3)               
                    snakeFrame.setLocation(centerX+20, centerY+80);
                
                
                if (count == 4)               
                    snakeFrame.setLocation(centerX+75, centerY+75);
                
                
                if (count == 5)               
                    snakeFrame.setLocation(centerX, centerY-50);
                
                
                if (count == 6)                
                    snakeFrame.setLocation(centerX-100,centerY+75);
                
                
                if (count == 7)               
                    snakeFrame.setLocation(centerX-50, centerY);
                
                
                if (count == 8)            
                    snakeFrame.setLocation(centerX-30, centerY+50); 
                
                
                if (count == 9)              
                    snakeFrame.setLocation(centerX-30, centerY-70);                  
                
                
                if (count == 10)              
                   snakeFrame.setLocation(centerX-100,centerY+75);
                
                
                if (count == 11)
                {                                       
                    error = "Failed to scanning barcode";
                    snakeFrame.repaint();
                    snakeFrame.dispose();
                    return false;
                }

                count++;

                AppiumDriverInstance.pressHardWareButtonbyKeyCode(); //Scan again
            }
            
            AppiumDriverInstance.pause(15000);

            snakeFrame.repaint();
            snakeFrame.dispose();
            
            Narrator.stepPassed("Successfully scanned barcode - " + barcode);
        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean scanAndRescanBarcode(String barcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);
            
            if (System.getProperty("user.name").contains("Device Farm"))
            {
                AppiumDriverInstance.killChromeOnlyInstance();
            }

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!popUpMessageRescan())
            {
                if (count == 0)               
                    snakeFrame.setLocation(centerX, centerY+75);

                if (count == 1)
                    snakeFrame.setLocation(centerX+55, centerY);


                if (count == 2)               
                    snakeFrame.setLocation(centerX, centerY+100);


                if (count == 3)               
                    snakeFrame.setLocation(centerX+20, centerY+80);


                if (count == 4)               
                    snakeFrame.setLocation(centerX+75, centerY+75);


                if (count == 5)               
                    snakeFrame.setLocation(centerX, centerY-50);


                if (count == 6)                
                    snakeFrame.setLocation(centerX-100,centerY+75);


                if (count == 7)               
                    snakeFrame.setLocation(centerX-50, centerY);


                if (count == 8)            
                    snakeFrame.setLocation(centerX-30, centerY+50); 


                if (count == 9)              
                    snakeFrame.setLocation(centerX-30, centerY-70);                  


                if (count == 10)              
                   snakeFrame.setLocation(centerX-100,centerY+75);


                if (count == 11)
                {                                       
                    error = "Failed to scanning barcode";
                    snakeFrame.repaint();
                    snakeFrame.dispose();
                    return false;
                }
                
                count++;
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            }
         
            snakeFrame.repaint();
            snakeFrame.dispose();

            Narrator.stepPassed("Successfully scanned barcode - " + barcode);
            
        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean rescanBarcodeAndWait(String barcode)
    {
        int count = 0;
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = screenSize.width / 28;
            int centerY = (screenSize.height / 2) + 200;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();
            snakeFrame.setSize(jLabel.getSize().width + 100, jLabel.getSize().height + 100);

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            while (!rescanButton())
            {
                if (count == 10)
                {
                    error = "Failed to wait for the pop up message";
                    return false;
                }

                count++;
                AppiumDriverInstance.pressHardWareButtonbyKeyCode();
            }
            count = 0;

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            AppiumDriverInstance.pause(10000);

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean scanTA38Barcode(String barcode)
    {
        try
        {
            //Create Frame
            JFrame snakeFrame = new JFrame();
            ImageIcon imageIcon = new ImageIcon(System.getProperty("user.dir") + "\\Barcodes\\" + barcode + ".png");
            JLabel jLabel = new JLabel(imageIcon);

            //Can set the default size here
            //snakeFrame.setBounds(100, 200, 800, 800);
            java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = 50;
            int centerY = 600;

            snakeFrame.setLocation(centerX, centerY);

            //Must set visible - !
            snakeFrame.setVisible(true);

            //Load image - will be autoSized.
            snakeFrame.add(jLabel);
            snakeFrame.pack();

            AppiumDriverInstance.pressHardWareButtonbyKeyCode();

            if (!popUpMessage())
            {
                error = "Failed to click pop up message";
                return true;
            }

            snakeFrame.repaint();
            snakeFrame.dispose();

        }
        catch (Exception e)
        {
            narrator.logError("Failed to press Hardware Scan button." + e.getMessage());
            return false;
        }

        return true;
    }

    //===================================================Notification====================================================
    public static boolean popUpMessage()
    {
        int count = 0;

        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 1))
        {
            if (count == 2)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.crashWait(), 1))
                {
                    error = "App crashed";
                    return false;
                }
                
                else
                {                                 
                    error = "Failed load the confirmation dialog.";
                    return false;
                }
            }
            count++;

        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
        {
            error = "Failed to touch the OK button.";
            return false;
        }
        return true;
    }
    
    public static boolean popUpMessageRescan()
    {
        int count = 0;

        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.rescanButton(), 2))
        {
            if (count != 10)
            {
                error = "Failed to wait for the pop up message";
                return false;
            }
            count++;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rescanButton()))
        {
            error = "Failed to click on the rescanButton";
            return false;
        }
        
        AppiumDriverInstance.pressHardWareButtonbyKeyCode();
        
        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.okPopupMessageButton()))
        {
            error = "Failed to touch the OK button.";
            return false;
        }
        
        return true;
    }
    
    public static boolean popupMessageHalt()
    {
        int count = 0;

        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.okPopupMessageButton(), 1))
        {
            if (count == 2)
            {
                if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.crashWait(), 1))
                {
                    error = "App crashed";
                    return false;
                }
                
                else
                {                                 
                    error = "Failed load the confirmation dialog.";
                    return false;
                }
            }
            count++;

        }
        return true;
    }

    public static boolean rescanButton()
    {
        int count = 0;

        while (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.rescanButton(), 2))
        {
            if (count != 10)
            {
                error = "Failed to wait for the pop up message";
                return false;
            }
            count++;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.rescanButton()))
        {
            error = "Failed to click on the rescanButton";
            return false;
        }
        return true;
    }
    
    public boolean CorrectStoreSwipe(Narrator narrator, int startX, int startY, int endX, int endY)
    {
        String temp = "";
        int counter = 0;
        TouchAction action = new TouchAction(Driver);

        while (counter < 10)
        {

            List<MobileElement> list = AppiumDriverInstance.Driver.findElements(By.id(MobileDoddlePageObjects.storeList()));

            // ============================================== SWIPE DOWN ================================================
            for (int i = 0; i < list.size(); i++)
            {
                temp = list.get(i).getText();
                if (temp.equals(narrator.getData("StoreSelection")))
                {
                    break;
                }

            }
            if (temp.equals(narrator.getData("StoreSelection")))
            {

                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(narrator.getData("StoreSelection"))))
                {
                    error = "Failed to select " + narrator.getData("StoreSelection") + " from list.";
                    return false;
                }
                if (!popUpMessage())
                {
                    error = "Failed to handle popup message";
                    return false;
                }
                break;
            }

            if (counter <= 5)
            {
                action.longPress(startX, startY).moveTo(endX, endY).release().perform();
            }
            else
            {
                // ============================================== SWIPE UP ================================================

                for (int i = 0; i < 5; i++)
                {
                    for (int t = 0; t < list.size(); t++)
                    {
                        temp = list.get(t).getText();
                        if (temp.equals(narrator.getData("StoreSelection")))
                        {
                            break;
                        }

                    }

                    if (temp.equals(narrator.getData("StoreSelection")))
                    {

                        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.spinnerOptionSelection(narrator.getData("StoreSelection"))))
                        {
                            error = "Failed to select " + narrator.getData("StoreSelection") + " from list.";
                            return false;
                        }
                        if (!popUpMessage())
                        {
                            error = "Failed to handle popup message";
                            return false;
                        }
                        break;
                    }
                    else
                    {
                        action.longPress(endX, endY).moveTo(startX, startY).release().perform();
                    }

                }
                break;
            }

            if (!popUpMessage())
            {
                error = "Failed to handle popup message";
                return false;
            }
            counter++;
        }
        return true;
    }

    public boolean clearBarcodeFolder()
    {
        File index = new File(System.getProperty("user.dir") + "\\Barcodes");

        String[] entries = index.list();
        for (String s : entries)
        {
            File currentFile = new File(index.getPath(), s);
            currentFile.delete();
        }

        return true;
    }

    public boolean wifiSwitchOff()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }

            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
            String wifiText = element.getAttribute("name");

            System.out.println("");

            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                {
                    error = "Failed to click the wifi button with index";
                    return false;

                }

            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiDisconnected()))
        {
            error = "Failed to wait for the wifi to be disconnected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned off Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public boolean wifiSwitchOn()
    {
        try
        {
            Driver.openNotifications();
        }
        catch (Exception e)
        {
            error = "Failed to open notifications - " + e;
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to wait for the quick settings button";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.quickSettingsBtn()))
        {
            error = "Failed to click the quick settings button";
            return false;
        }

        try
        {
            if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
            {
                error = "Failed to wait for the wifi button with index to be visible";
                return false;
            }

            MobileElement element = (MobileElement) AppiumDriverInstance.Driver.findElement(By.xpath(MobileDoddlePageObjects.wifiBtnWithIndex()));
            String wifiText = element.getAttribute("name");

            System.out.println("");

            if (wifiText.contains("Wi-Fi") || wifiText.contains("WiFi"))
            {
                if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiBtnWithIndex()))
                {
                    error = "Failed to click the wifi button with index";
                    return false;

                }

            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to wait for the wifi switch to be visible";
            return false;
        }

        if (!AppiumDriverInstance.clickElementbyXpath(MobileDoddlePageObjects.wifiSwitch()))
        {
            error = "Failed to click wifi switch";
            return false;
        }

        if (!AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.wifiConnected()))
        {
            error = "Failed to wait for the wifi to be connected";
            return false;
        }

        Narrator.stepPassedWithScreenShot("Successfully turned on Wifi");

        Driver.pressKeyCode(AndroidKeyCode.BACK);

        AppiumDriverInstance.pause(2000);

        return true;
    }

    public static boolean putPreadvisedSkipPhotoConfigFalse()
    {
        System.out.println(MobileDoddlePageObjects.preadvisedSkipPhotoConfigFalse());
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.preadvisedSkipPhotoConfigFalse());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            changeAPIResponseColor("ifPreadvisedSkipPhoto\": false");

            System.out.println(line);
            Narrator.stepPassed(line);
            Narrator.stepPassed(MobileDoddlePageObjects.preadvisedSkipPhotoConfigFalse());
//            if (!line.contains("MULTIPLE_CONTAINERS"))
//            {
//                narrator.testFailed(line);
//                return false;
//            }
//
//            Narrator.stepPassed(line);
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public static boolean putPreadvisedSkipPhotoConfigTrue()
    {
        System.out.println(MobileDoddlePageObjects.preadvisedSkipPhotoConfigTrue());
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.preadvisedSkipPhotoConfigTrue());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            changeAPIResponseColor("ifPreadvisedSkipPhoto\": true");

            System.out.println(line);
            Narrator.stepPassed(line);
            Narrator.stepPassed(MobileDoddlePageObjects.preadvisedSkipPhotoConfigTrue());
//            if (!line.contains("MULTIPLE_CONTAINERS"))
//            {
//                narrator.testFailed(line);
//                return false;
//            }
//
//            Narrator.stepPassed(line);
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public static boolean getPreadvisedDisableParcelItemisation()
    {
        System.out.println(MobileDoddlePageObjects.preadvicedDisableParcelItemisation());
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.preadvicedDisableParcelItemisation());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);

//            if (!line.contains("MULTIPLE_CONTAINERS"))
//            {
//                narrator.testFailed(line);
//                return false;
//            }
//
//            Narrator.stepPassed(line);
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public static boolean multiplePopUpMessages()
    {

        for (int i = 0; i < 5; i++)
        {
            if (!popUpMessage())
            {
                error = "Failed to handle alert popups";
                return false;
            }
        }

        return true;
    }

    public static boolean newBearer()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost preadvicePost = new HttpPost("https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        SettersAndGetters.setPreadvicePost(preadvicePost);
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");

            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;
            SettersAndGetters.setNewBearer(newBearer);
            System.out.println(newBearer);

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            errorMessage = error;
            return false;
        }

        return true;
    }

    public static boolean returnsNewBearer()
    {
        HttpClient client = HttpClientBuilder.create().build();

        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=store-returns-preadvice-write");

            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;
            SettersAndGetters.setNewBearer(newBearer);
            System.out.println(newBearer);

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            errorMessage = error;
            return false;
        }

        return true;
    }

//    public static boolean returnsBearerToken()
//    {
//        HttpClient client = HttpClientBuilder.create().build();
//        HttpPost preadvicePost = new HttpPost("https://stage-apigw.doddle.it/v1/parcels/preadvice?api_key=40AGV34DJUO153Z5DDJAJ23PE");
//        SettersAndGetters.setPreadvicePost(preadvicePost);
//        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
//        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
//        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());
//
//        try
//        {
//            input = new StringEntity("grant_type=client_credentials&scope=store-returns-preadvice-write");
//
//            input.setContentType("application/json");
//            tokenPost.setEntity(input);
//            HttpResponse response = client.execute(tokenPost);
//            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//            line = rd.readLine();
//
//            String regex = "\\{\"access_token\":\"";
//            String[] lines = line.split(regex);
//            String[] liness = lines[1].split("\",\"token_type\"");
//
//            line = liness[0];
//            newBearer = line;
//            SettersAndGetters.setNewBearer(newBearer);
//            System.out.println(newBearer);
//
//            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
//            {
//                narrator.testFailed(line);
//                return false;
//            }
//        }
//        catch (Exception e)
//        {
//            error = "exeption: " + e.getMessage();
//            errorMessage = error;
//            return false;
//        }
//
//        return true;
//    }

    public static void preAadviceCollectionParcel(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBody(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void preAadviceCollectionParcelItemID(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBody(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String referenceID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern = Pattern.compile("itemId:(.*?)preadvisedDelivery");;
            Matcher matcher = pattern.matcher(referenceID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            referenceID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setBarCodeRefID(referenceID);
            collectionReferenceIDarr.clear();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void preAadviceCollectionParcelSettingReferenceID(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBody(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String referenceID = line.replaceAll("[-+.^:,\"]", "");
            Pattern pattern = Pattern.compile("referenceId(.*)}}.*[^0-9].*");
            Matcher matcher = pattern.matcher(referenceID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            referenceID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setBarCodeRefID(referenceID);
            collectionReferenceIDarr.clear();

            String itemID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");
            matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    //2
    public static void preAadviceCollectionParcelSettingOrderID2(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBody2(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String retailerOrderID = line.replaceAll("[+.^,\"]", "");
            //Pattern pattern = Pattern.compile("retailerOrderId(.*)}}.*[^0-9].*");
            Pattern pattern = Pattern.compile("retailerOrderId:(.*?)expectedDate");
            Matcher matcher = pattern.matcher(retailerOrderID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                orderIDArray.add(matcher.group(1));
            }

            retailerOrderID = orderIDArray.get(0);
            SettersAndGetters.setGeneratedBarCode(retailerOrderID);
            orderIDArray.clear();
            String itemID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");
            matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    //3
    public static void preAadviceCollectionParcelSettingOrderID3(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBody3(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String retailerOrderID = line.replaceAll("[+.^,\"]", "");
            //Pattern pattern = Pattern.compile("retailerOrderId(.*)}}.*[^0-9].*");
            Pattern pattern = Pattern.compile("retailerOrderId:(.*?)expectedDate");;
            Matcher matcher = pattern.matcher(retailerOrderID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                orderIDArray.add(matcher.group(1));
            }
            retailerOrderID = orderIDArray.get(0);
            SettersAndGetters.setGeneratedBarCode(retailerOrderID);
            orderIDArray.clear();
            String itemID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");
            matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void preAadviceCollectionParcelSettingOrderID(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBody(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String retailerOrderID = line.replaceAll("[+.^,\"]", "");
            //Pattern pattern = Pattern.compile("retailerOrderId(.*)}}.*[^0-9].*");
            Pattern pattern = Pattern.compile("retailerOrderId:(.*?)expectedDate");;
            Matcher matcher = pattern.matcher(retailerOrderID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                orderIDArray.add(matcher.group(1));
            }
            retailerOrderID = orderIDArray.get(0);
            SettersAndGetters.setGeneratedBarCode(retailerOrderID);
            orderIDArray.clear();
            String itemID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");
            matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void preAadviceCollectionParcelSettingOrderIDWithoutCustomerBlock(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBodyNoCustomerBlock(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String retailerOrderID = line.replaceAll("[+.^,\"]", "");
            //Pattern pattern = Pattern.compile("retailerOrderId(.*)}}.*[^0-9].*");
            Pattern pattern = Pattern.compile("retailerOrderId:(.*?)expectedDate");;
            Matcher matcher = pattern.matcher(retailerOrderID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                orderIDArray.add(matcher.group(1));
            }
            retailerOrderID = orderIDArray.get(0);
            SettersAndGetters.setGeneratedBarCode(retailerOrderID);
            orderIDArray.clear();

            String itemID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");;
            matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    public static void preAadviceCollectionParcelSettingOrderIDWithoutCustomerBlock2(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBodyNoCustomerBlock2(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String retailerOrderID = line.replaceAll("[+.^,\"]", "");
            //Pattern pattern = Pattern.compile("retailerOrderId(.*)}}.*[^0-9].*");
            Pattern pattern = Pattern.compile("retailerOrderId:(.*?)expectedDate");;
            Matcher matcher = pattern.matcher(retailerOrderID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                orderIDArray.add(matcher.group(1));
            }
            retailerOrderID = orderIDArray.get(0);
            //SettersAndGetters.setGeneratedBarCode(retailerOrderID);
            orderIDArray.clear();

            String itemID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");;
            matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    public static void preAadviceCollectionParcelSettingReferenceIDNoCustomerBlock(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBodyNoCustomerBlock(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
            String referenceID = line.replaceAll("[-+.^:,\"]", "");
            Pattern pattern = Pattern.compile("referenceId(.*)}}.*[^0-9].*");
            Matcher matcher = pattern.matcher(referenceID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            referenceID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setBarCodeRefID(referenceID);
            collectionReferenceIDarr.clear();

            String itemID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");;
            matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static boolean preAdvicePreBookedReturn(String clientVersion, String deviceIdentifier, String newBearer, String email, String storeId, String retailerId, String value)
    {
        returnsBearerToken();
        
//        newBearer = SettersAndGetters.getNewBearer();
//
//        value = "12345678";
//        retailerId = "ASOS";
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-api.doddle.io/v1-store-returns/preadvice");
            httpPost.addHeader(header, headerValue);
            String body = "{\n"
                    + "    \"meta\" : {\n"
                    + "         \"clientVersion\" : \"10.1.2\",\n"
                    + "         \"deviceIdentifier\": \"16201522506977\"\n"
                    + "},    \"resource\" : {\n"
                    + "        \"customer\" : {\n"
                    + "        \"email\" : \"mootest@doddle.test\",\n"
                    + "    \"name\" : {\n"
                    + "         \"firstName\" : \"Barry\",\n"
                    + "         \"lastName\" : \"Scott\"\n"
                    + "}},    \"location\" : {\n"
                    + "         \"storeId\" : \"456DVT\"\n"
                    + "},    \"retailerId\" : \"ASOS\", \n"
                    + "    \"retailerItemData\": {\n"
                    + "     \"returnReferences\": {\n"
                    + "          \"ref1\": {\n"
                    + "             \"title\": \"ASOS Order Number\", \n"
                    + "             \"value\": \"12345678\"\n"
                    + "    }\n"
                    + "   }\n"
                    + "  }\n"
                    + " }\n"
                    + "}";
            input = new StringEntity(body);
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println("");
            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
//            String referenceID = line.replaceAll("[-+.^:,\"]", "");
//            Pattern pattern = Pattern.compile("referenceId(.*)}}.*[^0-9].*");
//            Matcher matcher = pattern.matcher(referenceID);
//
//            while (matcher.find())
//            {
//                System.out.println(matcher.group(1));
//                collectionReferenceIDarr.add(matcher.group(1));
//            }
//            referenceID = collectionReferenceIDarr.get(0);
//            SettersAndGetters.setBarCodeRefID(referenceID);
//            collectionReferenceIDarr.clear();
//
//            String itemID = line.replaceAll("[+.^,\"]", "");
//            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");;
//            matcher = pattern2.matcher(itemID);
//
//            while (matcher.find())
//            {
//                System.out.println(matcher.group(1));
//                collectionReferenceIDarr.add(matcher.group(1));
//            }
//            itemID = collectionReferenceIDarr.get(0);
//            SettersAndGetters.setItemID(itemID);
//            collectionReferenceIDarr.clear();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public static void preAadviceCollectionParcelSettingReferenceIDNoCustomerBlockNoRetailerID(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionPreAdviceBodyNoRetialerID(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String referenceID = line.replaceAll("[-+.^:,\"]", "");
            Pattern pattern = Pattern.compile("referenceId(.*)}}.*[^0-9].*");
            Matcher matcher = pattern.matcher(referenceID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            referenceID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setBarCodeRefID(referenceID);
            collectionReferenceIDarr.clear();

            String itemID = line.replaceAll("[+.^,\"]", "");
            Pattern pattern2 = Pattern.compile("itemId:(.*?)preadvisedDelivery");
            matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void doReturnsAPICall(String itemID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet httpGET = new HttpGet("https://stage-apigw.doddle.it/v1/items/returns/" + itemID + "?api_key=40AGV34DJUO153Z5DDJAJ23PE");
            httpGET.addHeader(header, headerValue);

            HttpResponse response = client.execute(httpGET);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void getDisabledInputAPICall()
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet httpGET = new HttpGet("https://stage-apigw.doddle.it/v3/retailers/ASOS");
            httpGET.addHeader(header, headerValue);

            HttpResponse response = client.execute(httpGET);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void doCollectionsAPICall(String itemID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet httpGET = new HttpGet("https://stage-apigw.doddle.it/v1/items/collections/" + itemID + "?api_key=40AGV34DJUO153Z5DDJAJ23PE");
            httpGET.addHeader(header, headerValue);

            HttpResponse response = client.execute(httpGET);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void doLabelValueAPICall(String itemID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet httpGET = new HttpGet("https://stage-apigw.doddle.it/v1/items/collections/labelValue/" + itemID + "?api_key=40AGV34DJUO153Z5DDJAJ23PE");
            httpGET.addHeader(header, headerValue);

            HttpResponse response = client.execute(httpGET);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void doStorageLabelValueAPICall(String itemID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet httpGET = new HttpGet("https://stage-apigw.doddle.it/v1/items/labelValue/" + itemID + "?api_key=40AGV34DJUO153Z5DDJAJ23PE");
            httpGET.addHeader(header, headerValue);

            HttpResponse response = client.execute(httpGET);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void doCarrierDHLAPICall(String itemID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet httpGET = new HttpGet("https://stage-apigw.doddle.it/v1/carriers/DHL");
            httpGET.addHeader(header, headerValue);

            HttpResponse response = client.execute(httpGET);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static boolean changeAPIResponseColor(String textToChange)
    {
        try
        {
            //String replace = SettersAndGetters.getAPIResponse();
            Shortcuts.line = Shortcuts.line.replace(textToChange, "<span style=\"color:#3eb24d; \">" + textToChange + "</span>");
            SettersAndGetters.setAPIResponse(Shortcuts.line);
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static boolean changeValidationErrorMessageColor(String textToChange)
    {
        try
        {
            SettersAndGetters.setValidationErrorText(SettersAndGetters.getValidationErrorText().replace(textToChange, "<span style=\"color:#c0392b; \">" + textToChange + "</span>"));

            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static boolean putStoreSystemUIConfigSingleContainer()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.singleContainerStoreConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);

            if (!line.contains("SINGLE_CONTAINER"))
            {
                narrator.testFailed(line);
                return false;
            }

            Narrator.stepPassed(line);
            Narrator.stepPassed(MobileDoddlePageObjects.singleContainerStoreConfig());
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public static boolean putStoreSystemUIConfigMultipleContainers()
    {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.multipleContainerStoreConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);

            if (!line.contains("MULTIPLE_CONTAINERS"))
            {
                narrator.testFailed(line);
                return false;
            }

            Narrator.stepPassed(line);
            Narrator.stepPassed(MobileDoddlePageObjects.multipleContainerStoreConfig());
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public static boolean scrollToSubElement(String text)
    {
        try
        {
            Driver.findElementById("com.doddle.concession:id/login_store_spinner").click();

            if (!ReusableFunctionalities.multiplePopUpMessages2())
            {
                error = "Failed to handle popup";
                return false;
            }

//         WebElement list = Driver.findElement(By.className("android.widget.ListView"));
            if (AppiumDriverInstance.waitForElementByXpath(MobileDoddlePageObjects.storeSelection(text), 2))
            {
                Driver.findElementByXPath(MobileDoddlePageObjects.storeSelection(text)).click();

            }
            else
            {
                Driver.scrollTo(text).click();
            }
            return true;

        }
        catch (Exception e)
        {
            e.printStackTrace();
            error = "EXCEPTION - " + e.getMessage();
            System.out.println(error);
            return false;
        }

    }

    public static boolean databaseRecordsLabels(String fileName)
    {
        int count = 1;
        String keyValue = "";
        String value = "";
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> keyData = new ArrayList<>();
        String directory = System.getProperty("user.dir") + "//DatabaseRecordsLabels//" + fileName;
        HashMap<String, String> databaseRecords = new HashMap<>();

        try
        {
            File spreadSheet = new File(directory);
            FileInputStream file = new FileInputStream(spreadSheet);
            XSSFWorkbook excellWorkbook = new XSSFWorkbook(file);

            XSSFSheet sheet = excellWorkbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();

            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();

                //for each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext())
                {
                    Cell cells = cellIterator.next();

                    switch (cells.getCellType())
                    {
                        case Cell.CELL_TYPE_NUMERIC:
                            break;
                        case Cell.CELL_TYPE_STRING:
                        {
                            if (keyValue.equals(""))
                            {
                                keyValue = cells.getStringCellValue();

                                if (keyValue.contains("{"))
                                {
                                    keys = getStringBetweenBrackets(keyValue);
                                }
                                else
                                {
                                    if (keyValue.contains("\""))
                                    {
                                        keyValue = getString(keyValue);
                                    }
                                    else
                                    {
                                        if (keyValue.contains(" "))
                                        {
                                            keyValue = "";
                                        }
                                    }
                                }

                            }
                            else
                            {
                                value = cells.getStringCellValue();
                                if (value.contains(" "))
                                {
                                    value = "placeholder";
                                }

                                if (value.contains("\""))
                                {
                                    value = getString(value);
                                }

                            }

                            if (!keyValue.equals("") && !value.equals("") || !keys.isEmpty())
                            {
                                if (databaseRecords.containsKey(keyValue)) //in case the same is entered twice
                                {
                                    keyValue = keyValue + " " + count;
                                    databaseRecords.put(keyValue, value);
                                    keyValue = "";
                                    value = "";
                                    count++;
                                }
                                else
                                {
                                    if (keys.size() > 0)
                                    {
                                        value = "hiddenValue";
                                        for (int i = 0; i < keys.size(); i++)
                                        {
                                            databaseRecords.put(keys.get(i), value);
                                        }
                                        //Still thinking if this should be added.. but so far it's working fine
//                                    String keyFlag = "ENDOFCASE";
//                                    value = "NOVALUE";
//                                    databaseRecords.put(keyFlag, value);

                                        keys.clear();
                                        keyValue = "";
                                        value = "";
                                    }
                                    else
                                    {
                                        databaseRecords.put(keyValue, value);
                                        keyValue = "";
                                        value = "";
                                    }
                                }

                            }
                        }
                        break;
                    }
                }
            }

            SettersAndGetters.setDatabaseRecords(databaseRecords);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return true;
    }

    public static String getString(String data)
    {
        String information = "";
        Pattern p = Pattern.compile("\"([^\"]*)\"");
        Matcher m = p.matcher(data);

        if (m.find())
        {
            information = m.group(1);
        }
        return information;
    }

    public static ArrayList<String> getStringBetweenBrackets(String data)
    {
        ArrayList<String> information = new ArrayList<>();
        int counter = 0;
        try
        {
            Pattern pattern = Pattern.compile("\"([^\"]*)\"");
            Matcher matcher = pattern.matcher(data);

            while (matcher.find())
            {
                information.add(matcher.group(1));
                pattern = Pattern.compile("\\{([^}]*)\\}");
                matcher = pattern.matcher(data);
                if (matcher.find())
                {
                    String inforInBrakets = matcher.group(1);
                    pattern = Pattern.compile("\"([^\"]*)\"");
                    matcher = pattern.matcher(inforInBrakets);
                    while (matcher.find())
                    {
                        if (counter % 2 == 0)
                        {
                            information.add(matcher.group(1));
                        }
                        counter++;
                    }

                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }

        return information;
    }

    public static boolean putStoreSystemConfigNoCollectionChargeTrue()
    {
        System.out.println(MobileDoddlePageObjects.preadvisedNoIDChargeConfig());
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        HttpPut storeConfigPut = new HttpPut("https://stage-apigw.doddle.it/v2/stores/456DVT");

        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=DoddleServers");
            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response1 = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;

            storeConfigPut.addHeader(MobileDoddlePageObjects.header(), "Bearer " + newBearer);
            input = new StringEntity(MobileDoddlePageObjects.preadvisedNoIDChargeConfig());
            input.setContentType("application/json");
            storeConfigPut.setEntity(input);
            HttpResponse response2 = client.execute(storeConfigPut);
            rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));

            line = rd.readLine();

            System.out.println(line);

//            if (!line.contains("MULTIPLE_CONTAINERS"))
//            {
//                narrator.testFailed(line);
//                return false;
//            }
//
//            Narrator.stepPassed(line);
        }
        catch (Exception e)
        {
            error = "Exception - " + e.getMessage();
            return false;
        }

        return true;
    }

    public static void preAadviceCollectionParcelSettingReferenceIDNoCustomerBlockNoRetailerID(String labelValue, String shelfID, String newBearer, String retailerOrderID)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBodyNoCustomerWithCustomOrderID(labelValue, shelfID, retailerOrderID));
            input = new StringEntity(MobileDoddlePageObjects.collectionPreAdviceBodyNoRetialerID(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);

            String referenceID = line.replaceAll("[-+.^:,\"]", "");
            Pattern pattern = Pattern.compile("referenceId(.*)}}.*[^0-9].*");
            Matcher matcher = pattern.matcher(referenceID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            referenceID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setBarCodeRefID(referenceID);
            SettersAndGetters.setGeneratedBarCode(referenceID);
            collectionReferenceIDarr.clear();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

    }

    public static void preAadviceCollectionParcelExpectedStatus(String labelValue, String shelfID, String newBearer)
    {
        String header = "Authorization";
        String headerValue = "Bearer " + newBearer;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost("https://stage-apigw.doddle.it/v1/items/collections");
            httpPost.addHeader(header, headerValue);
            input = new StringEntity(MobileDoddlePageObjects.collectionsPreAdviceBodyNotPassedGoodsIn(labelValue, shelfID));
            input.setContentType("application/json");
            httpPost.setEntity(input);

            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            SettersAndGetters.setAPIResponse(line);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static String changeFailedAPIResponseColor(String textToChange)
    {
        String failedMessage = textToChange;
        try
        {
            failedMessage = failedMessage.replace(textToChange, "<span style=\"color:#FF1810; \">" + textToChange + "</span>");
            return failedMessage;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return failedMessage;
        }
    }
    
    public static void cleanCollections()
    {
        try
        {
            Shortcuts.newBearer();
            
            SeleniumDriverInstance = new SeleniumDriverUtility(Enums.BrowserType.Chrome);
            
            SeleniumDriverInstance.startDriver();
            
            SeleniumDriverInstance.navigateTo("http://tmpstage-couchbaseadmin-445592012.eu-west-1.elb.amazonaws.com:4985/_admin/db/doddle_stores_sync/channels/store_456DVT");
            
            if (!SeleniumDriverInstance.waitForElementByXpath("//*[contains(@href,'ITEM_COLLECTION')]", 180))
            {
                error = "Failed to load ITEM_COLLECTION";
                SeleniumDriverInstance.Driver.close();
            }
            
            List <WebElement> elements = SeleniumDriverInstance.Driver.findElements(By.xpath("//*[contains(@href,'ITEM_COLLECTION')]"));
            
            for (int i = 0; i < elements.size(); i++) 
            {
                String itemsID = elements.get(i).getText();
                System.out.println("PROCESSING ITEM::"+itemsID);
                itemsID = itemsID.replace("ITEM_COLLECTION::", "");
                doCleanUpAPICall(itemsID.trim(), SettersAndGetters.getNewBearer());
            }
            
            SeleniumDriverInstance.shutDown();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public static void doCleanUpAPICall(String itemID, String bearerToken)
    {
        String header = "Authorization";
        String headerValue = "Bearer "+bearerToken;

        try
        {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPut httpPut = new HttpPut("https://stage-apigw.doddle.it/v1/items/collections/" + itemID + "?api_key=40AGV34DJUO153Z5DDJAJ23PE");
            httpPut.addHeader(header, headerValue);

            StringEntity input = new StringEntity(MobileDoddlePageObjects.collectionsCleanUpBody());
            input.setContentType("application/json");
            httpPut.setEntity(input);
            
            HttpResponse response = client.execute(httpPut);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            System.out.println("ITEM:: "+itemID+" SUCCESSFULLY CLEANED");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.err.println("FAILED TO CLEAN ITEM:: "+itemID);
            System.out.println(e.getMessage());
        }
    }
    
    public static String amazonBarcode()
    {
        String regex = "[QH]{1}[0-9]{11}";
        Xeger generator = new Xeger(regex);
        String amazonBarcode = generator.generate();

        return amazonBarcode;
    }
    
    public static boolean returnsBearerToken()
    {
        HttpClient client = HttpClientBuilder.create().build();

        HttpPost tokenPost = new HttpPost("https://stage-apigw.doddle.it/v1/oauth/token?api_key=40AGV34DJUO153Z5DDJAJ23PE");
        tokenPost.addHeader(MobileDoddlePageObjects.header(), MobileDoddlePageObjects.headerValue());
        tokenPost.addHeader(MobileDoddlePageObjects.contentType(), MobileDoddlePageObjects.contentValue());

        try
        {
            input = new StringEntity("grant_type=client_credentials&scope=store-returns-preadvice-write");

            input.setContentType("application/json");
            tokenPost.setEntity(input);
            HttpResponse response = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();

            String regex = "\\{\"access_token\":\"";
            String[] lines = line.split(regex);
            String[] liness = lines[1].split("\",\"token_type\"");

            line = liness[0];
            newBearer = line;
            SettersAndGetters.setNewBearer(newBearer);
            System.out.println(newBearer);

            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch (Exception e)
        {
            error = "exeption: " + e.getMessage();
            errorMessage = error;
            return false;
        }

        return true;
    }
	
	
    public static boolean prebookReturn(String clientVersion, String deviceIdentifier, String email, String storeID, String retailerID, String prebookedLabelValue)
    {
        try
        {

            HttpClient client = HttpClientBuilder.create().build();

            HttpPost tokenPost = new HttpPost("https://stage-api.doddle.io/v1-store-returns/preadvice");
            tokenPost.addHeader(MobileDoddlePageObjects.header(), "Bearer "+SettersAndGetters.getNewBearer());
            
            input = new StringEntity(MobileDoddlePageObjects.returnsPreAdviceBody(clientVersion, deviceIdentifier, email, storeID, retailerID, prebookedLabelValue));
            System.out.println(MobileDoddlePageObjects.returnsPreAdviceBody(clientVersion, deviceIdentifier, email, storeID, retailerID, prebookedLabelValue));
            input.setContentType("application/json");
            
            tokenPost.setEntity(input);
            
            HttpResponse response = client.execute(tokenPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();

            
            referenceID = line.substring(line.lastIndexOf(":") + 1).replaceAll("\\p{P}","");
            SettersAndGetters.setBarCodeRefID(referenceID);

            String itemID = line;
            Pattern pattern2 = Pattern.compile("itemId\":\"(.*?)\",\"referenceId");
            Matcher matcher = pattern2.matcher(itemID);

            while (matcher.find())
            {
                System.out.println(matcher.group(1));
                collectionReferenceIDarr.add(matcher.group(1));
            }
            itemID = collectionReferenceIDarr.get(0);
            SettersAndGetters.setItemID(itemID);
            collectionReferenceIDarr.clear();
            
            SettersAndGetters.setAPIResponse(line);
            
            System.out.println(line);
            
            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch(Exception e)
        {
            error = "exeption: " + e.getMessage();
            errorMessage = error;
            return false;
        }
        return true;
    }
	
    public static boolean getReturnData(String itemToRetrieve)
    {
        try
        {
            HttpClient client = HttpClientBuilder.create().build();

            HttpGet tokenGET = new HttpGet("https://stage-apigw.doddle.it/v1/items/returns/"+itemToRetrieve);
            tokenGET.addHeader(MobileDoddlePageObjects.header(), "Bearer "+SettersAndGetters.getNewBearer());

            input.setContentType("application/json");
            
            HttpResponse response = client.execute(tokenGET);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            line = rd.readLine();
            
            SettersAndGetters.setAPIResponse(line);
            
            System.out.println(line);
            
            if (line.contains("Forbidden") || line.contains("Bad Request") || line.contains("access denied"))
            {
                narrator.testFailed(line);
                return false;
            }
        }
        catch(Exception e)
        {
            error = "exeption: " + e.getMessage();
            errorMessage = error;
            return false;
        }
        return true;
    }
    
    public static String getPrinter()
    {
        if (System.getProperty("user.name").contains("Device Farm"))
        {
            printer = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.doddlePrinterOption());
        }
        
        else
        {
            printer = AppiumDriverInstance.retrieveTextByXpath(MobileDoddlePageObjects.dvtPrinterOption());
        }
        
        return printer;
    }

}

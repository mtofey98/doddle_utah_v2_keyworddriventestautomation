package KeywordDrivenTestFramework.Entities;

/**
 * Created with IntelliJ IDEA. User: fnell Date: 2013/04/26 Time: 3:20 PM To
 * change this template use File | Settings | File Templates.
 */
public class Enums
{

    public enum BrowserType
    {

        IE, FireFox, Chrome, Safari
    }

    public enum ResultStatus
    {

        PASS, FAIL, WARNING, UNCERTAIN
    }

    public enum RelativePosition
    {

        Above, Below, Right, Left
    }

    public enum MobilePlatform
    {
        Android, iOS
    }

    public enum Device
    {
        ZebraMC40("udid", "15269522500185", "http://0.0.0.0:4723/wd/hub", MobilePlatform.Android),
        ZebraTC70_Doddle("udid", "16201522506977", "http://0.0.0.0:4723/wd/hub", MobilePlatform.Android),
        ZebraTC70_Dvt("udid", "16072522500917", "http://0.0.0.0:4723/wd/hub", MobilePlatform.Android),
        HoneyWellD75E("udid", "16135J0054", "http://0.0.0.0:4723/wd/hub", MobilePlatform.Android),
        ZebraTC51("udid", "16348522501409", "http://0.0.0.0:4723/wd/hub", MobilePlatform.Android),
        Emulator("udid", "emulator-5554", "http://127.0.0.1:4722/wd/hub", MobilePlatform.Android);
        
        
        public final String CapabilityName;
        public final String DeviceID;
        public final String ServerURL;
        public final MobilePlatform platform;

        // This constructor defines and instantiates the parameters declared above. Parameter order is specified here and will 
        // define the order in which the enum types' properties are specified. 
        Device(String CapabilityName, String DeviceID, String ServerURL, MobilePlatform _platform)
        {
            this.CapabilityName = CapabilityName;
            this.DeviceID = DeviceID;
            this.ServerURL = ServerURL;
            this.platform = _platform;
        }

    }

    public enum DeviceConfig
    {

        //Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
//Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
//Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
//Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
//Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
//Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
//Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
//Capability information is stored here.
        //AppName and AppFilePath are for both APK and IPA.
        //Uses the format Name(deviceName, platformName, automationName, Version, appPackage, appActivity, appName, appFilePath)
        zebraMC40("15269522500185", "Android", "Appium", "4.4", "com.doddle.concession", "com.doddle.concession.login.LoginActivity", "10.4.2-stage-debug.apk", System.getProperty("user.dir") + "\\APK Archives"),
        zebraTC70("16201522506977", "Android", "Appium", "4.4.3", "com.doddle.concession", "com.doddle.concession.login.LoginActivity", "10.4.2-stage-debug.apk", System.getProperty("user.dir") + "\\APK Archives"),
        honeyWellD75E("16135J0054", "Android", "Appium", "4.4.4", "com.doddle.whitelabel.mock", "com.doddle.whitelabel.mock.login.LoginActivity", "Whitelabel-2.4.2-stage-debug.apk", System.getProperty("user.dir") + "\\APK Archives" + "\\M&S"),
        zebraTC51("16348522501409", "Android", "Appium", "4.4.3", "com.doddle.concession", "com.doddle.concession.login.LoginActivity", "9.8.2-stage-debug.apk", System.getProperty("user.dir") + "\\APK Archives" + "\\M&S"),
        Doddle("5554", "Android", "Appium", "4.4", "com.doddle.concession", "com.doddle.concession.login.LoginActivity", "concession_mvp_v.6.18_staging(1).apk", "C:\\Users\\gdean@dvt.co.za\\Downloads"),
        Doddle2("5554", "Android", "Appium", "4.4", "com.doddle.concession", "com.doddle.concession.login.LoginActivity", "9.13.2-stage-debug.apk", "C:\\Users\\hduplessis\\Documents\\doddle_mastered\\APK Archives"),
        Test("deviceName", "platformName", "automationName", "Version", "appPackage", "appActivity", "ApplicationName", "ApplicationFIlePath");

        public final String deviceName;
        public final String platformName;
        public final String automationName;
        public final String Version;
        public final String appPackage;
        public final String appActivity;
        public String ApplicationName;
        public final String ApplicationFilePath;

        // This constructor defines and instantiates the parameters declared above. Parameter order is specified here and will 
        // define the order in which the enum types' properties are specified. 
        DeviceConfig(String deviceName, String platformName, String automationName, String Version, String appPackage, String appActivity, String ApplicationName, String ApplicationFilePath)
        {
            this.deviceName = deviceName;
            this.platformName = platformName;
            this.automationName = automationName;
            this.Version = Version;
            this.appPackage = appPackage;
            this.appActivity = appActivity;
            this.ApplicationName = ApplicationName;
            this.ApplicationFilePath = ApplicationFilePath;
        }
    }

    public enum Database
    {
        // Set Database Connection Information Here. 
        Example("org.apache.derby.jdbc.EmbeddedDriver", "jdbc:derby://localhost:1527/sample", "app", "app");

        public final String Driver;
        public final String ConnectionString;
        public final String username;
        public final String password;

        Database(String Driver, String ConnectionString, String username, String password)
        {
            this.Driver = Driver;
            this.ConnectionString = ConnectionString;
            this.username = username;
            this.password = password;
        }

    }

    public enum Environment
    {
        // Add environment urls here, parameter order is defined by the constructor (Environment) definition below
        // Please note that adding an addtional environment type will require you to comma-seperate them.
        // Visit http://docs.oracle.com/javase/tutorial/java/javaOO/enum.html to learn more about Java Enum declarations. 

        // Here we are declaring the Dev Environment type, and passing the following two properties, a url and a connection string, 
        // which are defined below as both being string literals:
        // DEV[FirstPageURL,FirstDatabaseConnectionString]
        TEST("https://www.gmail.com/"),
        DoddleStaging1("https://staging.doddle.com"),
        DoddleStaging2("https://staging.doddle.it");
        // For each system (website1, database1, website2 etc.) within the defined environment (Dev, QA, Prod etc.)
        // you will have to declare the appropriate string to store its properties (URL or connection string etc.).
        public final String PageUrl;

//        public final String ForgotPasswordURL;
        // This constructor defines and instantiates the parameters declared above. Parameter order is specified here and will 
        // define the order in which the enum types' properties are specified. 
        Environment(String pageUrl)
        {
            this.PageUrl = pageUrl;
        }

    }

    public static Device resolveDevice(String device)
    {
        switch (device.toUpperCase())
        {
            case "ZEBRAMC40":
                return Device.ZebraMC40;

            case "ZEBRATC70_DODDLE":
                return Device.ZebraTC70_Doddle;

            case "ZEBRATC70_DVT":
                return Device.ZebraTC70_Dvt;

            case "HONEYWELLD75E":
                return Device.HoneyWellD75E;

            case "ZEBRATC51":
                return Device.ZebraTC51;

            default:
                return Device.ZebraTC70_Dvt;
        }
    }

//    public static DeviceConfig resolveDeviceConfig(String deviceConfig)
//    {
//        switch (deviceConfig.toUpperCase()) {
//            case "HOLLARDANDROID":
//                return DeviceConfig.HollardAndroid;
//            
//            default:
//                return null;
//        }
//    }
    public static Environment resolveTestEnvironment(String environment)
    {
        switch (environment.toUpperCase())
        {
            case "TEST":
                return Environment.TEST;
            case "DODDLESTAGING1":
                return Environment.DoddleStaging1;
            default:
                return null;
        }
    }

    public static DeviceConfig resolveDeviceConfig(String deviceConfig)
    {
        switch (deviceConfig.toUpperCase())
        {
            case "ZEBRAMC40":
                return DeviceConfig.zebraMC40;
            case "ZEBRATC_70":
                return DeviceConfig.zebraTC70;
            case "HONEYWELLD75E":
                return DeviceConfig.honeyWellD75E;
            case "ZEBRATC51":
                return DeviceConfig.zebraTC51;
            case "TEST":
                return DeviceConfig.Test;
            default:
                return DeviceConfig.zebraTC70;
        }
    }
}
